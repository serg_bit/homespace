<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "profile_reviews".
 *
 * @property integer $id
 * @property integer $to
 * @property integer $from
 * @property integer $date
 * @property string $review
 */
class ProfileReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to', 'from', 'date', 'review'], 'required'],
            [['to', 'from', 'date'], 'integer'],
            [['review'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'to' => 'To',
            'from' => 'From',
            'date' => 'Date',
            'review' => 'Review',
        ];
    }
    public function addComment($message, $to, $from){
        $comment = new ProfileReviews([
            'review' => $message,
            'to' => $to,
            'from' => $from,
            'date' => time(),
        ]);
        $comment->save();
    }

    public function getComment($to, $from){
        $comment = ProfileReviews::find()->where(['to'=>$to, 'from'=>$from])->orderBy('id DESC')->one();
        return $comment;
    }
}
