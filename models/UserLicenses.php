<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_licenses".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $images
 * @property string $number
 * @property string $issued_by
 * @property integer $moderation
 */
class UserLicenses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_licenses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'images', 'number', 'issued_by'], 'required'],
            [['user_id', 'moderation'], 'integer'],
            [['images', 'number', 'issued_by'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'images' => 'Images',
            'number' => 'Number',
            'issued_by' => 'Issued By',
            'moderation' => 'Moderation',
        ];
    }
}
