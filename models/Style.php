<?php

namespace frontend\models;

class Style extends \yii\db\ActiveRecord
{
    public static function allStyle($lang){
        if($lang == 'ru'){
            return self::find()->select(['name_ru', 'id'])->indexBy('id')->column();
        }else
            return self::find()->select(['name_en', 'id'])->indexBy('id')->column();
    }
}