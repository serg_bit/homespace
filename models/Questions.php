<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $email
 * @property string $theme
 * @property string $question
 * @property integer $status
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'theme', 'question'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['question'], 'string'],
            [['name', 'email', 'theme'], 'string', 'max' => 255],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'theme' => 'Theme',
            'question' => 'Question',
            'status' => 'Status',
        ];
    }

    public function sendQuestion($data){
        $data1 = array('Questions' => $data);
        $this->load($data1);
        if ($this->validate()) {
//            $this->save();
            $this->sendMail($data);
        } else {
            echo 'not correct';
        }

    }

    public function sendMail($data){
        $to  = "mitins@outlook.com" ;
        $subject = "Home Space - ".$data['theme'];

        $message = '
            <html>
                <head>
                    <title>Home Space</title>
                </head>
                <body>
                    <h3>Сообщение: </h3>
                    <p>'.$data['question'].'</p>
                </body>
            </html>';

        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: Сообщение от ".$data['name']." <".$data['email'].">\r\n";

        mail($to, $subject, $message, $headers);
    }
}
