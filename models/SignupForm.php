<?php
namespace frontend\models;

use yii\helpers\Url;
use common\models\User;
use yii\base\Model;
use yii\db\Query;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $company;
    public $specialization;
    public $first_name;
    public $last_name;
    public $country;
    public $city;
    public $roles;
    public $captcha;
    private $account_activation_token;

    const ROLE_USER = 1;
    const ROLE_MODER = 5;
    const ROLE_ADMIN = 10;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['first_name', 'last_name', 'city', 'username', 'email', 'password', 'roles'], 'required'],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['password', 'string', 'min' => 6],

            ['first_name', 'filter', 'filter' => 'trim'],
            ['first_name', 'string', 'min' => 2, 'max' => 255],


            ['last_name', 'filter', 'filter' => 'trim'],
            ['last_name', 'string', 'min' => 2, 'max' => 255],

            ['city', 'filter', 'filter' => 'trim'],
            ['city', 'string', 'min' => 2, 'max' => 255],

            ['country', 'filter', 'filter' => 'trim'],
            ['roles', 'filter', 'filter' => 'trim'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            
            ['captcha', 'captcha'],

        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup($token)
    {
        if ($this->validate()) {
            if(isset($this->company)){
                $company = $this->company;
            }else $company = null;

            if(isset($this->specialization)){
                $specialization = $this->specialization;
            }else $specialization = null;

            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;

            $user->status = User::STATUS_DELETED;
            //This is the constant for new user role  
            $user->roles = $this->roles;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->company = $company;
            $user->specialization = $specialization;
            $user->country = $this->country;
            $user->city = $this->city;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $this->account_activation_token = $token;
            $user->account_activation_token = $this->account_activation_token;
            if ($user->save()) {
                Yii::$app->authManager->getRole($this->roles);
                $db = Yii::$app->db;
                
                $db->createCommand()->insert('auth_assignment', [
                    'item_name' => $this->roles,
                    'user_id' => $user->id,
                    'created_at' => microtime(true),
                ])->execute();
                $this->sendActivationEmail($this->email, 'info@homespace.today');
                $this->addStyles($user->id);
                return $user;
            }
        }

        return null;
    }

    /**
     * Sends activation email
     */

    public function addStyles($id) {
        $styles = isset($_COOKIE['user_style']) ? $_COOKIE['user_style'] : 0;
        if($styles != 0)
            $user_style = substr($styles, 0, -1);
        Yii::$app->db->createCommand("UPDATE `user`
                                      SET `styles` = '".$user_style."'
                                      WHERE `id` = ".$id.";")->execute();
    }
    public function sendActivationEmail($email, $from) {

        $to  = $email;
        $subject = "Home Space Today - Registration";
        $message = '
            <html>
                <head>
                    <title>Home Space Today</title>
                </head>
                <body>
                    <h3>Registration confirm</h3>
                    <p>Your account link <a href="'.Yii::$app->urlManager->createAbsoluteUrl(['site/confirm-user', 'atoken' => $this->account_activation_token ]).'">Home Space Today</a></p>
                </body>
            </html>';

        $headers  = "Content-type: text/html; charset=utf-8 \r\n";
        $headers .= "From: Home Space Today<".$from.">\r\n";

        mail($to, $subject, $message, $headers);
    }
}
