<?php

namespace frontend\models;

use frontend\account\models\Portfolio;
use frontend\account\models\ProductsList;


class WorksHelper{

    public static function getAllworks($lang){
        $all_works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title , portfolio.likes
                                                FROM  `portfolio` , `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1 ORDER BY RAND() LIMIT 12")->queryAll();

        return $all_works;
    }



    public static function countWorks($status){

        $count = Portfolio::find()->where(['status' => $status])->count();

        return $count;
    }


    public static function getStWorks($sort, $status, $lang, $pagination, $tags){
        if($sort == 'views') {
            if (empty($tags)){

                $st_works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title, portfolio.likes
                                                FROM  `portfolio` ,  `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1
                                                AND portfolio.status = '$status'
                                                ORDER BY portfolio.views DESC
                                                LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

            }else{

                $st_works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title, portfolio.likes
                                                FROM  `portfolio` ,  `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1
                                                AND portfolio.status = '$status'
                                                AND item_description.tags LIKE '%".$tags."%'
                                                ORDER BY portfolio.views DESC
                                                LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

            }


        }else if($sort == 'likes'){

            if(empty($tags)) {

                $st_works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title, portfolio.likes
                                                FROM  `portfolio` , `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1
                                                AND portfolio.status = '$status'
                                                ORDER BY portfolio.likes DESC
                                                LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();
            }else{

                $st_works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title, portfolio.likes
                                                FROM  `portfolio` , `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1
                                                AND portfolio.status = '$status'
                                                AND item_description.tags LIKE '%".$tags."%'
                                                ORDER BY portfolio.likes DESC
                                                LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

            }
        }

        return $st_works;
    }


    public static function getProducts($sort, $pagination, $tags){

        if($sort == 'likes') {

            if(empty($tags)) {

                $products = \Yii::$app->db->createCommand("SELECT id_prod, product_name, main_image, price, likes
                                                    FROM  `products_list`
                                                    WHERE stock = 1
                                                    ORDER BY likes DESC
                                                    LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();
            }else{

                $products = \Yii::$app->db->createCommand("SELECT id_prod, product_name, main_image, price, likes
                                                    FROM  `products_list`
                                                    WHERE stock = 1
                                                    WHERE meta_tags LIKE '%".$tags."%'
                                                    ORDER BY likes DESC
                                                    LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

            }

        }else if($sort == 'hight'){

            if(empty($tags)) {

                $products = \Yii::$app->db->createCommand("SELECT id_prod, product_name, main_image, price, likes
                                                    FROM  `products_list`
                                                    WHERE stock = 1
                                                    ORDER BY price DESC
                                                    LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();
            }else{

                $products = \Yii::$app->db->createCommand("SELECT id_prod, product_name, main_image, price, likes
                                                    FROM  `products_list`
                                                    WHERE stock = 1
                                                    WHERE meta_tags LIKE '%".$tags."%'
                                                    ORDER BY price DESC
                                                    LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

            }
        }else{

            if (empty($tags)) {

                $products = \Yii::$app->db->createCommand("SELECT id_prod, product_name, main_image, price, likes
                                                    FROM  `products_list`
                                                    WHERE stock = 1
                                                    ORDER BY price
                                                    LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();
            } else {

                $products = \Yii::$app->db->createCommand("SELECT id_prod, product_name, main_image, price, likes
                                                    FROM  `products_list`
                                                    WHERE stock = 1
                                                    WHERE meta_tags LIKE '%" . $tags . "%'
                                                    ORDER BY price
                                                    LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

            }
        }

        return $products;
    }


    public static function countProducts(){

        $count = ProductsList::find()->where(['stock' => 1])->count();

        return $count;

    }


    public static function getWorks($sort, $status, $lang, $pagination)
    {

        if ($sort == 'views' ) {

            $works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title, portfolio.likes
                                                FROM  `portfolio` ,  `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1
                                                AND portfolio.status = '$status'
                                                ORDER BY portfolio.views DESC
                                                LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();

        }else{

            $works = \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, images, title, portfolio.likes
                                                FROM  `portfolio` ,  `item_description`
                                                WHERE item_description.language = '$lang'
                                                AND item_description.item_id = portfolio.item_id
                                                AND portfolio.moderation = 1
                                                AND portfolio.status = '$status'
                                                ORDER BY portfolio.likes DESC
                                                LIMIT $pagination->limit OFFSET $pagination->offset")->queryAll();
        }

        return $works;
    }

}