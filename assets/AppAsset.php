<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap-theme.min.css',
        'css/bootstrap.min.css',
        'css/ico.css',
        'css/lightbox.css',
        'css/site.css',
        'css/main.css',
        'css/style.css',
        'css/styles.css',
        'css/media.css',
        'https://fonts.googleapis.com/css?family=Ubuntu&subset=latin,cyrillic',
        'https://fonts.googleapis.com/css?family=Roboto:400,300,400italic&subset=latin,cyrillic',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        'css/magnific-popup.css',
        'css/font-awesome.css',
        
        
    ];
    public $js = [
          'js/bootstrap.min.js',
          'js/jquery.nicescroll.min.js',
          'js/main.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
