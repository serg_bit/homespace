var honours_count = getCountHonours('.count-honours');
var honours_chacked = false;
var honour_image;
$('#honours_save').click(function(e) {

	var honours_data = getHonoursValue('.count-honours');
	if(honours_chacked != false){
		var request = $.ajax({
			data : {honours : honours_data},
	        url: '/account/edit/save-honours',
	        type: 'post',
	        dataType: 'html'
	    });

	    request.done(function( message ) {

			$('#honours-save-error').css('visibility', 'hidden');
			$('#honours-save-valid').html(message);
			$('#honours-save-valid').css('visibility', 'visible');
			setTimeout(funcTimer1, 5000);

	    });

	}
});

function funcTimer1() {
	$('#honours-save-valid').css('visibility', 'hidden');
	$('#honours-save-error').css('visibility', 'hidden');
}

$( "body" ).delegate( ".del_honour", "click", function(e) {
	e.preventDefault();
	var honour_id = $(this).parent().attr('data-honour-id');
	if(honour_id == ""){
		$(this).parent().remove();
		honours_count--;
	}else{
	
		var request = $.ajax({
			data : {id : honour_id},
	        url: '/account/edit/delete-honour',
	        type: 'post',
	        dataType: 'html'
	    });

	    request.done(function( message ) {

			$('#honours-save-error').css('visibility', 'hidden');
			$('#honours-save-valid').html(message);
			$('#honours-save-valid').css('visibility', 'visible');
			setTimeout(funcTimer1, 5000);

	    });
			
		$(this).parent().remove();
		honours_count--;
	}

});

$('#add_honour').click(function(e) {
	e.preventDefault();
	honours_count = getCountHonours('.count-honours');
	var del_text = $('#hidden-honours-text').attr('data-del-text');
	var honour_text = $('#hidden-honours-text').attr('data-honour-text');
	if(honours_count >= 0 && honours_chacked != false){
		$('.all-honours').append('<div class="input-edit count-honours" data-honour-id=""><img class="honour-image" src="/images/camera4.png" alt=""><div class="input-container"><input class="text-input floating-label" type="text" name="sample" onchange="changeInput(this)" value=""/><label for="sample">'+honour_text+'</label></div><a href="#" class="del del_honour">'+del_text+'<i></i></a></div>');
		honours_count++;
	}

});

function getCountHonours(object_class){

	var count = 0;
	honours_chacked = true;
	var temp;

	$(object_class).each(function() {
		temp = $(this).find('input').val();
		img = $(this).find('img').attr('src');
		if(temp == "" || img == '/images/camera4.png'){
			honours_chacked = false;
		}
	    count++;
	});

	return count;
}

function getHonoursValue(object_class){

	var data = [];
	var count = 0;
	var temp;
	var img;
	honours_chacked = true;
	$(object_class).each(function() {
		temp = $(this).find('input').val();
		img = $(this).find('img').attr('src');
		if(temp == "" || img == '/images/camera4.png'){
			honours_chacked = false;
		}else{
			data[count] = new Array($(this).attr('data-honour-id'), temp, img);
		}
	    count++;
	});
	return data;
}

$('form').submit(function(e) {
    e.preventDefault();
});

function honourChange(files)
{
	var file = files.files;
	if(file.length == 1){
		uploadHonour(file);
	}
}

var uploadHonour = function(files){
	var formData = new FormData();
	formData.append('file', files[0]);
	var request = $.ajax({
		data : formData,
		url: '/account/edit/loading-image',
		type: 'post',
		dataType: 'html',
		contentType: false,
		processData: false
	});

	request.done(function( url ) {
		$(honour_image).attr('src', url);
	});

};

$( "body" ).delegate( ".honour-image", "click", function() {
	honour_image = this;
	$('#open_browse4').trigger('click');
});

