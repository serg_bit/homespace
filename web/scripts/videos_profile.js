var videos_count = getCountVideos('.videos_count');
var video_checked = false;

$('#videos_save').click(function() {
	var video_data = getVideosData('.videos_count');
	if(video_checked != false){
		var request = $.ajax({
			data : {videos : video_data},
	        url: '/account/edit/save-videos',
	        type: 'post',
	        dataType: 'html',
	    });

	    request.done(function( message ) {
			$('#videos-save-error').css('visibility', 'hidden');
			$('#videos-save-valid').html(message);
			$('#videos-save-valid').css('visibility', 'visible');
			setTimeout(funcTimer3, 5000);
	    });
	}
});

$('#add_video').click(function(e) {
	e.preventDefault();
	videos_count = getCountVideos('.videos_count');
	var del_text = $('#hidden-videos-text').attr('data-del-text');
	var number_text = $('#hidden-videos-text').attr('data-link-text');
	var name_text = $('#hidden-videos-text').attr('data-name-text');

	if(videos_count >= 0 && video_checked != false){
		$('.all-videos').append('<div class="clearfix"></div><div class="video videos_count" data-video-id=""><img class="video-img" src="/images/no-video.png" alt=""><form class="forma-input"><div class="input-container namb"><input class="text-input floating-label video-input" type="text" name="link" value="" onchange="changeInput(this)"/><label for="link">'+number_text+'</label></div><div class="input-container namb"><input class="text-input floating-label video-input" type="text" name="name" value="" onchange="changeInput(this)" /><label for="name">'+name_text+'</label></div></form><a href="#" class="del del_video">'+del_text+'<i></i></a></div>');
		videos_count++;
	}
});

$('body').delegate( ".del_video", "click", function(e) {
	e.preventDefault();
	var video_id = $(this).parent().attr('data-video-id');
	if(video_id == ""){
		$(this).parent().remove();
		videos_count--;
	}else{
	
		var request = $.ajax({
			data : {id : video_id},
	        url: '/account/edit/delete-video',
	        type: 'post',
	        dataType: 'html'
	    });

	    request.done(function( message ) {

			$('#videos-save-error').css('visibility', 'hidden');
			$('#videos-save-valid').html(message);
			$('#videos-save-valid').css('visibility', 'visible');
			setTimeout(funcTimer3, 5000);
	    });
			
		$(this).parent().remove();
		videos_count--;
	}
});

$('body').delegate( '.video input[name=link]', 'keypress', function(e) {
	if(e.keyCode == 13) {
		var link = $(this).val();
		var parent_div = $(this).parent().parent().parent().find('.video-img');
		$(parent_div).replaceWith('<iframe class="video-img" width="330" height="181" src="'+link+'" frameborder="0" allowfullscreen></iframe>');
	}

});

$('form').submit(function(e) {
	e.preventDefault();
});



function getCountVideos(object_class){

	var count = 0;
	video_checked = true;
	var temp1, temp2;

	$(object_class).each(function() {
		temp1 = $(this).find('input[name=link]').val();
		temp2 = $(this).find('input[name=name]').val();
		if(temp1 == "" || temp2 == ""){
			video_checked = false;
		}
	    count++;
	});

	return count;
}

function getVideosData(object_class){

	var data = [];
	var count = 0;
	var temp1, temp2;

	video_checked = true;

	$(object_class).each(function() {
		temp1 = $(this).find('input[name=link]').val();
		temp2 = $(this).find('input[name=name]').val();

		if(temp1 == "" || temp2 == ""){
			video_checked = false;
		}else{
			data[count] = new Array($(this).attr('data-video-id'), temp1, temp2);
		}
	    count++;
	});
	return data;
}

function funcTimer3() {
	$('#videos-save-valid').css('visibility', 'hidden');
	$('#videos-save-error').css('visibility', 'hidden');
}

