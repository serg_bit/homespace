var action_img = 0;

$('#profile-photo').click(function (e) {
    e.preventDefault();
    $('#open_browse').trigger('click');
    that_use = 1;
    action_img = 0;
});

$('form').submit(function (e) {
    e.preventDefault();
});

$('.bg-profile').click(function (e) {
    e.preventDefault();
    $('#open_browse3').trigger('click');
});

$('#company-logo').click(function (e) {
    e.preventDefault();
    $('#open_browse').trigger('click');
    action_img = 1;
});


$('#main_save').click(function () {
    var formData = $('#form_main_information').serializeArray();
    var profile_img = $('#profile-photo').find('img').attr('src');
    var specialization = $('#select_checked_1').html();
    var background_use = $('#main-profil').attr('data-url');
    var logo = $('#company-logo').find('img').attr('src');
    var request = $.ajax({
        data: {
            formData: formData,
            profile_img: profile_img,
            specialization: specialization,
            background: background_use,
            logo: logo
        },
        url: '/account/edit/save-information',
        type: 'post',
        dataType: 'html'
    });

    request.done(function (message) {

        $('#main-save-valid').html(message);
        $('#main-save-valid').css('visibility', 'visible');
        $('#security-save-error').css('visibility', 'hidden');
        setTimeout(funcTimer4, 5000);

    });

    request.fail(function (jqXHR, textStatus) {
        alert(textStatus);
    });
});


$('#security_save').click(function () {
    var pass = $('#new_pass').val();
    var new_pass = $('#confirm_pass').val();
    if (new_pass == pass) {
        var formData = $('.security-info').serializeArray();
        var request = $.ajax({
            data: {formData: formData},
            url: '/account/edit/save-security',
            type: 'post',
            dataType: 'html'
        });

        request.done(function (message) {

            $('#security-save-valid').html(message);
            $('#security-save-valid').css('visibility', 'visible');
            $('#security-save-error').css('visibility', 'hidden');
            setTimeout(funcTimer5, 5000);

        });
    }
});

function fileLogoChange(files) {
    var file = files.files;
    if (file.length == 1)
        uploadImg(file);
}

function userBackground(files) {
    action_background = false;
    var file = files.files;
    if (file.length == 1)
        uploadBg(file);
}

var uploadImg = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    formData.append('path', 'avatar');
    var request = $.ajax({
        data: formData,
        url: '/account/edit/loading-image',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false
    });

    request.done(function (url) {

        if (action_img == 0) {
            $('#profile-photo').find('img').attr('src', url);
        } else {
            $('#company-logo').find('img').attr('src', url);
        }

    });

};

var uploadBg = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: '/account/edit/load-back',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false
    });

    request.done(function (name) {
        $('#main-profil').css('background', 'url(/media/profile/background/' + name + ')no-repeat');
        $('#main-profil').attr('data-url', name);
    });

};


function funcTimer4() {
    $('#main-save-valid').css('visibility', 'hidden');
    $('#main-save-error').css('visibility', 'hidden');
}

function funcTimer5() {
    $('#security-save-valid').css('visibility', 'hidden');
    $('#security-save-error').css('visibility', 'hidden');
}

function SortChangeLi(value, obj_name) {

}

$('#all-information-save').click(function () {
    $('#main_save').trigger('click');
    $('#security_save').trigger('click');
    $('#honours_save').trigger('click');
    $('#licenses_save').trigger('click');
    $('#videos_save').trigger('click');
});

$('#save-information-agent').click(function () {
    $('#videos_save').trigger('click');
    var data = [],
        profile_img = $('#profile-photo').find('img').attr('src'),
        background = $('#main-profil').attr('data-url'),
        first_name = $("input[name='first_name']").val(),
        last_name = $("input[name='last_name']").val(),
        city = $("input[name='city']").val(),
        phone = $("input[name='phone']").val(),
        message = $("textarea[name='message']").val(),
        link_user_site = $("input[name='site']").val(),
        link_fb = $("input[name='facebook']").val(),
        link_tw = $("input[name='twitter']").val(),
        link_inst = $("input[name='instagram']").val(),
        link_behance = $("input[name='behance']").val(),
        link_google = $("input[name='google']").val(),
        link_pint = $("input[name='pinterest']").val(),
        link_tumblr = $("input[name='tumblr']").val(),
        link_linkedin = $("input[name='linkedin']").val(),
        link_blg = $("input[name='blogger']").val(),
        email = $("input[name='email']").val(),
        username = $("input[name='username']").val(),
        password = $("input[name='password']").val(),
        confirm = $("input[name='confirm']").val(),
        send_data = $.ajax({
            data: {
                profile_img: profile_img,
                background: background,
                first_name: first_name,
                last_name: last_name,
                city: city,
                phone: phone,
                message: message,
                link_user_site: link_user_site,
                link_fb: link_fb,
                link_tw: link_tw,
                link_inst: link_inst,
                link_behance: link_behance,
                link_google: link_google,
                link_pint: link_pint,
                link_tumblr: link_tumblr,
                link_linkedin: link_linkedin,
                link_blg: link_blg,
                email: email,
                username: username,
                password: password,
                confirm: confirm,
            },
            url: '/account/edit/save-main-information-agent',
            type: 'post',
            dataType: 'html'
        });
    send_data.done(function (message) {
        $('#save-information-agent-valid').html(message);
        $('#save-information-agent-valid').css('visibility', 'visible');
        setTimeout(funcTimer4, 5000);
    });
})
