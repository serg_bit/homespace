// Create new folder
$('#showCreate').click(function (e) {
    e.preventDefault();
    $('#addNewFolder').toggle();
});

$("#folder").keypress(function (event) {
    var key_code = (event.keyCode ? event.keyCode : event.which);
    if(key_code == 13){
        $('#create_folder').trigger('click');
    }
});

$('#create_folder').on('click', function (e) {
    e.preventDefault();
    var name = $('input#folder').val();
    var root = $('input[type=hidden]').val();

    if (name != '') {
        var create = $.ajax({
            data: {
                name: name,
                root: root
            },
            url: '/account/like-it/folder-create',
            type: 'post',
            dataType: 'html'
        });

        create.done(function (response) {
            location.reload();
        })
    }

});

//End Create

$('i.icon-show8').click(function(){
    $(this).next().toggle();
});

$('.del_folder').on('click', function () {
    var id_folder = $(this).attr('data-attr-id');

    if (id_folder) {

        var delete_folder = $.ajax({
            data: {
                id_folder: id_folder
            },
            url: '/account/like-it/folder-delete',
            type: 'post',
            dataType: 'html'
        });

        delete_folder.done(function (response) {
            location.reload();
        })

    }

});


$('.re_folder').on('click', function(){
    var id_folder = $('.re_folder').attr('data-attr-id');
    var parent = $(this).parents('.folder-bottom');
    var input_container = $(parent).find('.input-container');
    var this_input = $(input_container).find('input');
    var base_text = $(this_input).val();
    $(input_container).toggle();

    $(this_input).keypress(function (event) {
        var key_code = (event.keyCode ? event.keyCode : event.which);
        var new_name = $(this).val();
        if(key_code == 13 && new_name != "" && new_name != base_text){
            var categories = $.ajax({
                data: {
                    id_folder: id_folder,
                    name: new_name
                },
                url: '/account/like-it/folder-rename',
                type: 'post',
                dataType: 'html'
            });

            categories.done(function (response) {
                location.reload();
            });
        }
    });

});

var some_draggable_elementh,
    some_droppable_elementh,
    id_product,
    id_folder;
$(function() {
    $("div.catalog-item").mouseenter(function(){
        some_draggable_elementh = $(this);
        some_draggable_elementh.draggable(
            {cursor: "move", revert: true,}
        );
    })
    some_droppable_elementh = $("div.name-folder");
    some_droppable_elementh.droppable({
        drop: function (event, ui) {
            some_draggable_elementh.hide();
            id_product = some_draggable_elementh.find('img').attr('id_prod');
            id_folder = some_droppable_elementh.find('a.re_folder').attr('data-attr-id');
            var add_product = $.ajax({
                data: {
                    id_product: id_product,
                    id_folder: id_folder
                },
                url: '/account/like-it/add-products-to-catalog',
                type: 'post',
                dataType: 'html'
            });
            add_product.done(function(response){
                //alert(response);
            })
        }
    })
})