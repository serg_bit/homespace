var dropzone = document.getElementById('dropzone1');
var dropzone2 = document.getElementById('dropzone2');
var dropzone3 = document.getElementById('dropzone3');
var dropzone_id = '#dropzone1';
var img_url1 = $('#img_hidden1').val();
var img_url2 = $('#img_hidden2').val();
var img_url3 = $('#img_hidden3').val();
var check_before_save = 0;



var upload = function(files){
	var formData = new FormData();
	formData.append('file', files[0]);
	var request = $.ajax({
		data : formData,
        url: 'upload',
        type: 'post',
        dataType: 'html',
		contentType: false, 
	    processData: false,
    });

    request.done(function( url ) {
    	$( dropzone_id ).css('background', 'url('+url+')');
    	if(dropzone_id == "#dropzone1"){
			img_url1 = url;
    	}else if( dropzone_id == "#dropzone2" ){
    		img_url2 = url;
    	}else if( dropzone_id == "#dropzone3" ){
    		img_url3 = url;
    	}
    });

    request.fail(function( jqXHR, textStatus ) {
    	alert( textStatus );
    });

};

dropzone.ondrop = function(e){
	e.preventDefault();
	var img = $("#dropzone1").find("img");
	if(img.length != 1){
		this.className = 'dropzone';
		dropzone_id = '#dropzone1';
		upload(e.dataTransfer.files);
    }
};

dropzone.ondragover = function(){
	this.className = 'dropzone dragover';
	return false;
};

dropzone.ondragleave = function(){
	this.className = 'dropzone';
	return false;
};


dropzone2.ondrop = function(e){
	e.preventDefault();
	var img = $("#dropzone2").find("img");
	if(img.length != 1){
		this.className = 'dropzone';
		dropzone_id = '#dropzone2';
		upload(e.dataTransfer.files);
    }
};

dropzone2.ondragover = function(){
	this.className = 'dropzone dragover';
	return false;
};

dropzone2.ondragleave = function(){
	this.className = 'dropzone';
	return false;
};

dropzone3.ondrop = function(e){
	e.preventDefault();
	var img = $("#dropzone3").find("img");
	if(img.length != 1){
		this.className = 'dropzone';
		dropzone_id = '#dropzone3';
		upload(e.dataTransfer.files);
    }
};

dropzone3.ondragover = function(){
	this.className = 'dropzone dragover';
	return false;
};

dropzone3.ondragleave = function(){
	this.className = 'dropzone';
	return false;
};



$( ".dropzone" ).click(function() {
	$('#open_browse').trigger('click');
	dropzone_id = '#'+$(this).attr('id');
});

$('.input_item_tags').keypress(function(e){
	var values = [];

	$(this).each(function() {
		var cnt = 0;
		values[0] = $(this).val();
		var re = /[,]/gi;
		var m;
		while (m = re.exec(values[0])) {
			cnt++;
		}
		if(cnt == 5){
			e.preventDefault();
		}

	});
});


function fileChange(files)
{
	var file = files.files;
	if(file.length == 1)
		upload(file);
}

$('.save_item').click(function() {
	check_before_save = 0;
	var item_id = $('#item_id_hidden').val();
	var titles = itemEach('.input_item_title');
	var tags = itemTagsEach('.input_item_tags');
	var short_description = itemEach('.short_description');
	var description = editorHtml();
	var cat_id = $('.item-catalog-category').val();
	var catalog_id = 0;
	if($('input').is('#catalog1')){
		if($('#catalog1').prop( "checked")){
			catalog_id = 1;
			cat_id = 0;
		}
	}
    if(check_before_save == 7 && img_url1 != "" && img_url2 != "" && img_url3 != ""){
		var request = $.ajax({
			data : { item_id : item_id, image1 : img_url1, image2 : img_url2, image3 : img_url3 , titles : titles, tags : tags, description : description, short_description : short_description,  cat_id : cat_id, catalog_id : catalog_id},
	        url: 'update',
	        type: 'post',
	        dataType: 'html'
	    });

	    request.done(function( url ) {

	    	window.location = url;

	    });

	    request.fail(function( jqXHR, textStatus ) {
	    	alert( textStatus );
	    });
	}else{
		$('#portfolio-save-error').css('visibility', 'visible');
		setTimeout(funcTimer1, 5000);
	}
		
});

$('input[name=catalog]').click(function(){
	if($(this).prop( "checked")){
		$('.category-block').css( "visibility", "hidden");
	}else
		$('.category-block').css( "visibility", "visible");
});

$(function () {
	$('.myTab a:last').tab('show')
});

function funcTimer1() {
	$('#portfolio-save-error').css('visibility', 'hidden');
}

function itemEach(items){

	var values = [];
	var count = 0;

	$(items).each(function() {
		values[count] = $(this).val();
		if(values[count] == ""){
			$(this).css('border', '1px solid red');
		}else {
			check_before_save++;
			$(this).css('border', 'none');
		}
		count++;
	});

	return values;

}

function itemTagsEach(items){

	var values = [];
	var count = 0;

	$(items).each(function() {
		var cnt = 0;
		values[count] = $(this).val();
		var re = /[,]{0,1}[0-9a-zа-я\-\_\s]+[,]{0,1}/gi;
		var m;
		while (m = re.exec(values[count])) {
			cnt++;
		}
		if(values[count] == "" || cnt > 5 || cnt == 0){
			$(this).css('border', '1px solid red');
		}else {
			check_before_save++;
			$(this).css('border', 'none');
		}
		count++;
	});

	return values;

}

function editorHtml(){

	var description_ru =  $('textarea[name=description_ru]').val();
	var description_en =  $('textarea[name=description_en]').val();

	if(description_ru != "" && description_en != ""){

		check_before_save++;
	}

	var values = [];

	values[0] = description_ru;
	values[1] = description_en;

	return values;

}
if($('input').is('#catalog1')) {
	if ($('#catalog1').prop("checked")) {
		$('.category-block').css("visibility", "hidden");
	} else
		$('.category-block').css("visibility", "visible");
}