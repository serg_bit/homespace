var action_modal = 0;
var current_selection_id = '';
var current_selection_img = '';
var current_selection_title = '';
var current_selection_item;
var checked_collection = false;
var count_id = 500;

$( "body" ).delegate( ".select-item", "click", function(e) {
    e.preventDefault();
    current_selection_item = $(this).parent();
    if(action_modal == 0) {
        var request = $.ajax({
            url: '/account/edit/modal-items',
            type: 'post',
            dataType: 'html'
        });

        request.done(function (html) {
            action_modal++;
            $('.modal-body').html(html);
        });
    }

    $( "body" ).delegate( ".portfolio-item", "click", function() {
        $(this).find('input[type=radio]').prop('checked', true);
        current_selection_id = $(this).find('img').attr('data-item');
        current_selection_img = $(this).find('img').attr('src');
        current_selection_title = $(this).find('img').attr('data-title');
    });

});

$( "body" ).delegate( ".delete-collection", "click", function(e) {
    e.preventDefault();
    var current_item = $(this).parent().parent().parent();
    if($(current_item).attr('id') == "" || $(current_item).attr('id') == undefined){
        $(current_item).remove();
    }else{

        var request = $.ajax({
            data : {id : $(current_item).attr('id')},
            url : '/account/edit/delete-collection',
            type : 'post',
            dataType : 'html',
            });

            request.done(function( message ) {
                $('#collections-save-error').css('visibility', 'hidden');
                $('#collections-save-valid').html(message);
                $('#collections-save-valid').css('visibility', 'visible');
                setTimeout(funcTimer7, 5000);
                $(current_item).remove();
            });


    }

});

$( "body" ).delegate( ".delete-item", "click", function(e) {
    e.preventDefault();
    var current_item = $(this).parent().parent().parent();
    $(current_item).find('img').attr('src', '/images/no-img.png');
    $(current_item).attr('id', ' ');
    $(current_item).find('h5').html('');
    $(current_item).find('input[type=radio]').attr('data-item', '');
});

$('#apply_this_item').click(function(e){
    $( ".close" ).trigger( "click" );
    $(current_selection_item).attr('id', current_selection_id);
    $(current_selection_item).find('input[type=radio]').attr('data-item', current_selection_id);
    $(current_selection_item).find('img').attr('src', current_selection_img);
    $(current_selection_item).find('h5').html(current_selection_title);
});

$('#collections_save').click(function(e){
    var collections_data = gethCollectionsData('.one-collection');
    if(checked_collection == true) {
        var request = $.ajax({
            data: {data : collections_data},
            url: '/account/edit/save-collections',
            type: 'post',
            dataType: 'html',
        });

        request.done(function (message) {
            $('#collections-save-error').css('visibility', 'hidden');
            $('#collections-save-valid').html(message);
            $('#collections-save-valid').css('visibility', 'visible');
            setTimeout(funcTimer7, 5000);
        });
    }
});

$('#add_collection').click(function(e){
    e.preventDefault();
    checkCollectionsData('.one-collection');
    var del_text = $('#hidden-collections-text').attr('data-del-text');
    var name_text = $('#hidden-collections-text').attr('data-name-text');
    var main_photo = $('#hidden-collections-text').attr('data-main-photo');

    if(checked_collection == true){
        var temp_id1 = count_id + 1;
        var temp_id2 = count_id + 2;
        $('.all-collections').append('<br><div class="one-collection" id=""><form class="forma-input"><div class="input-container"><a href="#" class="del delete-collection">'+del_text+'<i></i></a><input onchange="changeInput(this)" class="text-input floating-label" type="text" name="collection-name" /> <label for="collection-name">'+name_text+'</label></div></form><div class="portfolio_items"><div class="portfolio-items" id=""><a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a> <h5 class="name"></h5> <div class="checkbox"> <p><input id="test'+count_id+'" name="main'+count_id+'"  type="radio" data-item="" checked/> <label for="test'+count_id+'">'+main_photo+'</label> </p><div class="licenses-del"> <a href="#" class="del delete-item">'+del_text+'<i></i></a> </div> </div> </div> <div class="portfolio-items" id=""> <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a> <h5 class="name"></h5> <div class="checkbox"><p><input id="test'+temp_id1+'" name="main'+count_id+'" type="radio" data-item=""/> <label for="test'+temp_id1+'">'+main_photo+'</label> </p> <div class="licenses-del"> <a href="#" class="del delete-item">'+del_text+'<i></i></a> </div> </div> </div> <div class="portfolio-items" id=""> <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a> <h5 class="name"></h5> <div class="checkbox"> <p> <input id="test'+temp_id2+'" name="main'+count_id+'" type="radio" data-item=""/><label for="test'+temp_id2+'">'+main_photo+'</label></p> <div class="licenses-del"> <a href="#" class="del delete-item">'+del_text+' <i></i></a> </div> </div> </div></div><form class="portfolio-textarea"> <textarea class="text-ar"></textarea></form></div>');
        count_id = count_id + 5;
    }
});



function gethCollectionsData(object_class){
    var data = [];
    var count = 0;
    var items = "";
    var main = "";
    checked_collection = true;
    $(object_class).each(function() {
        items = "";
        main = "";
        var id = $(this).attr('id');
        var name = $(this).find('input[type=text]').val();
        var description = $(this).find('textarea').val();
        $('.portfolio-items',this).each(function() {
            if($(this).attr('id') != ""){
                items += $(this).attr('id')+'|';
            }
        });

        main = $('input:radio:checked',this).attr('data-item');

        if(name == "" || description == ""){
            checked_collection = false;
        }


        data[count] = new Array(id, name, description, items, main);

        count++;
    });
    return data;
}

function checkCollectionsData(object_class){
    checked_collection = true;
    $(object_class).each(function() {

        var name = $(this).find('input[type=text]').val();
        var description = $(this).find('textarea').val();

        if(name == "" || description == ""){
            checked_collection = false;
        }

    });
}

function funcTimer7() {
    $('#collections-save-valid').css('visibility', 'hidden');
    $('#collections-save-error').css('visibility', 'hidden');
}



