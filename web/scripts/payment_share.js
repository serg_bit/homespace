var publish = false;
jQuery( document ).ready(function( $ ){
    $('body').on('click', 'a.share-it-now-fb', function(e) {
        e.preventDefault();
        var object_id = $(this).attr('data-id');
        var object_type = $(this).attr('data-type');
        FB.ui(
            {
                method: 'feed',
                link: $(this).attr('data-url'),
                picture: $(this).attr('data-image'),
                caption: 'Reference Documentation',
                description: $(this).attr('data-description'),
            },
            function(response) {
                if (response && response.post_id) {
                    var post = $.ajax({
                        data: {payment : 1},
                        type: 'post',
                        url: '/account/payment/payment-share',
                        dataType: 'html'
                    });
                    post.done(function(action){
                        if(action == 3){
                            $('#publish').css('background-color', '#006666');
                            publish = true;
                            $('.button-bottom .fa-check-square-o').css('color', '#006666');
                        }
                    });
                }
            }
        );

    });

    $('body').on('click', 'a.share-it-now-pint', function(e) {
        e.preventDefault();
        var left = Math.round((screen.width/2)-350);
        var top = Math.round((screen.height/2)-300);
        var params = "left="+left+",top="+top+",width=700,height=600,menubar=no,location=yes,resizable=no,scrollbars=yes,status=yes"
        var newWin = window.open($(this).attr('href'), "Pinterest", params);
        $( newWin ).unload(function() {
            var post = $.ajax({
                            type: 'post',
                            url: '/account/payment/payment-share',
                            dataType: 'html'
                        });
            post.done(function(response){
                if(response == 3){
                    $('#publish').css('background-color', '#006666');
                    publish = true;
                    $('.button-bottom .fa-check-square-o').css('color', '#006666');
                }
            });
        });
    });
});

// 1517240635237951
//FB
window.fbAsyncInit = function() {
    FB.init({
        appId      : '1746633468898413',
        xfbml      : true,
        version    : 'v2.5'
    });

};

$('#publish').click(function(event){
    event.preventDefault();
    if(publish){
        location.reload();
    }
});

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_EN/sdk.js#xfbml=1&version=v2.5&appId=1746633468898413";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// TW
window.twttr = (function (d,s,id) {
    var t, js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return; js=d.createElement(s); js.id=id;
    js.src="https://platform.twitter.com/widgets.js"; fjs.parentNode.insertBefore(js, fjs);
    return window.twttr || (t = { _e: [], ready: function(f){ t._e.push(f) } });
}(document, "script", "twitter-wjs"));

// On ready, register the callback...
//twttr.ready(function (twttr) {
//    twttr.events.bind('tweet', function (event) {
//
//    });
//});



