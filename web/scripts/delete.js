$('.delete_item').click(function(e) {
	e.preventDefault();
	var id = $(this).attr('item');
	$.ajax({
		data : { id : id},
        url: 'delete',
        type: 'post',
        dataType: 'html'
    });

});