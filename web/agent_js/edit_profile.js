/**
 * Created by Serg on 28.06.2016.
 */
var action_img = 0;

$('input[name=main-video-link]').keypress(function(e) {
    if(e.keyCode == 13) {
        var link = $(this).val();
        if(link != "" && link.indexOf("https://www.youtube.com/embed/") >= 0)
            $('#video-frame').html('<iframe class="video-img" width="330" height="181" src="'+link+'" frameborder="0" allowfullscreen></iframe>');
        else
            $('#video-frame').html('<img class="video-img" src="/images/no-video.png" alt="" height="181" width="330">');
    }
});

$('#main_save').click(function(){
    var main = $('form#main-user-info').serializeArray();
    var soc = $('form#user-social').serializeArray();
    var message = $('textarea[name=message]').val();
    var avatar = $('#profile-photo').find('img').attr('src');
    var company = $('input[name=company-name]').val();
    var logo = $('#company-logo').find('img').attr('src');
    var bg =  $('.agent-profil').attr('data-url');
    var request = $.ajax({
        data: {
            main: main,
            social: soc,
            message: message,
            avatar: avatar,
            company: company,
            logo: logo,
            bg: bg
        },
        url: '/account/profile-agent/save-info',
        type: 'post',
        dataType: 'html'
    });

    request.done(function (message) {
        $('#main-save-valid').html(message);
        $('#main-save-valid').css('visibility', 'visible');
        $('#security-save-error').css('visibility', 'hidden');
        setTimeout(funcTimer4, 5000);
    });
});

$('#security_save').click(function () {
    var pass = $('#new_pass').val();
    var new_pass = $('#confirm_pass').val();
    if (new_pass == pass) {
        var formData = $('.security-info').serializeArray();
        var request = $.ajax({
            data: {formData: formData},
            url: '/account/edit/save-security',
            type: 'post',
            dataType: 'html'
        });

        request.done(function (message) {

            $('#security-save-valid').html(message);
            $('#security-save-valid').css('visibility', 'visible');
            $('#security-save-error').css('visibility', 'hidden');
            setTimeout(funcTimer5, 5000);

        });
    }
});

$('#all-information-save').click(function () {
    $('#main_save').trigger('click');
    $('#security_save').trigger('click');
    $('#videos_save').trigger('click');
});

$('form').submit(function (e) {
    e.preventDefault();
});

// Change Images
$('#profile-photo').click(function (e) {
    e.preventDefault();
    $('#open_browse').trigger('click');
    action_img = 0;
});

$('#company-logo').click(function (e) {
    e.preventDefault();
    $('#open_browse').trigger('click');
    action_img = 1;
});

$('.bg-profile').click(function (e) {
    e.preventDefault();
    $('#open_browse2').trigger('click');
});

function fileLogoChange(files) {
    var file = files.files;
    if (file.length == 1)
        uploadImg(file);
}

var uploadImg = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    formData.append('path', 'avatar');
    var request = $.ajax({
        data: formData,
        url: '/account/edit/loading-image',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false
    });

    request.done(function (url) {

        if (action_img == 0) {
            $('#profile-photo').find('img').attr('src', url);
        } else {
            $('#company-logo').find('img').attr('src', url);
        }

    });

};

function userBackground(files) {
    action_background = false;
    var file = files.files;
    if (file.length == 1)
        uploadBg(file);
}

var uploadBg = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: '/account/edit/load-back',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false
    });

    request.done(function (name) {
        $('.agent-profil').css('background', 'url(/media/profile/background/' + name + ')no-repeat');
        $('.agent-profil').attr('data-url', name);
    });

};

function funcTimer4() {
    $('#main-save-valid').css('visibility', 'hidden');
    $('#main-save-error').css('visibility', 'hidden');
}

function funcTimer5() {
    $('#security-save-valid').css('visibility', 'hidden');
    $('#security-save-error').css('visibility', 'hidden');
}



