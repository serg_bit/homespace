//-----------------------------------------------------------------
// Adding product. The modal window. agent Page
//-----------------------------------------------------------------
$(document).on("click", "a.create-product-card", function (e) {
    e.preventDefault();
    $('div.open-create-product').show();
})

var dropzone = $('#dropzone1');
var dropzone2 = $('#dropzone2');
var dropzone3 = $('#dropzone3');
var dropzone_id = '#dropzone1';
var img_url1 = "";
var img_url2 = "";
var img_url3 = "";
var check_before_save = 0;

//------------------------------
//upload files
//------------------------------
var upload = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: 'projects-agent/upload',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false,
    });

    request.done(function (url) {
        $(dropzone_id).css('background', 'url(' + url + ')');
        if (dropzone_id == "#dropzone1") {
            img_url1 = url;
        } else if (dropzone_id == "#dropzone2") {
            img_url2 = url;
        } else if (dropzone_id == "#dropzone3") {
            img_url3 = url;
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert(textStatus);
    });

};

dropzone.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone1").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        dropzone_id = '#dropzone1';
        upload(e.dataTransfer.files);
    }
};

dropzone.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};


dropzone2.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone2").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        dropzone_id = '#dropzone2';
        upload(e.dataTransfer.files);
    }
};

dropzone2.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone2.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};

dropzone3.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone3").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        dropzone_id = '#dropzone3';
        upload(e.dataTransfer.files);
    }
};

dropzone3.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone3.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};


$(".dropzone").click(function () {
    $('#open_browse').trigger('click');
    dropzone_id = '#' + $(this).attr('id');
});


function fileAgentChange(files) {
    var file = files.files;
    if (file.length == 1)
        upload(file);
}

//--------------------------------
//search for tags (autocomplete)
//--------------------------------
var tags_input = $("input#add-tags"),
    tags_id = [];

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}
tags_input.keypress(function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    var search_tags = extractLast($(this).val());
    var search = $.ajax({
        data: {
            search_tags: search_tags,
        },
        url: 'projects-agent/search-tags',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var res = [];
        var i;
        for (i = 0; i < action.length; i++) {
            if (action[i].tag != null) {
                res.push({value: action[i].tag, label: action[i].tag, id: action[i].id});
            }
        }

        $(".agent-modal-product .modal-content #add-tags").autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.ui.autocomplete.filter(
                    res, extractLast(request.term)));
            },
            focus: function () {
                return false;
            },
            select: function (e, ui) {
                var terms = split(this.value);
                terms.pop();
                terms.push('#' + ui.item.value);
                terms.push("");
                this.value = terms.join(", ");
                tags_id.push(ui.item.id);
                return false;
            }
        });
    })
})

//-------------------------------
//add new product
//-------------------------------
$(document).on('click', '#save-product', function () {
    $('div.error-message span').text('');
    $('div.error-message').hide();
    $('div.success-message').hide();
    var product_name = $("input[name='product-name']").val(),
        company_name = $("input[name='company-name']").val(),
        product_collection = $("select[name='product-collection']").val(),
        prod_description = $("textarea[name='description']").val(),
        price = $("input[name='public-price']").val(),
        send_data = $.ajax({
            data: {
                'product_name': product_name,
                'company_name': company_name,
                'product_collection': product_collection,
                'description': prod_description,
                'price': price,
                'image1': img_url1,
                'image2': img_url2,
                'image3': img_url3,
                'tags_id': tags_id,
            },
            url: 'projects-agent/add-new-product',
            type: 'post',
            dataType: 'html',
        });
    send_data.done(function (data) {
        if (data == 'done!') {
            $("input[name='product-name']").val('');
            $("input[name='company-name']").val('');
            $("textarea[name='description']").val('');
            $("input[name='public-price']").val('');
            $("input#add-tags").val('');
            dropzone.css('background', '');
            dropzone2.css('background', '');
            dropzone3.css('background', '');
            img_url1 = "";
            img_url2 = "";
            img_url3 = "";
            tags_id = "";
            $('div.success-message').show();
        } else {
            $('div.error-message span').text(data);
            $('div.error-message').show();
        }
    })
})

//-------------------------------
//user statistic(graph)
//-------------------------------




