//-------------------------------
// error messages
//-------------------------------
var get = window.location;
var lang_param = get['href'].search(/\/en\//i);
if (lang_param != -1) {
    var error_message = 'Fill all fields!',
        check_fields_error1 = 'The field ',
        check_fields_error2 = 'can not be empty!',
        name_field_logo = 'Logo',
        name_field_avatar = 'Photo',
        password_error = 'Error when entering a password (the password is at least 6 characters or passwords are not the same)';
} else {
    var error_message = 'Заполните все поля!',
        check_fields_error1 = 'Поле ',
        check_fields_error2 = 'не может быть пустым!',
        name_field_logo = 'Логотип',
        name_field_avatar = 'Фото',
        password_error = 'Ошибка при вводе пароля (длина пароля менее 6 символов либо пароли не совпадают)';
}

//-------------------------------
// upload logo(dropzone)
//-------------------------------
var dropzone = $("#dropzone1"),
    id_upload_el = '',
    company_logo = '',
    company_avatar = '';

dropzone.ondrop = function (e) {
    e.preventDefault();
    var img = $("#dropzone1").find("img");
    if (img.length != 1) {
        this.className = 'dropzone';
        id_upload_el = 'logo';
        upload(e.dataTransfer.files);
    }
};

dropzone.ondragover = function () {
    this.className = 'dropzone dragover';
    return false;
};

dropzone.ondragleave = function () {
    this.className = 'dropzone';
    return false;
};

dropzone.click(function () {
    $('#open_browse').trigger('click');
    id_upload_el = 'logo';
});
//-------------------------------
// upload avatar
//-------------------------------
$('#profile-photo').click(function (e) {
    e.preventDefault();
    $('#open_browse1').trigger('click');
    id_upload_el = 'avatar';
    that_use = 1;
    action_img = 0;
});
//-------------------------------
// upload avatar(consultant)
//-------------------------------

$('#consultant-photo').click(function (e) {
    e.preventDefault();
    $('#open_browse1').trigger('click');
    id_upload_el = 'consultant';
    that_use = 1;
    action_img = 0;
});

//-------------------------------
// upload image(main functions)
//-------------------------------

function fileChange(files) {
    var file = files.files;
    if (file.length == 1) {
        upload(file);
    }
}
var upload = function (files) {
    var formData = new FormData();
    formData.append('file', files[0]);
    var request = $.ajax({
        data: formData,
        url: 'projects-agent/upload',
        type: 'post',
        dataType: 'html',
        contentType: false,
        processData: false,
    });

    request.done(function (url) {
        switch (id_upload_el) {
            case 'logo':
                $("#dropzone1").css('background', 'url(' + url + ')');
                company_logo = url;
                break;
            case 'avatar':
                $("a#profile-photo img").attr("src", url);
                company_avatar = url;
                break;
            case 'consultant':
                $("a#consultant-photo img").attr("src", url);
                break;
        }
    });

    request.fail(function (jqXHR, textStatus) {
        alert(textStatus);
    });
};
//-------------------------------
// add consultants (deprecated)
//-------------------------------

$(document).on("click", "#add-consultant", function (e) {
    e.preventDefault();
    $("div.consultant p.pass span").text('');
    $("div.consultant p.pass").hide();
    var consultant_first_name = $("input[name='consultant-first-name']").val(),
        consultant_last_name = $("input[name='consultant-last-name']").val(),
        consultant_phone = $("input[name='consultant-phone']").val(),
        consultant_position = $("input[name='consultant-position']").val(),
        consultant_avatar = $("a#consultant-photo img").attr("src"),
        messages_status = $("div.consultant").find("input[type='radio']:checked");
    if (consultant_position == '' || consultant_first_name == '' || consultant_last_name == '' || consultant_phone == '' || consultant_avatar == '') {
        $("div.consultant p.pass span").text(error_message);
        $("div.consultant p.pass").show();
    } else {
        $("ul.consultants-list").append("<li data-id=" + messages_status.attr('id') + "><a href='#'><img src=" + consultant_avatar + " alt='#'>" + consultant_first_name + " " + consultant_last_name + " <span>(" + consultant_position + ")</span></a><b>Give rights<i class='icon-black218 '></i></b></li>");
        $("input[name='consultant-first-name']").val('');
        $("input[name='consultant-last-name']").val('');
        $("input[name='consultant-position']").val('');
        $("input[name='consultant-phone']").val('');
        $("a#consultant-photo img").attr("src", '');
    }
})

//-------------------------------
// add categories (autocomplete)
//-------------------------------
var categories_input = $("input[name='categories']"),
    categories_id = [];

function split(val) {
    return val.split(/,\s*/);
}

function extractLast(term) {
    return split(term).pop();
}

categories_input.keypress(function (e) {
    var keycode = (e.keyCode ? e.keyCode : e.which);
    var search_categories = extractLast($(this).val());
    var search = $.ajax({
        data: {
            search_categories: search_categories,
        },
        url: '/account/advertising-tools/search-categories',
        type: 'post',
        dataType: 'html',
    });
    search.done(function (action) {
        action = JSON.parse(action || 'null');
        var res = [];
        var i;
        for (i = 0; i < action.length; i++) {
            if (action[i].category != null) {
                res.push({value: action[i].category, label: action[i].category, id: action[i].id});
            }
        }

        categories_input.autocomplete({
            minLength: 0,
            source: function (request, response) {
                response($.ui.autocomplete.filter(
                    res, extractLast(request.term)));
            },
            focus: function () {
                return false;
            },
            select: function (e, ui) {
                var terms = split(this.value);
                terms.pop();
                terms.push(ui.item.value);
                terms.push("");
                this.value = terms.join(", ");
                tags_id.push(ui.item.id);
                return false;
            }
        });
    })
})

//-------------------------------
// add video
//-------------------------------
$("input[name='video-link']").keypress(function (e) {
    if (e.keyCode == 13) {
        var link = $(this).val();
        var parent_div = $("div.video");
        $(parent_div).replaceWith('<iframe class="video-img" width="330" height="181" src="' + link + '" frameborder="0" allowfullscreen></iframe>');
    }
});

//-------------------------------
// check fields error message
//-------------------------------
var show_error_message = function (elem) {
    $('p.check-form-error-message span').text(check_fields_error1 + elem + " " + check_fields_error2);
    $('p.check-form-error-message').show();
}

//-------------------------------
// check fields passwords error message
//-------------------------------
var show_error_password_message = function () {
    $('p.check-form-error-message span').text(password_error);
    $('p.check-form-error-message').show();
}

//-------------------------------
// create company
//-------------------------------
$(document).on("click", "a#create-company", function (e) {
    e.preventDefault();
    $('p.check-form-error-message span').text('');
    $('p.check-form-error-message').hide();
    var company_name = $("input[name='company-name']"),
        brand_name = $("input[name='brand-name']"),
        company_position = $("input[name='position']"),
        company_first_name = $("input[name='first-name']"),
        company_last_name = $("input[name='last-name']"),
        company_city = $("input[name='city']"),
        company_phone = $("input[name='phone']"),
        company_address = $("input[name='address']"),
        company_site = $("input[name='site']"),
        link_fb = $("input[name='facebook']"),
        link_twitter = $("input[name='twitter']"),
        link_instagram = $("input[name='instagram']"),
        link_behance = $("input[name='behance']"),
        link_google = $("input[name='google']"),
        link_pinterest = $("input[name='pinterest']"),
        link_tumblr = $("input[name='tumblr']"),
        link_linkedin = $("input[name='linkedin']"),
        link_blogger = $("input[name='blogger']"),
        company_message = $("textarea[name='message']"),
        company_email = $("input[name='email']"),
        company_login = $("input[name='login']"),
        company_password = $("input[name='password']"),
        company_confirm_password = $("input[name='confirm']"),
        company_video_link = $("input[name='video-link']"),
        company_video_name = $("input[name='video-name']");
    if (company_name.val() == '') {
        show_error_message($("label[for='company-name']").text());
    } else if (brand_name.val() == '') {
        show_error_message($("label[for='brand-name']").text());
    } else if (company_position.val() == '') {
        ;
        show_error_message($("label[for='position']").text());
    } else if (company_first_name.val() == '') {
        show_error_message($("label[for='first-name']").text());
    } else if (company_last_name.val() == '') {
        show_error_message($("label[for='last-name']").text());
    } else if (company_city.val() == '') {
        show_error_message($("label[for='city']").text());
    } else if (company_phone.val() == '') {
        show_error_message($("label[for='phone']").text());
    } else if (company_address.val() == '') {
        show_error_message($("label[for='address']").text());
    } else if (company_logo == '') {
        show_error_message(name_field_logo);
    } else if (company_avatar == '') {
        show_error_message(name_field_avatar);
    } else if (company_email.val() == '') {
        show_error_message($("label[for='email']"));
    } else if (company_login.val() == '') {
        show_error_message($("label[for='login']"));
    } else if (company_password.val() == '') {
        show_error_message($("label[for='password']"));
    } else if (company_password.val() != company_confirm_password.val() && company_password.val().length < 6) {
        show_error_password_message($("label[for='password']"));
    } else {
        var send_company_data = $.ajax({
            data: {
                company_name: company_name,
                brand_name: brand_name,
                brand_name: first_name,
                company_first_name: company_first_name,
                company_last_name: company_last_name,
                company_city: company_city,
                company_phone: company_phone,
                company_address: company_address,
                company_site: company_site,
                link_fb: link_fb,
                link_twitter: link_twitter,
                link_instagram: link_instagram,
                link_behance: link_behance,
                link_google: link_google,
                link_pinterest: link_pinterest,
                link_linkedin: link_linkedin,
                link_tumblr: link_tumblr,
                link_blogger: link_blogger,
                company_message: company_message,
                company_email: company_email,
                company_login: company_login,
                company_password: company_password,
                company_confirm_password: company_confirm_password,
                company_video_link: company_video_link,
                company_video_name: company_video_name,
                company_logo: company_logo,
                company_avatar: company_avatar,
            },
            url: '/account/edit/create-new-company',
            type: 'post',
            dataType: 'html'
        });
        send_data.done(function (message) {
            $('#save-information-agent-valid').html(message);
            $('#save-information-agent-valid').css('visibility', 'visible');
            setTimeout(funcTimer4, 5000);
        });
    }
})



