$(document).ready(function () {

//---------Сортировка по тегам---------------------//

    $('.sort-teg').click(function (e) {
        e.preventDefault();
        $('.sort-teg').removeClass('active-category');
        $(this).addClass('active-category');
        var category = $(this).attr('data-value');
        $('#catalog-category').val(category);
        $('#catalog-form').submit();
    });

//----------Сортировка общая --------------------------------//

    $('.select-options li').click(function(){
        var value = $(this).attr('rel');
        var parent_class = $(this).parent().parent().parent().attr('name');
        sortSubmit(value, parent_class);
    });
    $('#tag_field').change(function(e){
        e.preventDefault();
        sortSubmit($(this).val(), 'meta_tags');
    });

    function sortSubmit(value, parent_class) {
        $('#productslist-' + parent_class).val(value);
        $('#productslist-form').submit();
    }

    //---------------добавление товара в ордер--------------------//

    $('.add-order').click(function () {
        var get_prod = $(this).attr('id_prod');
        var message_add  = $(".success_add").attr('message_add');
        $(".addToOrder").attr('id_prod', get_prod);
        $(".addToOrder").click(function () {
            var id_prod = $(this).attr('id_prod');
            $.ajax({
                data: {id_prod: id_prod},
                url: '/account/catalog/addorder',
                type: 'post',
                dataType: 'html',
                success: function (data) {
                    $(".success_add").html('<i class="icon-basic14"></i>'+ message_add);
                    function close() {
                        $('.add-order-model').modal('hide');
                        $(".success_add").html('');
                    }

                    setTimeout(close, 3000);
                }
            });
        });
        // console.log(id_prod);


    });

    //----------удаление, перемещение товара из ордера,папки-----------//

    $('.move-remove').click(function () {
        var message_del  = $(".success_move").attr('message_del');
        var message_move = $(".success_move").attr('message_move');
        var get_id_order = $(this).attr('id_order');
        $(".remove-order").attr('id_order', get_id_order);
        $(".remove-order").click(function(){
            var id_order = $(this).attr('id_order');
            $.ajax({
                data: {id_order: id_order},
                url: '/account/order/delete',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if(data==1){
                        $(".success_move").html('<i class="icon-basic14"></i>'+ message_del);
                        function close() {
                            $('.add-order-model').modal('hide');
                            $(".success_add").html('');
                        }

                        setTimeout(close, 3000);
                        location.reload();
                    }
                    //alert('Deleted from collect order');
                }
            });
        });
        //if (document.URL.search('/order/')>0){

        var select = $('.move-to').find('.select-options li');
        select.click(function(){
            var value_opt = $(this).attr('rel');
            $.ajax({
                data: {folder: value_opt, id_order: get_id_order,},
                url: '/account/order/move-to',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if (data == 1) {
                        $(".success_move").html('<i class="icon-basic14"></i>' + message_move);
                        function close() {
                            $('.add-order-model').modal('hide');
                            $(".success_add").html('');
                            $('#select_checked_3').empty();

                        }

                        setTimeout(close, 2000);
                        location.reload();
                    }
                }
            });
        });
    });

    //-------------новая папка в complete order----------//

    $('#showCreate').click(function (e) {
        e.preventDefault();
        $('#addNewFolder').toggle();
    });
    $("#folder").keypress(function (e) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var folder_name = $(this).val();
            if (folder_name == "") {
                folder_name = 'Name folder';
            }
            $.ajax({
                data: {title: folder_name,},
                url: '/account/order/newfolder',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if(data=='0'){
                        $('.folder-error').show();
                    }
                    else if(data=='1'){
                        location.reload();
                    }
                }
            });
        }
    });
    $('#create_folder').click(function () {
        var folder_name = $('#folder').val();
        if (folder_name == "") {
            folder_name = 'Name folder';
        }
        $.ajax({
            data: {title: folder_name},
            url: '/account/order/newfolder',
            type: 'post',
            dataType: 'html',
            cache: false,
            success: function (data) {
                if(data=='0'){
                    $('.folder-error').show();
                }
                else if(data=='1'){
                    location.reload();
                }
            }
        });
    });
//-------------новая папка в catalog----------//


var catalog_select = $('.catalog-select').find('.select-options li');
    catalog_select.click(function(){
        var value_opt = $(this).attr('rel');
        var id_prod = $(".addToOrder").attr('id_prod');
        var succes_message_add = $(".success_add").attr('text');
        if(value_opt =="+ Add folder"){
            $('#addFolder').show();
        } else{
            $('#addFolder').hide();
            $.ajax({
                data: {id_folder : value_opt, id_prod : id_prod,},
                url: '/account/catalog/addtofolder',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if(data==1){
                        $(".success_add").html('<i class="icon-basic14"></i>' + succes_message_add);
                        function close() {
                            $('.add-order-model').modal('hide');
                            $(".success_add").html('');
                            $('#select_checked_3').empty();

                        }
                        setTimeout(close, 2000);
                    }
                }
            });
        }

    });



    $("#folder_into").keypress(function (e) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var succes_message_add = $(".success_add").attr('text');
        if (keycode == '13') {
            var folder_name = $(this).val();
            if (folder_name == "") {
                folder_name = 'Name folder';
            }
            var id_prod = $(".addToOrder").attr('id_prod');
            $.ajax({
                data: {title : folder_name, id_prod : id_prod},
                url: '/account/catalog/createaddfolder',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if(data==1){
                        $('#addFolder').hide();
                        $(".success_add").html('<i class="icon-basic14"></i>' + succes_message_add);
                        function close() {
                            $('.add-order-model').modal('hide');
                            $(".success_add").html('');
                            $('#select_checked_3').empty();
                        }
                        setTimeout(close, 2000);
                    }
                }
            });
        }
    });

//--------------новая папка в папке--------------------//
    $('#create-child-folder').click(function (e) {
        e.preventDefault();
        $('#child-folder').toggle();
    });
    $("#folder-in-folder").keypress(function (e) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var folder_name = $(this).val();
            if (folder_name == "") {
                folder_name = 'Name folder';
            }
            var parent = $(this).attr('folder');
            $.ajax({
                data: {title : folder_name, folder : parent},
                url: '/account/order/newfolder',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {
                    if(data=='0'){
                        $('.folder-error').show();
                    }
                    else if(data=='1'){
                        location.reload();
                    }
                }
            });
        }
    });
    $('#create_in_folder').click(function () {
        var folder_name = $('#folder-in-folder').val();
        var parent = $('#folder-in-folder').attr('folder');
        if (folder_name == "") {
            folder_name = 'Name folder';
        }
        $.ajax({
            data: {title: folder_name, folder : parent},
            url: '/account/order/newfolder',
            type: 'post',
            dataType: 'html',
            cache: false,
            success: function (data) {
                if(data=='0'){
                    $('.folder-error').show();
                }
                else if(data=='1'){
                    location.reload();
                }
            }
        });
    });



    //---------удалить папку--------//

    $('.del_folder').click(function () {
        var id_folder = $(this).closest('.name-folder').attr('id_folder');
        var url = $(this).attr('url');
        $.ajax({
            data: {id_folder: id_folder},
            url: url,
            type: 'post',
            dataType: 'html',
            cache: false,
            success: function (data) {
                if(data==1){
                    location.reload();
                }
            }
        });

    });

    //---------rename folder-----//

    $('.re_folder').click(function (e) {
        e.preventDefault();
        var p = $(this).closest('.change-folder').find('p');
        p.hide();
        $(this).closest('ul').css({'display':'none'});
        var input = $(this).closest('.change-folder').find('.input-container');
        input.show();
        var text = $(this).closest('.change-folder').find('.title');
        var current_text = text.val();
        text.val(current_text);
        text.focus();
        var id_folder = $(this).closest('.name-folder').attr('id_folder');
        text.keypress(function (e) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                var folder_name = text.val();
                if (folder_name == "") {
                    $(this).closest('.change-folder').find('.input-container label').addClass('for-folder-error');
                    return;
                } else if(folder_name==current_text){
                    input.hide();
                    $(this).closest('.change-folder').find('p').show();
                } else {
                    $.ajax({
                        data: {title: folder_name, id_folder: id_folder},
                        url: '/account/order/rename',
                        type: 'post',
                        dataType: 'html',
                        cache: false,
                        success: function (data) {
                            if(data==0){
                                $(this).closest('.change-folder').find('.input-container label').addClass('for-folder-error');
                            } else if(data==1) {
                                input.hide();
                                p.text(folder_name).show();
                            }


                        }
                    });
                }

            }
        });

    });

//-----------стили для формы создания проэкта-заявки------------------------//
    var app_name = $('#application-name').attr('data-name');
    var app_upload = $('#application-upload').attr('data-upload');

    $("<label class='control-label' for='title_app'>" + app_name + "</label>").insertAfter('input#application-title_app');
    $("<i class='icon-direction13'></i>" + app_upload + "</a>").insertAfter('input#application-file');

    //-------Coздание проэкта------//

    $('.reset-app').click(function (e) {
        $(".new_project").hide();
        new_project = 1;
    });
    $(".shared-with").click(function () {
        $(".shared-upload  .select").slideToggle("fast");
    });


    //----------Drag-n-drop для картинок--------//

    //$("div.catalog-item a img").mousedown(function(e){
    //    $(this).css("position : absolute");
    //    moveAt(e);
    //    $(document).append($(this));
    //    $(this).css("zIndex : 1000");
    //})

    //$("div.catalog-item a img").draggable({ drag:function(event, ui){
    //    ui.position.left = x;
    //    ui.position.top = y;
    //}});
    //$("div.catalog-item").css("z-index : 9999");
    //$("div.name-folder").css("z-index : 1");
    //
    //$( "div.catalog-img-box").parent().draggable({
    //    containment: ".central-content"
    //});
//------------Прикрепленные файлы-------------------//

    $('.atachFile').click(function (e) {
        e.preventDefault();
        $(this).next('.atached').slideToggle();
    });


// -------------------страница входящие заявки----------------//
    $('.inbox_app').click(function (e) {
        e.preventDefault();
        var id_app = $(this).find('.send-to').attr('id_app');
        var status = $(this).find('.status_app').text();
        if (status == 0) {
            $(this).parent().removeClass();
            $.ajax({
                data: {app_id: id_app},
                url: '/account/application/read',
                type: 'post',
                dataType: 'html',
                cache: false,
                success: function (data) {

                }
            });
        }
        $(this).next().next().next('.app_coment').slideToggle();

    });
// ---------------------ответ на заявку------------------//
    var filesForSend;

    $('.submit-app').click(chooseFile);

    function chooseFile(e) {
        e.preventDefault();

        var text = $(this).parent().parent().find('.app_review').val();
        var app_id = $(this).attr('id_app');
        var app_coment =   $('div[send_id_app='+app_id+']');
        var app_comment_par =app_coment.parent();
        var container = app_coment.parent().find('.input-app-file');
        if (filesForSend) {

            //var app_coment = $(this).parent().parent().parent();

            fileLists =  container.find('.app-before-send');
            fileList = fileLists.find('span.file-name'),
                i = 0,
                files = "",
                data = new FormData();
            $.each(filesForSend, function (key, value) {
                ext = value.name.split(".");

                if (ext[ext.length - 1] != 'doc' && ext[ext.length - 1] != 'txt' && ext[ext.length - 1] != 'rtf' && ext[ext.length - 1] != 'jpg' && ext[ext.length - 1] != 'pdf' && ext[ext.length - 1] != 'png') {
                    alert("Формат файла(" + ext[ext.length - 1] + ") не поддерживается!");
                    return;
                }

                if (value.size > 3000000) {
                    alert("Превышен максимальный размер файлов - 3МБ!");
                    return;
                }

                data.append(key, value);
            });
            for (i = 0; i < fileList.length; i++) {

                files = files + fileList[i].innerHTML;

            }
            data.append('text', text);
            data.append('fileNames', files);
            data.append('app_id', app_id);
            $(this).parent().parent().find('app_review').val('');

            $.ajax({
                data: data,
                url: '/account/application/send',
                type: 'post',
                dataType: 'html',
                contentType: false,
                processData: false,
                beforeSend: function (){
                    container.find('.app-before-send').empty();
                    app_coment.empty();
                },
                success: function (action) {
                    action = JSON.parse(action || 'null');
                    var uploadedFiles = "",
                        n = 0;
                    for (n = 0; n < action.file_name.length; n++) {
                        uploadedFiles += "<li><a href='/media/application/send_app/" + action.file_name[n] + "' download='/media/application/send_app/" + action.file_name[n] + "'>" + action.file_name[n].substr(4, action.file_name[n].length - 1) + "</a></li>";
                    }
                    var str = $("<div class='reviews aplication'><h4 class='promote'><?= Yii::t('account', 'profile_reviews') ?></h4><div class='reviews-comment'>" +
                                    "<div class='reviews-photo'>" +
                                         "<img src='" + action.url_avatar + "' alt=''>" +
                                         "<p class='name'>" + action.sender_username + "" +
                                            "<span>" + action.sender_specialization + "</span>" +
                                        "</p>" +
                                    "</div>" +
                                    "<div class='my-content-messages'>" +
                                        "<p>" + action.message + "</p>" +
                                        "<ul class='uploading-files'>" + uploadedFiles + "</ul>" +
                                    "</div>" +
                                    "<p class='data'>" + action.created_at + "</p>" +
                                "</div>" +
                            "</div> ");
                    //app_coment.parent().find('.reviews aplication').append(str);
                    app_comment_par.append(str);
                }
            });
            //console.log(filesForSend);
        }

            //---------вариант без файлов-------//

         else {
            var csrfToken = $('meta[name="csrf-token"]').attr("content");
            //var app_comment_par =app_coment.parent();
            $.ajax({
                data: {text: text, app_id: app_id, _csrf: csrfToken},
                url: '/account/application/text',
                type: 'post',
                dataType: 'html',
                beforeSend: function (){
                    container.find('.app-before-send').empty();
                    app_coment.empty();
                },
                success: function (action) {
                    action = JSON.parse(action || 'null');
                    var str = $("<div class='reviews aplication'>" +
                                    "<h4 class='promote'><?= Yii::t('account', 'profile_reviews') ?></h4>" +
                                    "<div class='reviews-comment'>" +
                                        "<div class='reviews-photo'>" +
                                            "<img src='" + action.url_avatar + "' alt=''>" +
                                            "<p class='name'>" + action.sender_username + "" +
                                            "<span>" + action.sender_specialization + "</span>" +
                                            "</p>" +
                                        "</div>" +
                                        "<div class='my-content-messages'>" +
                                            "<p>" + action.message + "</p>" +
                                        "</div>" +
                                        "<p class='data'>" + action.created_at + "</p>" +
                                    "</div>" +
                                "</div> ");
                    //app_coment.parent().find('.reviews aplication').append(str);
                    app_comment_par.append(str);
                }
            });
        }
    }

    $('.answer_file').change(uploadApp);

    function uploadApp() {
        var app_coment = $(this).parent().parent().parent();
        var container = app_coment.parent().find('.input-app-file');
        var messages = container.parent().parent().find('.input-app-file');
        messages.empty();
        filesForSend = this.files,
            //var files = this.files,
            i = 0,
            str = "",
            fnames = "";
//console.log(filesForSend);
        if (filesForSend.length > 0) {
            container.append("<ul class='app-before-send'></ul>");
        }
        for (i = 0; i < filesForSend.length; i++) {

            fnames = fnames + "<li><a href='#' class='del-before-app' style='display : table;'><span class='file-name'>" + filesForSend[i].name + " </span><span class='del-before-app'>(x)</span></a></li>";

        }
        container.find('.app-before-send').append(fnames);
//    $("ul.app-before-send").append(fnames);
        $("li a span.del-before-app").click(function (e) {
            e.preventDefault();
            $(this).parent().remove();
        });
    };
    //------------------ написать сообщение-----------------//
    $(".send-chat-message").click(function (e) {

        $(".chat-block").slideToggle("fast");

    });
    //----------------- кнопка like--------------------//
    $("div.social-cont a").click(function(e){

        e.preventDefault();
        //console.log($(this).attr("id"));
        var prod_id = $(this).attr("id");
        var like = $.ajax({
            data: {id: prod_id},
            type: 'post',
            url: '/account/catalog/likes',
            dataType: 'html'
        });

        like.done(function(action){

            $("span[likes_id="+ prod_id +"]").html(action);

        })
    });


        //$.widget( "custom.combobox", {
        //    _create: function() {
        //        this.wrapper = $( "<div>" )
        //            .addClass( "custom-combobox input-container " )
        //            .insertAfter( this.element );
        //
        //        this.element.hide();
        //        this._createAutocomplete();
        //        this._createShowAllButton();
        //    },
        //
        //    _createAutocomplete: function() {
        //        var selected = this.element.children( ":selected" ),
        //            value = selected.val() ? selected.text() : "";
        //
        //        this.input = $( "<input>" )
        //            .appendTo( this.wrapper )
        //            .val( value )
        //            .attr( "title", "" )
        //            .addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left floating-label" )
        //            .autocomplete({
        //                delay: 0,
        //                minLength: 0,
        //                source: $.proxy( this, "_source" )
        //            })
        //            .tooltip({
        //                tooltipClass: "ui-state-highlight"
        //            });
        //
        //        this._on( this.input, {
        //            autocompleteselect: function( event, ui ) {
        //                ui.item.option.selected = true;
        //                this._trigger( "select", event, {
        //                    item: ui.item.option
        //                });
        //            },
        //
        //            autocompletechange: "_removeIfInvalid"
        //        });
        //    },
        //
        //    _createShowAllButton: function() {
        //        var input = this.input,
        //            wasOpen = false;
        //
        //        $( "<a>" )
        //            .attr( "tabIndex", -1 )
        //            .attr( "title", "Show All Countries" )
        //            .tooltip()
        //            .appendTo( this.wrapper )
        //            .button({
        //                icons: {
        //                    primary: "ui-icon-triangle-1-s"
        //                },
        //                text: false
        //            })
        //            .removeClass( "ui-corner-all" )
        //            .addClass( "custom-combobox-toggle ui-corner-right" )
        //            .mousedown(function() {
        //                wasOpen = input.autocomplete( "widget" ).is( ":visible" );
        //            })
        //            .click(function() {
        //                input.focus();
        //
        //                // Close if already visible
        //                if ( wasOpen ) {
        //                    return;
        //                }
        //
        //                // Pass empty string as value to search for, displaying all results
        //                input.autocomplete( "search", "" );
        //            });
        //    },
        //
        //    _source: function( request, response ) {
        //        var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
        //        response( this.element.children( "option" ).map(function() {
        //            var text = $( this ).text();
        //            if ( this.value && ( !request.term || matcher.test(text) ) )
        //                return {
        //                    label: text,
        //                    value: text,
        //                    option: this
        //                };
        //        }) );
        //    },
        //
        //    _removeIfInvalid: function( event, ui ) {
        //
        //        // Selected an item, nothing to do
        //        if ( ui.item ) {
        //            return;
        //        }
        //
        //        // Search for a match (case-insensitive)
        //        var value = this.input.val(),
        //            valueLowerCase = value.toLowerCase(),
        //            valid = false;
        //        this.element.children( "option" ).each(function() {
        //            if ( $( this ).text().toLowerCase() === valueLowerCase ) {
        //                this.selected = valid = true;
        //                return false;
        //            }
        //        });
        //
        //        // Found a match, nothing to do
        //        if ( valid ) {
        //            return;
        //        }
        //
        //        // Remove invalid value
        //        this.input
        //            .val( "" )
        //            .attr( "title", value + " didn't match any item" )
        //            .tooltip( "open" );
        //        this.element.val( "" );
        //        this._delay(function() {
        //            this.input.tooltip( "close" ).attr( "title", "" );
        //        }, 2500 );
        //        this.input.autocomplete( "instance" ).term = "";
        //    },
        //
        //    _destroy: function() {
        //        this.wrapper.remove();
        //        this.element.show();
        //    }
        //});


    //$(function() {
    //    $( "#signupform-country" ).combobox();
    //});

});

