jQuery( document ).ready(function( $ ){

    $(".action-categories a").click(function(e){
        e.preventDefault();
        var cat_id =  $(this).attr('data-id');
        var categories = $.ajax({
            data: {cat_id : cat_id},
            url: '/catalog/projects',
            type: 'post',
            dataType: 'html'
        });

        categories.done(function(){
            location.reload();
        })

    });

    $(".add-to-my-projects").click(function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var folder_id = $('.catalog-id').attr('data-id');
        var ajax = $.ajax({
            data: {id : id, folder : folder_id},
            url: '/catalog/add-to-like-it',
            type: 'post',
            dataType: 'html'
        });

        ajax.done(function(response){
            alert(response);
        })
    });

});

function SortChangeLi(value, parent_class) {
    var post = $.ajax({
        data: {value: value, parent_class: parent_class},
        url: '/catalog/change-sort',
        type: 'post',
        dataType: 'html'
    });

    post.done(function () {
        location.reload();
    });
}
