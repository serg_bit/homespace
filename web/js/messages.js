$(document).ready(function(){
    $(".message-from-moon").hide();

    $("p.write a").click(function(e){

        e.preventDefault();
        $(".message-from-moon").slideToggle();

    })
//---------поиск пользователей по вводу--------//
    var messageList = $(".message-from-moon").find("input.text-input");

    messageList.keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var searchUser = $(this).val();

        var search = $.ajax({
                    data : {
                        search : searchUser,
                    },
                    url: '/account/message/searchrecipients',
                    type: 'post',
                    dataType: 'html',
                });
        search.done(function(action){
            action = JSON.parse(action || 'null');
            var availableUsers = [];
            var availableUsersId = [];
            var i = 0;

            for (i=0; i<action.length; i++){

                if(action[i].first_name != null && action[i].last_name != null){

                    availableUsers.push({value: action[i].first_name + " " + action[i].last_name, label: action[i].first_name + " " + action[i].last_name, id: action[i].id});

                }

            }

            $(messageList).autocomplete({
                    minLength: 0,
                    source: availableUsers,
                    focus: function( event, ui ) {
                        $( "#availableUsers" ).val( ui.item.label );
                        return false;
                    },
                    select: function( event, ui ) {
                        $(".ui-autocomplete-input" ).val(ui.item.label);
                        $(".ui-autocomplete-input" ).attr("id", ui.item.id );
                        return false;
                    }
                })
                .data( "item.autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a id='"+item.id+"'>" + item.label + "</a>" )
                    .appendTo( ul );
            };
        });
    })



    var filesForSend;

    $('input[type=file].files-to-moon').change(function(){

        $("div.uploading-files-list").empty();

        filesForSend = this.files;
        var files = this.files,
            i = 0,
            str = "",
            fnames="";

        if (files.length>0) {

            $("div.uploading-files-list").append("<ul class='uploading-files view-before-send'></ul>");

        }

        for(i=0; i<files.length; i++){

            fnames = fnames + "<li><a href='#' class='del-before-upload' style='display : table;'><span class='file-name'>" + files[i].name + " </span><span class='del-before-upload'>(x)</span></a></li>";

        }

        $("div.uploading-files-list ul.uploading-files").append(fnames);
        $("li a span.del-before-upload").click(function(e){

            e.preventDefault();
            $(this).parent().remove();

        })

    });

    $("input.button#message-to-moon").click(function(e){
        e.preventDefault();
        var recipient = $(".message-from-moon").find("input.text-input").attr("id");
        var text = $("textarea.message-to-moon").val();

 
        if(recipient != ""){

            if(filesForSend) {

                fileList = $("ul.view-before-send").find("span.file-name"),
                    i = 0,
                    files = "",
                    data = new FormData();


                $.each(filesForSend, function (key, value) {
                    ext = value.name.split(".");
                    //console.log(value.size);

                    if (ext[ext.length - 1] != 'doc' && ext[ext.length - 1] != 'txt' && ext[ext.length - 1] != 'rtf' && ext[ext.length - 1] != 'jpg' && ext[ext.length - 1] != 'pdf' && ext[ext.length - 1] != 'png') {
                        alert("Формат файла(" + ext[ext.length - 1] + ") не поддерживается!");
                        return;
                    }

                    if (value.size > 2000000) {
                        alert("Превышен максимальный размер файла - 3МБ!");
                        return;
                    }

                    data.append(key, value);

                });

                for (i=0; i < fileList.length; i++){

                    files = files + fileList[i].innerHTML;

                }

                data.append( 'text', text );
                data.append('fileNames', files);
                data.append('recipient', recipient);

                $("textarea.message-to-moon").val("");
                $("div.uploading-files-list").empty();

                message = $.ajax({
                    data :data  ,
                    url: '/account/message/send',
                    type: 'post',
                    dataType: 'html',
                    contentType: false,
                    processData: false,
                });
            //---------варинат без файлов-------// 

            }else if (text !==""){

                message = $.ajax({
                    data : {
                        recipient : recipient,
                        text : text,
                    },
                    url: '/account/message/message',
                    type: 'post',
                    dataType: 'html',
                });
                $("textarea.message-to-moon").val("");

            }else{

                return;

            }

        }else{

            return;

        }

    })



    //---------список загруженных файлов с возможностью удаления-------//
    var allFiles;

    $('input[type=file]#file-message-input').change(function(){
        $("div.uploading-files-main").empty();

        allFiles = this.files;
        var files = this.files,
            i = 0,
            str = "",
            fnames="";

        if (files.length>0) {

            $("div.uploading-files-main").append("<ul class='uploading-files view-before-send'></ul>");

        }

        for(i=0; i<files.length; i++){

            fnames = fnames + "<li><a href='#' class='del-before-upload' style='display : table;'><span class='file-name'>" + files[i].name + " </span><span class='del-before-upload'>(x)</span></a></li>";

        }

        $("div.uploading-files-main ul.uploading-files").append(fnames);
        $("li a span.del-before-upload").click(function(e){

            e.preventDefault();
            $(this).parent().remove();
        })


    });


    //---------отправка сообщения по клику-------//
    $("input.button#main-send").click(function(e){
        e.preventDefault();
        var text = $("textarea[name=message].better-call-soul").val(),
            recipient = $("div.recipient").attr("id"),
            message;
        //---------если файлы были добавлены-------//    
        if(allFiles) {
            fileList = $("ul.view-before-send").find("span.file-name"),
                i = 0,
                files = "",
                data = new FormData();


            $.each(allFiles, function (key, value) {
                ext = value.name.split(".");
                //console.log(value.size);

                if (ext[ext.length - 1] != 'doc' && ext[ext.length - 1] != 'txt' && ext[ext.length - 1] != 'rtf' && ext[ext.length - 1] != 'jpg' && ext[ext.length - 1] != 'pdf' && ext[ext.length - 1] != 'png') {
                    alert("Формат файла(" + ext[ext.length - 1] + ") не поддерживается!");
                    return;
                }

                if (value.size > 2000000) {
                    alert("Превышен максимальный размер файла - 3МБ!");
                    return;
                }

                data.append(key, value);

            });

            for (i=0; i < fileList.length; i++){

                files = files + fileList[i].innerHTML;

            }

            data.append( 'text', text );
            data.append('fileNames', files);
            data.append('recipient', recipient);

            $("textarea[name=message]").val("");
            $("div.uploading-files-main").empty();

            message = $.ajax({
                data :data  ,
                url: '/account/message/send',
                type: 'post',
                dataType: 'html',
                contentType: false,
                processData: false,
            });

            message.done(function(action){

                //action = JSON.parse(action || 'null');
                //
                //var uploadedFiles = "",
                //    n = 0;
                //
                //
                //for(n=0; n<action.file_name.length; n++){
                //
                //
                //    uploadedFiles += "<li><a href='/media/upload/"+ action.file_name[n] +"' download='/media/upload/"+ action.file_name[n] +"'>"+ action.file_name[n].substr(4, action.file_name[n].length-1) +"</a></li>";
                //
                //}
                //
                //
                //
                //var str = $("<div class='reviews-comment my-reviews-comment'><div class='from-content-messages'>"+ action.message +"<ul class='uploading-files'>"+ uploadedFiles +"</ul></div><div class='reviews-photo'><img src='"+ action.url_avatar +"' alt=''><p class='name'>"+ action.sender_username +"<span>"+ action.sender_specialization +"</span></p></div><p class='data'>"+ action.created_at +"</p></div><hr class='sline'>");
                //$("div.all-correspondence").append(str);
                location.reload();


            });

            //---------варинат без файлов-------// 

        }else{

            message = $.ajax({
                data : {
                        recipient : recipient,
                        text : text,
                        },
                url: '/account/message/message',
                type: 'post',
                dataType: 'html',
            });
            $("textarea[name=message]").val("");

            message.done(function(action){
                //action = JSON.parse(action || 'null');
                //
                //var str = $("<div class='reviews-comment my-reviews-comment'><div class='from-content-messages'>"+ action.message +"</div><div class='reviews-photo'><img src='"+ action.url_avatar +"' alt=''><p class='name'>"+ action.sender_username +"<span>"+ action.sender_specialization +"</span></p></div><p class='data'>"+ action.created_at +"</p></div><hr class='sline'>");
                //$("div.all-correspondence").append(str);
                location.reload();

            });


        }


    })
    
    //---------отправка сообщений по Enter------//
    $("textarea[name=message].better-call-soul").keypress(function(e){

        if(e.keyCode == 13) {

            if ($("textarea[name=message].better-call-soul").val() !== ""){

                $( "input.button#main-send" ).trigger( "click" );

                e.preventDefault();

            }else{

                e.preventDefault();

            }
        }
    });

    //---------поиск сообщений по собеседнику-------//

    $("input[type=search].search-interlocutors").keypress(function(event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var searchInterlocutor = $(this).val();
        var all_recipients =  $("input[type=search].search-interlocutors");
        var searchMesages = $.ajax({
            data : {
                search : searchInterlocutor,
            },
            url: '/account/messages/searchinterlocutors',
            type: 'post',
            dataType: 'html',
        });
        searchMesages.done(function(action){

            action = JSON.parse(action || 'null');
            var availableUsers = [];
            var availableUsersId = [];
            var i = 0;

            for (i=0; i<action.length; i++){

                if(action[i].first_name != null && action[i].last_name != null){

                    availableUsers.push({value: action[i].first_name + " " + action[i].last_name, label: action[i].first_name + " " + action[i].last_name, id: action[i].id});

                }

            }
            $(all_recipients).autocomplete({
                    minLength: 0,
                    source: availableUsers,
                    focus: function( event, ui ) {
                        $( "#availableUsers" ).val( ui.item.label );
                        return false;
                    },
                    select: function( event, ui ) {
                        $(".ui-autocomplete-input" ).val(ui.item.label);
                        $(".ui-autocomplete-input" ).attr("id", ui.item.id );
                        //alert (ui.item.id);
                        location.assign("/account/message/index?"+ ui.item.id +"");
                        return false;
                    }
                })
                .data( "item.autocomplete" )._renderItem = function( ul, item ) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( "<a id='"+item.id+"'>" + item.label + "</a>" )
                    .appendTo( ul );
            };

        })

    })


//отключаю действие по умолчанию на пагинацию на странице сообщений
    var str = document.URL,
        searchUrl = str.search('/account/message/');

    if(searchUrl>0){
        $('body').delegate( ".hvr-radial-out a", "click", function(e) {
            e.preventDefault();

            var link = $(this).attr('href');

            $('.all-correspondence').load(link+' .all-correspondence > *');

        });

    }



})




