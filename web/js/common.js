if ($(".myshare").length === 0) {
    $(document).ready(function () {
        var hasClass = function (el, cl) {
            return el.className.match(new RegExp('(\\s|^)' + cl + '(\\s|$)'))
        }
        var addClass = function (el, cl) {
            if (!hasClass(el, cl)) el.className += " " + cl;
        }
        var removeClass = function (el, cl) {
            if (hasClass(el, cl)) {
                el.className = el.className.replace(new RegExp('(\\s|^)' + cl + '(\\s|$)'), ' ');
            }
        }

        var inputs = document.getElementsByClassName('text-input');

        for (var i = 0; i < inputs.length; i++) {
            var input = inputs[i];

            if ($(input).attr('value') != '') {
                addClass(input, 'dirty')
            } else {
                removeClass(input, 'dirty')
            }
            input.addEventListener('keyup', function (e) {
                if (this.value != '') {
                    addClass(this, 'dirty')
                } else {
                    removeClass(this, 'dirty')
                }
            });

            if (input.hasAttribute('maxrows') && !isNaN(parseInt(input.getAttribute('maxrows')))) {
                var maxrows = parseInt(input.getAttribute('maxrows'));

                //Setup InputClone and MaxHeight.
                var maxInputHeight = document.createElement('div');
                maxInputHeight.className = 'maxHeight';
                for (var j = 0; j < maxrows; j++) {
                    maxInputHeight.appendChild(document.createElement('br'));
                }

                input.parentNode.appendChild(maxInputHeight);

                var inputCloneContainer = document.createElement('div');
                inputCloneContainer.className = 'inputClone';
                var inputClone = document.createElement('span');
                inputCloneContainer.appendChild(inputClone);
                input.parentNode.appendChild(inputCloneContainer);

                input.addEventListener('keyup', function (e) {
                    inputClone.innerHTML = this.value;
                    var height = inputClone.getBoundingClientRect().height;
                    var maxHeight = maxInputHeight.getBoundingClientRect().height;
                    height = Math.min(height, maxHeight);
                    this.style.height = height + 'px';
                });
            }
        }


    });
}

    var selectCount = 1;
    $('.selects select').each(function () {
        var $this = $(this),
			numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled" id="select_checked_' + selectCount + '"></div>');
        selectCount++;
        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');


        var click = 1;
        $styledSelect.click(function (e) {
            e.stopPropagation();
			
            if (click) {
				$('ul.select-options').hide();
				$(this).addClass('active').next('ul.select-options').show();
				$(this).parents('.selects').children('p').css('color', '#006666');
				$(this).parents('.selects').children('i').css('color', '#006666');
               
                click = 0;
            } else {				
				$(this).removeClass('active').next('ul.select-options').hide();
				$(this).parents('.selects').children('p').css('color', '#666666');
				$(this).parents('.selects').children('i').css('color', '#cacaca');
                click = 1;
            }
        });


        $listItems.click(function (e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            //$(this).parent('.select').parent('.selects').children('p').css('color','#666666');
            click = 1;
            var parent_class = $this.parent().parent().attr('name');
            var tegs = $('#tag_field').val();
            if ($(".script").length == 0) {
                SortChangeLi($this.val(), parent_class);
            } 

            //console.log($this.val());
        });

        $(document).click(function () {
            $styledSelect.removeClass('active');
            $('.selects').children('p').css('color', '#666666');
            $('.selects').children('i').css('color', '#cacaca');
            $list.hide();
            click = 1;
        });


    });

    function clickOnModalImg(current) {
        $(current).parent().find('input').prop("checked", true);
        $(current).parent().find('input').trigger('click');
    }

$( "body" ).delegate( ".oppen-comments", "click", function() {
    $('.all-reviews-comment').slideToggle();
});

