function changeInput(current) {
    if ($(current).val() != '') {
        $(current).addClass('dirty');
    } else {
        $(current).removeClass('dirty')
    }
}

$('.question input').each(function() {
    $(this).bind( "change", function() {
        if ($(this).val() != '') {
            $(this).addClass('dirty');
        } else {
            $(this).removeClass('dirty')
        }
    });
});

$('.footer-menu .footer-more').click(function(e){
    e.preventDefault();
    $('.question').toggleClass('active');
});

$('.question p a').click(function(e){
    e.preventDefault();
    $('.question').toggleClass('active');
});

$('#ticket-form').submit(function(e) {
    e.preventDefault();
    var status = validQuestionForm('#ticket-form .has-text');
    if(status == 4){
        var formData = $('#ticket-form').serializeArray();
        var request = $.ajax({
            data : formData,
            url: '/site/ticket',
            type: 'post',
            dataType: 'html'

        });

        request.done(function( message ) {
            if(message == 'not correct'){
                $('#ticket-form input[name=email]').parent().addClass('has-error');
            }else{
                clearForm('#ticket-form .has-text');
                $('.question').toggleClass('active');

            }
        });
    }
});

var count_selected_style = 0;

$( "body" ).delegate( ".choose-content-style input[type=checkbox]", "click", function() {
    if($(this).prop( "checked")){
        if(count_selected_style < 5){
            count_selected_style++;
        }else  $(this).prop( "checked", false );
    }else{
        count_selected_style--;
    }
});

$( "body" ).delegate( ".choose-content-style a", "click", function(e) {
    e.preventDefault();
    $(this).parent().find('input').click();
});

$('#choose-style-done').click(function(e) {
    e.preventDefault();
    var items = getUserSelectStyle('.choose-content-style input[type=checkbox]');
    $.cookie('user_style', items, { expires: 3});
    $('.choose-style').modal('hide');
});

function getUserSelectStyle(item){
    items_id = "";
    $(item).each(function() {
        if($(this).prop( "checked")){
            items_id += $(this).attr('data-item_id')+',';
        }
    });

    console.log(items_id);

    return items_id;
}

function validQuestionForm(item){
    var status = 0;
    $(item).each(function() {
        if($(this).val() == ''){
            $(this).parent().addClass('has-error');
        }else{
            status++;
            $(this).parent().removeClass('has-error');
        }
    });

    return status;
}

function clearForm(item){
    $(item).each(function() {
        $(this).val('');
        $(this).removeClass('dirty');
    });
}

$('#link-sign-up').click(function(e){
    e.preventDefault();
    $('.login-model').modal('hide');
    $('.how_it_words').modal('show');
});

$('.form-inline button').click(function(e){
    e.preventDefault();
    var email = $('.form-inline input').val();
    if(email){
        var request = $.ajax({
            data : {email : email},
            url: '/site/mobile-form',
            type: 'post',
            dataType: 'html'

        });

        request.done(function( ) {
            $('body h4').css('visibility', 'visible');
            setTimeout(function(){location.reload();}, 2000);

        });

    }

});

$('.login-page').click(function(){
    document.cookie = "current-page="+window.location.href+"; path=/;";
});

$('.current-page-role a').click(function(){
    document.cookie = "current-page="+window.location.href+"; path=/;";
});

if ( ($(window).height() + 100) < $(document).height() ) {
    $('#top-link-block').removeClass('hidden').affix({
        offset: {top:100}
    });
}


function stickySideBar () {
    if($('.left-sidebar').length && $('.centralScroll').length&& $('.scrollSidebar .row').length) {
        var left = $('.scrollSidebar .row').position().left;
        var top  = $('.scrollSidebar .row').position().top;
        
        if($(this).scrollTop() >= 0 && $(this).scrollTop() < ($('.centralScroll').height() - $('.left-sidebar').height() ) ){
           $('.left-sidebar').removeClass('absolute').addClass('fixed').css({
            top: top + 10  + 'px',
            left: left - 15  + 'px'
        });
       } else  {
        $('.left-sidebar').removeClass('fixed').addClass('absolute').css({
            top: $('.centralScroll').height() - $('.left-sidebar').height(),
            left: '0'
        });
    } 
}
}

$(document).ready(stickySideBar);
$(window).scroll(stickySideBar);
$(window).resize(stickySideBar);

function stickySideBar1 () {
    if($('.left-sidebar').length && $('.centralScroll1').length&& $('.scrollSidebar1 .row').length) {
        var left = $('.scrollSidebar1 .row').position().left;
        var top  = $('.scrollSidebar1 .row').position().top;
        
        if($(this).scrollTop() >= 0 && $(this).scrollTop() < ($('.centralScroll1').height() - $('.left-sidebar').height() ) ){
           $('.left-sidebar').removeClass('absolute').addClass('fixed').css({
            top: top + 10  + 'px',
            left: left  + 'px'
        });
       } else  {
        $('.left-sidebar').removeClass('fixed').addClass('absolute').css({
            top: $('.centralScroll1').height() - $('.left-sidebar').height(),
            left: '15px'
        });
    } 
}
}

$(document).ready(stickySideBar1);
$(window).scroll(stickySideBar1);
$(window).resize(stickySideBar1);