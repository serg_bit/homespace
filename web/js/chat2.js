$(document).ready(function () {
    $(".chat-block").hide();
    var str = document.URL,
        doom = str.search('/about/user/');
    if (document.URL.search('/message') > 0) {
        return;
    }
    //------------------------------------------------------
    // проверка на URL (about/user - страница пользователя)
    //----------------------------------------------------
    if (doom > 0) {
        //------------------------------------------------
        // отправка текстового сообщения в чате по Enter
        //------------------------------------------------
        $("p.send textarea.message-input").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                if ($(this).val() !== "" && $(this).val().length > 0) {
                    var request = $.ajax({
                        data: {
                            message: $(this).val(),
                            recipient: $("div.pf-new").attr("data-user")
                        },
                        url: '/site/send',
                        type: 'post',
                        dataType: 'html',
                    });
                    request.done(function (action) {
                        action = JSON.parse(action);
                        $("div.correspondence-chat-list").append("<div class='player-1'>" +
                            "<div class='player1-response-data'>" +
                            "<p>" + action.request_message + "</p>" +
                            "<p class='data'>" + action.request_time + "</p>" +
                            "</div>" +
                            "<div class='player1-avatar'>" +
                            "<img id = '" + action.request_message_id + "' src='" + action.url_avatar + "'>" +
                            "</div>" +
                            "</div><hr class='sline'>");
                        $("p.send textarea.message-input").val("");
                        showLastMessage();
                    });
                } else {
                    event.preventDefault()
                }
            }
        })
        //---------------------------
        // отправка файла по change
        //--------------------------
        var files;
        $('input[type=file]#file-chat').change(function () {
            files = this.files;
            var i = 0,
                data = new FormData(),
                string = "",
                ext;
            $.each(files, function (key, value) {
                ext = value.name.split(".");
                //console.log(value.size);
                if (ext[ext.length - 1] != 'doc' && ext[ext.length - 1] != 'txt' && ext[ext.length - 1] != 'rtf' && ext[ext.length - 1] != 'jpg' && ext[ext.length - 1] != 'pdf' && ext[ext.length - 1] != 'png') {
                    alert("Формат файла(" + ext[ext.length - 1] + ") не поддерживается!");
                    return;
                }
                if (value.size > 3000000) {
                    alert("Превышен максимальный размер файла - 3МБ!");
                    return;
                }
                data.append(key, value);
                data.append('recipient', $("div.pf-new").attr("data-user"));

            });
            var uploadFiles = $.ajax({
                data: data,
                url: '/site/upload',
                type: 'post',
                dataType: 'json',
                contentType: false,
                processData: false,
            })
            uploadFiles.done(function (action) {
                for (i = 0; i < action.file_name.length; i++) {
                    string = string + "<li><a href='/media/upload/" + action.file_name[i] + "' download='" + action.file_name[i] + "'>" + action.file_name[i].substr(4, action.file_name[i].length - 1) + "</a></li>";
                }
                $("div.correspondence-chat-list").append("<div class='player-1'>" +
                    "<div class='player1-response-data'>" +
                    "<ul>" + string + "</ul>" +
                    "<p class='data'>" + action.request_date + "</p>" +
                    "</div>" +
                    "<div class='player1-avatar'>" +
                    "<img id = '" + action.request_message_id + "' src='" + action.url_avatar + "'>" +
                    "</div>" +
                    "</div><hr class='sline'>");
                showLastMessage();
            })
        });
        //------------------------------------------------------------------
        //по загрузке страницы - получение последнего сообщения из переписки
        //------------------------------------------------------------------
        window.onload = function (e) {
            var getLastMessage = $.ajax({
                data: {recipient: $("div.pf-new").attr("data-user")},
                url: '/site/getlastmessage',
                type: 'post',
                dataType: 'html',
            });
            getLastMessage.done(function (message) {
                var n;
                var string = "";
                message = JSON.parse(message || 'null');
                if (message.type == 'send') {
                    if (message.file_name != null) {
                        string = string + "<ul>";
                        for (n = 0; n < message.file_name.length; n++) {
                            string = string + "<li><a href='/media/upload/" + message.file_name[n] + "' download='" + message.file_name[n] + "'>" + message.file_name[n].substr(4, message.file_name[n].length - 1) + "</a></li>";
                        }
                        string = string + "</ul>";
                    }
                    if (message.response_message != null) {
                        string = string + "<p>" + message.response_message + "</p>";
                    }
                    $("div.correspondence-chat-list").append("<div class='player-1'>" +
                        "<div class='player1-response-data'>" + string +
                        "<p class='data'>" + message.response_date + "</p>" +
                        "</div>" +
                        "<div class='player1-avatar'>" +
                        "<img id = '" + message.response_message_id + "' src='" + message.url_avatar + "'>" +
                        "</div>" +
                        "</div><hr class='sline'>");
                } else if (message.type == 'get') {
                    if (message.file_name != null) {
                        string = string + "<ul>";
                        for (n = 0; i < message.file_name.length; n++) {
                            string = string + "<li><a href='/media/upload/" + message.file_name[n] + "' download='" + message.file_name[n] + "'>" + message.file_name[n].substr(3, message.file_name[n].length - 1) + "</a></li>";
                        }
                        string = string + "</ul>";
                    }
                    if (message.response_message != null) {
                        string = string + "<p>" + message.response_message + "</p>";
                    }
                    $("div.correspondence-chat-list").append("<div class='player-2' id='" + message.sender_id + "'>" +
                        "<div class='player2-avatar'>" +
                        "<img id = '" + message.response_message_id + "' src='" + message.url_avatar + "' alt=''>" +
                        "</div>" +
                        "<div class='player2-response-data'>" + string +
                        "</div>" +
                        "<p class='data'>" + message.response_date + "</p>" +
                        "</div><hr class='sline'>");
                } else {
                    return;
                }
            });
            var interval = setInterval(letsChat, 2000);
        };
        //--------------------------------------------
        // Отправка сообщений на странице пользователя
        //--------------------------------------------
        $(".send-chat-message").click(function (e) {
            $(".chat-block").slideToggle("fast");
        })
        //-------------------------------
        //в любой другой части  сайта ()
        //-------------------------------
    } else {
        window.onload = function (e) {
            var interval = setInterval(letsChat, 2000);
        }
        //----------------------------------------------
        // отправка текстового сообщения в чате по Enter
        //----------------------------------------------
        $("p.send textarea.message-input").keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                if ($(this).val() !== "" && $(this).val().length > 0) {
                    var request = $.ajax({
                        data: {
                            message: $(this).val(),
                            recipient: $("div.player-2").attr('id')
                        },
                        url: '/site/send',
                        type: 'post',
                        dataType: 'html',
                    });
                    request.done(function (action) {
                        action = JSON.parse(action);
                        $("div.correspondence-chat-list").append("<div class='player-1'>" +
                            "<div class='player1-response-data'>" +
                            "<p>" + action.request_message + "</p>" +
                            "<p class='data'>" + action.request_time + "</p>" +
                            "</div>" +
                            "<div class='player1-avatar'>" +
                            "<img id = '" + action.request_message_id + "' src='" + action.url_avatar + "'>" +
                            "</div>" +
                            "</div><hr class='sline'>");
                        $("p.send textarea.message-input").val("");
                        showLastMessage();
                    });
                } else {
                    event.preventDefault();
                }
            }
        })
        var files;
        //--------------------------
        // отправка файла по change
        //--------------------------
        $('input[type=file]#file-chat').change(function () {
            files = this.files;
            var i = 0,
                data = new FormData(),
                string = "",
                ext;
            $.each(files, function (key, value) {
                ext = value.name.split(".");
                if (ext[ext.length - 1] != 'doc' && ext[ext.length - 1] != 'txt' && ext[ext.length - 1] != 'rtf' && ext[ext.length - 1] != 'jpg' && ext[ext.length - 1] != 'pdf' && ext[ext.length - 1] != 'png') {
                    alert("Формат файла(" + ext[ext.length - 1] + ") не поддерживается!");
                    return;
                }
                if (value.size > 3000000) {
                    alert("Превышен максимальный размер файла - 3МБ!");
                    return;
                }
                data.append(key, value);
                data.append('recipient', $("div.player-2").attr('id'));

            });
            var uploadFiles = $.ajax({
                data: data,
                url: '/site/upload',
                type: 'post',
                dataType: 'json',
                contentType: false,
                processData: false,
            })
            uploadFiles.done(function (action) {
                for (i = 0; i < action.file_name.length; i++) {
                    string = string + "<li><a href='/media/upload/" + action.file_name[i] + "' download='" + action.file_name[i] + "'>" + action.file_name[i].substr(4, action.file_name[i].length - 1) + "</a></li>";
                }
                $("div.correspondence-chat-list").append("<div class='player-1'>" +
                    "<div class='player1-response-data'>" +
                    "<ul>" + string + "</ul>" +
                    "<p class='data'>" + action.request_date + "</p>" +
                    "</div>" +
                    "<div class='player1-avatar'>" +
                    "<img src='" + action.url_avatar + "'>" +
                    "</div>" +
                    "</div><hr class='sline'>");
                showLastMessage();
            })
        });
    }
    $("a.close").click(function () {
        $(".chat-block").hide();
    });
    function showLastMessage() {
        var div = $("#container");
        div.scrollTop(div.prop('scrollHeight'));
    }
    //---------------------------------------------------------------
    //проверка на полученное сообщение. POST-запрос, интервал 2 сек.
    //---------------------------------------------------------------
    function letsChat() {
        var count;
        var response = $.ajax({
            data: {'request': 'get'},
            url: '/site/chat',
            type: 'post',
            dataType: 'html',
        });
        response.done(function (message) {
            message = JSON.parse(message || 'null');
            if (message == null) return;
            for (count = 0; count < message.length; count++) {
                var img = $("div.correspondence-chat-list").find("img");
                if ($("div.correspondence-chat-list").has("div.player-2")) {
                    var sender_id = $("div.player-2").attr('id');
                } else {
                    var sender_id = "";
                }
                var n = 0;
                var string = "";
                if (message[count].status == 0) {
                    if ((document.URL.search('/about/user/') < 0 && img.length == 0 && message[count] != 'null') || (sender_id == message[count].sender_id) || (document.URL.search('/about/user/') > 0)) {
                        if (message[count].file_name != null) {
                            string = string + "<ul>";
                            for (n = 0; n < message[count].file_name.length; n++) {
                                string = string + "<li><a href='/media/upload/" + message[count].file_name[n] + "' download='" + message[count].file_name[n] + "'>" + message[count].file_name[n].substr(4, message[count].file_name[n].length - 1) + "</a></li>";
                            }
                            string = string + "</ul>";
                        }
                        if (message[count].response_message != null) {
                            string = string + "<p>" + message[count].response_message + "</p>";
                        }
                        $("div.correspondence-chat-list").append("<div class='player-2' id='" + message[count].sender_id + "'>" +
                            "<div class='player2-avatar'>" +
                            "<img id = 'uniq" + message[count].response_message_id + "' src='" + message[count].url_avatar + "' alt=''>" +
                            "</div>" +
                            "<div class='player2-response-data'>" + string +
                            "</div>" +
                            "<p class='data'>" + message[count].response_date + "</p>" +
                            "</div><hr class='sline'>");
                        showLastMessage();
                        $(".chat-block").show();
                        if ($(".chat-block").has("div.player-2 img#" + message[count].response_message_id + "")) {
                            var messageUpdate = $.ajax({
                                data: {'getmessage': message[count].response_message_id},
                                url: '/site/messagestatusupdate',
                                type: 'post',
                                dataType: 'html',
                            });
                        }
                    }
                    return;
                }
            }
        });
    }
})








