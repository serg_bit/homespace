$(".chat-block").hide();

window.onload = function(e) {

    var getMess = $.ajax({
                data : {recipient : $("div.name").attr("id")},
                url: '/site/getlastmessage',
                type: 'post',
                dataType: 'html',
            });

            getMess.done(function(message){

                message = JSON.parse(message || 'null');

                if(message.flag = 1){

                    if(action.file_name != null){

                        //console.log(action.file_name.length);

                        for(n=0; n<action.file_name.length; n++){

                            string = string + "<a href='/media/upload/" + action.file_name[n] + "' download='" + action.file_name[n] + "'><p>" + action.file_name[n].substr(3, action.file_name[n].length-1) + "</p></a>";

                        }



                        $("div.message-content").append("<div class='player-2' id = '" + action.sender_id + "'>" + "<img width='24px' id = '" + action.response_message_id + "' height='48px' src='" + action.sender_avatar + "'>" + string + "</div>");

                    }else{

                        $("div.message-content").append("<div class='player-2' id = '" + action.sender_id + "'>" + "<img width='24px' id = '" + action.response_message_id + "' height='48px' src='" + action.sender_avatar + "'>" + "<p>" + action.response_message + "</p>" + "</div>");

                    }

                }else{
                    $("div.message-content").append("<div class='player-1'>"+"<p>"+message.response_message+"</p>"+"<i><img id='"+message.response_message_id+"' width='24px' height='48px' src='"+message.sender_avatar+"'></i>"+"</div>");
                }
                //$("div.message-content").append("<div class='player-2' id = '" + action.response_message_id + "'>" + "<img width='24px' height='48px' src='" + $(".user-avatar").attr("src") + "'>" + "<p>" + action.response_message + "</p>" + "</div>");
            });



    function getMessage(){
        var response = $.ajax({
            data : {recipient : $("div.name").attr("id")},
            url: '/site/getmessage',
            type: 'post',
            dataType: 'html',
            //success:
        });


    response.done(function( action ) {

        action = JSON.parse(action || 'null');

        if (action != 'null' ) {

                var img = $("div.message-content").find("img");

                if ( action.response_message_id > img[img.length - 1].id ) {

                    $("div.message-content").append("<div class='player-2' id = '" + action.response_message_id + "'>" + "<img width='24px' id = '" + action.response_message_id + "' height='48px' src='" + $(".user-avatar").attr("src") + "'>" + "<p>" + action.response_message + "</p>" + "</div>");

                    $(".chat-block").show();

                }else{ return;}

            }else{

                return;

            }


        });


    }
    var interval = setInterval(getMessage,2000);

};

$(".send-chat-message").click(function(e){
    $(".chat-block").slideToggle("fast");

    //function getLastMessage(){
    //    var getMess = $.ajax({
    //        data : {recipient : $("div.name").attr("id")},
    //        url: '/site/getlastmessage',
    //        type: 'post',
    //        dataType: 'html',
    //    });
    //
    //    getMess.done(function(action){
    //        console.log(action);
    //    });
    //}



    //$("a.close").click(function(){
    //    clearInterval(interval);
    //
    //});

})

$( "a.close" ).click(function() {
    $(".chat-block").hide();
});

$("textarea.message-input").keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13') {

            var request = $.ajax({
                data : { message : $(this).val(),
                        recipient : $("div.name").attr("id")},
                url: '/site/send',
                type: 'post',
                dataType: 'html',
            });
            request.done(function( action ) {

                action = JSON.parse(action);
                $("div.message-content").append("<div class='player-1'>"+"<p>"+action.request_message+"</p>"+"<i><img id = '" + action.request_message_id + "' width='24px' height='48px' src='"+action.sender_avatar+"'></i>"+"</div>");
                $("textarea.message-input").val("");

        });
    }
})


var files;
$('input[type=file]#file-chat').change(function(){

    files = this.files;
    var i = 0,
    data = new FormData(),
    string = "",
    ext;

    $.each( files, function( key, value ){
        ext = value.name.split(".");
        //console.log(value.size);

        if (ext[ext.length-1]!='doc' && ext[ext.length-1]!='txt' && ext[ext.length-1]!='rtf' && ext[ext.length-1]!='jpg' && ext[ext.length-1]!='pdf' && ext[ext.length-1]!='png') {
            alert("Формат файла("+ext[ext.length-1]+") не поддерживается!");
            return;
        }

        if (value.size>2000000){
            alert("Превышен максимальный размер файла - 2МБ!");
            return;
        }

        data.append( key, value );
        data.append( 'recipient', $("div.name").attr("id") );

    });

    var uploadFiles = $.ajax({
        data :data  ,
        url: '/site/upload',
        type: 'post',
        dataType: 'json',
        contentType: false,
        processData: false,
      })

    uploadFiles.done(function(action){
        for (i=0; i<action.file_name.length; i++) {
            string = string + "<a href='/media/upload/" + action.file_name[i] + "' download='" + action.file_name[i] + "'><p>" + action.file_name[i] + "</p></a>";
        }
        $("div.message-content").append("<div class='player-1'><div class='file-list'>" + string + "</div><i><img width='24px' height='48px' src='" + action.sender_avatar + "'></i>" + "</div>");
        $("textarea.message-input").val("");
    })
});

