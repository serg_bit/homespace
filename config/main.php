<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
Yii::setAlias('@img', '/frontend/web/images');
Yii::setAlias('@basepath', 'http://hspace.alscon-clients.com/');
return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
//                'google' => [
//                    'class' => 'yii\authclient\clients\GoogleOpenId'
//                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    /** TODO Change clientId and clientSecret to actual */
                    'clientId' => '1746633468898413',
                    'clientSecret' => 'adfdc03811b0406bff7183a0b09ecafa',
                ],
                'twitter' => [
                    'class' => 'yii\authclient\clients\Twitter',
                    /** TODO Change consumerKey and consumerSecret to actual */
                    'consumerKey' => 'Se6BQMLqEQdCwhx8HYhIAtkHL',
                    'consumerSecret' => 'O5HcXaZOws0CzHkC9qydoZqkf6qFdddDZgN64ynyawiLNXrr4b',
                ],
                /*'behance' => [
                    'class' => 'frontend\models\Behance',
                    /** TODO Change consumerKey and consumerSecret to actual */
                  /*  'clientId' => 'q7MPeWXQxhsyB0KGhI9foUpbNslWZLym',
                ],*/    
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class'=>'frontend\components\LangUrlManager',
            'rules' => [
                '/' => 'site/index',
                '<controller:\w+>/<action:\w+>/'=>'<controller>/<action>',
            ],
        ],
        'assetManager' => [
             'basePath' => '@webroot/assets',
             'baseUrl' => '@web/assets'
        ],
        'request' => [
            'baseUrl' => '',
            'class' => 'frontend\components\LangRequest'
        ],
        'language'=>'ru-RU',
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/language',
                    'sourceLanguage' => 'ru',
                    'fileMap' => [
                        'main' => 'main.php',
                        'titles' => 'titles.php',
                    ],
                ],
            ],
        ]
    ],
    'params' => $params,
    'modules' => [
        'account' => [
            'class' => 'frontend\account\Account',
            'layout' => 'account',
        ],
       
    ],
];
