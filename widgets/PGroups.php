<?php
namespace frontend\widgets;

use yii;
use frontend\account\models\Groups;
use frontend\models\Likes;

class PGroups extends \yii\bootstrap\Widget
{

    private $user_id;

    public function init(){
        $this->user_id = Yii::$app->user->identity->id;
    }

    public function run() {
        return $this->render('groups/view', [
            'groups' => $this->getGroups($this->user_id, 1),
        ]);
    }

    private function getGroups($user_id, $object)
    {
        $temp_groups = Groups::find()->where(['paid_status' => 1])->all();
        $data = array();
        foreach ($temp_groups as $value) {
            $my_like = Likes::find()->where(['user_id' => $user_id, 'object_id' => $value['id'], 'category' => $object])->one();
            if ($my_like == null) {
                $my_like = '';
            } else $my_like = 1;

            $data[] = [
                'id' => $value['id'],
                'image' => '/images/' . $value['images'],
                'title' => $value['title'],
                'views' => $value['views'],
                'followers' => $value['followers_count'],
                'likes' => Likes::find()->where(['object_id' => $value['id'], 'category' => $object])->count(),
                'user_like' => $my_like,
                'url' => Yii::$app->urlManager->createUrl(['account/join-group/group?' . $value['id']]),
            ];

        }
        return $data;
    }

}