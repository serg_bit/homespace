<?php
namespace frontend\widgets;

use yii;
use frontend\account\models\AppRecipients;
use frontend\account\models\Application;
use frontend\models\Publicity;

class SideBar extends \yii\bootstrap\Widget
{
    private $user_role = '';

    public function init()
    {
        $this->user_role = Yii::$app->user->identity->roles;
    }

    public function run()
    {
        return $this->render('side_bar/view', [
            'side_bar' => $this->getSideBar($this->user_role),
            'video' => Publicity::find()->select('video')->where(['role' => Yii::$app->user->identity->roles])->andWhere(['<>', 'video', ''])->asArray()->one(),
        ]);
    }

    private function getSideBar($role)
    {
        $side_bar = [];
        $label = Yii::t('account', 'side_menu');
        if ($role == 'designer') {
            $no_read = AppRecipients::find()->where(['to' => Yii::$app->user->identity->id, 'status' => 0])->count();
            $sent = Application::find()->where(['user_id' => Yii::$app->user->identity->id])->count();
            $side_bar = [
                ['label' => $label['portfolio'], 'url' => Yii::$app->urlManager->createUrl(['account/portfolio']), 'ico' => 'icon-1', 'sub_menu' => [
                    ['label' => $label['all_portfolio'], 'url' => Yii::$app->urlManager->createUrl(['account/portfolio'])],
                    ['label' => $label['share'], 'url' => Yii::$app->urlManager->createUrl(['account/portfolio/share'])],
                ]],
                ['label' => 'Like it', 'url' => Yii::$app->urlManager->createUrl(['account/like-it']), 'ico' => 'icon-catalog', 'sub_menu' => false],
                ['label' => $label['order'], 'url' => Yii::$app->urlManager->createUrl(['account/order']), 'ico' => 'icon-6', 'sub_menu' => false],
                ['label' => $label['app'], 'url' => Yii::$app->urlManager->createUrl(['account/application']), 'ico' => 'icon-4', 'sub_menu' => [
                    ['label' => sprintf($label['app_in'], $no_read), 'url' => Yii::$app->urlManager->createUrl(['account/application'])],
                    ['label' => sprintf($label['app_sent'], $sent), 'url' => Yii::$app->urlManager->createUrl(['account/application/appsend'])],
                ]],
                ['label' => $label['messages'], 'url' => Yii::$app->urlManager->createUrl(['account/messages']), 'ico' => 'icon-black218', 'sub_menu' => false],
                ['label' => $label['group'], 'url' => Yii::$app->urlManager->createUrl(['account/join-group']), 'ico' => 'icon-3', 'sub_menu' => false],
                ['label' => $label['catalog'], 'url' => Yii::$app->urlManager->createUrl(['account/catalog']), 'ico' => 'icon-catalog', 'sub_menu' => false],
                ['label' => $label['offers'], 'url' => Yii::$app->urlManager->createUrl(['account/offers']), 'ico' => 'icon-7', 'sub_menu' => false],
                ['label' => $label['edit'], 'url' => Yii::$app->urlManager->createUrl(['account/edit']), 'ico' => 'icon-5', 'sub_menu' => false],
            ];
        } elseif ($role == 'homeowner') {
            $side_bar = [
                ['label' => 'Like it', 'url' => Yii::$app->urlManager->createUrl(['account/like-it']), 'ico' => 'icon-1', 'sub_menu' => false],
                ['label' => 'Application', 'url' => '#', 'ico' => 'icon-catalog', 'sub_menu' => false],
                ['label' => $label['messages'], 'url' => Yii::$app->urlManager->createUrl(['account/messages']), 'ico' => 'icon-black218', 'sub_menu' => false],
                ['label' => $label['group'], 'url' => Yii::$app->urlManager->createUrl(['account/join-group']), 'ico' => 'icon-3', 'sub_menu' => false],
                ['label' => $label['offers'], 'url' => Yii::$app->urlManager->createUrl(['account/offers']), 'ico' => 'icon-7', 'sub_menu' => false],
                ['label' => 'My contractors', 'url' => '#', 'ico' => 'icon-3', 'sub_menu' => false],
                ['label' => 'Press', 'url' => '#', 'ico' => 'icon-catalog', 'sub_menu' => false],
                ['label' => $label['edit'], 'url' => '#', 'ico' => 'icon-5', 'sub_menu' => false]
            ];
        } elseif ($role == 'agent') {
            $label = Yii::t('account', 'side_menu_agent');
            $label2 = Yii::t('account', 'side_menu');
            $no_read = AppRecipients::find()->where(['to' => Yii::$app->user->identity->id, 'status' => 0])->count();
            $sent = Application::find()->where(['user_id' => Yii::$app->user->identity->id])->count();
            $side_bar = [
                ['label' => $label['profile'], 'url' => Yii::$app->urlManager->createUrl(['account/profile-agent']), 'ico' => 'icon-user168', 'sub_menu' => [
                    ['label' => $label['profile'], 'url' => Yii::$app->urlManager->createUrl(['account/profile-agent'])],
                    ['label' => $label['edit'], 'url' => Yii::$app->urlManager->createUrl(['account/profile-agent/edit'])],
                ]],
                ['label' => $label['project'], 'url' => Yii::$app->urlManager->createUrl(['account/projects-agent']), 'ico' => 'icon-project', 'sub_menu' => [
                    ['label' => $label['project'], 'url' => Yii::$app->urlManager->createUrl(['account/projects-agent'])],
                    ['label' => 'Select portfolio', 'url' => Yii::$app->urlManager->createUrl(['account/projects-agent/select'])],
                ]],
                ['label' => $label['advertising_tools'], 'url' => Yii::$app->urlManager->createUrl(['account/advertising-tools']), 'ico' => 'icon-advertising', 'sub_menu' => [
                    ['label' => "Limited accounts", 'url' => Yii::$app->urlManager->createUrl(['account/advertising-tools'])],
                    ['label' => "Promotional teasers", 'url' => Yii::$app->urlManager->createUrl(['account/promotional-teaser'])],
                    ['label' => $label2['offers'], 'url' => Yii::$app->urlManager->createUrl(['account/offers'])],
                ]],
                ['label' => 'Like it', 'url' => Yii::$app->urlManager->createUrl(['account/like-it']), 'ico' => 'icon-1', 'sub_menu' => false],
                ['label' => $label['my_designers_and_architectors'], 'url' => Yii::$app->urlManager->createUrl(['account/designers-and-architects']), 'ico' => 'icon-designer', 'sub_menu' => false],
                ['label' => $label['collect_order'], 'url' => Yii::$app->urlManager->createUrl(['account/order']), 'ico' => 'icon-6', 'sub_menu' => false],
                ['label' => $label2['app'], 'url' => Yii::$app->urlManager->createUrl(['account/application']), 'ico' => 'icon-4', 'sub_menu' => [
                    ['label' => sprintf($label2['app_in'], $no_read), 'url' => Yii::$app->urlManager->createUrl(['account/application'])],
                    ['label' => sprintf($label2['app_sent'], $sent), 'url' => Yii::$app->urlManager->createUrl(['account/application/appsend'])],
                ]],
                ['label' => $label['accounting'], 'url' => '#', 'ico' => 'icon-anccountion', 'sub_menu' => false],
            ];
        }
        return $side_bar;
    }
}
