<?php
namespace frontend\widgets;

use yii;
use frontend\models\Publicity;

class Slider extends \yii\bootstrap\Widget
{

    public function init(){}

    public function run() {
        $limit = Yii::$app->congfigs->get('slider_count');
        $banners = Publicity::find()->where(['status' => 1, 'position' => 2])->orderBy('RAND()')->limit($limit)->all();
        return $this->render('slider/view',[
            'banners' => $banners
        ]);
    }

}