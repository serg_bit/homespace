<?php //var_dump($products_collections); ?>
<!--START MODAL-->
<div class="modal fade agent-modal-product" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <h5 class="title">Choose product</h5>
            <p class="mini-title">Search</p>
            <form class="forma-input">
                <div class="input-container">
                    <input class="text-input floating-label" name="sample" type="text">
                    <label for="sample">Company name</label>
                </div>
                <div class="input-container">
                    <input class="text-input floating-label" name="sample" type="text">
                    <label for="sample">Product Name</label>
                </div>
                <div class="selects">
                    <select class="sort">
                        <?php foreach ($products_collections as $collection) : ?>
                            <option value="<?= $collection['id'] ?>"><?= $lang === 'ru' ? $collection['ru'] : $collection['en'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <input type="submit" class="button" value="Search">
            </form>
            <div class="add-company">
                <div class="no-object">
                    <p>No result</p>
                    <a href="#" class="button">Add company</a>
                </div>
                <div class="products" style="display:none;">
                    <div class="product ">
                        <img src="/images/produkt.png" alt="#">
                        <p>Chair “Motherhood”</p>
                    </div>
                    <div class="product ">
                        <img src="/images/produkt.png" alt="#">
                        <p>Chair “Motherhood”</p>
                    </div>
                </div>
                <div class="clearfix"></div>
                <p class="upload-portfolio download-portfolio join">Create product card<a class="create-product-card"
                                                                                          href="#"></a></p>
            </div>
            <div class="open-create-product">


                <div class="clearfix"></div>
                <div class="google-search">
                    <input type="text" placeholder="search">
                </div>
                <div class="object-add">
                    <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileAgentChange(this);" />
                    <div class="dropzone" id="dropzone1"><?= Yii::t('account', 'drop_zone') ?></div>
                    <div class="dropzone" id="dropzone2"><?= Yii::t('account', 'drop_zone') ?></div>
                    <div class="dropzone" id="dropzone3"><?= Yii::t('account', 'drop_zone') ?></div>
<!--                    <ul>-->
<!--                        <li><a href="#">lorem</a></li>-->
<!--                        <li><a href="#">ipsum</a></li>-->
<!--                        <li><a href="#">dolor</a></li>-->
<!--                        <li><a href="#">sit</a></li>-->
<!--                    </ul>-->
                </div>
                <div class="input-container">
                    <input class="text-input floating-label" id="add-tags" type="text" size="50" placeholder="#tags...">
                </div>
                <form class="forma-input">
                    <div class="input-container">
                        <input class="text-input floating-label" name="company-name" type="text">
                        <label for="sample">Company name</label>
                    </div>
                    <div class="input-container">
                        <input class="text-input floating-label" name="product-name" type="text">
                        <label for="sample">Product Name</label>
                    </div>
                    <div class="selects">
                        <select class="sort" name="product-collection">
                            <?php foreach ($products_collections as $collection) : ?>
                                <option value="<?= $collection['id'] ?>"><?= $lang === 'ru' ? $collection['ru'] : $collection['en'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="input-container">
                        <input class="text-input floating-label" name="public-price" type="text">
                        <label for="sample">Public price</label>
                    </div>
                </form>
                <form action="#">
                    <textarea name="description" id="#"></textarea>
                    <input type="button" id="save-product" class="button" value="Save"/>
                </form>
                <div class="error-message" style="display: none">
                    <span class="text-danger"></span>
                </div>
                <div class="success-message" style="display: none">
                    <span class="text-success"><?= Yii::t('account', 'product_was_created_successfully') ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!--END MODAL-->
<?php

