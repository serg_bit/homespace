<!-- Groups -->
<?php if($groups){ ?>
    <div class="row">
        <h4 class="promote promote-bottom"><?= Yii::t('account', 'group_to_promote') ?></h4>
        <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'join_group') ?><a href="<?= \Yii::$app->urlManager->createUrl(['account/join-group']) ?>"></a></p>
        <div class="clearfix"></div>
        <div id="owl-demo-3" class="slider-index">
            <?php foreach ($groups as $group) {?>
                <div class="item">
                    <div class="portfolio-item white">
                        <a href="<?= $group['url'] ?>"><img src="<?= $group['image'] ?>" alt="<?= $group['title'] ?>"></a>
                        <div class="like-ico">
                            <i class="icon-eye110"><?= $group['views'] ?></i>
                            <i class="icon-user168"><?= $group['followers'] ?></i>
                            <i class="icon-heart297<?= $group['user_like'] ?>"><?= $group['likes'] ?></i>
                        </div>
                        <a class="like"><i class="icon-heart297<?= $group['user_like'] ?>"></i></a>
                        <p><?= $group['title'] ?></p>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>
<!-- End Groups -->

<?php
$script_groups = <<< JS
   $("#owl-demo-3").owlCarousel({
        items : 2,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3],
        navigationText:false,
        navigation:true,
        pagination:false
    });
JS;
$this->registerJs($script_groups, yii\web\View::POS_READY);
?>

