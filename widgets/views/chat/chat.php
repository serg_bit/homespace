<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<!--START CHAT-->
	<div class="chat-block" style="display:none">
		<div class="chat">
			<div class="name" id="<?= $from ?>">Message</div>

			<a href="#" class="roll-up"><i class="icon-11"></i></a>
			<a href="#" class="close"><i class="icon-cancel30"></i></a>
			<div class="message-content"  id="container">
				<div class="correspondence-chat-list">
					
				</div>
				<p class="send"><textarea class="message-input" placeholder="Type message..." ></textarea></p>
			</div>

			<input type="file" id="file-chat" name="file" accept=".txt, .pdf, .rtf, .doc, .jpg, .png" multiple="" /><div class="add"></div>
		</div>
	</div>
<!--END CHAT-->

<?php
//$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/new-js.js', ['depends'=>'frontend\assets\AppAsset']);
////$this->registerJsFile('scripts/common.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
//$this->registerJsFile('js/action.js', ['depends'=>'frontend\assets\AppAsset']);
?>