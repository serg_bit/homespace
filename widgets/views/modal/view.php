<?php
use yii\helpers\Url;
?>

<?php if($name == "best3works"){ ?>

    <div class="modal fade" id="best_3_works" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Best 3 Works</h4>
                </div>
                <div class="modal-body" id="modal_container1">

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="apply_this_item">Apply</button>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php if($name == "style"){ ?>

    <div class="modal fade" id="styleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Название модали</h4>
                </div>
                <div class="modal-body" id="container">

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" id="apply-style">OK</button>
                </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php if($name == "add-order-model"){?>
    <div class="modal fade add-order-model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="add">
                    <h4></i><?= Yii::t('account', 'order') ?></h4>
                    <a href="#" class="addToOrder" id_prod = ""><i class="icon-books72"></i></i><?= Yii::t('account', 'order') ?></a>
                </div>
                <?php $folder_list = \frontend\account\models\OrderFolder::find()->select(['id_folder','title'])->where(['id_user'=>\Yii::$app->user->identity->id])->asArray()->all();
                if(\frontend\models\Language::getCurrent()->url==='en'){
                    array_unshift($folder_list,['id_folder'=>'+ Add folder','title'=>'+ Add folder']);
                } else {array_unshift($folder_list,['id_folder'=>'+ Add folder','title'=>'+ Добавить папку']);} ?>

                <p><?= Yii::t('account', 'order_add_folder') ?></p>
                <span><?php if(\frontend\models\Language::getCurrent()->url==='en'){echo 'or';} else echo 'или'; ?></span>
                <div class="selects prod catalog-select">
                    <?= \yii\helpers\Html::dropDownList('id_folder', null, \yii\helpers\ArrayHelper::map($folder_list,'id_folder','title'),['prompt' => '']) ?>
                </div>
                <div class="forma-input" id="addFolder" style="display: none">
                    <div class="input-container">
                        <input id="folder_into" class="text-input floating-label" type="text" name="sample" value=""/>
                        <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                    </div>
                </div>
                <div class="success_add" message_add ="<?php if(\frontend\models\Language::getCurrent()->url==='en'){echo 'Added to collect order!';} else echo 'Добавлено в собрать заказ!'; ?>" text="<?php if(\frontend\models\Language::getCurrent()->url==='en'){echo 'Added to the selected folder!';} else echo 'Добавлено в выбранную папку!'; ?>"></div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($name == "move-order-model"){?>
    <div class="modal fade move-order-model" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="move">
                    <h4></i><?= Yii::t('account', 'order_move') ?></h4>
                    <a href="#" class="remove-order" id_order = ""><i class="icon-rubbish"></i></i><?= Yii::t('account', 'button_delete') ?></a>
                    <span><?php if(\frontend\models\Language::getCurrent()->url==='en'){echo 'or';} else echo 'или'; ?></span>
                </div>
                <?php $folder_list = \frontend\account\models\OrderFolder::find()->select(['id_folder','title'])->where(['id_user'=>\Yii::$app->user->identity->id])->asArray()->all();
//                if(\frontend\models\Language::getCurrent()->url==='en'){
//                    array_unshift($folder_list,['id_folder'=>'+ Add folder','title'=>'+ Add folder']);
//                } else {array_unshift($folder_list,['id_folder'=>'+ Add folder','title'=>'+ Добавить папку']);} ?>



                <div class="selects prod move-to">
                    <p><?= Yii::t('account', 'order_move_to') ?></p>
                    <?= \yii\helpers\Html::dropDownList('id_folder', null, \yii\helpers\ArrayHelper::map($folder_list,'id_folder','title'),['prompt' => '']) ?>
                </div>
                <div class="forma-input" id="addFolder" style="display: none">
                    <div class="input-container">
                        <input id="folder_into" class="text-input floating-label" type="text" name="sample" value=""/>
                        <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                    </div>
                </div>
                <div class="success_move" message_del ="<?php if(\frontend\models\Language::getCurrent()->url==='en'){echo 'Deleted successfully!';} else echo 'Успешно удалено!'; ?>" message_move="<?php if(\frontend\models\Language::getCurrent()->url==='en'){echo 'Moved to the selected folder!';} else echo 'Перемещено в выбранную папку!'; ?>"></div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if($name == "tariff-plan"){?>
<!--START MODAL-->
<div class="modal fade how-it-word" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <?= Yii::t('modal', 'account_first_modal_text') ?>
        </div>
    </div>
</div>
<!--END MODAL-->
    <!--START MODAL-->

    <div class="modal fade pay" tabindex="-1" role="dialog" id="modal-pay" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="pay-content special-offers-catalog page-in-catalog">
                    <?= Yii::t('modal', 'pay_alternative_text1') ?>
                    <div class="pay-block">
                        <?= Yii::t('modal', 'pay_alternative_text2') ?>
                        <div class="payment">
                            <a href="#"><img src="/images/western.png" alt=""></a>
                            <a href="#"><img src="/images/paypay.png" alt=""></a>
                            <a href="#"><img src="/images/visa.png" alt=""></a>
                        </div>
                    </div>
                    <?= Yii::t('modal', 'pay_alternative_text3') ?>
                    <p>80% of clients are choosing a contractor after examining his comments, and other activity on social networks. Your comments should display your vision and philosophy that guide you in your work.</p>
                    <div class="catalog-box">
                        <?php if($products):?>
                            <?php foreach($products as $product): ?>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>">
                                            <img src="<?= Yii::getAlias('@products/'.$product['main_image']) ?>" alt="">
                                        </a>
                                        <div class="limited-offer">
                                            <p><b>Limited offer</b> <br>save 30%</p>
                                        </div>
                                        <div class="like-calc"><i class="icon-heart2971"></i><span><?= $product['likes'] ?></span></div>
                                        <div class="like-calc item-price">
                                            <i class="icon-label49"></i>
                                            <span><?= $product['price'] ?>$</span>
                                            <p class="last-price">3200$</p>
                                        </div>
                                    </div>
                                    <div class="catalog-caption">
                                        <div class="social-cont">
                                            <i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $product['id_prod'] ?>" data-type="2" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>" data-image="<?= Url::home(true).'media/products/'.$product['main_image'] ?>" data-description="<?= $product['product_name'] ?>"></a></i>
                                            <i class="twitter-share"><a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $product['product_name'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>"></a></i>
                                            <i class="icon-pinterest4"><a class="share-it-now-pint" href="https://www.pinterest.com/pin/create/button/?url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>&media=<?= Url::home(true).'media/products/'.$product['main_image'] ?>&description=<?= $product['product_name'] ?>"></a></i>
                                        </div>
                                        <p><?= $product['product_name'] ?></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
<!--                        <div class="invite">-->
<!--                            <ul>-->
<!--                                --><?//= Yii::t('modal', 'pay_alternative_text4') ?>
<!--                            </ul>-->
<!--                            <p class="mail"><i class="icon-symbol20 active"></i>--><?//= Yii::$app->user->identity->email ?><!--</p>-->
<!--                            <form class="forma-input">-->
<!--                                <div class="input-edit">-->
<!--                                    <i class="icon-locked59"></i>-->
<!--                                    <div class="input-container">-->
<!--                                        <input class="text-input floating-label" type="text" name="sample1" />-->
<!--                                        <label for="sample1">--><?//= Yii::t('main', 'password') ?><!--</label>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </form>-->
<!--                            <a href="#" class="button">--><?//= Yii::t('main', 'modal_to_invite') ?><!--</a>-->
<!--                        </div>-->
<!--                        <div class="catalog-item-mini">-->
<!--                            --><?php //if($projects): ?>
<!--                                --><?php //foreach($projects as $project): ?>
<!--                                    --><?php //$img = explode("|", $project['images']); ?>
<!--                                    <div class="catalog-item">-->
<!--                                        <div class="catalog-img-box">-->
<!--                                            <a href="#">-->
<!--                                                <img src="--><?//= Yii::getAlias('@portfolio/'.$img['0']); ?><!--" alt="">-->
<!--                                            </a>-->
<!--                                            <div class="like-calc"><i class="icon-heart2971"></i><span>--><?//= $project['likes'] ?><!--</span></div>-->
<!--                                        </div>-->
<!--                                        <div class="catalog-caption">-->
<!--                                            <p>--><?//= $project['title'] ?><!--</p>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                --><?php //endforeach; ?>
<!--                            --><?php //endif; ?>
<!--                        </div>-->
                    </div>
                    <?= Yii::t('modal', 'pay_alternative_text5') ?>
                    <div class="clearfix"></div>
                    <div class="button-bottom">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        <a href="#" class="button" id="publish"><?= Yii::t('main', 'modal_publish') ?></a>
                        <a href="#" data-toggle="modal" data-target="#modal-pay" class="button"><?= Yii::t('main', 'modal_refuse') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END MODAL-->
    <!--START MODAL-->
    <div class="modal fade pay" tabindex="-1" role="dialog" id="modal-pay-2b" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="pay-content special-offers-catalog page-in-catalog">
                    <h4>Уникальная возможность!</h4>
                    <p class="text">Ваши работы украсят страницы известных производителей. Все Ваши проекты, которые получили лайки в социальных сетях автоматически будут бесплатно показаны домовладельцам согласно их запросов.</p>
                    <h4>Тарифы</h4>
                    <div class="pay-block">
                        <h2>Free</h2>
                        <ul>
                            <li>до 9 фото проектов; </li>
                            <li>инструменты взаимодействия со всеми учасниками рынка;</li>
                            <li>показ всех работ клиентам согласно рейтингу(больше лайков - выше рейтинг).</li>
                        </ul>
                    </div>
                    <div class="pay-block">
                        <h2>Premium</h2>
                        <ul>
                            <li>до 81 фото проектов</li>
                            <li>инструменты взаимодействия со всеми учасниками рынка</li>
                            <li>показ всех работ клиентам согласно рейтингу(больше лайков- выше рейтинг)</li>
                            <li>3 фото вашего портфолио войдут в топ-каталог</li>
                        </ul>
                        <a href="#" class="button">Оплатить 29$</a>
                        <div class="payment">
                            <a href="#"><img src="/images/western.png" alt=""></a>
                            <a href="#"><img src="/images/paypay.png" alt=""></a>
                            <a href="#"><img src="/images/visa.png" alt=""></a>
                        </div>
                    </div>
                    <div class="pay-block">
                        <h2>Premium+</h2>
                        <ul>
                            <li>до 81 фото проектов</li>
                            <li>инструменты взаимодействия со всеми учасниками рынка</li>
                            <li>показ всех работ клиентам согласно рейтингу(больше лайков - выше рейтинг)</li>
                            <li>3 фото вашего портфолио войдут в топ- каталог</li>
                            <li>репутация</li>
                            <li>респект</li>
                            <li>легкий вход в проект и отношение с клиентами</li>
                        </ul>
                    </div>
                    <h4>Поднимите свой рейтинг и получите Premium+Free!</h4>
                    <div class="catalog-box">
                        <h4>Портфолио</h4>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-31.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                            </div>
                            <form action="" class="form-comment">
                                <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                <input type="submit" value="Отправить" class="button">
                            </form>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/catalog-img-7.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                                <form action="" class="form-comment">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                    <input type="submit" value="Отправить" class="button">
                                </form>
                            </div>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/catalog-img-2.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                                <form action="" class="form-comment">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                    <input type="submit" value="Отправить" class="button">
                                </form>
                            </div>
                        </div>
                        <h4>Каталог товаров</h4>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-31.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                            </div>
                            <form action="" class="form-comment">
                                <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                <input type="submit" value="Отправить" class="button">
                            </form>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/catalog-img-7.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                                <form action="" class="form-comment">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                    <input type="submit" value="Отправить" class="button">
                                </form>
                            </div>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/catalog-img-2.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                                <form action="" class="form-comment">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                    <input type="submit" value="Отправить" class="button">
                                </form>
                            </div>
                        </div>
                        <h4>Просмотренные товары</h4>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/mainPage-column-31.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                            </div>
                            <form action="" class="form-comment">
                                <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                <input type="submit" value="Отправить" class="button">
                            </form>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/catalog-img-7.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                                <form action="" class="form-comment">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                    <input type="submit" value="Отправить" class="button">
                                </form>
                            </div>
                        </div>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="">
                                    <img src="/images/catalog-img-2.jpg" alt="">
                                </a>
                                <div class="limited-offer">
                                    <p><b>Limited offer</b> <br>save 30%</p>
                                </div>
                                <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                                <div class="like-calc item-price">
                                    <i class="icon-label49"></i>
                                    <span>1800$</span>
                                    <p class="last-price">3200$</p>
                                </div>
                            </div>
                            <div class="catalog-caption">
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a>
                                </div>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                    Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                <a class="button button-comment">Комментировать</a>
                                <form action="" class="form-comment">
                                    <textarea name="" id="" cols="30" rows="10" placeholder="Your comment..."></textarea>
                                    <input type="submit" value="Отправить" class="button">
                                </form>
                            </div>
                        </div>
                    </div>
                    <h4>Управляйте Вашей активностью в соцсетях</h4>
                    <div class="radio-button">
                        <p>
                            <input id="test2" name="featured"  type="radio" checked>
                            <label for="test2">в течении 10 дней</label>
                        </p>
                        <p>
                            <input id="test3" name="featured"  type="radio" checked>
                            <label for="test3">в течении 20 дней</label>
                        </p>
                    </div>
                    <p class="text-bottom">80% клиентов принимает решение о выборе контрактора после изучения комментариев и другой активности в соцсетях. Ваши комментарии должны отобразить Ваше видение и философию, которыми Вы руководствуетесь в своем творчестве.</p>
                    <div class="clearfix"></div>
                    <div class="button-bottom">
                        <a href="#" class="button">Опубликовать</a>
                        <a href="#" class="button">Отказаться от Premium+</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END MODAL-->
<?php } ?>

<?php if($name == "modal-guest"){?>
    <!--START VIDEO MODAL-->
    <div class="modal fade first-modal-video" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index : 9999">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <h4 align="center">Here we will show how it works</h4>
                <p align="center"><iframe id="play" width="640" height="480" src="https://www.youtube.com/embed/uqnFVEftAAE?enablejsapi=1&version=3" frameborder="0" allowfullscreen></iframe></p>
                <div class="controlDiv play"></div>
                <div class="progressBar">
                    <div class="elapsed"></div>
                </div>
            </div>
        </div>
    </div>
    <!--END VIDEO MODAL-->
    <?php
    $this->registerJsFile('js/jquery.cookie.js', ['depends'=>'frontend\assets\AppAsset']);
    $script_modal = <<< JS
        var modal_action = false;
        if (!$.cookie('enter_count')) {
            $.cookie('enter_count', 1);
            modal_action = true;
        }else{

            var count = parseInt($.cookie('enter_count'), 10);

            $.cookie('enter_count', count+1);
        }

        if ($.cookie('enter_count') < 2) {
            setTimeout(function() {
                $('.first-modal-video').modal('show');
                $("#play").attr('src',$("#play").attr('src')+"&autoplay=1");
            }, 4000);
        }

        $(".first-modal-video").on('hidden.bs.modal', function () {
            $(".modal-content p iframe").attr('src', '');
            if (!$.cookie('choose_style') && modal_action == true) {
	  		    	setTimeout(function() {
	  		    var lang = '$lang'
	  		    var request = $.ajax({
	  		        data: {lang : lang},
                    url: '/site/request-style',
                    type: 'post',
                    dataType: 'html'
                });

                request.done(function(html) {
                    $('.choose-style .choose-content').html(html);
                });

	  		    $('.choose-style').modal('show');
	  		}, 3000);
	  		    $.cookie('choose_style', 1);
            }
        });
JS;
    $this->registerJs($script_modal, yii\web\View::POS_READY);
    ?>

<!--    --><?php //if($route == 'about/item' || $route == 'demo/index'){?>
        <!--START MODAL-->
        <div class="modal fade how_it_words" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="words block-1 current-page-role">
                        <?= Yii::t('modal', 'choose_role_text_designer') ?>
                        <a href="<?= Url::to(['site/signup', 'role' => 'designer']) ?>" class="button"><?= Yii::t('main', 'modal_registration') ?></a>
                    </div>
                    <div class="words block-2 current-page-role">
                        <?= Yii::t('modal', 'choose_role_text_homeowner') ?>
                        <a href="<?= Url::to(['site/signup']) ?>" class="button"><?= Yii::t('main', 'modal_registration') ?></a>
                    </div>
                    <div class="words block-3 current-page-role">
                        <?= Yii::t('modal', 'choose_role_text_professionals') ?>
                        <a href="<?= Url::to(['site/signup', 'role' => 'service']) ?>" class="button"><?= Yii::t('main', 'modal_registration') ?></a>
                    </div>
                    <div class="words block-4 current-page-role">
                        <?= Yii::t('modal', 'choose_role_text_supplier') ?>
                        <a href="<?= Url::to(['site/signup', 'role' => 'agent']) ?>" class="button"><?= Yii::t('main', 'modal_registration') ?></a>
                    </div>
                    <div class="words block-5 current-page-role">

                        <?= Yii::t('modal', 'choose_role_text_partner') ?>
                        <a href="<?= Url::to(['site/signup', 'role' => 'partner']) ?>" class="button"><?= Yii::t('main', 'modal_registration') ?></a>
                    </div>
                    <div class="words block-6 current-page-role">
                        <?= Yii::t('modal', 'choose_role_text_manufacturer') ?>
                        <a href="<?= Url::to(['site/signup', 'role' => 'manufacturer']) ?>" class="button"><?= Yii::t('main', 'modal_registration') ?></a>
                    </div>
                </div>
            </div>
        </div>
        <!--END MODAL-->
<!---->
<!--        --><?php
//        $script_modal = <<< JS
//                if (modal_action == false) {
//	  		        setTimeout(function() { $('.how_it_words').modal('show'); }, 3000);
//                }
//JS;
//        $this->registerJs($script_modal, yii\web\View::POS_READY);
//        ?>
<!---->
<!--    --><?php //} ?>

    <?php if($route == 'site/index'){?>
    <!--START MODAL-->
    <div class="modal fade choose-style" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4><?= Yii::t('modal', 'choose_style_title_text') ?></h4>
                </div>
                <div class="choose-content">
                    <!--AJAX CONTENT-->
                </div>
                <div class="modal-footer">
                    <a href="#" id="choose-style-done" class="button">Done</a>
                </div>
            </div>
        </div>
    </div>
    <!--END MODAL-->
        <?php
        $script_modal = <<< JS
         var lang = '$lang'
         if (!$.cookie('choose_style') && modal_action == false) {
	  		setTimeout(function() {
	  		    var request = $.ajax({
	  		        data: {lang : lang},
                    url: '/site/request-style',
                    type: 'post',
                    dataType: 'html'
                });

                request.done(function(html) {
                    $('.choose-style .choose-content').html(html);
                });

	  		    $('.choose-style').modal('show');
	  		}, 3000);
	  		$.cookie('modal_choose_style', 1);
         }

JS;
        $this->registerJs($script_modal, yii\web\View::POS_READY);
        ?>

    <?php } ?>

<?php } ?>

