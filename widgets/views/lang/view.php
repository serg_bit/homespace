<?php
use yii\helpers\Html;
?>
<ul class="language">
    <li><a class="active"><?= $current->name;?></a></li>
    <?php foreach ($langs as $lang):?>
        <li class="item-lang">
            <?= Html::a($lang->name, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl()) ?>
        </li>
    <?php endforeach;?>
</ul>