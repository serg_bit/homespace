<?php
namespace frontend\widgets;

use common\models\User;
use yii;
use frontend\models\Publicity;

class Banner extends \yii\bootstrap\Widget
{
    public $position;

    public function init(){
        $this->position = !empty($this->position) ? $this->position : 'left';
    }

    public function run() {

        return $this->render('banner/view', [
        	'publicity' => $this->getBanners($this->position),
            'position' => $this->position

        ]);
    }

    public function actionGet() {

        $id = isset($_POST["id"]) ? $_POST["id"] : 'no';
        echo $id;
    }

    private function getBanners($position){
        $country_id = Yii::$app->user->identity->country;

        $country = $country_id!='' ? $country_id : 0;

        $banners = [];
        if($position == 'left'){
            $count = Publicity::find()->where(['status' => 1, 'position' => 1, 'country_id'=>$country])->count();
            if($count == 0){
                $limit = rand(1, 3);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>0])->orderBy('RAND()')->limit($limit)->all();
            }elseif($count > 1){
                $limit = rand(1, $count);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>$country])->orderBy('RAND()')->limit($limit)->all();

            }else  $banners[] = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>$country])->one();

        }else if($position == 'right'){
            $count = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>$country])->count();
            if($count == 0){
                $limit = rand(1, 3);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>0])->orderBy('RAND()')->limit($limit)->all();
            }elseif($count > 1){
                $limit = rand(1, $count);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>$country])->orderBy('RAND()')->limit($limit)->all();
            }else  $banners[] = Publicity::find()->where(['status' => 1, 'position' => 1,'country_id'=>$country])->one();
        }else if($position == 'bottom'){
            $count = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>$country])->count();
            if($count == 0){
                //$limit = rand(1, 3);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>0])->orderBy('RAND()')->one();
            }elseif($count > 1){
                //$limit = rand(1, $count);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>$country])->orderBy('RAND()')->one();
            }else  $banners[] = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>$country])->one();
        } else if($position =='top'){
            $count = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>$country])->count();
            if($count == 0){
                //$limit = rand(1, 3);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>0])->orderBy('RAND()')->one();
            }elseif($count > 1){
                //$limit = rand(1, $count);
                $banners = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>$country])->orderBy('RAND()')->one();
            }else  $banners[] = Publicity::find()->where(['status' => 1, 'position' => 0,'country_id'=>$country])->one();
        }

        return $banners;
    }
}