<?php
namespace frontend\widgets;

use yii;
use frontend\account\models\ProductsCollections;
use frontend\models\Language;


class AddProduct extends \yii\bootstrap\Widget
{

    public function init(){}

    public function run()
    {
        $lang = Language::getCurrent()->url;
        $products_collections = ProductsCollections::find()->asArray()->all();
        return $this->render('add_product/view', [
            'lang' => $lang,
            'products_collections' => $products_collections,
        ]);

    }

}