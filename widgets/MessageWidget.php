<?php

namespace frontend\widgets;

use frontend\models\MessageForm;

class MessageWidget extends \yii\bootstrap\Widget
{
    public function init(){}

    public function run() {
        $model = new MessageForm();
        return $this->render('message/message', [
            'model' => $model,
        ]);
    }
}