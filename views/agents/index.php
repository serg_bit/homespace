<?php
use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;

$this->title = 'Agents';
$this->params['breadcrumbs'][] = $this->title;
?>

    <!--START CONTENT-->
    <section class="catalog-id" data-id="1">
        <div class="container container-style-catalog scrollSidebar1">
            <div class="row">

                <!--START CENTER-->
                <?php if (!Yii::$app->user->isGuest): ?>
                <style>
                    .left-sidebar {
                        padding-left: 0;
                        padding-top: 0;
                    }
                </style>
                <div class="col-lg-3 left-sidebar">
                    <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                    <?= SideBar::widget(); ?>
                </div>
                <div class="col-lg-9 catalog-column profile-agent-comps centralScroll1 agents">

                    <?php else: ?>

                    <div class="col-lg-12 catalog-column profile-agent-comps agents">
                        <?php endif; ?>
                        <div class="row">
                            <div class="content">
                                <div class="portfolio-top profile-conten-me ">
                                    <div class="catalog-box special-offers-catalog page-in-catalog">
                                        <div class="clearfix"></div>
                                        <h4 class="promote promote-bottom">Agents</h4>
<!--                                        <p class="upload-portfolio download-portfolio join">Add-->
<!--                                            <a href="#"></a>-->
<!--                                        </p>-->
                                        <div class="clearfix"></div>
                                        <form class="forma-input">
                                            <div class="search">
                                                <div class="input-edit">
                                                    <div class="input-container">
                                                        <input class="text-input floating-label" name="sample1"
                                                               type="text">
                                                        <label for="sample1">Search</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="selects">
                                                <select name="#">
                                                    <option value="#">Search by</option>
                                                    <option value="#">Search by</option>
                                                    <option value="#">Search by</option>
                                                    <option value="#">Search by</option>
                                                </select>
                                            </div>
                                        </form>
                                        <?php if ($agents) : ?>
                                        <div class="companys">
                                            <?php while ($agents): ?>
                                                <?php $agent = array_pop($agents); ?>

                                                <div class="company agents"
                                                     style="background-image: url('/media/profile/background/<?= $agent['background'] ?>');">
                                                    <a href="<?= Yii::$app->urlManager->createUrl(['agents/profile', 'id' => $agent['id']]) ?>"><img
                                                        class="company-img"
                                                        src="<?= Yii::getAlias('@avatar/' . $agent['avatar']) ?>"
                                                        alt="img"></a>
                                                    <div class="name-company">
                                                        <h5><?= $agent['first_name'] ?>
                                                            <span><?= $agent['last_name'] ?></span></h5>
                                                        <p><?= $agent['country'] ?></p>
                                                    </div>
                                                    <ul class="description">
                                                        <li>Manufacturers: <span><?= $agent['companies'] ?></span></li>
                                                    </ul>
                                                    <div class="author"><img
                                                            src="<?= Yii::getAlias('@avatar/' . $agent['logo']) ?>"
                                                            alt="#">
                                                        <p class="name"><?= $agent['company'] ?></p>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                            <?php endif; ?>
                                            <div class="clearfix"></div>
                                            <div class="bread-crumbs">
                                                <?= LinkPager::widget(['pagination' => $pagination,
                                                    'disabledPageCssClass' => false,
                                                    'nextPageLabel' => '',
                                                    'prevPageLabel' => '',
                                                    'options' => ['class' => 'hvr-radial-out1, agent-pagination'],
                                                ]); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$this->registerJsFile('js/agents.js', ['depends' => 'frontend\assets\AppAsset']);