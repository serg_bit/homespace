<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\widgets\WLang;
use yii\helpers\Url;
use frontend\models\Language;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <link rel="shortcut icon" href="<?= Yii::getAlias('@img'); ?>/favicon.png" type="image/x-icon" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode('Home Space Today '.$this->title) ?></title>
    <?php $this->head() ?>
</head>

<script type="text/javascript">
    var results = document.cookie.match ( '(^|;) ?client_width=([^;]*)(;|$)' );

    if (!results){
        var client_width = window.screen.width;
        document.cookie = "client_width="+client_width;
        location.reload();
    }
</script>

<?php $client_width = isset($_COOKIE['client_width']) ? $_COOKIE['client_width'] : 1366 ?>


<body>

<?php $this->beginBody() ?>

<?php if($client_width > 1024): ?>

<div class="filter">
<!--START HEADER-->
        <?php $lang = Language::getCurrent()->url; ?>
        <?php if($lang == 'en'){?>
            <?php $link_home = Url::home(); ?>
        <?php }else{ ?>
            <?php $link_home = Url::home().$lang.'/'; ?>
        <?php } ?>
        <header class="header">
            <div class="wrapper block-1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="logo">
                                <a href="<?= $link_home ?>"><img src="<?= Yii::getAlias('@img'); ?>/logo.png" /></a>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <?= frontend\widgets\Banner::widget(['position' => 'top']);?>
                        </div>
                        <div class="col-lg-2">
                            <div class="log-in">

                                <?= WLang::widget();?>
                               <?php if (Yii::$app->user->isGuest) { ?>
                                <ul class="log">
                                    <li><a class="login-page" data-toggle="modal" data-target="#login" href="#">Log in</a></li>
                                </ul>
                                <?php } else { ?>
                                   <ul class="log">
                                      <li><a class="login-page" href="<?= Yii::$app->urlManager->createUrl(['site/logout']) ?>" data-method="post">Logout<?= ' ('.Yii::$app->user->identity->first_name ?>)</a></li>
                                   </ul>
                                <?php } ?>
                                <div class="search">
                                    <label for=""><input type="search" placeholder="Search"><input type="submit" value=""></label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wrapper block-2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="header-menu">
                                <?php
                                $label = Yii::t('main', 'top_menu');
                                if(Yii::$app->user->identity->roles == 'agent'){
                                    $profile = array('label' => $label['profile'], 'route' =>'account/profile-agent',  'url' => Yii::$app->urlManager->createUrl(['account/profile-agent']),);
                                }else{
                                    $profile = array('label' => $label['profile'], 'route' =>'account/profile',  'url' => Yii::$app->urlManager->createUrl(['account/profile']),);
                                }
                                $menuItems = array(
                                    array('label' => $label['home'], 'route' =>'site/index', 'url' => $link_home,),
                                    array('label' => $label['full_catalog'], 'route' => 'catalog/projects', 'url' => Yii::$app->urlManager->createUrl(['catalog/projects']),),
                                    array('label' => $label['design'], 'route' => 'catalog/decor', 'url' => Yii::$app->urlManager->createUrl(['catalog/decor']),),
                                    array('label' => $label['products'], 'route' => 'catalog/products', 'url' => Yii::$app->urlManager->createUrl(['catalog/products']),),
                                    array('label' => $label['showroom'], 'route' => 'site/about', 'url' => Yii::$app->urlManager->createUrl(['site/about']),),
                                    array('label' => $label['manufacture'], 'route' => 'manufacturers', 'url' => Yii::$app->urlManager->createUrl(['manufacturers']),),
                                    array('label' => $label['service'], 'route' => 'site/about', 'url' => Yii::$app->urlManager->createUrl(['site/about']),),
                                    $profile,
                                );
                                ?>


                                <?php foreach ($menuItems as $item) { ?>
                                    <?php if(Yii::$app->controller->getRoute()=== $item['route'] || Yii::$app->controller->getRoute()===$item['route'].'/index' ){ ?>
                                        <li class="active"><a href="<?= $item['url'] ?>"><?=$item['label'] ?></a></li>
                                    <?php }else{ ?>
                                        <li><a href="<?= $item['url'] ?>"><?=$item['label'] ?></a></li>
                                    <?php } ?>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>		
        </header>
        <!--END HEADER-->

<!--    <div class="container">-->
        <?= Alert::widget() ?>
        <?= $content ?>

<!--    </div>-->

</div>
<?php
$lang = Language::getCurrent()->url;
$script_search = <<< JS
    var search_lang = '$lang';
    if(search_lang == 'en'){
        search_lang = '';
    }else{
        search_lang = '/'+search_lang;
    }
    $('.search input[type=submit]').click(function(e){
        var text = '';
        text = $('.search input[type=search]').val();
        if(text != ''){
            window.location = search_lang+"/search?q="+text;
        }
    });

    $('.search input[type=search]').keypress(function(e){
        if(e.keyCode == 13) {
           $('.search input[type=submit]').trigger('click');
        }
    });

JS;
$this->registerJs($script_search, yii\web\View::POS_READY);

?>

<?= $this->render('footer'); ?>

<?php else: ?>

<div class="container">
    <h1 align="center">Homespace.today only for desktop now!</h1>
    <h3 align="center">Send this link to your email to view later.</h3>
    <h4 align="center">Successful!</h4>
    <form class="form-inline" role="form" action="/site/mobile-form" method="post">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" name="email" class="form-control" >
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
    <style>
        h4{
            color: #00a157;
            visibility: hidden;
        }
        form{
            text-align: center;
        }
    </style>

<?php endif; ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
