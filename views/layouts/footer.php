<!--START FOOTER-->
<?php

use yii\helpers\Html;
use frontend\widgets\Chat;
use frontend\widgets\VideoForMain;
use yii\helpers\Url;
use frontend\models\Language;
?>
<?= Html::csrfMetaTags() ?>

<?php $lang = Language::getCurrent()->url; ?>
<?php if($lang == 'en'){?>
    <?php $link_home = Url::home(); ?>
<?php }else{ ?>
    <?php $link_home = Url::home().$lang.'/'; ?>
<?php } ?>

<footer class="footer">
    <div class="wrapper block-1">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="logo">
                        <a href="<?= $link_home ?>"><img src="<?= Yii::getAlias('@img') ?>/logo-footer.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="footer-text">
                        <p><?= Yii::t('modal', 'footer_text') ?></p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="footer-menu">
                        <ul>
                            <?php
                            if(Yii::$app->user->identity->roles == 'agent'){
                                $link_to_profile = Yii::$app->urlManager->createUrl(['account/profile-agent']);
                            }else{
                                $link_to_profile = Yii::$app->urlManager->createUrl(['account/profile']);
                            }
                            $label = Yii::t('main', 'footer_menu');
                            $menuItems = array(
                                array('label' => $label['home'], 'url' => $link_home ,),
                                array('label' => $label['profile'], 'url' => $link_to_profile,),
                                array('label' => $label['messages'], 'url' => Yii::$app->urlManager->createUrl(['account/messages']))
                            );
                            ?>

                            <?php foreach ($menuItems as $item) { ?>
                                    <li><a href="<?= $item['url'] ?>"><?= $item['label'] ?></a></li>
                            <?php } ?>


                        </ul>
                        <a href="#" class="footer-more"><?= Yii::t('modal', 'footer_ticket_message') ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrapper block-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="copy-right">© 2014-<?= date('Y', time()) ?>. <?= Yii::t('modal', 'text_copyrigth') ?></div>
                    <ul class="footer-soc">
                        <li><a href="https://www.facebook.com/" target="_blank"><i class="icon-facebook55"></i></a></li>
                        <li><a href="https://twitter.com" target="_blank"><i class="icon-twitter1"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>
<div class="question">
    <p><?= Yii::t('main', 'question_title') ?> <a href="#">X</a></p>
    <form class="forma-input" id="ticket-form">
        <div class="input-container">
            <input class="text-input floating-label has-text" type="text" name="name" value="" />
            <label for="sample"><?= Yii::t('main', 'question_name') ?></label>
        </div>
        <div class="input-container">
            <input class="text-input floating-label has-text" type="text" name="email" value=""/>
            <label for="sample">E-mail</label>
        </div>
        <div class="input-container">
            <input class="text-input floating-label has-text" type="text" name="theme" value=""/>
            <label for="sample"><?= Yii::t('main', 'question_theme') ?></label>
        </div>
        <div class="input-container">
            <textarea class="has-text" cols="30" rows="10" name="question" placeholder="<?= Yii::t('main', 'question_message') ?>"></textarea>
        </div>
        <div class="block-button">
            <input class="button" type="submit" name="send" value="<?= Yii::t('main', 'question_send') ?>">
        </div>
    </form>
</div>

<div id="top-link-block" class="hidden">
    <a href="#top" class="well well-sm"  onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
        <img src="/images/scroll_up.png" alt="">
    </a>
</div>

<!--END FOOTER-->
</div>
<!--Вызов модального окна для аунтификации-->
<?= Chat::widget() ?>
<?= frontend\widgets\LoginWidget::widget() ?>
<?= frontend\widgets\AddProduct::widget() ?>

<?= frontend\widgets\Modal::widget(['name' => 'best3works']) ?>
<?= frontend\widgets\Modal::widget(['name' => 'style']) ?>
<?= frontend\widgets\Modal::widget(['name' => 'tariff-plan']) ?>
<?= frontend\widgets\Modal::widget(['name' => 'modal-guest']) ?>
<?= VideoForMain::widget() ?>
<!--модальное окно для каталога(выбор товара)-->
<?= frontend\widgets\Modal::widget(['name' => 'add-order-model']); ?>
<?= frontend\widgets\Modal::widget(['name' => 'move-order-model']); ?>
<?php if (!Yii::$app->user->isGuest) : ?>
    <?php $this->registerJsFile('js/chat2.js', ['depends'=>'frontend\assets\AppAsset']) ?>
<?php endif; ?>
<?php $this->registerJsFile('js/autocomplete.js', ['depends'=>'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('http://platform.twitter.com/widgets.js', ['depends'=>'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/jquery.cookie.js', ['depends'=>'frontend\assets\AppAsset']); ?>
<?php if(Yii::$app->controller->getRoute() == 'site/index') : ?>
    <?php $this->registerJsFile('js/https://www.youtube.com/iframe_api"', ['depends'=>'frontend\assets\AppAsset']); ?>

<?php endif; ?>
