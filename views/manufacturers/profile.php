<?php

use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;

$this->title = 'Manufacturers';
$this->params['breadcrumbs'][] = $this->title;
?>


<!--START CONTENT-->

<section>
    <div class="container scrollSidebar">
        <div class="row">
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget();?>
            </div>
            <!--START CENTER-->
            <div class="col-lg-9 central-content profile-agent company centralScroll">
                    <div class="row">
                        <div class="content overall-profile-manufacturer">
                            <div class="news">
                                <img src="/images/portfolio-1.jpg" alt="">
                                <p>News 1</p>
                            </div>
                            <div class="news">
                                <img src="/images/portfolio-1.jpg" alt="">
                                <p>News 1</p>
                            </div>
                            <div class="news">
                                <img src="/images/portfolio-1.jpg" alt="">
                                <p>News 1</p>
                            </div>
                            <div class="agent-profil" style="background-image: url(/images/bg-agents.png);">

                                <div class="agents">
                                    <div class="company-description">
                                        <img src="/images/tiny-logo.png" alt="#" class="logos">

                                        <h4>Company name <span>brand agency</span><b>made in New Zealand</b></h4>
                                        <a href="#"><i class="icon-black218"></i>Message</a>
                                    </div>
                                    <div class="agent">
                                        <img src="/images/img-2.png" alt="#" class="photo">
                                        <h5 class="name">Sam Clarintence</h5>
                                        <p>Director</p>
                                    </div>
                                    <div class="description">
                                        <ul>
                                            <li><a href="#"><i class="icon-follow1"></i>3859 Followers</a></li>
                                            <li><a href="#"><i class="icon-home"></i>London</a></li>
                                            <li><a href="#"><i class="icon-follow"></i>3859 Following</a></li>
                                            <li><a href="#"><i class="icon-location"></i>Koren Oswald st. 23</a></li>
                                            <li><a href="#"><i class="icon-telephone46"></i>+3859621476398</a></li>
                                            <li><a href="#"><i class="icon-webpage2"></i>https://www.pinterest.com</a></li>
                                        </ul>
                                    </div>
<!--                                    <div class="view-buttons">-->
<!--                                        <i class="icon-another_eye"></i>-->
<!--                                        <a href="#" class="view-button">View my profile as others see it</a>-->
<!--                                    </div>-->
                                    <ul class="soc">
                                        <li><a href="//www.facebook.com/" target="_blank"><i class="icon-facebook55"></i></a></li>
                                        <li><a href="//twitter.com/" target="_blank"><i class="icon-twitter1"></i></a></li>
                                        <li><a href="//www.instagram.com/" target="_blank"><i class="icon-instagram12"></i></a></li>
                                        <li><a href="//www.pinterest.com" target="_blank"><i class="icon-pinterest3"></i></a></li>
                                        <li><a href="//www.tumblr.com/" target="_blank"><i class="icon-logotype1"></i></a></li>
                                        <li><a href="//www.linkedin.com/" target="_blank"><i class="icon-id16"></i></a></li>
                                        <li><a href="//www.blogger.com/" target="_blank"><i class="icon-blogger8"></i></a></li>
                                    </ul>
                                </div>
                                <div class="agent-text ">
                                    <ul class="categories">
                                        <li><a href="#">Categories:</a></li>
                                        <li><a href="#">Furniture</a></li>
                                        <li><a href="#">Furniture</a></li>
                                        <li><a href="#">Furniture</a></li>
                                        <li><a href="#">Furniture</a></li>
                                        <li><a href="#">Furniture</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus lectus, a mollis nunc interdum eu. Proin vitae maximus elit, non molestie est. Proin vitae maximus elit, non molestie est.</p>
                                </div>
                                <div class="mesages comp">


                                    <form class="forma-input">
                                        <div class="search">
                                            <div class="input-edit">
                                                <div class="input-container">
                                                    <input class="text-input floating-label" name="sample1" type="text">
                                                    <label for="sample1">Search people</label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <ul>
                                        <li>
                                            <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b></li>
                                        <li>
                                            <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b></li>
                                        <li>
                                            <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>
                                        </li>
                                    </ul>
                                </div>
                                <div class="video">
                                    <iframe width="466px" height="260" src="https://www.youtube.com/embed/LaP1H-JJroU" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                                <a href="#">Entrust the estimation</a>
                            </div>
                            <div class="showrooms">
                                <h4 class="promote promote-bottom">Showrooms</h4>
                                <div class="clearfix"></div>

                                <div class="selects">
                                    <i class="icon-home "></i>
                                    <select name="#" id="#">
                                        <option value="#">Town</option>
                                        <option value="#">Town</option>
                                        <option value="#">Town</option>
                                        <option value="#">Town</option>
                                    </select>
                                </div>
                                <div class="hashtag">
                                    <a href="#">#lorem</a>
                                    <a href="#"> #ipsum</a>
                                    <a href="#"> #dolor</a>
                                    <a href="#"> #sit</a>
                                    <a href="#"> #consectetuer</a>
                                    <a href="#"> #adipiscing</a>
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus lectus, a mollis nunc interdum eu. Proin vitae maximus elit, non molestie est. Aliquam at tortor aliquam, tincidunt sem sit amet, suscipit erat. Integer sed dapibus metus. Nullam fringilla velit ut porttitor...</p>
                                <div class="catalog-box">
                                    <div class="clearfix"></div>
                                    <div class="catalog-item">
                                        <div class="catalog-img-box">
                                            <a href="#">
                                                <img src="/images/img-10.png" alt="">
                                            </a>
                                        </div>
                                        <div class="catalog-caption">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus lectus, a mollis nunc interdum eu. Proin vitae maximus elit, non molestie est...</p>
                                        </div>
                                        <p class="town">London</p>
                                    </div>
                                    <div class="catalog-item">
                                        <div class="catalog-img-box">
                                            <a href="#">
                                                <img src="/images/img-10.png" alt="">
                                            </a>
                                        </div>
                                        <div class="catalog-caption">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris aliquet lacus lectus, a mollis nunc interdum eu. Proin vitae maximus elit, non molestie est...</p>
                                        </div>
                                        <p class="town">Dublin</p>
                                    </div>
                                </div>
                            </div>

                            <hr class="sline">
                            <div class="catalog-box special-offers-catalog page-in-catalog">
                                <div class="clearfix"></div>
                                <h4 class="promote promote-bottom">Product</h4>
                                <div class="clearfix"></div>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="">
                                            <img src="/images/mainPage-column-32.jpg" alt="">
                                        </a>
                                        <div class="limited-offer">
                                            <p><b>Limited offer</b>
                                                <br>save 30%</p>
                                        </div>
                                        <div class="like-calc item-price">
                                            <i class="icon-label49"></i>
                                            <span>1800$</span>
                                            <br>
                                            <p class="last-price">3200$</p>
                                        </div>
                                    </div>
                                    <div class="catalog-caption">
                                        <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                            <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                    </div>
                                </div>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="">
                                            <img src="/images/mainPage-column-32.jpg" alt="">
                                        </a>
                                        <div class="limited-offer">
                                            <p><b>Limited offer</b>
                                                <br>save 30%</p>
                                        </div>

                                        <div class="like-calc item-price">
                                            <i class="icon-label49"></i>
                                            <span>1800$</span>
                                            <br>
                                            <p class="last-price">3200$</p>
                                        </div>
                                    </div>
                                    <div class="catalog-caption">

                                        <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                            <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                    </div>
                                </div>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="">
                                            <img src="/images/mainPage-column-31.jpg" alt="">
                                        </a>
                                        <div class="limited-offer">
                                            <p><b>Limited offer</b>
                                                <br>save 30%</p>
                                        </div>
                                        <div class="like-calc item-price">
                                            <i class="icon-label49"></i>
                                            <span>1800$</span>
                                            <br>
                                            <p class="last-price">3200$</p>
                                        </div>
                                    </div>
                                    <div class="catalog-caption">

                                        <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                            <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                    </div>
                                </div>
                                <div class="catalog-item">
                                    <div class="catalog-img-box">
                                        <a href="">
                                            <img src="/images/mainPage-column-32.jpg" alt="">
                                        </a>
                                        <div class="limited-offer">
                                            <p><b>Limited offer</b>
                                                <br>save 30%</p>
                                        </div>
                                        <div class="like-calc item-price">
                                            <i class="icon-label49"></i>
                                            <span>1800$</span>
                                            <br>
                                            <p class="last-price">3200$</p>
                                        </div>
                                    </div>
                                    <div class="catalog-caption">

                                        <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                            <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                                    </div>
                                </div>
                            </div>
                            <div class="bread-crumbs">
                                <ul class="hvr-radial-out">
                                    <li>
                                        <a href="#"></a>
                                    </li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">6</a></li>
                                    <li><a href="#">7</a></li>
                                    <li>
                                        <a href="#"></a>
                                    </li>
                                </ul>
                            </div>
                            <hr class="sline">
                            <div class="clearfix"></div>
                            <h4 class="promote promote-bottom">Project catalog</h4>
                            <div class="folder">
                                <div class="name-folder ">
                                    <div class="folder-img">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <p>Name folder</p>
                                        <i class="icon-show8"></i>
                                        <ul>
                                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="name-folder ">
                                    <div class="folder-img">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <p>Name folder</p>
                                        <i class="icon-show8"></i>
                                        <ul>
                                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="name-folder ">
                                    <div class="folder-img">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <p>Name folder</p>
                                        <i class="icon-show8"></i>
                                        <ul>
                                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="name-folder ">
                                    <div class="folder-img">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <img src="/images/img-9.png" alt="">
                                        <p>Name folder</p>
                                        <i class="icon-show8"></i>
                                        <ul>
                                            <li><a href="#"><i class="icon-edit45"></i>Rename</a></li>
                                            <li><a href="#"><i class="icon-rubbish"></i>Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
                        </div>
                    </div>
            </div>
            <!--END CENTER-->
        </div>
    </div>
</section>

<!--END CONTENT-->
<!--END CONTENT-->



