<?php

use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;

$this->title = 'Manufacturers';
$this->params['breadcrumbs'][] = $this->title;
?>


<!--START CONTENT-->
<section class="catalog-id" data-id="1">
    <div class="container container-style-catalog scrollSidebar1">
        <div class="row">

            <!--START CENTER-->
            <?php if (!Yii::$app->user->isGuest): ?>
            <style>
                .left-sidebar {
                    padding-left: 0;
                    padding-top: 0;

                }
            </style>
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget(); ?>
            </div>
            <div class="col-lg-9 catalog-column profile-agent-comps centralScroll1">
                <?php else: ?>
                <div class="col-lg-12 catalog-column profile-agent-comps">
                    <?php endif; ?>

                    <div class="row">
                        <div class="content">
                            <div class="portfolio-top profile-conten-me">
                                <div class="catalog-box special-offers-catalog page-in-catalog">
                                    <div class="clearfix"></div>
                                    <h4 class="promote promote-bottom">Manufacturers</h4>
                                    <?php if (Yii::$app->user->identity->roles == 'agent'): ?>
                                        <p class="upload-portfolio download-portfolio join">Add<a
                                                href="<?= Yii::$app->urlManager->createUrl(['account/advertising-tools/create']) ?>"></a>
                                        </p>
                                    <?php endif; ?>
                                    <div class="clearfix"></div>
                                    <form class="forma-input">
                                        <div class="search">
                                            <div class="input-edit">
                                                <div class="input-container">
                                                    <input class="text-input floating-label" name="sample1" type="text">
                                                    <label for="sample1">Search</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="selects">
                                            <select name="#">
                                                <option value="#">Categories</option>
                                                <option value="#">Categories</option>
                                                <option value="#">Categories</option>
                                                <option value="#">Categories</option>
                                            </select>
                                        </div>
                                        <div class="selects">
                                            <select name="view">
                                                <option>6</option>
                                                <option>9</option>
                                                <option>12</option>
                                                <option>24</option>
                                            </select>
                                        </div>
                                    </form>
                                    <!--                                    --><?php //var_dump($companies); ?>
                                    <!--                                    --><?php //die(); ?>
                                    <?php if ($companies) : ?>
                                        <div class="companys">
                                            <?php foreach ($companies as $company): ?>
                                                <div class="company" style="background: url('/media/upload/<?= $company['background'] ?>')">
                                                    <?php $company_attr = explode("||", $company['company']); ?>
                                                    <a href="<?= Yii::$app->urlManager->createUrl(['manufacturers/profile', 'id' => $company['id']]) ?>">  <img class="company-img" src="<?= '/media/upload/' . $company['logo'] ?>" alt="#"></a>
                                                    <div class="name-company">
                                                        <h5><?= $company_attr[0] ?> <span>(<?= $company_attr[1] ?>
                                                                )</span></h5>
                                                        <p>made in <span><?= $company['country'] ?></span></p>
                                                    </div>
                                                    <ul class="categories">
                                                        <li><a href="#">Categories:</a></li>
                                                        <?php if ($company['collections']) : ?>
                                                            <?php $collections = explode(',', $company['collections']['collection']) ?>
                                                            <?php while ($collections) :
                                                                $collection = array_shift($collections); ?>
                                                                <li>
                                                                    <a href="#"><?= $collection ?></a>
                                                                </li>
                                                            <?php endwhile; ?>
                                                        <?php endif; ?>
                                                    </ul>
                                                    <ul class="description">
                                                        <li>Products:
                                                            <span><span><?= $company['products_count'] ?></span></li>
                                                        <li>New: <span>26(test)</span></li>
                                                        <li>Special offers: <span>16(test)</span></li>
                                                        <li>Sale: <span>20%(test)</span></li>
                                                    </ul>
                                                    <div class="author">
                                                        <img src="<?= '/media/upload/' . $company['avatar'] ?>" alt="#">
                                                        <p class="name"><?= $company['first_name'] . ' ' . $company['last_name'] ?></p>
                                                    </div>
                                                </div>

                                            <?php endforeach; ?>
                                        </div>

                                        <div class="clearfix"></div>
                                        <div class="bread-crumbs">
                                            <?= LinkPager::widget(['pagination' => $pagination,
                                                'disabledPageCssClass' => false,
                                                'nextPageLabel' => '',
                                                'prevPageLabel' => '',
                                                'options' => ['class' => 'hvr-radial-out1, agent-comp-pagination'],
                                            ]); ?>
                                        </div>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CENTER-->
            </div>
</section>
<!--END CONTENT-->



