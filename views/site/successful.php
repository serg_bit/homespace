<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Successful';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--START CONTENT-->
<section>
    <div class="container">
        <div class="row">

            <!--START CENTER-->
            <div class="col-lg-12  central-content about-content about-full">
                <div class="content">
                    <h1><?= Html::encode($this->title) ?></h1>

                    <p>Thank you for signing up. Check your email for further instructions</p>
                </div>
            </div>
            <!--END CENTER-->

        </div>
    </div>
</section>
<!--END CONTENT-->
