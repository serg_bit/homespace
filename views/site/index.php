<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use frontend\models\Language;

$this->title = '';
$this->params['breadcrumbs'][] = $this->title;
?>

	<!--START CONTENT-->
	<section>
		<div class="container-fluid">
			<div class="row">
				<?= frontend\widgets\Slider::widget();?>
			</div>
		</div>
	</section>

	<!--Begin full catalog section-->
	<section>
		<div class="container container-full-catalog my-full-catalog">
			<div class="row">
				<div class="col-lg-12">
					<div class="rubric-caption"><?= Yii::t('main', 'best_projects') ?></div>
					<div class="catalog-box">
						<div class="catalog-row">

							<?php foreach ($all_works as $all_item) : ?>

								<?php foreach ($all_item as $item) : ?>

									<?php $img = explode("|", $item['images']);?>
									<div class="catalog-item">
										<div class="catalog-img-box">

											<a href="<?= Url::to(['/about/user/', 'id' => $item['owner_id'], 'item' => $item['item_id']]); ?>"><img src="<?= Yii::getAlias('@portfolio/'.$img[0]); ?>" alt=""></a>
											<div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $item['likes'] ?></span></div>

										</div>
										<div class="catalog-caption">
											<div class="social-cont">
												<a class="main-like icon-heart297<?php if(isset($item['my_likes']) && $item['my_likes'] === '1'){echo ' active-social-button';} ?>" id="<?=$item['item_id']?>" <?php if(\Yii::$app->user->isGuest) {echo "data-toggle='modal' data-target='#login'";} ?>></a>
												<a class="main-share icon-share-2"></a>
											</div>
											<div class="share-container">
												<i class="twitter-share">
													<a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $item['title'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$item['item_id']]) ?>"></a></i>
												<i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $item['item_id'] ?>" data-type="1" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$item['item_id']]) ?>" data-image="<?= Url::home(true).'media/portfolio/'.$img[0] ?>" data-description="<?= $item['title'] ?>"></a></i>

											</div>
											<div class="clearfix"></div>
											<hr class="border-section">
											<p><?= $item['title'] ?></p>
										</div>
									</div>

								<?php endforeach; ?>

							<?php endforeach; ?>

							<div class="bread-crumbs pretty-pagination">

							</div>

						</div>
						<hr class="border-section">
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
$lang = Language::getCurrent()->url;
$script_main_pag = <<< JS
     $('.pretty-pagination').load('/$lang/site/works .bread-crumbs > *');

	 $('body').delegate( ".hvr-radial-out1 a", "click", function(e) {
        e.preventDefault();
        var link = $(this).attr('href');
        $('.my-full-catalog').load(link+' .container-full-catalog > *');
        //$( "body" ).scrollTop( 100 );
        $('html,body').animate({scrollTop:100}, 'fast');
     });
JS;
$this->registerJs($script_main_pag, yii\web\View::POS_READY);

?>
	<!--End full catalog section-->

		<!--Begin produts catalog section-->
		<section>
		<div class="container page-catalog page-in-catalog special-offers-catalog">
		<div class="row">
			<div class="col-lg-12">
				<div class="sort-instruments">
					<div class="rubric-caption"><?= Yii::t('main', 'special_offers') ?></div>
					<div class="sel">
						<div class="selects" name="productssort">
							<p><?= Yii::t('main', 'sort_by') ?></p>
							<i class="icon-sort"></i>
							<select class="sort">
								<option class="hiden"></option>
								<option value="likes"><?= Yii::t('main', 'most_liked') ?></option>
								<option value="hight"><?= Yii::t('main', 'price_high_to_low') ?></option>
								<option value="low"><?= Yii::t('main', 'price_low_to_high') ?></option>
							</select>
						</div>
						<div class="selects" name="productscount">
							<p><?= Yii::t('main', 'view_by') ?></p>
							<i class="icon-view"></i>
							<select class="view">
							<option>12</option>
							<option>9</option>
							<option>6</option>
							</select>
						</div>
					</div>
					<form class="forma-input">
						<i class="icon-tag"></i>
						<div class="input-container">
							<input class="text-input floating-label products-tags" type="text" name="sample" value="<?= $products_tags ?>"/>
							<label for="sample"><?= Yii::t('main', 'tags') ?></label>
						</div>
					</form>
				</div>
				<div class="clearfix"></div>
				<div class="catalog-box">
					<div class="catalog-row">

					<?php foreach($products as $product) : ?>

						<div class="catalog-item">
							<div class="catalog-img-box">
								<a href="<?= Url::to(['/about/product/', 'id' => $product['id_prod']]) ?>">
									<img src="<?= Yii::getAlias('@products/'.$product['main_image']) ?>" alt="">
								</a>
								<div class="limited-offer">
									<p><b><?= Yii::t('main', 'limited_offer') ?></b> <br>save 30%</p>
								</div>
								<div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $product['likes'] ?></span></div>
								<div class="like-calc item-price">
									<i class="icon-label49"></i>
									<span><?= $product['price'] ?></span><br>
									<p class="last-price">3200$</p>
								</div>
							</div>
							<div class="catalog-caption">
								<div class="social-cont social-cont-so">
									<a class="main-like product-like icon-heart297<?php if(isset($product['like_status']) && $product['like_status'] === 'like'){echo ' active-social-button';} ?>" id="<?= $product['id_prod'] ?>" <?php if(\Yii::$app->user->isGuest) {echo "data-toggle='modal' data-target='#login'";} ?>></a>
									<a class="main-share icon-share-2 main-share-so"></a>
								</div>
								<div class="share-container">
									<i class="twitter-share"><a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $product['product_name'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>"></a></i>
									<i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $product['id_prod'] ?>" data-type="2" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>" data-image="<?= Url::home(true).'media/products/'.$product['main_image'] ?>" data-description="<?= $product['product_name'] ?>"></a></i>
								</div>

								<p><?= $product['product_name']; ?></p>
							</div>
						</div>

					<?php endforeach; ?>

				</div>
				<div class="bread-crumbs">
					<?= LinkPager::widget(['pagination' => $products_pagination,
						'disabledPageCssClass' => false,
						'nextPageLabel' => '',
						'prevPageLabel' => '',
						'options'=>['class'=>'hvr-radial-out'],
					]); ?>
				</div>
				</div>
			</div>
		</div>
		</section>
	<!--Begin style catalog section-->
	<section>
		<div class="container container-full-catalog container-style-catalog">
			<div class="row">
				<div class="col-lg-12 bordered-catalog">
					<hr class="border-section">
					<div class="sort-instruments">
						<div class="rubric-caption"><?= Yii::t('main', 'style') ?></div>
						<div class="sel">
							<div class="selects" name="stsort">
								<p><?= Yii::t('main', 'sort_by') ?></p>
								<i class="icon-sort"></i>
								<select class="sort">
									<option class="hiden"></option>
									<option value="likes"><?= Yii::t('main', 'most_liked') ?></option>
									<option value="views"><?= Yii::t('main', 'most_viewed') ?></option>
								</select>
							</div>
							<div class="selects" name="stcount">
								<p><?= Yii::t('main', 'view_by') ?></p>
								<i class="icon-view"></i>
								<select class="view">
									<option>12</option>
									<option>9</option>
									<option>6</option>
								</select>
							</div>
						</div>
						<form class="forma-input">
							<i class="icon-tag"></i>
							<div class="input-container">
								<input class="text-input floating-label stworks-tags" type="text" name="sample" value="<?= $stworks_tags ?>"/>
								<label for="sample"><?= Yii::t('main', 'tags') ?></label>
							</div>
						</form>
					</div>
					<div class="clearfix"></div>
					<div class="catalog-box">
						<div class="catalog-row">
							<?php foreach($st_works as $st_work) : ?>
								<?php $img = explode("|", $st_work['images']);?>
									<div class="catalog-item">
									<div class="catalog-img-box">
										<a href="<?= Url::to(['/about/user/', 'id' => $st_work['owner_id'], 'item' => $st_work['item_id']]); ?>"><img src="<?= Yii::getAlias('@portfolio/'.$img[0]); ?>" alt=""></a>
										<div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $st_work['likes'] ?></span></div>
									</div>
									<div class="catalog-caption">
										<div class="social-cont">
											<a class="main-like icon-heart297 <?php if(isset($st_work['like_status']) && $st_work['like_status'] === 'like'){echo ' active-social-button';} ?>" id="<?=$st_work['item_id']?>" <?php if(\Yii::$app->user->isGuest) {echo "data-toggle='modal' data-target='#login'";} ?>></a>
											<a class="main-share icon-share-2"></a>


										</div>

										<div class="share-container">
											<i class="twitter-share"><a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $st_work['title'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$st_work['item_id']]) ?>"></a></i>
											<i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $st_work['item_id'] ?>" data-type="1" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$st_work['item_id']]) ?>" data-image="<?= Url::home(true).'media/portfolio/'.$img[0] ?>" data-description="<?= $st_work['title'] ?>"></a></i>

										</div>

										<div class="clearfix"></div>
										<hr class="border-section">
										<p><?= $st_work['title'] ?></p>
									</div>
								</div>

							<?php endforeach; ?>

						</div>



						<div class="bread-crumbs">
							<?= LinkPager::widget(['pagination' => $st_works_pagination,
								'disabledPageCssClass' => false,
								'nextPageLabel' => '',
								'prevPageLabel' => '',
								'options'=>['class'=>'hvr-radial-out'],
							]); ?>
						</div>
						<div class="clearfix"></div>
						<hr class="border-section">
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--END style catalog section-->

	<!--End style catalog section-->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
				</div>
			</div>
		</div>
	</section>

	<!--END CONTENT-->

<?php

$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/main.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/browser.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jquery.uploadThumbs.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/lightbox.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);


?>