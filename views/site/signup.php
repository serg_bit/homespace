<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
/* @var $country \common\models\Country */
/* @var $language \frontend\models\Language */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;


$this->title = \Yii::t('titles', 'sign_up');
$this->params['breadcrumbs'][] = $this->title;

?>
<!--START CONTENT-->
<section class="sing-up-bg">
    <div class="container">
        <div class="row">
            <!--START CENTER-->
            <div class="col-lg-12 sing-up">
                <div class="content">
                    <h4><?= Yii::t('main', 'sign_up') ?></h4>
                    <?php $form = ActiveForm::begin([
                        'id' => 'form-signup',
                        'options' => [
                            'class'=> 'forma-input'
                        ],
                        'fieldConfig' => [
                            'template' => '{input}{label}',
                            'options' => [
                                'class' => 'input-container'
                            ],
                            'inputOptions' => [
                                'class' => 'text-input floating-label',
                            ],
                        ],

                    ]); ?>
                    <div class="input-edit">
                        <i class="icon-user168"></i>
                        <?= $form->field($model, 'first_name')->label(Yii::t('main', 'first_name')) ?>
                    </div>
                    <div class="input-edit">
                        <i class="icon-user168"></i>
                        <?= $form->field($model, 'last_name')->label(Yii::t('main', 'last_name')) ?>
                    </div>

                    <?php if($visibility == true){ ?>
                         <div class="input-edit">
                             <i class="icon-user168"></i>
                             <?= $form->field($model, 'company')->label(Yii::t('main', 'company')) ?>
                         </div>
                        <div class="input-edit ui-widget">
                            <i class="icon-home"></i>
                            <?= $form->field($model, 'specialization', [ 'inputOptions' => [ 'class' => 'text-input floating-label dirty ']])->dropDownList($specialization, ['prompt'=>Yii::t('main', 'specialization')])->label('') ?>
                        </div>
                    <?php } ?>

                    <div class="input-edit ui-widget">
                        <i class="icon-home"></i>
                        <?= $form->field($model, 'country', [ 'inputOptions' => [ 'class' => 'text-input floating-label dirty ']])->dropDownList($country, ['prompt'=>Yii::t('main', 'country')])->label('') ?>
                    </div>
                    <div class="input-edit">
                        <i class="icon-home"></i>
                        <?= $form->field($model, 'city')->label(Yii::t('main', 'city')) ?>
                    </div>
                    <div class="input-edit">
                        <i class="icon-symbol20"></i>
                        <?= $form->field($model, 'email')->label(Yii::t('main', 'email')) ?>
                    </div>
                    <div class="input-edit">
                        <i class="icon-id16"></i>
                        <?= $form->field($model, 'username')->label(Yii::t('main', 'login')) ?>
                    </div>
                    <div class="input-edit">
                        <i class="icon-locked59"></i>
                        <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('main', 'password')) ?>
                    </div>
                    <?= $form->field($model, 'roles')->hiddenInput(['value' => $role])->label(false); ?>

                    <div class="input-edit">
                        <?= $form->field($model, 'captcha')->widget(Captcha::className(), [
                            'options' => [
                                'class' => 'text-input floating-label'
                            ]
                        ])->label(Yii::t('main', 'captcha')) ?>
                    </div>
                    <div class="soc-sign">
                        <ul>
                            <li><a href="/site/auth?authclient=twitter" class="auth-link twitter"><span
                                        class="auth-icon twitter"></span><i class="icon-twitter1"></i></a></li>
                            <li><a href="/site/auth?authclient=facebook" class="auth-link facebook" data-popup-width="860"
                                   data-popup-height="480"><i class="icon-facebook55"></i></a></li>

                        </ul>

                    </div>
                        <div class="block-button">
                            <?= Html::submitButton(Yii::t('main', 'sign_up_but'), ['class' => 'button', 'name' => 'signup-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
            <!--END CENTER-->
        </div>
    </div>
</section>
<!--END CONTENT-->
<?php
$script_slider = <<< JS
      $('#form-signup input').each(function() {
         $(this).bind( "change", function() {
            if ($(this).val() != '') {
                $(this).addClass('dirty');
            } else {
                $(this).removeClass('dirty');
            }
         });

          $(this).addClass('dirty');
      });
JS;
$this->registerJs($script_slider, yii\web\View::POS_READY);
?>





