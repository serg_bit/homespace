<?php
use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;
use frontend\account\models\Catalog;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

$this->title = \Yii::t('titles', 'full_catalog');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CONTENT-->
    <section class="catalog-id" data-id="2">
        <div class="container container-style-catalog special-offers-catalog scrollSidebar1">
            <div class="row">
                <!--START CENTER-->
                <div class="col-lg-12 central-content page-catalog page-full-catalog">
                    <div class="content">
                        <div class="row">
                            <?php if(!Yii::$app->user->isGuest): ?>
                            <style>
                                .left-sidebar{
                                    padding-left: 0;
                                    padding-top: 0;

                                }
                                .catalog-item {
                                    width: 30% !important;
                                }

                                .catalog-item:nth-of-type(2n) {
                                    margin-right: 20px !important;
                                }

                                .catalog-item:nth-of-type(2n+1) {
                                    margin-right: 25px !important;
                                }

                                .sort-instruments .forma-input {
                                    width: 43% !important;
                                }
                                .share-container{
                                    margin-left: 20px;
                                }

                            </style>
                            <div class="col-lg-3 left-sidebar">
                                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                                <?= SideBar::widget();?>

                            </div>
                            <div class="col-lg-9 catalog-column catalog-instruments-column centralScroll1">
                                <?php else: ?>
                                <div class="col-lg-12 catalog-column catalog-instruments-column">
                                    <?php endif; ?>
                                    <div class="sort-instruments">
                                        <div class="sidebar-caption">Products<br></div>
                                        <div class="sel">
                                            <div class="selects stsort" name="stsort">
<!--                                                <p>--><?//= Yii::t('main', 'sort_by') ?><!--</p>-->
                                                <i class="icon-sort"></i>
                                                <select class="sort">
                                                    <?php foreach($sort_by as $sort): ?>
                                                        <option value="<?= $sort['value']?>"><?= $sort['title']?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="selects stcount" name="stcount">
<!--                                                <p>--><?//= Yii::t('main', 'view_by') ?><!--</p>-->
                                                <i class="icon-view"></i>
                                                <select class="view">
                                                    <?php foreach($view_by as $view): ?>
                                                        <option><?= $view ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <form class="forma-input projects-tags">
                                            <i class="icon-tag"></i>
                                            <div class="input-container">
                                                <input class="text-input floating-label" type="text" name="tags" value="<?= $tags ?>" />
                                                <label for="sample"><?= Yii::t('main', 'tags') ?></label>
                                            </div>
                                        </form>
                                    </div>
<!--                                    <div class="clearfix"></div>-->
<!--                                <div class="sort-menu">-->
<!--                                    <ul class="sort-categories">-->
<!--                                        <li><a href="#" data-value = "1" class="sort-teg --><?php //if ($category == '1'){ echo ' active-category'; } ?><!--">Furniture</a></li>-->
<!--                                        <li><a href="#" data-value = "2" class="sort-teg --><?php //if ($category == '2'){ echo ' active-category'; } ?><!--" >Bed & Bath</a></li>-->
<!--                                        <li><a href="#" data-value = "3" class="sort-teg --><?php //if ($category == '3'){ echo ' active-category'; } ?><!--" >Lighting</a></li>-->
<!--                                        <li><a href="#" data-value = "4" class="sort-teg --><?php //if ($category == '4'){ echo ' active-category'; } ?><!--" >Floor covering</a></li>-->
<!--                                        <li><a href="#" data-value = "5" class="sort-teg --><?php //if ($category == '5'){ echo ' active-category'; } ?><!--" >Wall decor</a></li>-->
<!--                                        <li><a href="#" data-value = "6" class="sort-teg --><?php //if ($category == '6'){ echo ' active-category'; } ?><!--" >Art & Decor</a></li>-->
<!--                                        <li><a href="#" data-value = "7" class="sort-teg --><?php //if ($category == '7'){ echo ' active-category'; } ?><!--" >Outdoor living</a></li>-->
<!--                                    </ul>-->
<!--                                </div>-->
                                <!--                            --><?php //Pjax::begin(); ?>
                                    <div class="clearfix"></div>
                                    <?php if($items['products']):?>
                                        <div class="catalog-box">
                                            <div class="catalog-row">
                                            <?php foreach($items['products'] as $product):?>
                                                    <div class="catalog-item">
                                                        <div class="catalog-img-box">
                                                            <a href="<?= Url::to(['/about/product/', 'id' => $product['id_prod']]) ?>">
                                                                <img src="<?= Yii::getAlias('@products/'.$product['main_image']) ?>" alt="">
                                                            </a>
<!--                                                            <p data-id="--><?//= $product['id_prod'] ?><!--" class="add-order add-to-my-projects"><i class="icon-books72"></i>Add to My Products</p>-->
                                                            <div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $product['likes'] ?></span></div>
                                                            <div class="like-calc item-price">
                                                                <i class="icon-label49"></i>
                                                                <span><?= $product['price'] ?>$</span><br>
                                                            </div>
                                                        </div>
                                                        <div class="catalog-caption">
                                                            <div class="social-cont social-cont-so">
                                                                <a class="main-like product-like icon-heart297"></a>
                                                                <a class="main-share icon-share-2 main-share-so" style="width: 14% !important;"></a>
                                                            </div>
                                                            <div class="share-container">
                                                                <i class="twitter-share"><a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $product['product_name'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>"></a></i>
                                                                <i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $product['id_prod'] ?>" data-type="2" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>" data-image="<?= Url::home(true)?>media/products/<?= $product['main_image'] ?>" data-description="<?= $product['product_name'] ?>"></a></i>
                                                            </div>

                                                            <p><?= $product['product_name'] ?></p>
                                                        </div>
                                                    </div>
                                            <?php endforeach; ?>

                                            </div>
                                            <div class="bread-crumbs">
                                                <?= LinkPager::widget(['pagination' => $items['pagination'],
                                                    'disabledPageCssClass' => false,
                                                    'nextPageLabel' => '',
                                                    'prevPageLabel' => '',
                                                    'options'=>['class'=>'hvr-radial-out1'],
                                                ]); ?>
                                            </div>
                                            <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
                                        </div>
                                    <?php endif; ?>
                            </div>
                        </div>

                    </div>
                    <!--END CENTER-->
                </div>
            </div>
    </section>
    <!--END CONTENT-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/lightbox.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/catalog.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);

?>
