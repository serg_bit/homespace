<?php

use yii\widgets\LinkPager;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use yii\helpers\Url;

$this->title = \Yii::t('titles', 'full_catalog');
$this->params['breadcrumbs'][] = $this->title;
?>


<!--START CONTENT-->
<section class="catalog-id" data-id="1">
    <div class="container container-style-catalog scrollSidebar1">
        <div class="row">
            <!--START CENTER-->
            <div class="col-lg-12 central-content page-catalog page-full-catalog">
                <div class="content">
                    <div class="row">
                        <?php if(!Yii::$app->user->isGuest): ?>
                        <style>
                            .left-sidebar{
                                padding-left: 0;
                                padding-top: 0;

                            }
                            .catalog-item {
                                width: 30% !important;
                            }

                            .catalog-item:nth-of-type(2n) {
                                margin-right: 20px !important;
                            }

                            .catalog-item:nth-of-type(2n+1) {
                                margin-right: 25px !important;
                            }

                            .forma-input{
                                display: none;
                            }

                            .sort-instruments .forma-input {
                                width: 43% !important;
                            }

                        </style>
                        <div class="col-lg-3 left-sidebar">
                            <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                            <?= SideBar::widget();?>
                        </div>
                        <div class="col-lg-9 catalog-column catalog-instruments-column centralScroll1">
                        <?php else: ?>
                        <div class="col-lg-12 catalog-column catalog-instruments-column">
                        <?php endif; ?>
                            <div class="sort-instruments">
                                <div class="sidebar-caption">Projects<br></div>
                                <div class="sel">
                                    <div class="selects stsort" name="stsort">
<!--                                        <p>--><?//= Yii::t('main', 'sort_by') ?><!--</p>-->
                                        <i class="icon-sort"></i>
                                        <select class="sort">
                                            <?php foreach($sort_by as $sort): ?>
                                                <option value="<?= $sort['value']?>"><?= $sort['title']?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <div class="selects stcount" name="stcount">
<!--                                        <p>--><?//= Yii::t('main', 'view_by') ?><!--</p>-->
                                        <i class="icon-view"></i>
                                        <select class="view">
                                            <?php foreach($view_by as $view): ?>
                                                <option><?= $view ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <form class="forma-input projects-tags">
                                    <i class="icon-tag"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="tags" value="<?= $tags ?>" />
                                        <label for="sample"><?= Yii::t('main', 'tags') ?></label>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            <div class="sort-menu">
                                <ul class="sort-categories action-categories">
                                    <li><a href="#" data-id="0" <?php if($cat_id == 0) echo 'class="active-category"'?>>Livestream</a></li>
                                    <?php foreach($categories as $category): ?>
                                        <li><a href="#" data-id="<?= $category['id'] ?>" <?php if($cat_id == $category['id']) echo 'class="active-category"'?>><?= ($lang == 'en') ? $category['title_en'] : $category['title_ru'] ?></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                            <div class="catalog-box">
                                <div class="catalog-row all-projects-catalog">
                                    <?php if($projects['works']):?>
                                        <?php foreach ($projects['works'] as $project) : ?>
                                            <?php $img = explode('|', $project['images']); ?>
                                            <div class="catalog-item">
                                                <div class="catalog-img-box">
                                                    <a href="<?= Url::to(['/about/item/', 'id' => $project['item_id']]) ?>"><img src="<?= Yii::getAlias('@portfolio/'.$img['0']); ?>" alt=""></a>
                                                    <?php if(!Yii::$app->user->isGuest): ?>
                                                    <p data-id="<?= $project['item_id'] ?>" class="add-order add-to-my-projects"><i class="icon-books72"></i>Add to My Projects</p>
                                                    <?php endif; ?>
                                                    <div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $project['likes'] ?></span></div>
                                                </div>
                                                <div class="catalog-caption">
                                                    <div class="social-cont projects-social">
                                                         <a class="main-like icon-heart297<?php if(isset($project['my_likes']) && $project['my_likes'] === '1'){echo ' active-social-button';} ?>" id="<?=$project['item_id']?>" <?php if(\Yii::$app->user->isGuest) {echo "data-toggle='modal' data-target='#login'";} ?>></a>
                                                        <a class="main-share icon-share-2"></a>
                                                    </div>
                                                    <div class="share-container projects-share-container">
                                                        <i class="twitter-share">
                                                <a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $project['title'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$project['item_id']]) ?>"></a></i>
                                            <i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $project['item_id'] ?>" data-type="1" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/item/'.$project['item_id']]) ?>" data-image="<?= Url::home(true).'media/portfolio/'.$img[0] ?>" data-description="<?= htmlspecialchars($project['short_description']) ?>"></a></i>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <hr class="border-section">
                                                    <p><?= $project['title'] ?></p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    <?php endif; ?>

                                </div>
                                <div class="bread-crumbs">
                                    <?= LinkPager::widget(['pagination' => $projects['pagination'],
                                        'disabledPageCssClass' => false,
                                        'nextPageLabel' => '',
                                        'prevPageLabel' => '',
                                        'options'=>['class'=>'hvr-radial-out1'],
                                    ]); ?>
                                </div>
                                <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!--END CENTER-->
        </div>
    </div>
</section>
<!--END CONTENT-->

<?php

$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/lightbox.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/catalog.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);


?>

