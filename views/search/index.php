<?php
$this->title = Yii::t('titles', 'search');
?>
<section>
    <div class="container">
        <div class="catalog-box">
            <div class="catalog-row">
                <br>
                <h1>Search</h1>
                <h2>Query: <?= $q ?></h2>
                <br>
                <hr class="border-section">
                <br>
                <?php if($search_items){ ?>
                    <?php foreach($search_items as $item){ ?>
                        <?php $images = explode('|', $item['images']) ?>
                        <div class="catalog-item">
                            <div class="catalog-img-box">
                                <a href="/about/item/<?= $item['item_id'] ?>"><img src="<?= Yii::getAlias('@portfolio/'.$images[0]); ?>" alt=""></a>
                                <div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $item['likes'] ?></span></div>
                            </div>
                            <div class="catalog-caption">
                                <div class="clearfix"></div>
                                <hr class="border-section">
                                <p><?= $item['title'] ?></p>
                            </div>
                        </div>

                    <?php } ?>
                <?php }else{ ?>
                    <h3>Not Found</h3>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!--END CONTENT-->