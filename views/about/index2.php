<?php

use frontend\widgets\Banner;
use yii\widgets\LinkPager;
use frontend\widgets\SideBar;

$this->title = Yii::t('titles', 'profile').$user_information['first_name'].' '.$user_information['last_name'].')';
$this->params['breadcrumbs'][] = $this->title;


?>
<!--START CONTENT-->
<section>
    <div class="container scrollSidebar">
        <div class="row">

            <!--START CENTER-->
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget();?>

            </div>
            <div class="col-lg-9 profile-content b2 central-content centralScroll">
                <div class="content about-profile">
                        <!-- Main information -->
                        <div class="profil pf-new" style="background-image: url('/media/profile/background/<?= $user_information['background'] ?>')" data-user="<?= $user_information['id'] ?>">
                            <div class="trasnparent-layer">
                                <?php if($user_information['company'] != ""):?>
                                    <div class="members-comp">
                                        <div class="prof-comp-logo">
                                            <img src="<?= Yii::getAlias('@avatar/'.$user_information['logo']) ?>" alt="">
                                        </div>
                                        <div class="members-list">
                                            <p><?= $user_information['company'] ?></p>
                                            <?php $workers = $user_information['employees']; ?>
                                            <?php $workers = explode(',',$user_information['employees']); ?>
                                            <ul>
                                                <?php foreach($workers as $worker): ?>
                                                    <?php if($worker != ""):?>
                                                        <li><a href="#"><?= $worker ?></a></li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="profile-info">
                                    <div class="left-side-prof">
                                        <?php if($click_action && !\Yii::$app->user->isGuest): ?>
                                            <div class="profil-photo photo-like">
                                                <img src="<?= Yii::getAlias('@avatar/'.$user_information['avatar']) ?>" alt="">
                                                <i class="icon-heart297 <?= $my_like ?>" id="<?= $user_information['id'] ?>"><p class="count-likes"><?= $user_information['likes'] ?></p></i>
                                            </div>
                                        <?php else : ?>
                                            <div class="profil-photo photo-like">
                                                <img src="<?= Yii::getAlias('@avatar/'.$user_information['avatar']) ?>" alt="">
                                                <i class="icon-heart297 icon-heart2971"><p><?= $user_information['likes'] ?></p></i>
                                            </div>
                                        <?php endif; ?>

                                        <div class="pr-name">
                                            <p class="pr-heading"><?= $user_information['first_name'] ?> <?= $user_information['last_name'] ?></p>
                                            <p><?= $user_information['specialization'] ?></p>
                                        </div>
                                        <div class="pr-name pr-follows">
                                            <span class="follow"><i class="icon-follow1"></i><?= $followers ?> <?= Yii::t('account', 'profile_followers') ?></span>
                                            <span class="follow"><i class="icon-follow"></i><?= $following ?> <?= Yii::t('account', 'Following') ?></span>
                                            <span class="follow"><i class="icon-telephone46"></i><?= $user_information['phone'] ?></span>
                                            <span class="follow"><i class="icon-home"></i><?= $user_information['city'] ?></span>
                                            <span class="follow"><i class="icon-webpage2"></i><a href="//<?= $user_information['link_user_site'] ?>"><?= $user_information['link_user_site'] ?></a></span>
                                        </div>
                                    </div>
                                    <div class="right-side-prof">
                                        <div class="quote">
                                            <p><?= $user_information['message'] ?></p>
                                        </div>
                                        <div class="un-prof-line clearfix">
                                            <div class="soc-profile">
                                                <ul>
                                                    <?php if($user_information['link_fb'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_fb'] ?>" target="_blank" ><i class="icon-facebook55"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_tw'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_tw'] ?>" target="_blank" ><i class="icon-twitter1"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_inst'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_inst'] ?>" target="_blank" ><i class="icon-instagram12"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_behance'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_behance'] ?>" target="_blank" ><i class="icon-behance2"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_google'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_google'] ?>" target="_blank" ><i class="icon-google116"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_pint'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_pint'] ?>" target="_blank" ><i class="icon-pinterest3"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_tumblr'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_tumblr'] ?>" target="_blank" ><i class="icon-logotype1"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_linkedin'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_linkedin'] ?>" target="_blank" ><i class="icon-id16"></i></a></li>
                                                    <?php } ?>
                                                    <?php if($user_information['link_blg'] != ""){?>
                                                        <li><a href="//<?= $user_information['link_blg'] ?>" target="_blank" ><i class="icon-blogger8"></i></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                            <?php if($click_action){ ?>
                                                <div class="message send-chat-message">
                                                    <i class="icon-black218"></i><a href="#" class="message"><?= Yii::t('account', 'profile_message') ?></a>
                                                </div>
                                            <?php } ?>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- End Main Information -->
                        <hr class="sline">
                        <!--  Portfolio  -->
                        <?php if($projects){?>
                            <div class="portfolio-top b2">
                                <h4 class="promote"><?= Yii::t('account', 'my_items') ?></h4>

                                <div class="img-item">
                                    <a href="<?= $projects[0]['url'] ?>"><img src="<?= $projects[0]['image'] ?>" alt="<?= $portfolio[0]['title'] ?>"></a>
                                    <p><?= $projects[0]['title'] ?></p>
                                </div>
                                <h5><i class="icon-1"></i><?= Yii::t('account', 'about') ?></h5>
                                <p class="description-item"><?= $projects[0]['short_description'] ?></p>
                            </div>

                            <?php foreach ($projects as $item) {?>

                                <div class="about-user-item">
                                    <div class="profil-img" style="cursor:pointer;" data-link="<?= $item['url'] ?>" data-description="<?= $item['short_description'] ?>">
                                        <img src="<?= $item['image'] ?>" alt="<?= $item['title'] ?>">
                                        <div class="like-calc"><i class="icon-heart2971"></i><span class="count-likes"><?= $item['likes'] ?></span></div>
                                    </div>
                                    <div class="catalog-caption">
                                        <div class="social-cont share-social">
                                            <a class="main-like icon-heart297<?php if(isset($item['my_likes']) && $item['my_likes'] === '1'){echo ' active-social-button';} ?>" id="<?=$item['id']?>" <?php if(\Yii::$app->user->isGuest) {echo "data-toggle='modal' data-target='#login'";} ?>></a>
                                            <a class="main-share icon-share-2"></a>
                                        </div>
                                        <div class="share-container">
                                            <i class="twitter-share">
                                                <a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $item['title'] ?>&url=<?= $item['share_link'] ?>"></a></i>
                                            <i class="facebook-share"><a class="share-it-now-fb" href="#" data-id="<?= $item['id'] ?>" data-type="1" data-url="<?= $item['share_link'] ?>" data-image="<?= $item['share_img'] ?>" data-description="<?= $item['short_description'] ?>"></a></i>

                                        </div>
                                        <div class="clearfix"></div>
                                        <hr class="border-section">
                                        <p><?= $item['title'] ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                            <hr class="sline">
                        <?php } ?>
                        <!--  End Portfolio  -->

                        <!-- Honours -->
                        <?php if($honours){?>
                            <div class="portfolio-slider about-honours-slider">
                                <h4 class="promote"><?= Yii::t('main', 'honors') ?></h4>
                                <div id="owl-demo" class="honours-slider">
                                    <?php foreach($honours as $honour) : ?>
                                        <div class="item">
                                            <a href="<?= $honour['image'] ?>" ><img src="<?= $honour['image'] ?>" alt=""></a>
                                            <p><?= $honour['name'] ?></p>
                                        </div>
                                    <?php endforeach; ?>

                                </div>
                                <br>
                            </div>
                        <?php } ?>
                        <!-- End Honours -->

                        <!-- Licenses -->

                        <?php if($licenses){?>
                            <div class="licenses">
                                <h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_license') ?></h4>
                                <div class="cliearfix"></div>
                                <?php foreach ($licenses as $license) {?>
                                    <div class="licenses-item profile-licenses-item">
                                        <?php $images = explode('|', $license['images']); ?>

                                        <div class="licenses-imgs lb-container">
                                            <?php foreach ($images as $image) {?>
                                                <?php if($image != ""){?>
                                                    <a href="<?= $image ?>"><img src="<?= $image ?>" alt=""></a>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                        <div class="licenses-content">
                                            <h5><b>&nbsp<?= Yii::t('account', 'profile_license_number') ?>:</b><br>&nbsp&nbsp<?= $license['number'] ?></h5>
                                            <p><b>&nbsp<?= Yii::t('account', 'profile_license_issued') ?>:</b><br>&nbsp&nbsp<?= $license['issued_by'] ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>

                            <hr class="sline">

                        <?php } ?>
                        <!-- End Licenses -->

                        <!-- Reviews -->
                        <div class="reviews">
                            <?php if($reviews['reviews']):?>
                                <h4 class="promote"><?= Yii::t('main', 'reviews') ?></h4>
                            <?php endif; ?>
                            <div class="reviews-block">
                                <?php if($reviews['reviews']){?>
                                    <?php $first = array_shift($reviews['reviews']); ?>
                                    <div class="reviews-comment">
                                        <div class="reviews-photo new-review-photo">
                                            <a href="<?= $first['link'] ?>"><img src="<?= Yii::getAlias('@avatar/'. $first['image']); ?>" alt=""></a>
                                            <p class="name"><?= $first['author'] ?></p>
                                        </div>
                                        <p><?= $first['comment'] ?></p>
                                        <p class="data"><?= $first['date'] ?></p>
                                    </div>
                                    <i class="icon-arrow487 oppen-comments" style="cursor: pointer;"></i>
                                    <div class="all-reviews-comment">
                                        <?php foreach($reviews['reviews'] as $review) : ?>
                                            <div class="reviews-comment">
                                                <div class="reviews-photo new-review-photo">
                                                    <a href="<?= $review['link'] ?>"><img src="<?= Yii::getAlias('@avatar/'. $review['image']); ?>" alt=""></a>
                                                    <p class="name"><?= $review['author'] ?></p>
                                                </div>
                                                <p><?= $review['comment'] ?></p>
                                                <p class="data"><?= $review['date'] ?></p>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <br>
                                    <div class="bread-crumbs">
                                        <?= LinkPager::widget([
                                            'pagination' => $reviews['pagination'],
                                            'disabledPageCssClass' => false,
                                            'nextPageLabel' => '',
                                            'prevPageLabel' => '',
                                            'options'=>['class'=>'hvr-radial-out'], ]) ?>
                                    </div>
                                    <br>
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <?php if($click_action){ ?>
                                <div class="reviews-comment">
                                    <form action="">
                                        <textarea placeholder="<?= Yii::t('account', 'profile_reviews_place') ?>" id="comment"></textarea>
                                        <?php if(\Yii::$app->user->isGuest){ ?>
                                            <input type="submit" value="<?= Yii::t('account', 'send') ?>" class="button" id="guest-comment-send">
                                        <?php }else{ ?>
                                            <input type="submit" value="<?= Yii::t('account', 'send') ?>" class="button" id="comment-send">
                                        <?php } ?>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>

                        <!-- End Reviews -->

                        <!-- Videos -->
                        <?php if($videos){?>
                            <div class="video">
                                <h4 class="promote"><?= Yii::t('main', 'video') ?></h4>

                                <?php foreach($videos as $video) : ?>
                                    <p><i class="icon-videoplayer5"></i><?= $video['name'] ?></p>
                                    <iframe width="100%" height="360" src="<?= $video['link'] ?>" frameborder="0" allowfullscreen></iframe>
                                <?php endforeach; ?>

                            </div>
                        <?php } ?>
                        <!-- End Videos -->

                        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>

                    </div>
                </div>
                <!--END CENTER-->

            </div>
        </div>
</section>
<!--END CONTENT-->


<!--Вызов модального окна для переписки-->


<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jquery.magnific-popup.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/new-js.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);
$script_honours = <<< JS
      $("#owl-demo").owlCarousel({
            items: 3,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            navigationText: false,
            navigation: true,
            pagination: false

      });

      $('.lb-container').each(function(){
          $(this).magnificPopup({
              delegate: 'a',
              type: 'image',
              mainClass: 'mfp-img-mobile',
              gallery: {
                  enabled: true,
                  navigateByImgClick: true
              },
          });
      });

      $('.honours-slider').each(function(){
          $(this).magnificPopup({
              delegate: 'a',
              type: 'image',
              mainClass: 'mfp-img-mobile',
              gallery: {
                  enabled: true,
                  navigateByImgClick: true
              },
          });
      });

      $('.send-chat-message').trigger('click');
JS;
$this->registerJs($script_honours, yii\web\View::POS_READY);

?>

<?php if(\Yii::$app->user->isGuest): ?>
    <!--START CHAT-->
    <div class="chat-block">
        <div class="chat">
            <div class="name">Message</div>

            <a href="#" class="roll-up"><i class="icon-11"></i></a>
            <a href="#" class="close"><i class="icon-cancel30"></i></a>
            <div class="message-content"  id="container">
                <div class="correspondence-chat-list">

                </div>
                <p class="send"><textarea class="message-input" placeholder="Type message..." ></textarea></p>
            </div>

        </div>
    </div>
    <!--END CHAT-->
    <?php
    $script_chat = <<< JS
	$('.chat-block textarea').keypress(function(e){
    	if(e.keyCode == 13) {
    		$('.chat-block .message-input').val('');
			$('.how_it_words').modal('show')
    	}
    });

    $('.send-chat-message').click(function(e){
		e.preventDefault();
		$('.chat-block').css('display', 'block');
    });

    $('.chat-block .close').click(function(e){
		e.preventDefault();
		$('.chat-block').css('display', 'none');
    });


JS;
    $this->registerJs($script_chat, yii\web\View::POS_READY);

    ?>
<?php endif; ?>
