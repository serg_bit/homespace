<?php
use frontend\widgets\Banner;
use yii\helpers\Url;
use frontend\widgets\SideBar;

$this->title = ' | '.$product['product_name'];
$this->params['breadcrumbs'][] = $this->title;


?>

<section>
    <div class="container scrollSidebar">
        <div class="row">
            <!--START LEFT SIDEBAR-->
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget();?>

            </div>
            <!--END LEFT SIDEBAR-->
            <!--START CENTER-->
            <div class="col-lg-7 central-content page-catalog centralScroll">
                <div class="content">
                    <div class="row">
                        <div class="col-lg-12 clear">
                        <?php if($product):?>
                            <div class="img-gal">
                                <div class="featured">
                                    <img src="<?= Yii::getAlias('@products/'.$product['main_image']) ?>" alt="">
                                </div>
                                <?php $images = explode(",", $product['images'])?>
                                <div class="mini-photo thumbnails">
                                    <img src="<?= Yii::getAlias('@products/'.$product['main_image']) ?>" alt="" class="selected-img">
                                <?php foreach($images as $img): ?>
                                    <?php if($img != ""): ?>
                                        <img src="<?= Yii::getAlias('@products/'.$img) ?>" alt="" class="selected-img">
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="produkt-content">
                                <img src="/images/ikea.png" alt="" class="logo">
                                <h4 class="name">Vivamus fring</h4>
                                <h5 class="collection">Collection: <span>Furniture</span></h5>
                                <h4 class="name">Vivamus fringilla elementum</h4>
                                <i class="price icon-label49"><?= $product['price'] ?>$<span>3200$</span></i>
                                <div class="clearfix"></div>
                                <div class="social-cont">
                                    <a class="main-like icon-heart297"></a><span><?= $product['likes'] ?></span>
                                    <a class="main-share icon-share-2"></a>
                                </div>
                                <div style="opacity: 0;" class="share-container product-share-container">
                                    <a class="share-it-now-tw" href="https://twitter.com/intent/tweet?text=<?= $product['product_name'] ?>&url=<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>"></a>
                                    <a class="share-it-now-fb" href="#" data-id="<?= $product['id_prod'] ?>" data-type="2" data-url="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>" data-image="<?= Url::home(true).'media/products/'.$product['main_image'] ?>" data-description="<?= $product['product_name'] ?>"></a>
                                </div>

                                <div class="hashtag">
                                    <a href="#">#lorem1</a>
                                    <a href="#">#ipsum</a>
                                    <a href="#">#dolor</a>
                                    <a href="#">#sit</a>
                                    <a href="#">#consectetuer</a>
                                    <a href="#">#adipiscing</a>
                                    <a href="#">vivamus</a>
                                    <a href="#">#fringilla</a>
                                    <a href="#">#lacus</a>
                                    <a href="#">#eleum</a>
                                </div>
                                <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus fringilla lacus elementum vestibulum gravida. Interdum et malesuada fames ac ante ipsum primis in faucibus. Integer mattis nisi nibh, vitae viverra nibh dictum hendrerit. Fusce ut sodales metus. Vestibulum vulputate velit sit amet est pellentesque suscipit. Aliquam dignissim nisi nec gravida egestas. In bibendum venenatis risus et fringilla. Vestibulum vulputate velit sit amet est pellentesque suscipit. Aliquam dignissim nisi nec gravida egestas  bibendum  rfringilla.</p>
                                <a href="#" class="button order"><i class="icon-books72"></i>Add to order</a>

                                <hr class="sline">
                            </div>
                            <div class="produkt-comment">
                                <ul role="tablist">
                                    <li role="presentation" class="active"><a href="#Comments" aria-controls="Comments" role="tab" data-toggle="tab">Comments</a></li>
                                    <li role="presentation"><a href="#Facebook" aria-controls="Facebook" role="tab" data-toggle="tab">Facebook</a></li>
                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="Comments">
                                        <textarea name="#" id="#" placeholder="Your comment..."></textarea>
                                        <input type="submit" value="Sent">
                                        <hr class="sline">
                                        <div class="reviews-comment">
                                            <div class="reviews-photo">
                                                <img src="/images/team1.png" alt="">
                                                <p class="name">Sam Calduyibcdia<span>Designer</span></p>
                                            </div>
                                            <p>Loremeros, dignissim nec felis quis, lobortis tempus ligula. Nulla eget dolor accumsan, ornare urna vel, vestibulum velit. Proin et ex eu metus aliquet facilisis. Leo id semper congue, velit leo luctus ligula, vel vehicula lectus neque tempor mauris. Duis ac sagittis eros. Praesent lacinia, orci imperdiet, massa nisl posuere velit, eget ullamcorper ipsum magna eget arcu. Aenean sodales neque vite congue bibendum.</p>
                                            <p class="data">01.02.2016</p>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Facebook">
                                        <div class="fb-comments" data-href="<?= Yii::$app->urlManager->createAbsoluteUrl(['about/product/'.$product['id_prod']]) ?>" data-numposts="5" data-width="100%"></div>
                                    </div>

                                    <div class="showrooms">
                                        <h4>Suppliers & Showrooms</h4>
                                        <a href="#"><img src="<?= Yii::getAlias('@products/showrooms.png') ?>" alt="#"></a>
                                        <a href="#"><img src="<?= Yii::getAlias('@products/showrooms.png') ?>" alt="#"></a>
                                        <a href="#"><img src="<?= Yii::getAlias('@products/showrooms.png') ?>" alt="#"></a>
                                    </div>

                                </div>
                            </div>
                        <?php else: ?>
                            <h3>Not found</h3>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
            </div>
            <!--END CENTER-->
            <!--START RIGHT SIDEBAR-->
            <div class="col-lg-2 right-sidebar">
                <?= Banner::widget(['position' => 'right']);?>
            </div>
            <!--END RIGHT SIDEBAR-->
        </div>
    </div>
</section>
<!--END CONTENT-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/jscript.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);
$script_product = <<< JS
	$('.thumbnails img').click(function() {
			$(".featured img").fadeOut(0);
			$('.featured img').attr('src', $(this).attr('src')).fadeIn(500);
	});
JS;
$this->registerJs($script_product, yii\web\View::POS_READY);

?>