<?php
use frontend\widgets\Banner;
use yii\helpers\Url;
use frontend\widgets\SideBar;

$this->title = ' | '.$item['title'];
$this->params['breadcrumbs'][] = $this->title;


?>

<!--START CONTENT-->

<section>
    <div class="container scrollSidebar">
        <div class="row">
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget();?>
            </div>
            <!--START CENTER-->
            <div class="col-lg-9  central-content about-content about-full centralScroll">
                <div class="content">
                    <!-- Item -->
                    <?php if($item){ ?>

                    <div class="row">
                        <div class="sidebar-caption"><?= $item['title'] ?><br></div>
                        <p class="auth">
                            <a href="<?= $item['user_link'] ?>"><?= $item['user'] ?></a>
                            <span>, <?= Yii::t('main', 'role_designer') ?></span>
                        </p>
                    </div>
                    <div class="row row-portfolio">
                        <div class="about_portfolio-images">
                            <?php foreach ($item['images'] as $image) { ?>
                                <a href="<?= $image ?>" data-lightbox="roadtrip" style=background-image:url(<?= $image ?>)></a>
                            <?php } ?>

                        </div>
                        <div class="clearfix"></div>
                        <div class="about_portfolio-caption">
                            <p class="about-date"><?= $item['date'] ?></p>
                            <div class="hashtag agent-hashtag-project">
                                <?php foreach ($item['tags'] as $tag) { ?>
                                    <?= $tag ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="portfolio-article">
                            <?= $item['description'] ?>
                        </div>
                    </div>
                    <?php }else{ ?>
                        <h1><?= Yii::t('account', 'not_found') ?></h1>
                    <?php } ?>
                    <!-- End Item -->
                </div>
                <?php if($similar_works):?>
                <br>
                <hr class="border-section">
                <div class="rubric-caption">Similar works</div>
                <div class="catalog-box">
                    <div class="catalog-row">
                        <?php foreach($similar_works as $work): ?>
                            <?php $img = explode('|', $work['images']); ?>
                            <a href="<?= Url::to(['about/item', 'id' => $work['item_id']]); ?>">
                                <div class="catalog-item similar-item">
                                    <div class="catalog-img-box">
                                        <img src="<?= Yii::getAlias('@portfolio/'.$img[0]); ?>" alt="">
                                        <div class="like-calc"><i class="icon-heart2971"></i><span><?= $work['likes'] ?></span></div>
                                    </div>
                                </div>
                            </a>
                        <?php endforeach; ?>
                    </div>
                    <hr class="border-section">
                </div>
                <?php endif; ?>
                <?php if($user_styles):?>
                    <div class="rubric-caption">Style</div>
                    <div class="catalog-box">
                        <div class="catalog-row">
                            <?php foreach($user_styles as $work): ?>
                                <?php $img = explode('|', $work['images']); ?>
                                <a href="<?= Url::to(['about/item', 'id' => $work['item_id']]); ?>">
                                    <div class="catalog-item similar-item">
                                        <div class="catalog-img-box">
                                            <img src="<?= Yii::getAlias('@portfolio/'.$img[0]); ?>" alt="">
                                            <div class="like-calc"><i class="icon-heart2971"></i><span><?= $work['likes'] ?></span></div>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            <!--END CENTER-->

        </div>
    </div>
</section>

<!--END CONTENT-->

<?php
$this->registerJsFile('js/jquery.magnific-popup.min.js', ['depends'=>'frontend\assets\AppAsset']);
$script = <<< JS
    $('.about_portfolio-images').magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true
		},
	});
JS;
$this->registerJs($script, yii\web\View::POS_READY);

?>

