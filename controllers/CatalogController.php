<?php

namespace frontend\controllers;

use Yii;
use frontend\account\models\ProductsList;
use yii\data\Pagination;
use frontend\models\Language;
use frontend\models\CatalogCategories;
use frontend\account\models\Portfolio;
use frontend\account\models\LikeIt;

class CatalogController extends \yii\web\Controller{
    private $user_id = 0;

    public function init(){
        session_start();
        $this->user_id = Yii::$app->user->identity->id;
    }

    public function actionProjects()
    {
        if(isset($_POST['cat_id'])){
            $_SESSION['projects_category_id'] = $_POST['cat_id'];
        }

        $sort_by = (isset($_SESSION['catalog_sort_by'])) ? $_SESSION['catalog_sort_by']  : 'views';
        $view_by = (isset($_SESSION['catalog_view_by'])) ? $_SESSION['catalog_view_by']  : 9;
        if($_GET['tags']){
            $tags = $_GET['tags'];
            unset($_SESSION['projects_category_id']);
        }else $tags = "";

        if($sort_by == 'likes'){
            $sort[0] = [
                'value' => "likes",
                'title' => Yii::t('main', 'most_liked')
            ];
            $sort[1] = [
                'value' => "views",
                'title' => Yii::t('main', 'most_viewed')
            ];
        }else{
            $sort[0] = [
                'value' => "views",
                'title' => Yii::t('main', 'most_viewed')
            ];
            $sort[1] = [
                'value' => "likes",
                'title' => Yii::t('main', 'most_liked')
            ];
        }

        $view = [$view_by, 9, 12, 24];

        for($i = 1; $i < count($view); $i++){
            if($view[$i] == $view_by)
                unset($view[$i]);
        }

        if(isset($_SESSION['projects_category_id'])) $cat_id = $_SESSION['projects_category_id']; else $cat_id = 0;

        return $this->render('projects',[
            'projects' => $this->getAllProjects($tags, $sort_by, $view_by, $cat_id),
            'categories' => CatalogCategories::find()->where(['param' => 0])->all(),
            'tags' => $tags,
            'sort_by' => $sort,
            'view_by' => $view,
            'lang' => Language::getCurrent()->url,
            'cat_id' => $cat_id
        ]);
    }

    public function actionProducts()
    {
        $sort_by = (isset($_SESSION['catalog_sort_by'])) ? $_SESSION['catalog_sort_by']  : 'views';
        $view_by = (isset($_SESSION['catalog_view_by'])) ? $_SESSION['catalog_view_by']  : 9;
        $tags = (isset($_GET['tags'])) ? $_GET['tags'] : "";

        if($sort_by == 'likes'){
            $sort[0] = [
                'value' => "likes",
                'title' => Yii::t('main', 'most_liked')
            ];
            $sort[1] = [
                'value' => "views",
                'title' => Yii::t('main', 'most_viewed')
            ];
        }else{
            $sort[0] = [
                'value' => "views",
                'title' => Yii::t('main', 'most_viewed')
            ];
            $sort[1] = [
                'value' => "likes",
                'title' => Yii::t('main', 'most_liked')
            ];
        }

        $view = [$view_by, 9, 12, 24];

        for($i = 1; $i < count($view); $i++){
            if($view[$i] == $view_by)
                unset($view[$i]);
        }


        return $this->render('products', [
            'items' => $this->getAllProducts($tags, $sort_by, $view_by),
            'tags' => $tags,
            'sort_by' => $sort,
            'view_by' => $view,
            'lang' => Language::getCurrent()->url,
        ]);

    }

    public function actionDecor(){

        $sort_by = (isset($_SESSION['catalog_sort_by'])) ? $_SESSION['catalog_sort_by']  : 'views';
        $view_by = (isset($_SESSION['catalog_view_by'])) ? $_SESSION['catalog_view_by']  : 9;
        $tags = (isset($_GET['tags'])) ? $_GET['tags'] : "";

        if($sort_by == 'likes'){
            $sort[0] = [
                'value' => "likes",
                'title' => Yii::t('main', 'most_liked')
            ];
            $sort[1] = [
                'value' => "views",
                'title' => Yii::t('main', 'most_viewed')
            ];
        }else{
            $sort[0] = [
                'value' => "views",
                'title' => Yii::t('main', 'most_viewed')
            ];
            $sort[1] = [
                'value' => "likes",
                'title' => Yii::t('main', 'most_liked')
            ];
        }

        $view = [$view_by, 9, 12, 24];

        for($i = 1; $i < count($view); $i++){
            if($view[$i] == $view_by)
                unset($view[$i]);
        }


        return $this->render('decor',[
            'projects' => $this->getDesignDecor($tags, $sort_by, $view_by),
            'tags' => $tags,
            'sort_by' => $sort,
            'view_by' => $view,
            'lang' => Language::getCurrent()->url,
        ]);
    }

    public function actionChangeSort(){

        if($_POST['parent_class'] == 'stsort') {
            $_SESSION['catalog_sort_by'] = $_POST['value'];
        }

        if($_POST['parent_class'] == 'stcount'){
            $_SESSION['catalog_view_by'] = $_POST['value'];
        }

    }

    public function actionAddToLikeIt()
    {
        $item_id = Yii::$app->request->post('id', false);
        if($item_id){
            $folder = Yii::$app->request->post('folder', false);
            LikeIt::addMyItem($item_id, $folder, $this->user_id);
            echo 'Add to Like It';
        }
    }

    public function getAllProjects($tags, $sort_by, $view_by, $cat_id){
        if($tags){
            $count = Portfolio::countProjects(Language::getCurrent()->url, $tags);
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => $count[0]['cnt']
            ]);
        }else{
            if($cat_id != 0){
                $pagination = new Pagination([
                    'defaultPageSize' => $view_by,
                    'totalCount' => Portfolio::find()->where(['active' => 1, 'moderation' => 1, 'cat_id' => $cat_id])->count(),
                ]);
            }else
                $pagination = new Pagination([
                    'defaultPageSize' => $view_by,
                    'totalCount' => Portfolio::find()->where(['active' => 1, 'moderation' => 1])->count(),
                ]);
        }


        if(Yii::$app->user->identity->id){
            $u_id = Yii::$app->user->identity->id;
        }else $u_id = 0;

        $works = Portfolio::catalogProjects(Language::getCurrent()->url, $pagination->offset, $pagination->limit, $u_id, $sort_by, $tags, $cat_id);

        return array('works' => $works, 'pagination' => $pagination);

    }

    public function getDesignDecor($tags, $sort_by, $view_by){
        if($tags){
            $count = Portfolio::countDesignDecor(Language::getCurrent()->url, $tags);
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => $count[0]['cnt']
            ]);
        }else{
                $pagination = new Pagination([
                    'defaultPageSize' => $view_by,
                    'totalCount' => Portfolio::find()->where(['active' => 1, 'moderation' => 1, 'catalog_id' => 1])->count(),
                ]);
        }


        if(Yii::$app->user->identity->id){
            $u_id = Yii::$app->user->identity->id;
        }else $u_id = 0;

        $works = Portfolio::catalogDesignDecor(Language::getCurrent()->url, $pagination->offset, $pagination->limit, $u_id, $sort_by, $tags);

        return array('works' => $works, 'pagination' => $pagination);

    }

    public function getAllProducts($tags, $sort_by, $view_by){
        if($tags){
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => ProductsList::find()->andFilterWhere(['like', 'meta_tags', $tags])->count(),
            ]);

            $products = ProductsList::find()->andFilterWhere(['like', 'meta_tags', $tags])->orderBy([$sort_by => SORT_DESC])->offset($pagination->offset)->limit($pagination->limit)->all();
        }else{
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => ProductsList::find()->count(),
            ]);

            $products = ProductsList::find()->orderBy([$sort_by => SORT_DESC])->offset($pagination->offset)->limit($pagination->limit)->all();
        }

        return ['products' => $products, 'pagination' => $pagination];
    }


}