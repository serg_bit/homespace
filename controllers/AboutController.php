<?php

namespace frontend\controllers;

use yii;
use frontend\models\PortfolioCollections;
use frontend\account\models\User;
use frontend\models\UserHonours;
use frontend\models\UserVideos;
use frontend\models\UserLicenses;
use yii\data\Pagination;
use frontend\models\ProfileReviews;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\models\Language;
use frontend\models\ReviewsLimit;
use yii\web\Controller;
use frontend\models\Likes;
use yii\helpers\Url;
use frontend\account\models\ProductsList;

class AboutController extends Controller
{
    private $user_id;
    private $user;
    private $user2;
    private $moderation = 1;
    private $language = "ru";
    private $click_action = false;


    public function init()
    {
        $this->user_id = $_GET['id'];
        $this->language = Language::getCurrent()->url;
        $this->user = User::find()->where(['id' => $this->user_id])->one();
        $this->user2 = User::find()->where(['id' => \Yii::$app->user->identity->id])->one();

        if ($this->user_id != $this->user2->id) {
            $this->click_action = true;
        }

    }

    public function actionIndex()
    {
        return $this->render('about');
    }

    public function actionUser()
    {
        if (!$this->user_id || empty($this->user)) {
            return $this->render('error', [
                'error' => 'Пользователя не существует!'
            ]);

        } else {

            $my_like = Likes::find()->where(['user_id' => Yii::$app->user->identity->id, 'object_id' => $this->user_id, 'category' => 3])->one();
            if ($my_like) {
                $my_like = 'icon-heart2971';
            } else $my_like = '';

            if ($this->user->paid_status == 0) {
                return $this->render('index', [
                    'projects' => $this->getUserItems($this->user_id, $this->language, $this->moderation),
                    'my_like' => $my_like,
                    'followers' => 0,
                    'following' => 0,
                    'user_information' => $this->user,
                    'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                    'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                    'reviews' => $this->getUserReviews($this->user_id),
                    'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
                    'click_action' => $this->click_action,
                ]);
            } else {
                return $this->render('index2', [
                    'projects' => $this->getUserItems($this->user_id, $this->language, $this->moderation),
                    'my_like' => $my_like,
                    'followers' => 0,
                    'following' => 0,
                    'user_information' => $this->user,
                    'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                    'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                    'reviews' => $this->getUserReviews($this->user_id),
                    'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                    'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
                    'click_action' => $this->click_action,
                ]);
            }
        }
    }

    public function actionItem()
    {
        $item_id = isset($_GET["id"]) ? $_GET["id"] : 0;
        if ($item_id != 0) {

            $portfolio = Portfolio::findOne(['item_id' => $item_id]);
            $portfolio->views = $portfolio->views + 1;
            $portfolio->update();

        }


        return $this->render('item', [
            'item' => $this->getDesignerItem($item_id, $this->language),
            'similar_works' => Portfolio::rand3Works($item_id),
            'user_styles' => Portfolio::userStyles($item_id),
        ]);

    }

    public function actionProduct()
    {
        $item_id = Yii::$app->request->get('id', 0);
        $product = false;
        if ($item_id != 0) {
            $product = ProductsList::findOne(['id_prod' => $item_id]);
            $product->views = $product->views + 1;
            $product->update();

        }

        return $this->render('product', [
            'product' => $product
        ]);

    }

    //    Ajax
    public function actionComment()
    {
        $comment = new ProfileReviews();
        $comment_text = nl2br(trim($_POST['comment']));
        $data['check'] = ReviewsLimit::checkReviews(\Yii::$app->user->identity->id, 3);
        if ($data['check'] == true) {
            $comment->addComment($comment_text, $_POST['recipient'], \Yii::$app->user->identity->id);
            $data['comment'] = $comment_text;
            $data['date'] = date('j.m.y; H:i', time());
            $data['author'] = $this->user2->first_name . ' ' . $this->user2->last_name;
            $data['avatar'] = Yii::getAlias('@avatar/' . $this->user2->avatar);
        }
        $data = json_encode($data);


        return $data;
    }

    public function actionLikes()
    {
        if (Likes::find()->where(['object_id' => $_REQUEST['id'], 'category' => 3, 'user_id' => \Yii::$app->user->identity->id])->one()) {

            Likes::deleteAll(['object_id' => $_REQUEST['id'], 'category' => 3, 'user_id' => \Yii::$app->user->identity->id]);
            $user_likes = User::findOne(['id' => $_REQUEST['id']]);
            $user_likes->likes = $user_likes->likes - 1;
            $count_likes = $user_likes->likes;
            $user_likes->update();
        } else {
            $like = new Likes;
            $like->object_id = $_REQUEST['id'];
            $like->user_id = \Yii::$app->user->identity->id;
            $like->category = 3;
            $like->save();
            $user_likes = User::findOne(['id' => $_REQUEST['id']]);
            $user_likes->likes = $user_likes->likes + 1;
            $count_likes = $user_likes->likes;
            $user_likes->update();
        }
        return $count_likes;

    }

    // All get functions
    public function getUserItems($user_id, $lang, $moderation, $catalog_id = false)
    {
        $current_id = Yii::$app->request->get('item', false);
        if ($current_id)
            $count = 1;
        else $count = 0;
        $portfolio = array();
        if ($catalog_id) {
            $temp = Portfolio::find()->where(['owner_id' => $user_id, 'moderation' => $moderation, 'catalog_id' => $catalog_id])->all();
        } else
            $temp = Portfolio::find()->where(['owner_id' => $user_id, 'moderation' => $moderation])->all();
        foreach ($temp as $value) {

            $images = explode("|", $value['images']);
            $description = ItemDescription::find()->where(['item_id' => $value['item_id'], 'language' => $lang])->one();
            $short_d = substr($description['short_description'], 0, 650);
            $short_d = htmlspecialchars($short_d);

            if ($current_id == $value['item_id']) $cnt = 0;

            $portfolio[$cnt] = [
                'id' => $value['item_id'],
                'image' => Yii::getAlias('@portfolio/' . $images[0]),
                'title' => $description['title'],
                'short_description' => $short_d,
                'likes' => $value['likes'],
                'my_likes' => Likes::find()->where(['user_id' => $user_id, 'object_id' => $value['item_id'], 'category' => 2])->count(),
                'url' => Yii::$app->urlManager->createUrl(['about/item', 'id' => $value['item_id']]),
                'share_link' => Yii::$app->urlManager->createAbsoluteUrl(['about/item', 'id' => $value['item_id']]),
                'share_img' => Url::base(true) . "/media/portfolio/" . $images[0]

            ];

            $cnt = $count;
            $count++;

        }


        return $portfolio;
    }


    public function getUserReviews($to)
    {
        $data = array();
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => ProfileReviews::find()->where(['to' => $to])->count(),
        ]);
        $reviews = ProfileReviews::find()->where(['to' => $to])->offset($pagination->offset)->limit($pagination->limit)->orderBy('id DESC')->all();

        foreach ($reviews as $review) {

            $profile = User::find()->where(['id' => $review['from']])->one();
            $avatar = empty($profile->avatar) ? '/media/profile/avatar/noava.png' : $profile->avatar;
            if ($profile) {
                $data[] = [
                    'link' => Yii::$app->urlManager->createUrl(['about/user', 'id' => $review['from']]),
                    'author' => $profile->first_name . ' ' . $profile->last_name,
                    'image' => $avatar,
                    'comment' => $review['review'],
                    'date' => date('j.m.y; H:i', $review['date']),
                ];
            } else {

                $data[] = [
                    'link' => '',
                    'author' => '',
                    'image' => Yii::getAlias('@avatar/noava.png'),
                    'comment' => $review['review'],
                    'date' => date('j.m.y; H:i', $review['date']),
                ];

            }
        }
        $new_data = [
            'reviews' => $data,
            'pagination' => $pagination
        ];

        return $new_data;

    }

    private function getUserCollections($user_id, $lang)
    {
        $data = array();
        $collections = PortfolioCollections::find()->where(['user_id' => $user_id])->all();
        foreach ($collections as $value) {
            $items = array();
            $portfolio = Portfolio::find()->where(['item_id' => $value['main']])->one();
            $temp_img = explode("|", $portfolio['images']);
            $items[0]['image'] = Yii::getAlias('@portfolio/' . $temp_img[0]);
            $description = ItemDescription::find()->where(['item_id' => $value['main'], 'language' => $lang])->one();
            $items[0]['title'] = $description['title'];
            $items[0]['id'] = $value['main'];
            $items[0]['url'] = Yii::$app->urlManager->createUrl(['about/item', 'id' => $value['main']]);

            $items_id = explode("|", $value['items']);
            $i = 1;
            if ($items_id) {
                foreach ($items_id as $item) {
                    if ($item != "" && $item != $value['main'] && $item != " ") {
                        $portfolio = Portfolio::find()->where(['item_id' => $item])->one();
                        $temp_img = explode("|", $portfolio['images']);
                        $items[$i]['image'] = Yii::getAlias('@portfolio/' . $temp_img[0]);
                        $description = ItemDescription::find()->where(['item_id' => $item, 'language' => $lang])->one();
                        $items[$i]['title'] = $description['title'];
                        $items[$i]['id'] = $item;
                        $items[$i]['url'] = Yii::$app->urlManager->createUrl(['about/item', 'id' => $item]);
                        $i++;
                    }
                }
            }

            $about = substr($value['about'], 0, 980);
            $about = htmlspecialchars($about);

            $data[] = [
                'id' => $value['id'],
                'name' => $value['name'],
                'main' => $value['main'],
                'about' => $about,
                'items' => $items
            ];


        }


        return $data;
    }


    public function getDesignerItem($id, $lang)
    {

        $item = array();
        $temp_item = Portfolio::find()->where(['item_id' => $id, 'moderation' => 1])->one();
        if ($temp_item) {
            $temp_description = ItemDescription::find()->where(['item_id' => $id, 'language' => $lang])->one();
            $user = User::find()->where(['id' => $temp_item['owner_id']])->one();

            if ($lang == "ru") {
                $temp_day = Yii::t('main', 'days');
                $date = date('j ' . $temp_day[date('F', $temp_item['date'])] . ', Y', $temp_item['date']);
            } else  $date = date('j F, Y', $temp_item['date']);


            $temp_images = explode("|", $temp_item['images']);
            $images = [];
            $count = 0;
            foreach ($temp_images as $value) {
                if ($value != "") {
                    $images[$count] = '/media/portfolio/' . $value;
                    $count++;
                }
            }


            $temp_tags = explode(",", $temp_description['tags']);
            $tags = [];
            $count = 0;
            foreach ($temp_tags as $tag) {
                if ($tag != "") {
                    $tags[$count] = '<a href="#">#' . $tag . '</a>';
                }

                $count++;
            }


            $item = [
                'id' => $temp_item['item_id'],
                'title' => $temp_description['title'],
                'images' => $images,
                'date' => $date,
                'description' => html_entity_decode($temp_description['description']),
                'tags' => $tags,
                'user' => $user['first_name'] . ' ' . $user['last_name'],
                'user_link' => Yii::$app->urlManager->createUrl(['about/user', 'id' => $temp_item['owner_id']]),

            ];
        }


        return $item;

    }


}
