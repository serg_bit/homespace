<?php
namespace frontend\controllers;

use yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\account\models\UserCompanies;
use frontend\account\models\User;
use frontend\models\Language;
use frontend\account\models\UserStatistics;
use frontend\models\UserVideos;

class AgentsController extends Controller
{
    private $role;
    private $user_id;
    private $language = "ru";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
        $this->role = Yii::$app->user->identity->roles;
    }

    public function actionIndex()
    {
        $pagination = new Pagination([
            'defaultPageSize' => '6',
            'totalCount' => User::countAgents(),
        ]);
        $agents = User::selectsAgents($pagination);
        $i = -1;
        foreach ($agents as $agent) {
            $i++;
            $agents[$i]['companies'] = UserCompanies::selectCountCompaniesByAgent($agent['id']);
        }
        return $this->render('index', [
            'agents' => $agents,
            'pagination' => $pagination
        ]);
    }

    public function actionProfile(){
        $id = Yii::$app->request->get('id', false);
        if ($id && User::findOne(['id' => $id, 'roles' => 'agent'])) {
            $statistic = UserStatistics::findOne(['id_user' => $id]);
            if ($statistic) {
                if ($this->role)
                    $column = $this->role;
                else
                    $column = 'guest';
                $temp = explode(",", $statistic[$column]);
                $temp[0] += 1;
                $month = date("n", time());
                if ($month == $statistic->month) {
                    $temp[1] += 1;
                } else {
                    $statistic->month = $month;
                    $temp[1] = 1;
                }

                $statistic[$column] = $temp[0] . ',' . $temp[1];
                $statistic->update();

            } else
                UserStatistics::createNewUserRow($id);

            list($controller) = Yii::$app->createController('about');
            list($agent) = Yii::$app->createController('account/profile-agent');

            if ($id != $this->user_id) {
                $click_action = true;
            }else $click_action = false;

            return $this->render('profile', [
                'user_information' => User::findOne(['id' => $id, 'roles' => 'agent']),
                'videos' => UserVideos::find()->where(['user_id' => $id])->all(),
                'reviews' => $controller->getUserReviews($id),
                'approved' => $agent->getApprovedConfirmationProjects($id, 6, 1),
                'click_action' => $click_action
            ]);
        } else
            Yii::$app->getResponse()->redirect(array('agents'));
    }
}