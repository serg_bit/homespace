<?php

namespace frontend\controllers;

use yii;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Controller;
use frontend\account\models\User;
use frontend\models\Language;
use frontend\account\models\ProductsList;
use frontend\account\models\UserStatistics;
use frontend\account\models\CompaniesCollections;


class ManufacturersController extends Controller
{
    private $role;

    public function init()
    {
        $this->role = Yii::$app->user->identity->roles;
    }

    public function actionIndex()
    {
        $pagination = new Pagination([
            'defaultPageSize' => 9,
            'totalCount' => User::find()->where(['roles' => 'manufacturer'])->count()
        ]);
        $companies = User::companiesList($pagination);
        $i = -1;
//        array_reverse($companies);
        foreach ($companies as $company) {
            $i++;
            $companies[$i]['products_count'] = ProductsList::countProductsByCompanyId($company['id']);
            $companies[$i]['collections'] = CompaniesCollections::selectCollectionsByCompanyId($company['id']);
        }
        return $this->render('index', [
            'companies' => $companies,
            'pagination' => $pagination
        ]);
    }


    public function actionProfile()
    {
        $id = Yii::$app->request->get('id', false);
        if ($id && User::findOne(['id' => $id, 'roles' => 'manufacturer'])) {

            return $this->render('profile', []);
        } else
            Yii::$app->getResponse()->redirect(array('manufacturers'));
    }


}
