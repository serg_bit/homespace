<?php
namespace frontend\controllers;

use frontend\account\models\ProductsList;
use Yii;
use common\models\LoginForm;
use common\models\Country;
use common\models\User;
use common\models\Auth;
use common\models\UserSpecialization;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\EditForm;
use frontend\models\Messages;
use frontend\models\LikesFacebook;
use frontend\account\models\Specialization;
use frontend\models\Language;
use frontend\models\WorksHelper;
use frontend\account\models\ItemDescription;
use yii\helpers\Url;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use frontend\account\models\Likes;
use frontend\account\models\Portfolio;
use frontend\models\Questions;
use yii\web\Response;
use yii\widgets\ActiveForm;


/**
 * Site controller
 */
class SiteController extends Controller
{

    private $user_id;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    
    public function init(){
       session_start();
       if (!isset($_SESSION['st_works'])) {
            $_SESSION['st_works'] =  ['sort'=>'views',
                                    'tags'=> "",
                                    'count' => 9,
                                ];
        }
        if (!isset($_SESSION['products'])) {
            $_SESSION['products'] =  ['sort'=>'likes',
                                    'tags'=> "",
                                    'count' => 9,
                                ];
        }
    }
    
    
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $lang = Language::getCurrent()->url;

        if(isset($_SESSION['st_works'])) {
            $class = isset($_REQUEST['parent_class']) ? $_REQUEST['parent_class'] : "";
            $value = isset($_REQUEST['value']) ? $_REQUEST['value'] : "";
            $pagination[] = $_SESSION['pagination_main'][0] = 5;
            $pagination[] = $_SESSION['pagination_main'][1] = 7;
            $pagination[] = $_SESSION['pagination_main'][2] = 6;
            $pagination[] = $_SESSION['pagination_main'][3] = 6;

//            $_SESSION['st_works']['tags'] = isset($_REQUEST['stags']) ? $_REQUEST['stags'] : "";


        }

        if(isset($_SESSION['products'])) {
            $class = isset($_REQUEST['parent_class']) ? $_REQUEST['parent_class'] : "";
            $value = isset($_REQUEST['value']) ? $_REQUEST['value'] : "";
//            $_SESSION['best_works']['tags'] = isset($_REQUEST['bwtags']) ? $_REQUEST['bwtags'] : "";


        }

        if($class == 'stcount'){

            $_SESSION['st_works']['count'] =  $value;

        }else if($class == 'stsort'){

            $_SESSION['st_works']['sort'] = $value;

        }else if($class == 'productssort'){

            $_SESSION['products']['sort'] = $value;

        }else if($class == 'productscount'){

            $_SESSION['products']['count'] = $value;

        }else if($class == 'st_tags'){

            $_SESSION['st_works']['tags'] = $value;

        }else if($class == 'products_tags'){

            $_SESSION['products']['tags'] = $value;

        }

        $st_works_pagination = new Pagination([
            'defaultPageSize' => $_SESSION['st_works']['count'],
            'totalCount' => WorksHelper::countWorks('2'),
        ]);

        $products_pagination = new Pagination([
            'defaultPageSize' => $_SESSION['products']['count'],
            'totalCount' => WorksHelper::countProducts(),
        ]);


        $st_works_paid_status = '3';
        $st_works = WorksHelper::getStWorks($_SESSION['st_works']['sort'], $st_works_paid_status, $lang, $st_works_pagination, $_SESSION['st_works']['tags']);
        $products = WorksHelper::getProducts($_SESSION['products']['sort'], $products_pagination, $_SESSION['products']['tags']);



        if (!\Yii::$app->user->isGuest){

            $all_works  = Portfolio::mainPortfolioList(\Yii::$app->user->identity->id, $lang);
            $all_likes = Likes::find()->select(['object_id'])->where(['category' => 2 ,  'user_id' => \Yii::$app->user->identity->id])->asArray()->all();
            $products_likes = Likes::find()->select(['object_id'])->where(['category' => 4 ,  'user_id' => \Yii::$app->user->identity->id])->asArray()->all();
            $likes = array();
            $product_likes = array();

            foreach ($all_likes as $item){

                array_push($likes, $item['object_id']);

            }

            foreach($products_likes as $item){

                array_push($product_likes, $item['object_id']);

            }

            for ($n=0; $n<count($st_works); $n++){

                if(array_search($st_works[$n]['item_id'], $likes)){

                    $st_works[$n]['like_status'] = 'like';

                }else{

                    $st_works[$n]['like_status'] = 'dislike';

                }

            }

//            for ($i=0; $i<count($all_works); $i++){
//
//                if(array_search($all_works[$i]['item_id'], $likes)){
//
//                    $all_works[$i]['like_status'] = 'like';
//
//                }else{
//
//                    $all_works[$i]['like_status'] = 'dislike';
//
//                }
//
//            }


            for ($i=0; $i<count($products); $i++){

                if(array_search($products[$i]['id_prod'], $product_likes)){

                    $products[$i]['like_status'] = 'like';

                }else{

                    $products[$i]['like_status'] = 'dislike';

                }


            }

        }else{

            $all_works  = Portfolio::mainPortfolioList(0, $lang);

        }



//        $best_works = WorksHelper::getWorks($_SESSION['best_works']['sort'], '3', $lang, $best_works_pagination);

//        if($status == 0 || \Yii::$app->user->isGuest){
            return $this->render('index',[
                'st_works_pagination' => $st_works_pagination,
                'products_pagination' => $products_pagination,
                'st_works' => $st_works,
                'products' => $products,
                'all_works' => $all_works,
                'products_tags' => $_SESSION['products']['tags'],
                'stworks_tags' => $_SESSION['st_works']['tags'],
                'lang' => $lang,
            ]);

    }

    public function actionTicket()
    {
        $data = Yii::$app->request->post();
        if($data){
            if(Yii::$app->user->identity->id)
                $data['user_id'] = Yii::$app->user->identity->id;
            $q = new Questions();
            $q->sendQuestion($data);
        }
    }

    public function actionLikes()
    {
        if (Likes::find()->where(['object_id' => $_REQUEST['id'] , 'category' => 2 ,  'user_id' => \Yii::$app->user->identity->id])->one()) {

            Likes::deleteAll(['object_id' => $_REQUEST['id'] , 'category' => 2, 'user_id' => \Yii::$app->user->identity->id]);
            $portfolio_likes = Portfolio::findOne(['item_id' => $_REQUEST['id']]);
            $portfolio_likes->likes = $portfolio_likes->likes - 1;
            $portfolio_likes->update();

        } else {
            $like = new Likes;
            $like->object_id = $_REQUEST['id'];
            $like->user_id = \Yii::$app->user->identity->id;
            $like->category = 2;
            $like->save();
            $portfolio_likes = Portfolio::findOne(['item_id' => $_REQUEST['id']]);
            $portfolio_likes->likes = $portfolio_likes->likes + 1;
            $portfolio_likes->update();
        }
        $all_likes = Portfolio::findOne(['item_id' => $_REQUEST['id']]);
        return $all_likes->likes;
    }

    public function actionProductlikes()
    {
        if (Likes::find()->where(['object_id' => $_REQUEST['id'] , 'category' => 4 ,  'user_id' => \Yii::$app->user->identity->id])->one()) {

            Likes::deleteAll(['object_id' => $_REQUEST['id'] , 'category' => 4, 'user_id' => \Yii::$app->user->identity->id]);
            $portfolio_likes = ProductsList::findOne(['id_prod' => $_REQUEST['id']]);
            $portfolio_likes->likes = $portfolio_likes->likes - 1;
            $portfolio_likes->update();

        } else {
            $like = new Likes;
            $like->object_id = $_REQUEST['id'];
            $like->user_id = \Yii::$app->user->identity->id;
            $like->category = 4;
            $like->save();
            $portfolio_likes = ProductsList::findOne(['id_prod' => $_REQUEST['id']]);
            $portfolio_likes->likes = $portfolio_likes->likes + 1;
            $portfolio_likes->update();
        }

        $liked = ProductsList::find()->where(['id_prod' => $_REQUEST['id']])->one();
        return $liked->likes;
    }

    public function actionShareCounter()
    {

        $id = yii::$app->request->post('id', 0);
        $type = yii::$app->request->post('type', 0);
        if($id != 0){
            if($type == 1){
                // Portfolio share
                $portfolio = Portfolio::findOne(['item_id' => $id]);
                $portfolio->share_social = $portfolio->share_social + 1;
                $portfolio->update();
            }elseif($type == 2){
                // Products share
            }

        }

    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {

//        Yii::$app->response->cookies->add(new \yii\web\Cookie([
//            'name' => 'test',
//            'value' => Language::getCurrent()->url,
//            'expire' => time() + 86400 * 365,
//        ]));
//        if(Yii::$app->request->cookies['test'] == 'en'){
//
//            $lang = 'en';
//
//        }else{
//
//            $lang = '';
//
//        }

        Yii::$app->user->logout();

        $url = isset($_COOKIE['current-page']) ? $_COOKIE['current-page'] : Url::to(['/']);

        return $this->redirect($url);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSuccessful()
    {
        return $this->render('successful');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $role = Yii::$app->request->get('role', '');
        $country = Country::getAllCountries();
        $specialization = false;
        $visibility = false;

        if($role == 'designer'){
            $visibility = false;
        }elseif($role == 'manufacturer') {
            $visibility = true;
            $specialization = Specialization::allSpecialization(Language::getCurrent()->url, 1);
        }elseif($role == 'agent') {
            $visibility = true;
            $specialization = Specialization::allSpecialization(Language::getCurrent()->url, 1);
        }elseif($role == 'service') {
            $visibility = true;
            $specialization = Specialization::allSpecialization(Language::getCurrent()->url, 1);
        }elseif($role == 'partner') {
            $role = 'homeowner';
            $specialization = Specialization::allSpecialization(Language::getCurrent()->url, 1);
        }else{
            $visibility = false;
            $role = 'homeowner';
        }


        $model = new SignupForm();

        if ($model->load(Yii::$app->request->post())) {
            $token = md5(uniqid(rand(), TRUE));
            if($user = $model->signup($token)) {
                $user_style = isset($_COOKIE['user_style']) ? $_COOKIE['user_style'] : false;
                SetCookie("user_style","");
                if($user_style){
                    $user_style = substr($user_style, 0, -1);
                    Portfolio::incrementLikes($user_style);
                }

                User::ActivateUserByToken($token);
                $user = User::find()->where(['account_activation_token'=>$token])->one();
                Yii::$app->user->login($user, 3600 * 24 * 30);

                $url = isset($_COOKIE['current-page']) ? $_COOKIE['current-page'] : Url::to(['/']);

                return $this->redirect($url);

            }
            else {
                Yii::$app->session->setFlash('error', 'There was an error while registering user.');
            }
        }


        return $this->render('signup', [
            'model' => $model,
            'specialization' => $specialization,
            'country' => $country,
            'role' => $role,
            'visibility' => $visibility
        ]);
    }

    /**
     * Confirm user via email
     *
     * @return mixed
     */
    public function actionConfirmUser() {
        $token = Yii::$app->request->get('atoken');
        User::ActivateUserByToken($token);
        $user = User::find()->where(['account_activation_token'=>$token])->one();
        Yii::$app->user->login($user, 3600 * 24 * 30);

        return $this->goHome();
    }
    
    
    
    public function onAuthSuccess($client)
    {
        $attributes = $client->getUserAttributes();
        $auth = Auth::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();
        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                $user = $auth->user;
                Yii::$app->user->login($user);

            } else { // signup
                $name = explode(" ", $attributes['name']);
                $user_name = $name[0].'_'.$attributes['id'];
                if (User::find()->where(['username' => $user_name])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', "User with the same email as in {client} account already exists but isn't linked to it. Login using email first to link it.", ['client' => $client->getTitle()]),
                    ]);
                } else {
                    $user_email = isset($attributes['email']) ? $attributes['email'] : $name[0];
                    $password = Yii::$app->security->generateRandomString(6);
                        $user = new User([
                            /** TODO Need define logic of getting username */
                            'username' => $user_name,
                            'first_name' => $name[0],
                            'last_name' => $name[1],
                            'email' => $user_email,
                            'password' => $password,
                        ]);
                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();
                    $transaction = $user->getDb()->beginTransaction();
                    
                    if ($user->save()) {

                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $client->getId(),
                            'source_id' => (string)$attributes['id'],
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user);
                           
                                
                            
                        } else {
                            print_r($auth->getErrors());
                        }
                    } else {
                        print_r($user->getErrors());
                    }

                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();

            }
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionRequestStyle()
    {
        $lang = Yii::$app->request->post('lang', 'en');
        $styles = Portfolio::styleItems($lang);
        if($styles){
            foreach($styles as $style){
                $image = explode("|", $style['images']);
                echo '<div class="choose-content-style">
                        <a href="#">
                            <img src="'.Yii::getAlias('@portfolio/'.$image[0]).'" alt="">
                            <div class="bg"></div>
                        </a>
                        <p class="title">'.$style['name'].'</p>
                        <p>
                            <input id="style'.$style['item_id'].'" data-item_id="'.$style['item_id'].'" type="checkbox">
                            <label for="style'.$style['item_id'].'" ></label>
                        </p>
                      </div>';
            }
        }else{
            echo 'Not Found!!!';
        }
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionValid()
    {

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->login()) {
                    return $this->refresh();
                }
            }
        }

        $url = isset($_COOKIE['current-page']) ? $_COOKIE['current-page'] : Url::to(['/']);

        return $this->redirect($url);
    }


    public function actionSend()
    {
        //print_r($_REQUEST['message']);
        $message = new Messages;
        $message->to = $_REQUEST['recipient'];
        $message->from = \Yii::$app->user->identity->id;
        $message->message = $_REQUEST['message'];
        $message->save();

        if($message->save())
        {
            $requests = Messages::find()->select(["id_mes", "message", "created_at"])->where(['to' => $_REQUEST['recipient'], 'from' => \Yii::$app->user->identity->id])->orderBy('id_mes DESC')->one();
            $sender = User::findOne(\Yii::$app->user->identity->id);
            $data = array();
            $data['sender'] = $sender->username;

            if(!isset($sender->avatar))$data['sender_avatar'] = "no-img.png";
            else $data['sender_avatar'] = $sender->avatar;

            $data['url_avatar'] = Yii::getAlias('@avatar/'.$data['sender_avatar']);
            $data['request_message'] = $requests->message;
            $data['request_time'] = $requests->created_at;
            $data['request_message_id'] = $requests->id_mes;

            $data = json_encode($data);
            return($data);
        }

    }

    /**
     * @return string
     */

    public function actionChat(){

        if ($_POST['request'] == 'get'){

            $response = Messages::find()->select(['id_mes', 'message', 'file_app', 'created_at', 'from', 'status'])->where(['to' => \Yii::$app->user->identity->id, 'status' => 0])->orderBy('id_mes')->asArray()->all();
//            if ($response){

//            $sender = User::findOne($response->from);
            for($n=0; $n<count($response); $n++){


//                $data['sender_avatar'] = $sender->avatar;

                if ($response[$n]['file_app'] != NULL) {
                    $file_name = explode("|", $response[$n]['file_app']);
                    array_pop($file_name);
                    $data[$n]['file_name'] = $file_name;
                }
                else $data[$n]['file_name'] = NULL;
                $sender = User::findOne($response[$n]['from']);
                $data[$n]['response_message'] = !empty($response[$n]['message']) ? $response[$n]['message'] : NULL;
                $data[$n]['url_avatar'] = Yii::getAlias('@avatar/'.$sender->avatar);
                $data[$n]['status'] = $response[$n]['status'];
                $data[$n]['sender_id'] = $sender->id;
                $data[$n]['sender_username'] = $sender->username;
                $data[$n]['response_message_id'] = $response[$n]['id_mes'];
                $data[$n]['response_date'] = $response[$n]['created_at'];

            }

            $data = json_encode($data);


            return $data;

//            }

        }

    }


    public function actionMessagestatusupdate(){

        if($_REQUEST['getmessage']){

            $message = Messages::find()->where(['id_mes' => $_REQUEST['getmessage']])->one();
//            $message = Messages::findAll($_REQUEST['getmessage']]);
            $message->status = 1;
            $message->update();

        }



    }

    public function actionGetlastmessage(){
        $response = Messages::find()->select(["id_mes", "message", "created_at", "file_app", "status"])->where(['from' => $_REQUEST['recipient'], 'to' => \Yii::$app->user->identity->id])->orderBy('id_mes DESC')->one();
        $request = Messages::find()->select(["id_mes", "message", "created_at", "file_app", "status"])->where(['to' => $_REQUEST['recipient'], 'from' => \Yii::$app->user->identity->id])->orderBy('id_mes DESC')->one();



        if($response || $request) {

            if ($response->id_mes > $request->id_mes) {
                $sender = User::findOne($_REQUEST['recipient']);
                $message = $response;
                $data['type'] = "get";
                $data['url_avatar'] = Yii::getAlias('@avatar/'.$sender->avatar);
                $data['sender_id'] = $sender->id;

            } else {

                $current_user = User::findOne(\Yii::$app->user->identity->id);
                $message = $request;
                $data['type'] = "send";
                $data['url_avatar'] = Yii::getAlias('@avatar/'.$current_user->avatar);
                $data['sender_id'] = NULL;

            }


            $data['status'] = $message->status;

            if ($message->file_app != NULL){

                $file_name = explode("|", $message->file_app);
                array_pop($file_name);
                $data['file_name'] = $file_name;

            }else{

                $data['file_name'] = NULL;

            }

            $data['response_message'] = !empty($message->message) ? $message->message : NULL;
            $data['response_message_id'] = $message->id_mes;
            $data['response_date'] = $message->created_at;
            $data = json_encode($data);

            return $data;

        }else{

            return false;

        }
    }


    public function actionUpload(){
        $sender = User::findOne(\Yii::$app->user->identity->id);
        $path1 = Yii::getAlias('@media') . '/temp/';
        $path2 = Yii::getAlias('@media') . '/upload/';
        $i = -1;
        $files = "";

        if (isset($_FILES)) {
            foreach( $_FILES as $file ){
                $temp = explode("/", $file['type']);
                if($temp[1] == 'doc' || $temp[1] == 'txt'|| $temp[1] == 'rtf'|| $temp[1] == 'jpg'|| $temp[1] == 'png' || $temp[1] == 'rtf') {
                    $i++;
                    $ext = '.' . $temp[1];
                    $name = time().rand(000,999);
                    $file_name[$i] = rand(111, 999)."_".$file['name'];
                    move_uploaded_file($file['tmp_name'] , $path1.$name.$ext );

                    copy( $path1.$name.$ext , $path2.$file_name[$i] );

                    $files .= $file_name[$i];
                    $files .= "|";

                }
                $message = new Messages;
                $message->to = $_REQUEST['recipient'];
                $message->from = \Yii::$app->user->identity->id;
                $message->file_app = $files;

            }

            $message->save();
            $request = Messages::find()->select(["id_mes", "message", "created_at", "status"])->where(['to' => $_REQUEST['recipient'], 'from' => \Yii::$app->user->identity->id])->orderBy('id_mes DESC')->one();

            if(!isset($sender->avatar))$data['sender_avatar'] = "no-img.png";
            else $data['sender_avatar'] = $sender->avatar;
            $data['url_avatar'] = Yii::getAlias('@avatar/'.$data['sender_avatar']);
            $data['file_name'] = $file_name;
            $data['path'] = $path2;
            $data['request_date'] = $request->created_at;
            $data['request_message_id'] = $request->id_mes;
            $data = json_encode($data);

            return $data;

        }

    }

    public function actionWorks(){
        $pagination = new Pagination([
            'defaultPageSize' => 24,
            'totalCount' => Portfolio::find()->where(['active' => '1', 'moderation' => 1])->count(),
        ]);

        if(Yii::$app->user->identity->id){
            $u_id = Yii::$app->user->identity->id;
        }else $u_id = 0;

        $works = Portfolio::allWorks(Language::getCurrent()->url, $pagination->offset, $pagination->limit, $u_id);

        return $this->render('works',[

            'all_works' => $works,
            'pagination' => $pagination,

        ]);

    }


    public function actionMobileForm(){
        $email = isset($_POST['email']) ? $_POST['email'] : false;
        if($email){
            $to  = $email;
            $subject = "Home Space Today";
            $message = '
            <html>
                <head>
                    <title>Home Space Today</title>
                </head>
                <body>
                    <h3>Your link to Home Space Today</h3>
                    <a href="'.Yii::$app->urlManager->createAbsoluteUrl(['site/index']).'">HomeSpace.today</a>
                </body>
            </html>';

            $headers  = "Content-type: text/html; charset=utf-8 \r\n";
            $headers .= "From: Home Space Today<info@homespace.today>\r\n";

            mail($to, $subject, $message, $headers);
        }
    }


}
