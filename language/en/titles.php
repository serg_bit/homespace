<?php 

return [

	'account' => "| Account",
	'account_profile' => " - Profile",
	'profile' => '| Profile (',
	'portfolio' => ' - Portfolio',
	'share' => ' - Share',
	'portfolio_edit' => " - Edit (",
	'portfolio_create' => " - Create portfolio",
	'account_edit' => " - Edit profile",
	'edit_honours' => " - Edit honours",
	'edit_licenses' => " - Edit licenses",
	'edit_videos' => " - Edit videos",
	'edit_collections' => " - Edit collections",
	'edit_main' => " - Edit main information",
	'special_offers' => ' - Special offers',
	'about' => '| About',
	'search' => '| Search',
	'order' => ' - Collect order',
	'app_inbox' => ' - Inbox application',
	'app_send' => ' - Sent application',
	'catalog' => ' - Catalog',
	'sign_up' => '| Sign up',
	'full_catalog' => '| Full catalog',
	'messages' => ' - Messages',
	'group' => ' - My agents',
];