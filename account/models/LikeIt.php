<?php
/**
 * Created by PhpStorm.
 * User: Alscon13
 * Date: 12.05.2016
 * Time: 10:38
 */

namespace frontend\account\models;

use Yii;



class LikeIt extends \yii\db\ActiveRecord
{
    public static function addMyItem($id, $folder, $user_id)
    {
        Yii::$app->db->createCommand("CALL UpdateLikeIt($folder, $id, $user_id);")->execute();
    }

    public static function rootDesignerItems($folder, $user_id, $lang)
    {
       return  Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title, portfolio.likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND portfolio.item_id IN (SELECT item_id FROM `like_it_items` WHERE like_it_id = $folder AND user_id = $user_id);")->queryAll();
    }



    public static function createFolder($name, $root, $user_id){

        Yii::$app->db->createCommand("INSERT INTO `like_it` (`user_id`,`name_ru`,`name_en`, `relative`) VALUES ($user_id, '$name', '$name', $root );")->execute();
    }


    public static function deleteFolder( $id_folder){

        Yii::$app->db->createCommand("DELETE FROM `like_it` WHERE `id`=$id_folder;")->execute();
        Yii::$app->db->createCommand("DELETE FROM `like_it_items` WHERE `like_it_id` = $id_folder ;")->execute();

    }

    public static function renameFolder($id_folder, $name){

        Yii::$app->db->createCommand("UPDATE `like_it` SET `name_ru` = '$name', `name_en` = '$name' WHERE `id` = $id_folder;")->execute();

    }




}