<?php

namespace frontend\account\models;
use Yii;
use yii\data\ActiveDataProvider;


class ProductsTagsUnite extends \yii\db\ActiveRecord
{
    public $id;
    public $id_product;
    public $id_tag;

    public static function tableName()
    {
        return 'products_tags_unite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_product', 'tag'], 'integer'],
        ];
    }

}