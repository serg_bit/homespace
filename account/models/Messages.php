<?php

namespace frontend\account\models;

use Yii;

class Messages extends \yii\db\ActiveRecord{

    public static function tableName(){
        return 'messages';
    }


    public static function showLastMessage($user_id, $recipient){
        $sendMessage = Messages::find()->select([ 'id_mes','to','message','file_app', 'status', 'created_at'])->where(['from'=>$user_id, 'to' => $recipient])->orderBy('id_mes DESC')->asArray()->one();
        $getMessage =  Messages::find()->select([ 'id_mes','to','message','file_app', 'status', 'created_at'])->where(['from'=>$recipient, 'to' => $user_id])->orderBy('id_mes DESC')->asArray()->one();

        if (!empty($sendMessage) && !empty($getMessage)){

            if( $sendMessage['id_mes'] > $getMessage['id_mes'] ){

                $message = $sendMessage;
                $type = 'send';
                array_unshift($message, $type);

            }else{

                $message = $getMessage;
                $type = 'get';
                array_unshift($message, $type);

            }

        }else if(!empty($sendMessage) && empty($getMessage)){

            $message = $sendMessage;
            $type = 'send';
            array_unshift($message, $type);

        }else if(empty($sendMessage) && !empty($getMessage)){

            $message = $getMessage;
            $type = 'get';
            array_unshift($message, $type);

        }else{

            $message = false;

        }

        return $message;
    }


    //получаю всех отправителей и получателей, для текущего пользователя
    public static function getAllInterlocutors($user_id){
        $recipients = Messages::find()->select(['to'])->where(['from'=>$user_id])->orderBy('created_at DESC')->asArray()->all();
        $senders = Messages::find()->select(['from'])->where(['to'=>$user_id])->orderBy('created_at DESC')->asArray()->all();
        if (!empty($recipients)){

            foreach($recipients as $recipient){

                $res[] = $recipient['to'];

            }

        }else{

            $res = [];

        }

        if (!empty($senders)){

            foreach($senders as $sender){

                $send[] = $sender['to'];

            }

        }else{

            $send = [];

        }

            $interlocutors = array_merge($send, $res);
            $interlocutors = array_unique($interlocutors);



        return $interlocutors;
    }

    //вывод сообщений
    public static function getAllCorrespondence($currentUser, $interlocutor, $limit, $offset){

        $lastSendMessage = Messages::find()->where(['to' => $interlocutor, 'from' => $currentUser])->count();
        $lastGetMessage = Messages::find()->where(['to' => $currentUser, 'from' => $interlocutor])->count();

        if($lastSendMessage!=0 && $lastGetMessage!=0) {

            $all_correspondence = \Yii::$app->db->createCommand("SELECT  messages.id_mes, messages.to, messages.from, messages.message, messages.file_app, messages.status, messages.created_at
                                                FROM  `messages`
                                                WHERE (`to` = '$currentUser' AND `from` = '$interlocutor')
                                                OR (`to` = '$interlocutor' AND `from` = '$currentUser')
                                                ORDER BY `id_mes` DESC
                                                LIMIT " . $limit . " OFFSET " . $offset . " ")->query();
        }else if($lastGetMessage!=0 && $lastSendMessage==0){

            $all_correspondence = \Yii::$app->db->createCommand("SELECT  messages.id_mes, messages.to, messages.from, messages.message, messages.file_app, messages.status, messages.created_at
                                                FROM  `messages`
                                                WHERE `to` = '$interlocutor' AND `from` = '$currentUser'
                                                ORDER BY `id_mes` DESC
                                                LIMIT " . $limit . " OFFSET " . $offset . " ")->query();

        }else if($lastGetMessage==0 && $lastSendMessage!=0){

            $all_correspondence = \Yii::$app->db->createCommand("SELECT  messages.id_mes, messages.to, messages.from, messages.message, messages.file_app, messages.status, messages.created_at
                                                FROM  `messages`
                                                WHERE `to` = '$currentUser' AND `from` = '$interlocutor'
                                                ORDER BY `id_mes` DESC
                                                LIMIT " . $limit . " OFFSET " . $offset . " ")->query();

        }
        $message = array();

        foreach($all_correspondence as $item){

            $message[$item['id_mes']] = $item;

            if ($message[$item['id_mes']]['from'] == $currentUser){

                $message[$item['id_mes']]['type'] = 'send';

            }else{

                $message[$item['id_mes']]['type'] = 'get';

            }

            if($message[$item['id_mes']]['file_app'] != NULL){

                $message[$item['id_mes']]['files'] = explode('|',$message[$item['id_mes']]['file_app']);

            }

        }

        ksort($message);

        return $message;

    }

    //общее количество отправленных и полученных сообщений с конкретным пользователем
    public static function countCorrespondence($currentUser, $interlocutor){
        $count = Messages::find()->where(['to' => $currentUser, 'from' => $interlocutor])->orWhere(['to' => $interlocutor, 'from' => $currentUser])->count();
//        $countGet = Messages::find()->where(['to' => $currentUser, 'from' => $interlocutor])->count();
//        $countSend = Messages::find()->where(['to' => $interlocutor, 'from' => $currentUser])->count();
//        $count = $countGet + $countSend;

        return $count;
    }


}