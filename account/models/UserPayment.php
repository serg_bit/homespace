<?php

namespace frontend\account\models;

use Yii;

class UserPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_payment';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'date_start' => 'Date Start',
            'date_finish' => 'Date Finish',
            'share' => 'Share',
        ];
    }

    public static function updatePaymentData($user_id, $days){
        $start = time();
        $finish = $start + ($days * 86400);
        Yii::$app->db->createCommand("INSERT INTO `user_payment` (`user_id`, `date_start`, `date_finish`) VALUES ($user_id, $start, $finish) ON DUPLICATE KEY UPDATE `date_start` = $start, `date_finish` = $finish")->execute();
    }

    public static function activateDesignerPortfolio($user_id, $active){
        Yii::$app->db->createCommand("UPDATE `portfolio` SET `active` = $active WHERE `owner_id` = $user_id")->execute();
    }


}
