<?php

namespace frontend\account\models;
use Yii;
use yii\data\ActiveDataProvider;


class Catalog extends \yii\db\ActiveRecord {

    public function rules()
    {
        return [
            [['category'], 'integer'],
        ];
    }

    public static function tableName()
    {
        return 'catalog';
    }

    public function search($params)
    {
        $query = Catalog::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 6],
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'category' => $this->category,
        ]);
        return $dataProvider;
    }

    public static function countAllCatalogs($category){

        $count = Catalog::find()->where(['category' => $category])->count();

        return $count;

    }

    public static function showAllCatalogs($category, $offset, $limit){

        if(empty($category)){

            $catalogs = Catalog::find()->offset($offset)->limit($limit)->all();

        }else{

            $catalogs = Catalog::find()->where(['category' => $category])->offset($offset)->limit($limit)->all();

        }
        return $catalogs;

    }

}
