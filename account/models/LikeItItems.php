<?php

namespace frontend\account\models;

use Yii;

class LikeItItems extends \yii\db\ActiveRecord{

    public static function tableName(){
        return 'like_it_items';
    }

    public static function AddItemToFolder($id_product, $id_folder){
        $current_product = LikeItItems::findOne(['item_id' => $id_product, 'user_id'=>\Yii::$app->user->identity->id]);
        $current_product->like_it_id = $id_folder;
        return $current_product->update();
    }
}

