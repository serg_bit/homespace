<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "portfolio".
 *
 * @property integer $item_id
 * @property integer $owner_id
 * @property string $images
 * @property integer $status
 * @property integer $likes
 * @property integer $views
 * @property integer $group_id
 * @property integer $date
 */
class Portfolio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'portfolio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'images', 'date'], 'required'],
            [['owner_id', 'moderation', 'status', 'likes', 'views', 'group_id', 'date'], 'integer'],
            [['images'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'owner_id' => 'Owner ID',
            'images' => 'Images',
            'moderation' => 'Moderation',
            'status' => 'Status',
            'likes' => 'Likes',
            'views' => 'Views',
            'group_id' => 'Group ID',
            'date' => 'Date',
            'active' => 'Active'
        ];
    }

    public function getItemDescription()
    {
        return $this->hasMany(ItemDescription::className(), ['item_id' => 'item_id']);
    }

    public static function incrementLikes($items_id){
        Yii::$app->db->createCommand('UPDATE `portfolio` SET `likes` = `likes` + 1 WHERE `item_id` IN ('.$items_id.')')->execute();
    }

    public static function styleItems($lang){
        if($lang == 'ru'){
            return \Yii::$app->db->createCommand("SELECT item_id, name, images
                                                  FROM
                                                        (SELECT portfolio.item_id, portfolio.images, style.name_ru AS 'name', style.id
                                                        FROM  `style`
                                                        LEFT JOIN `portfolio` ON (style.id = portfolio.style_id)
                                                        WHERE portfolio.active = 1
                                                        AND style.status = 1
                                                        AND portfolio.status = 3
                                                        ORDER BY portfolio.style_id, rand()) AS s
                                                    GROUP BY s.id
                                                    ORDER by s.name")->queryAll();
        }else{
            return \Yii::$app->db->createCommand("SELECT item_id, name, images
                                              FROM
                                                  (SELECT portfolio.item_id, portfolio.images, style.name_en AS 'name', style.id
                                                  FROM  `style`
                                                  LEFT JOIN `portfolio` ON (style.id = portfolio.style_id)
                                                  WHERE portfolio.active = 1
                                                  AND style.status = 1
                                                  AND portfolio.status = 3
                                                  ORDER BY portfolio.style_id, rand()) AS s
                                              GROUP BY s.id
                                              ORDER by s.name")->queryAll();
        }


    }

    public static function mainPortfolioList($user_id = 131, $lang = 'ru'){
        $data[] = \Yii::$app->db->createCommand("SELECT item_id, owner_id, images, likes, @my:=
                                                        (SELECT COUNT(*)
                                                           FROM `likes`
                                                             WHERE category = 2
                                                             AND user_id = ".$user_id."
                                                             AND object_id = item_id
                                                        ) AS my_likes, @title:= (SELECT title
                                                                                 FROM `item_description`
                                                                                 WHERE item_description.item_id = portfolio.item_id
                                                                                 AND item_description.language = '$lang'
                                                                                 ) AS title
                                                    FROM `portfolio`
                                                    WHERE status = 3
                                                    AND active = 1
                                                    AND moderation = 1
                                                    ORDER BY RAND()
                                                    LIMIT 5
                                                ")->queryAll();
        $count = count($data[0]);
        $count = (5 - $count) + 7;
        $data[] = \Yii::$app->db->createCommand("SELECT item_id, owner_id, images, likes, @my:=
                                                        (SELECT COUNT(*)
                                                           FROM `likes`
                                                             WHERE category = 2
                                                             AND user_id = ".$user_id."
                                                             AND object_id = item_id
                                                        ) AS my_likes, @title:= (SELECT title
                                                                                 FROM `item_description`
                                                                                 WHERE item_description.item_id = portfolio.item_id
                                                                                 AND item_description.language = '$lang'
                                                                                 ) AS title
                                                FROM `portfolio`
                                                WHERE status = 2
                                                AND active = 1
                                                AND moderation = 1
                                                ORDER BY RAND()
                                                LIMIT ".$count."
                                                ")->queryAll();
        $count1 = count($data[1]);
        $count1 = ($count - $count1) + 6;
        $data[] = \Yii::$app->db->createCommand("SELECT item_id, owner_id, images, likes, @my:=
                                                        (SELECT COUNT(*)
                                                           FROM `likes`
                                                             WHERE category = 2
                                                             AND user_id = ".$user_id."
                                                             AND object_id = item_id
                                                        ) AS my_likes, @title:= (SELECT title
                                                                                 FROM `item_description`
                                                                                 WHERE item_description.item_id = portfolio.item_id
                                                                                 AND item_description.language = '$lang'
                                                                                 ) AS title
                                                    FROM `portfolio`
                                                    WHERE status = 0
                                                    AND active = 1
                                                    AND moderation = 1
                                                    ORDER BY likes DESC
                                                    LIMIT ".$count1."
                                                ")->queryAll();
//        $count2 = count($data[2]);
//        $count2 = ($count1 - $count2) + 6;
//        $data[] = \Yii::$app->db->createCommand("SELECT item_id, owner_id, images, likes, @my:=
//                                                        (SELECT COUNT(*)
//                                                           FROM `likes`
//                                                             WHERE category = 2
//                                                             AND user_id = ".$user_id."
//                                                             AND object_id = item_id
//                                                        ) AS my_likes, @title:= (SELECT title
//                                                                                 FROM `item_description`
//                                                                                 WHERE item_description.item_id = portfolio.item_id
//                                                                                 AND item_description.language = '$lang'
//                                                                                 ) AS title
//                                                    FROM `portfolio`
//                                                    WHERE status = 0
//                                                    AND active = 1
//                                                    ORDER BY share_social DESC
//                                                    LIMIT ".$count2."
//                                                ")->queryAll();

        return $data;
    }

    public static function allWorks($lang = 'en', $offset, $limit, $user_id = 0){
        return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, portfolio.likes, @my:= (SELECT COUNT(*) FROM `likes` WHERE likes.category = 2 AND likes.user_id = ".$user_id." AND likes.object_id = portfolio.item_id) AS my_likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               ORDER BY portfolio.likes DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
    }

    public static function rand3Works($id = 0){
        return \Yii::$app->db->createCommand("SELECT item_id, images, likes
                                              FROM portfolio
                                              WHERE item_id <> $id
                                              AND owner_id = (SELECT owner_id FROM portfolio WHERE item_id = $id)
                                              AND active = 1
                                              ORDER BY RAND()
                                              LIMIT 3")->queryAll();
    }

    public static function userStyles($id = 0){

        $data = [];
        $user_data = \Yii::$app->db->createCommand("SELECT styles
                                                    FROM `user`
                                                    WHERE id = (SELECT owner_id FROM portfolio WHERE item_id = $id);
                                                    ")->queryAll();
        if($user_data[0]['styles']){
            $data = \Yii::$app->db->createCommand("SELECT item_id, images, likes
                                                    FROM portfolio
                                                    WHERE item_id IN (".$user_data[0]['styles'].")
                                                    AND active = 1
                                                    ORDER BY RAND();
                                                    ")->queryAll();
        }

        if(!$data){
            $data = \Yii::$app->db->createCommand("SELECT item_id, images, likes
                                               FROM portfolio
                                               WHERE status = 3
                                               AND active = 1
                                               ORDER BY RAND()
                                               LIMIT 6;")->queryAll();
        }

        return $data;
    }

    public static function catalogProjects($lang = 'en', $offset, $limit, $user_id = 0, $sort_by, $tags, $cat_id){

        if($tags){
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, item_description.short_description, portfolio.likes, @my:= (SELECT COUNT(*) FROM `likes` WHERE likes.category = 2 AND likes.user_id = ".$user_id." AND likes.object_id = portfolio.item_id) AS my_likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND item_description.tags LIKE '%$tags%'
                                               ORDER BY portfolio.".$sort_by." DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
        }else
            if($cat_id != 0){
                return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, item_description.short_description, portfolio.likes, @my:= (SELECT COUNT(*) FROM `likes` WHERE likes.category = 2 AND likes.user_id = ".$user_id." AND likes.object_id = portfolio.item_id) AS my_likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND portfolio.cat_id = $cat_id
                                               ORDER BY portfolio.".$sort_by." DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();

            }else
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, item_description.short_description, portfolio.likes, @my:= (SELECT COUNT(*) FROM `likes` WHERE likes.category = 2 AND likes.user_id = ".$user_id." AND likes.object_id = portfolio.item_id) AS my_likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               ORDER BY portfolio.".$sort_by." DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
    }

    public static function countProjects($lang = 'en', $tags){

            return \Yii::$app->db->createCommand("SELECT COUNT(*) AS cnt
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND item_description.tags LIKE '%$tags%'")->queryAll();

    }

    public static function catalogDesignDecor($lang = 'en', $offset, $limit, $user_id = 0, $sort_by, $tags){

        if($tags){
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, item_description.short_description, portfolio.likes, @my:= (SELECT COUNT(*) FROM `likes` WHERE likes.category = 2 AND likes.user_id = ".$user_id." AND likes.object_id = portfolio.item_id) AS my_likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.catalog_id = 1
                                               AND portfolio.moderation = 1
                                               AND item_description.tags LIKE '%$tags%'
                                               ORDER BY portfolio.".$sort_by." DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
        }else

                return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.owner_id, portfolio.images, item_description.title, item_description.short_description, portfolio.likes, @my:= (SELECT COUNT(*) FROM `likes` WHERE likes.category = 2 AND likes.user_id = ".$user_id." AND likes.object_id = portfolio.item_id) AS my_likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.catalog_id = 1
                                               AND portfolio.moderation = 1
                                               ORDER BY portfolio.".$sort_by." DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
    }

    public static function countDesignDecor($lang = 'en', $tags){

        return \Yii::$app->db->createCommand("SELECT COUNT(*) AS cnt
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.catalog_id = 1
                                               AND portfolio.moderation = 1
                                               AND item_description.tags LIKE '%$tags%'")->queryAll();

    }

    public static function itemForPaymentModal($user_id, $lang){
        return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title, item_description.short_description, portfolio.likes
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               ORDER BY RAND()
                                               LIMIT 4")->queryAll();
    }

    public static function agentAllProjects($lang, $limit, $offset, $q = ''){
        if($q == '')
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND portfolio.catalog_id = 0
                                               ORDER BY portfolio.item_id DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
        else
            return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND portfolio.catalog_id = 0
                                               AND item_description.title LIKE '%$q%'
                                               ORDER BY portfolio.item_id DESC
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
    }

    public static function agentAllProjectsCount($lang, $q = ''){
        return \Yii::$app->db->createCommand("SELECT COUNT(*)
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               AND portfolio.catalog_id = 0
                                               AND item_description.title LIKE '%$q%'")->queryAll();
    }


}
