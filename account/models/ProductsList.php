<?php
namespace frontend\account\models;

use Yii;
use yii\data\ActiveDataProvider;


class ProductsList extends \yii\db\ActiveRecord
{
    public $count = 12;
//    public $sort;
//    public $id_prod;
//    public $user_id;
//    public $product_name;
//    public $company_name;
//    public $meta_tags;
//    public $price;
//    public $images;
//    public $main_image;
//    public $miniature_img;
//    public $prod_desc;


    public static function tableName()
    {
        return 'products_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_prod'], 'integer'],
//            [['meta_tags', 'sort'], 'string'],
            [['product_name', 'company_name', 'meta_tags', 'price', 'images', 'miniature_img', 'main_image', 'prod_desc', ], 'string'],
////            [['price'], 'number'],
////            [['prod_desc'], 'string'],
////            [['likes', 'id_catalog'], 'integer'],
////            [['product_name', 'meta_tegs', 'images', 'main_image'], 'string', 'max' => 255]
        ];
    }

    public function sort($params)
    {
        $query = ProductsList::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => $this->count],
        ]);
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $meta_tags = explode(',', $this->meta_tags);
        $trimmed_tags = array_map("trim", $meta_tags);
        $dataProvider->pagination->pageSize = $this->count;
        if ($this->sort === 'like') {
            $query->orderBy(['likes' => SORT_DESC]);
        } elseif ($this->sort === 'chip') {
            $query->orderBy(['price' => SORT_ASC]);
        } elseif ($this->sort === 'expensive') {
            $query->orderBy(['price' => SORT_DESC]);
        }
        foreach ($trimmed_tags as $tag) {
            $query->orFilterWhere([
                'like', 'meta_tags', $tag
            ]);
        }
        return $dataProvider;
    }

    public function saveNewProduct($company_id, $product_name, $meta_tag, $price, $description, $image1, $image2, $image3)
    {
        $this->product_name = $product_name;
        $this->meta_tags = $meta_tag;
        $this->price = $price;
        $this->user_id = $company_id;
        $this->prod_desc = $description;
        $this->main_image = $image1;
        $this->images = $image1 . (($image2 == null) ? '' :  ' ,' . $image2 ). (($image3 == null) ? '' : ' ,' . $image3);
        $this->miniature_img = $image3;
        if($this->save()) return $this->id_prod;
    }

    public static function countProductsByCompanyId($company_id){
        return $products = static::find()->where(['user_id' => $company_id])->count();
    }
}