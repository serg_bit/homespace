<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "app_recipients".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $app_id
 * @property integer $status
 */
class AppRecipients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_recipients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from', 'to', 'app_id', 'status'], 'required'],
            [['from', 'to', 'app_id', 'status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */

}