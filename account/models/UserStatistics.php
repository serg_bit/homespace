<?php

namespace frontend\account\models;
use Yii;
use yii\data\ActiveDataProvider;


class UserStatistics extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user_statistics';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'month', ], 'integer'],
            [['guests', 'homeowners', 'designers', 'agents', 'manufacturer', 'professionals', ], 'string'],
        ];
    }

}