<?php

namespace frontend\account\models;

use Yii;
use yii\web\Controller;


class User extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['roles', 'avatar', 'specialization', 'message', 'link_user_site', 'link_linkedin', 'link_tumblr', 'link_fb', 'link_tw', 'link_inst', 'link_behance', 'link_google', 'link_pint', 'link_blg', 'company', 'background'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
//    public function attributeLabels()
//    {
//        return [
//            'id' => 'ID',
//            'username' => 'Username',
//            'auth_key' => 'Auth Key',
//            'password_hash' => 'Password',
//            'account_activation_token' => 'Account Activation Token',
//            'password_reset_token' => 'Password Reset Token',
//            'email' => 'Email',
//            'first_name' => 'First Name',
//            'last_name' => 'Last Name',
//            'city' => 'City',
//            'country' => 'Country',
//            'roles' => 'Roles',
//            'status' => 'Activation Status',
//            'paid_status' => 'Paid Status',
//            'created_at' => 'Created At',
//            'updated_at' => 'Updated At',
//            'logo' => 'Logo',
//            'background' => 'Background',
//            'specialization' => 'Specialization',
//            'message' => 'About me',
//            'link_user_site' => 'Link to user site',
//            'link_linkedin' => 'Linkedin',
//            'link_tumblr' => 'Tumblr',
//            'link_fb' => 'Facebook',
//            'link_tw' => 'Twitter',
//            'link_inst' => 'Instagram',
//            'link_behance' => 'Behance',
//            'link_google' => 'Google',
//            'link_pint' => 'Pinterest',
//            'link_blg' => 'Blogger',
//            'avatar' => 'Avatar',
//            'likes' => 'Likes',
//            'company' => 'Company',
//        ];
//    }

    public function saveNewCompany($data)
    {
        $this->avatar = $data['avatar'];
        $this->background = $data['background'];
        $this->logo = $data['logo'];
        $this->first_name = $data['first_name'];
        $this->last_name = $data['last_name'];
        $this->phone = $data['phone'];
        $this->city = $data['city'];
        $this->country = $data['country'];
        $this->address = $data['address'];
        $this->message = $data['message'];
        $this->link_user_site = $data['link_user_site'];
        $this->link_fb = $data['link_fb'];
        $this->link_tw = $data['link_tw'];
        $this->link_inst = $data['link_inst'];
        $this->link_behance = $data['link_behance'];
        $this->link_google = $data['link_google'];
        $this->link_pint = $data['link_pint'];
        $this->link_tumblr = $data['link_tumblr'];
        $this->link_linkedin = $data['link_linkedin'];
        $this->link_blg = $data['link_blg'];
        $this->avatar = $data['avatar'];
        $this->background = $data['background'];
        $this->email = $data['email'];
        $this->username = $data['username'];
        $this->specialization = $data['position'];
        $this->company = $data['company'] . "||" . $data['brand'];
        $this->password_hash = $data['password_hash'];
        $this->specialization = $data['position'];
        $this->roles = 'manufacturer';
        $this->created_at = time();
        if ($this->save()) return $this->id;
    }

    public static function getCompaniesById($companies_id, $pagination)
    {
        return static::find()->where(['in', 'id', $companies_id])->orderBy('id DESC')->offset($pagination->offset)
            ->limit($pagination->limit)->asArray()->all();
    }

//------------------------------------------------
// get company name for autocomplete
//------------------------------------------------
    public static function searchCompanyName($text)
    {
        $companies = static::find()->select(['id', 'company'])->where(['and', ['like', 'company', $text], ['roles' => 'manufacturer']])->orderBy('id DESC')->asArray()->all();
        $company_name = array_map(function ($company) {
            return $array[] = ['id' => $company['id'], 'company_name' => explode('||', $company['company'])[0],];
        }, $companies);
        return $company_name;
    }
//------------------------------------------------
// get company name for manufacturer.php
//------------------------------------------------
    public static function selectCompanyById($id_company)
    {
        return static::findOne(['id' => $id_company]);
    }

    //------------------------------------------------
    // get company name for manufacturer.php
    //-

    public static function companiesList($pagination)
    {
        return static::find()->where(['roles' => 'manufacturer'])->orderBy('id DESC')->offset($pagination->offset)
            ->limit($pagination->limit)->asArray()->all();
    }
//------------------------------------------------
// edit company
//------------------------------------------------
    public static function editCompanyById($request)
    {
        $company = static::findOne(['id' => $request->post('id')]);
        $company->username = $request->post('company_login', $company->username);
        $company->password_hash = ($request->post('company_password') != null) ? Yii::$app->security->generatePasswordHash($request->post('company_password')) : $company->password_hash;
        $company->email = $request->post('company_email', $company->email);
        $company->first_name = $request->post('company_first_name', $company->first_name);
        $company->last_name = $request->post('company_last_name', $company->last_name);
        $company->city = $request->post('company_city', $company->city);
        $company->country = $request->post('company_country', $company->country);
        $company->address = $request->post('company_address', $company->address);
        $company->specialization = $request->post('company_position', $company->specialization);
        $company->message = $request->post('company_message', $company->message);
        $company->link_user_site = $request->post('company_site', $company->link_user_site);
        $company->link_fb = $request->post('link_fb', $company->link_fb);
        $company->link_tw = $request->post('link_twitter', $company->link_tw);
        $company->link_inst = $request->post('link_instagram', $company->link_inst);
        $company->link_linkedin = $request->post('link_linkedin', $company->link_linkedin);
        $company->link_tumblr = $request->post('link_tumblr', $company->link_tumblr);
        $company->link_behance = $request->post('link_behance', $company->link_behance);
        $company->link_google = $request->post('link_google', $company->link_google);
        $company->link_pint = $request->post('link_pinterest', $company->link_pint);
        $company->link_blg = $request->post('link_blogger', $company->link_blg);
        $company->company = $request->post('company_name', explode("||", $company->company)[0]) . "||" . $request->post('brand_name', explode("||", $company->company)[1]);
        if ($request->post('company_logo') != null) {
            $logo = explode('/', $request->post('company_logo'));
            $logo = array_pop($logo);
            $company->logo = $logo;
        }
        $company->updated_at = time();
        if ($request->post('company_avatar') != null) {
            $avatar = explode('/', $request->post('company_avatar'));
            $avatar = array_pop($avatar);
            $company->avatar = $avatar;
        }
        if ($request->post('company_bg') != null) {
            $background = explode('/', $request->post('company_bg'));
            $background = array_pop($background);
            $company->background = $background;
        }
        return $company->update();
    }
//------------------------------------------------
// get all agents
//------------------------------------------------
    public static function selectsAgents($pagination)
    {
        return static::find()->where(['roles' => 'agent'])->orderBy('id DESC')->offset($pagination->offset)
            ->limit($pagination->limit)->orderBy(['id'=>'DESC'])->asArray()->all();
    }
//------------------------------------------------
// get  count of agents
//------------------------------------------------
    public static function countAgents(){
        return static::find()->where(['roles' => 'agent'])->count();
    }

    public static function myDesignersAndArchitects($offset, $limit){
        return \Yii::$app->db->createCommand("SELECT id, background, logo, company, avatar, first_name, last_name, country, city, message, @my:= (SELECT COUNT(*) FROM `portfolio` WHERE `owner_id` = id) AS portfolio, @temp1:= 1 AS confirmed
                                               FROM  `user`
                                               WHERE roles = 'designer'
                                               LIMIT $limit
                                               OFFSET $offset;")->queryAll();
    }
}