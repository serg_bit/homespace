<?php

namespace frontend\account\models;

//use common\models\User;
use Yii;
use yii\web\UploadedFile;
use frontend\account\models\AppRecipients;

class Application extends \yii\db\ActiveRecord
{
    public $file;
    public $recipients;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['title_app', 'description_app', 'category_app','recipients'], 'required'],
            [['user_id', 'id_folder', 'id_prod'], 'integer'],
            [['description_app'], 'string'],
            [['title_app', 'url_app', 'category_app'], 'string', 'max' => 255],
            [['file'], 'file', 'maxFiles' => 10, 'extensions' => 'doc, txt, rtf, jpg, png, pdf, jpeg', 'maxSize' => 3000000,'tooBig' => 'Max size is 3mb'],
            [['recipients'], 'each', 'rule'=> ['integer']],



        ];

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title_app' => 'Name of application',
            'url_app' => 'Files',
            'description_app' => 'Description',
            'id_folder' => 'Folder',
            'id_prod' => 'Products',
            'category_app' => 'Category',
        ];
    }

    public function uploadApp()
    {

        if (Yii::$app->request->isPost) {
            $user_id = \Yii::$app->user->identity->id;
                 $this->file = UploadedFile::getInstances($this, 'file');
                $uploads = "";
                foreach ($this->file as $file) {
                    $name = rand(111, 999) . '_' . $file->baseName . '.' . $file->extension;
                    $file->saveAs(Yii::getAlias('@media') . '/application/new_app/' . $name);
                    $uploads .= $name . '|';
                }
                $this->url_app = substr($uploads, 0, -1);

            $this->user_id = $user_id;


                if ($this->save(false)) {
                    $last_id = $this->id_app;
                    foreach($this->recipients as $to){
                        $model = new AppRecipients();
                        $model->from = $user_id;
                        $model->to = $to;
                        $model->app_id = $last_id;
                        $model->status = 0;
                        $model->save();
                    }
                    return true;
                }
            //}
        }
    }
    public static function findFolder($id_folder){
        $image = (new \yii\db\Query())
            ->select(['p.miniature_img'])
            ->from(['p' => 'products_list','o' => 'orders'])
            ->where('p.id_prod=o.id_prod')->andWhere(['o.id_folder' => $id_folder])->limit(4)->all();
        $folder = (new \yii\db\Query())->from('order_folder')->where(['id_folder' => $id_folder, 'parent_status'=>0])->one();
        return array(['image'=>$image, 'folder'=>$folder]);
    }
    public static function findRecipient($app_id){
        return AppRecipients::find()->select('to')->where(['app_id' => $app_id])->column();
    }

    public static function coment($app_id)
    {
        $user_id = Yii::$app->user->identity->id;
        $coment = (new \yii\db\Query())
            ->select(['a.message', 'a.files', 'a.date', 'u.avatar', 'u.first_name', 'u.last_name', 'u.specialization', 'u.id'])
            ->from(['a' => 'app_recipients'])
            ->innerJoin('user u','a.to=u.id')
            ->where(['a.from' => $user_id, 'app_id' =>$app_id])->all();
        
        return $coment;
    }

}

