<?php

namespace frontend\account\models;

use Yii;

/**
 * This is the model class for table "agent_project_description".
 *
 * @property integer $id
 * @property integer $id_project
 * @property integer $id_agent
 * @property string $description
 * @property integer $approved
 */
class AgentProjectDescription extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agent_project_description';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_project', 'id_agent', 'description'], 'required'],
            [['id_project', 'id_agent', 'approved'], 'integer'],
            [['description'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_project' => 'Id Project',
            'id_agent' => 'Id Agent',
            'description' => 'Description',
            'approved' => 'Approved',
        ];
    }

    public static function agentAddDescription($id, $user_id, $desc){
        $temp = self::findOne(['id_project' => $id, 'id_agent' => $user_id, 'approved' => 0]);
        if ($temp) {
            $temp['description'] = $desc;
            $temp->update();
        }else
            Yii::$app->db->createCommand()->insert('agent_project_description', [
                'id_project' => $id,
                'id_agent' => $user_id,
                'description' => $desc,
            ])->execute();
    }

    public static function onConfirmationProjects($lang, $user_id, $approved, $limit, $offset){
        return \Yii::$app->db->createCommand("SELECT portfolio.item_id, portfolio.images, item_description.title
                                               FROM  `portfolio`
                                               LEFT JOIN `item_description` ON (item_description.item_id = portfolio.item_id)
                                               WHERE portfolio.item_id IN (SELECT `id_project` FROM `agent_project_description` WHERE `id_agent` = $user_id AND `approved` = $approved)
                                               AND item_description.language = '$lang'
                                               AND portfolio.active = 1
                                               AND portfolio.moderation = 1
                                               LIMIT $limit
                                               OFFSET $offset")->queryAll();
    }
}
