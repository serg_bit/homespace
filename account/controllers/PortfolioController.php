<?php

namespace frontend\account\controllers;

use Yii;
use yii\data\Pagination;
use frontend\models\Language;
use frontend\models\Style;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\models\CatalogCategories;

class PortfolioController extends \yii\web\Controller
{
	private $user_id;
    private $paid_status = 2;
    private $moderation = 1;
    private $item_count = 0;
    private $max_item_count = 9;

  	public function init()
    {
        if(Yii::$app->user->identity->roles != 'designer')
            Yii::$app->getResponse()->redirect(array('/account/profile'));
        $this->user_id = Yii::$app->user->identity->id;
        $this->item_count = Portfolio::find()->where(['owner_id' => $this->user_id])->count();
        $this->paid_status = Yii::$app->user->identity->paid_status;
    }

    public function actionIndex()
    {    
        $lang = Language::getCurrent()->url;
		$create = Yii::$app->urlManager->createUrl(['account/portfolio/create']);
        if($this->paid_status == 0){
            $data = $this->getUserPortfolio($lang, $this->user_id, 0, true);
            return $this->render('index', [
                'items' => $data['items'],
                'create_url' => $create,
                'max_item_count' => $this->max_item_count,
            ]);
        }else if($this->paid_status == 1){
            $data = $this->getUserPortfolio($lang, $this->user_id, 0);
            $this->max_item_count = 81;
            $decor = $this->getUserPortfolio($lang, $this->user_id, 1);
            return $this->render('index2', [
                'items' => $data['items'],
                'pagination' => $data['pagination'],
                'decor' => $decor['items'],
                'pagination2' => $decor['pagination'],
                'create_url' => $create,
                'best_items' => $this->getBestItems($this->user_id, 2),
                'max_item_count' => $this->max_item_count,
            ]);
        }else if($this->paid_status == 2){
            $data = $this->getUserPortfolio($lang, $this->user_id, 0);
            $this->max_item_count = 81;
            $decor = $this->getUserPortfolio($lang, $this->user_id, 1);
            return $this->render('index3', [
                'items' => $data['items'],
                'pagination' => $data['pagination'],
                'decor' => $decor['items'],
                'pagination2' => $decor['pagination'],
                'create_url' => $create,
                'best_items' => $this->getBestItems($this->user_id, 2),
                'max_item_count' => $this->max_item_count,
                'style' => $this->getStyleItem($this->user_id, 3, $lang),
                'style_list' => Style::allStyle($lang)
            ]);
        }

    }

    public function actionShare()
    {
        $lang = Language::getCurrent()->url;
        return $this->render('share', [
                'items' => $this->getUserPortfolio($lang, $this->user_id, 0, true),
            ]);
    }

    public function actionItem()
    {    
        $current_id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $edit = Yii::$app->urlManager->createUrl(['account/portfolio/edit', 'id' => $current_id]);
		$lang = Language::getCurrent()->url;

    	$item = $this->getItem($current_id, $lang, $this->user_id);

        if($item['title'] == "" && $item['images'] == ""){
            $item = false;
        }

        return $this->render('item', [
            'item' => $item,
            'edit_url' => $edit,
            'current_id' => $current_id,
        ]);
    }

    public function actionCreate()
    {    

        if($this->item_count < $this->max_item_count){
            return $this->render('create', [
                'categories' => CatalogCategories::find()->where(['param' => 0])->all(),
                'lang' => Language::getCurrent()->url,
                'paid_status' => $this->paid_status
            ]);
        }else Yii::$app->getResponse()->redirect(array('/account/portfolio'));

    }

    public function actionEdit()
    {    

        $current_id = isset($_GET["id"]) ? $_GET["id"] : 0;

        $item = $this->getEditItem($current_id, $this->user_id);
        if($item){
            $categories = CatalogCategories::find()->where(['param' => 0])->asArray()->all();
            $cat = null;
            for($i = 0; $i < count($categories); $i++){
                if($categories[$i]['id'] == $item['cat_id']){
                    $cat = $categories[$i];
                    unset($categories[$i]);
                    break;
                }
            }

            if($cat != null)
                array_unshift($categories, $cat);

        }

        return $this->render('edit', [
            'item' => $item,
            'item_id' => $current_id,
            'categories' => $categories,
            'lang' => Language::getCurrent()->url,
            'paid_status' => $this->paid_status
        ]);

    }

    public function actionDelete()
    {    

        $id = isset($_POST["id"]) ? $_POST["id"] : 0;

        if($id != 0){

            Portfolio::deleteAll('item_id = :item_id', [':item_id' => $id]);

            ItemDescription::deleteAll('item_id = :item_id', [':item_id' => $id]);

            Yii::$app->getResponse()->redirect(array('/account/portfolio'));

        }else Yii::$app->getResponse()->redirect(array('/account/portfolio'));
   
    }

    public function actionModalItems()
    {
        $lang = Language::getCurrent()->url;
        $modal_item = $this->getPortfolioModal($this->user_id, $lang);
        $count = 1;
        foreach ($modal_item as $item) {
            echo '<div id="work-'.$count.'" class="portfolio-item modal-item work" onclick="setBestItem(this)">';
            printf('<img src="%s" data-item="%s" data-link="%s">', $item['image'], $item['id'], $item['url']);
            echo '<p><input type="radio" id="best-img-'.$count.'" name="best" checked=""><label for="best-img-'.$count.'"></label></p>';
            echo '</div>';
            $count++;
        }
    }

    public function actionModalStyleItems()
    {
        $lang = Language::getCurrent()->url;
        $modal_item = $this->getPortfolioModal($this->user_id, $lang);
        $count = 1;
        foreach ($modal_item as $item) {
            echo '<div id="style-'.$count.'" class="portfolio-item modal-item work" onclick="setStyleItem(this)">';
            printf('<img src="%s" data-item="%s" data-link="%s">', $item['image'], $item['id'], $item['url']);
            echo '<p><input type="radio" id="style-img-'.$count.'" name="style" checked=""><label for="style-img-'.$count.'"></label></p>';
            echo '</div>';
            $count++;
        }
    }

    public function actionSetStyle()
    {
        $new_id = isset($_POST["id"]) ? $_POST["id"] : "";
        $current_id = isset($_POST["current_id"]) ? $_POST["current_id"] : "";
        $style_id = Yii::$app->request->post('style_id', 0);
        if( $current_id != ""){
            $item = Portfolio::findOne(['item_id' => $current_id, 'owner_id' => $this->user_id]);
            $item->status = 1;
            $item->style_id = 0;
            $item->save();
        }

        if($new_id != ""){
            $item = null;
            $item = Portfolio::findOne(['item_id' => $new_id, 'owner_id' => $this->user_id]);
            $item->status = 3;
            $item->style_id = $style_id;
            $item->save();
        }

        echo "Changes saved correctly!";
    }

    public function actionUpload()
    {

        if (isset($_FILES['file'])) {
      
			$file = $_FILES['file'];
            $temp = explode("/", $file['type']);

            if($temp[1] == 'png' || $temp[1] == 'jpeg'){
            	$path = Yii::getAlias('@media').'/temp/';
            	$ext = '.'.$temp[1];
            	$name = time();
            	move_uploaded_file ( $file['tmp_name'] , $path.$name.$ext );
                echo '/media/temp/'.$name.$ext;
            }
        }

    }

    public function actionStatus()
    {

    	$current_id = isset($_POST["last"]) ? $_POST["last"] : "";
        $new_id = isset($_POST["new_id"]) ? $_POST["new_id"] : "";

        if($current_id != ""){
        	$item = null;
			$item = Portfolio::findOne(['item_id' => $current_id, 'owner_id' => $this->user_id]);
			$item->status = 1;
			$item->save();
        }

        if($new_id != ""){
        	$item = null;
        	$item = Portfolio::findOne(['item_id' => $new_id, 'owner_id' => $this->user_id]);
        	$item->status = 2;
        	$item->save();
        }

    }

    public function actionSave()
    {

        $titles = isset($_POST["titles"]) ? $_POST["titles"] : "";
        $description = isset($_POST["description"]) ? $_POST["description"] : "";
        $short_description = isset($_POST["short_description"]) ? $_POST["short_description"] : "";
        $tags = isset($_POST["tags"]) ? $_POST["tags"] : "";
        $cat_id = isset($_POST["cat_id"]) ? $_POST["cat_id"] : 0;
        $catalog_id = isset($_POST["catalog_id"]) ? $_POST["catalog_id"] : 0;

        $descriptions[0] = array( 'language' => 'ru', 'title' => $titles[0], 'description' => htmlentities($description[0]), 'short_description' => $short_description[0], 'tags' => $tags[0]);
        $descriptions[1] = array( 'language' => 'en', 'title' => $titles[1], 'description' => htmlentities($description[1]), 'short_description' => $short_description[1], 'tags' => $tags[1]);


        $images[0] = isset($_POST["image1"]) ? $_POST["image1"] : "";
        $images[1] = isset($_POST["image2"]) ? $_POST["image2"] : "";
        $images[2] = isset($_POST["image3"]) ? $_POST["image3"] : "";

        $image_url = "";

        $path = Yii::getAlias('@media');

        foreach ($images as $image) {

            if($image != ""){
                $name =  explode("/", $image);
                $name = array_pop ( $name );
                if (file_exists($path.'/temp/'.$name)) {
                    copy( $path.'/temp/'.$name , $path.'/portfolio/'.$name );
                }
                $image_url .= $name."|";
            }

        }


        $portfolio = new Portfolio();
		$portfolio->owner_id = $this->user_id;
		$portfolio->images = $image_url;
        $portfolio->catalog_id = $catalog_id;
        $portfolio->cat_id = $cat_id;
		$portfolio->date = time();

		if($portfolio->save()){
			$last_id = $portfolio->item_id;
          
            foreach ($descriptions as $value) {
            	$description = null;
			    $description = new ItemDescription();
				$description->isNewRecord = true;
                $description->item_id = $last_id;
                $description->language = $value['language'];
                $description->title = $value['title'];
                $description->short_description = $value['short_description'];
                $description->description = $value['description'];
                $description->tags = $value['tags'];
                $description->save();
                
            }

			echo Yii::$app->urlManager->createUrl(['account/portfolio']);
		}
    
    }

    public function actionUpdate()
    {
        $item_id = isset($_POST["item_id"]) ? $_POST["item_id"] : 0;
        $titles = isset($_POST["titles"]) ? $_POST["titles"] : "";
        $description = isset($_POST["description"]) ? $_POST["description"] : "";
        $short_description = isset($_POST["short_description"]) ? $_POST["short_description"] : "";
        $tags = isset($_POST["tags"]) ? $_POST["tags"] : "";
        $cat_id = isset($_POST["cat_id"]) ? $_POST["cat_id"] : 0;
        $catalog_id = isset($_POST["catalog_id"]) ? $_POST["catalog_id"] : 0;

        $descriptions[0] = array( 'language' => 'ru', 'title' => $titles[0], 'description' => htmlentities($description[0]), 'short_description' => $short_description[0], 'tags' => $tags[0]);
        $descriptions[1] = array( 'language' => 'en', 'title' => $titles[1], 'description' => htmlentities($description[1]), 'short_description' => $short_description[1], 'tags' => $tags[1]);


        $images[0] = isset($_POST["image1"]) ? $_POST["image1"] : "";
        $images[1] = isset($_POST["image2"]) ? $_POST["image2"] : "";
        $images[2] = isset($_POST["image3"]) ? $_POST["image3"] : "";

        $image_url = "";

        $path = Yii::getAlias('@media');

        foreach ($images as $image) {

            if($image != ""){
                $name =  explode("/", $image);
                $name = array_pop ( $name );
                if (file_exists($path.'/temp/'.$name)) {
                    copy( $path.'/temp/'.$name , $path.'/portfolio/'.$name );
                }
                $image_url .= $name."|";
            }

        }


        $portfolio = Portfolio::findOne(['item_id' => $item_id, 'owner_id' => $this->user_id]);
        $portfolio->images = $image_url;
        $portfolio->catalog_id = $catalog_id;
        $portfolio->cat_id = $cat_id;
        $portfolio->moderation = $this->moderation;
        if($portfolio->save()){
            foreach ($descriptions as $value) {
                $description = null;
                $description = ItemDescription::findOne(['item_id' => $item_id, 'language' => $value['language']]);
                $description->item_id = $item_id;
                $description->language = $value['language'];
                $description->title = $value['title'];
                $description->short_description = $value['short_description'];
                $description->description = $value['description'];
                $description->tags = $value['tags'];
                $description->save();
    
            }

        }


        echo Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $item_id]);
    
    }


    private function getUserPortfolio($lang, $owner_id, $catalog_id = 0, $status = false)
    {
        if($status){
           $cnt =  Portfolio::find()->where(['owner_id' => $owner_id, 'moderation' => $this->moderation])->count();
        }else $cnt =  Portfolio::find()->where(['owner_id' => $owner_id, 'moderation' => $this->moderation, 'catalog_id' => $catalog_id])->count();
    	$pagination = new Pagination([
			'defaultPageSize' => 10,
			'totalCount' => $cnt
		]);

        $portfolio = array();
        $count = 0;
        if($status){
            $temp = Portfolio::find()->where(['owner_id' => $owner_id, 'moderation' => $this->moderation])->offset($pagination->offset)->limit($pagination->limit)->all();
        }else
        $temp = Portfolio::find()->where(['owner_id' => $owner_id, 'moderation' => $this->moderation, 'catalog_id' => $catalog_id])->offset($pagination->offset)->limit($pagination->limit)->all();
        foreach ($temp as $value) {

            $images = explode("|", $value['images']);
            $description = ItemDescription::find()->where(['item_id' => $value['item_id'], 'language' => $lang])->one();

            $portfolio['items'][$count] = [
            	'id' => $value['item_id'],
                'image' => '/media/portfolio/'.$images[0],
                'title' => $description['title'],
                'url' =>  Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $value['item_id'] ]),
                'share_link' => Yii::$app->urlManager->createAbsoluteUrl(['about/item', 'id' => $value['item_id'] ]),

            ];

            $count++;
        }

		$portfolio['pagination'] = $pagination;

        return $portfolio;
       
    }

    private function getPortfolioModal($owner_id, $lang)
    {
        $portfolio = array();
        $temp = Portfolio::find()->where(['owner_id' => $owner_id, 'moderation' => $this->moderation])->all();
        foreach ($temp as $value) {

            $description = ItemDescription::find()->where(['item_id' => $value['item_id'], 'language' => $lang])->one();

            $images = explode("|", $value['images']);

            $portfolio[] = [
            	'id' => $value['item_id'],
                'image' => '/media/portfolio/'.$images[0],
                'url' =>  Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $value['item_id'] ]),
                'description' => $description['short_description']
            ];

        }


        return $portfolio;
       
    }

    private function getItem($id, $lang, $owner_id)
    {

        $item = array();
        $temp_item = Portfolio::find()->where(['owner_id' => $owner_id, 'item_id' => $id])->one();
        $temp_description = ItemDescription::find()->where(['item_id' => $temp_item['item_id'], 'language' => $lang])->one();

        if($lang == "ru"){
            $temp_day = Yii::t('main', 'days');
            $date = date('j '.$temp_day[date('F', $temp_item['date'])].', Y', $temp_item['date']);
        }else  $date = date('j F, Y', $temp_item['date']);


        $temp_images = explode("|", $temp_item['images']);
        $count = 0;
        foreach ($temp_images as $value) {
            if($value != ""){
                $images[$count] = '/media/portfolio/'.$value;
                $count++;
            }
        }


        $temp_tags = explode(",", $temp_description['tags']);
        $count = 0;
        foreach ($temp_tags as $tag) {
            if($tag != ""){
                $tags[$count] = '<a href="#">#'.$tag.'</a>';
            }

            $count++;
        }


        $item = [
            'title' => $temp_description['title'],
            'images' => $images,
            'date' => $date,
            'description' => html_entity_decode($temp_description['description']),
            'tags' => $tags,

        ];


        return $item;
       
    }

    private function getEditItem($id, $owner_id)
    {

        $item = array();
        $temp_item = Portfolio::find()->where(['owner_id' => $owner_id, 'item_id' => $id])->one();
        $temp_description = ItemDescription::find()->where(['item_id' => $temp_item['item_id']])->all();

        $temp_images = explode("|", $temp_item['images']);
        $count = 0;
        foreach ($temp_images as $value) {
            if($value != ""){
                $images[$count] = '/media/portfolio/'.$value;
                $count++;
            }
        }

        $count = 0;
        foreach ($temp_description as $value) {
            $description[$count] = [
                'title' => $value['title'],
                'short_description' => $value['short_description'],
                'description' => html_entity_decode($value['description']),
                'tags' => $value['tags']
            ];
            $count++;
        }


        $item = [
            'images' => $images,
            'description_ru' => $description[0],
            'description_en' => $description[1],
            'catalog_id' => $temp_item['catalog_id'],
            'cat_id' => $temp_item['cat_id'],
        ];

        if($temp_item['images'] == "")
        	$item = false;

        return $item;
       
    }

    private function getBestItems($owner_id, $status)
    {
    	$items = Portfolio::find()->where(['owner_id' => $owner_id, 'status' => $status])->all();
    	$count = 0;
    	foreach ($items as $item) {
    		$temp_images = explode("|", $item['images']);

    		$data[$count] = [
    			'id' => $item['item_id'],
    			'image' => '/media/portfolio/'.$temp_images[0],
    			'url' => Yii::$app->urlManager->createUrl(['account/portfolio/item', 'id' => $item['item_id'] ])
    		];

    		$count++;
    	}

    	if($count < 3){

    		for ($i=$count; $i < 3; $i++) { 
    			$data[$i] = [
    				'id' => '',
    				'image' => '/images/no-img.png',
    				'url' => ''
    			];
    		}
    	}


    	return $data;
    }

    private function getStyleItem($user_id, $status, $lang)
    {
        $data = [];
        $item = Portfolio::find()->where(['owner_id' => $user_id, 'status' => $status, 'moderation' => $this->moderation])->one();
        if($item) {
            $description = ItemDescription::find()->where(['item_id' => $item['item_id'], 'language' => $lang])->one();
            $image = explode('|', $item['images']);
            $data = [
                'id' => $item['item_id'],
                'image' => '/media/portfolio/'.$image[0],
            ];
        }else{
            $data = [
                'id' => '',
                'image' => '/images/camera3.png',
            ];
        }

        return $data;

    }


}
