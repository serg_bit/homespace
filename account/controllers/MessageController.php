<?php

namespace frontend\account\controllers;

use yii;
use yii\web\Controller;
use frontend\account\models\Groups;
use frontend\account\models\Messages;
use frontend\account\models\Likes;
use frontend\account\models\User;
use frontend\account\models\CountGroupsForm;
use frontend\models\Language;
use yii\data\Pagination;
use yii\helpers\Json;


class MessageController extends \yii\web\Controller
{

    private $currentUserId;
    private $currentUser;


    public function init()
    {
        $this->currentUserId = \Yii::$app->user->identity->id;
        $this->currentUser  = User::findOne($this->currentUserId);
    }


    public function actionIndex()
    {

        $interlocutorId = Yii::$app->request->getQueryString();
        $interlocutor = $this->getUserInformation($interlocutorId);
        $pagination = new Pagination([
            'defaultPageSize' => 10,
            'totalCount' => Messages::countCorrespondence($this->currentUserId,$interlocutorId),
        ]);

        $correspondence = Messages::getAllCorrespondence($this->currentUserId, $interlocutorId, $pagination->limit, $pagination->offset);

//        $status = Yii::$app->db->createCommand()->update("messages", ["status" => 1], ['to'=>$this->currentUserId, 'from'=>$interlocutorId])->execute();

        return $this->render('index', [
        'correspondence' => $correspondence,
        'currentUser' => $this->currentUser,
        'interlocutor' => $interlocutor,
        'pagination' => $pagination,
        ]);
    }


    public function actionSend(){

        $path1 = Yii::getAlias('@media') . '/temp/';
        $path2 = Yii::getAlias('@media') . '/upload/';
        $i = -1;
        $files = "";

        if (isset($_FILES)) {

            foreach( $_FILES as $file ){

                $temp = explode("/", $file['type']);
                if($temp[1] == 'doc' || $temp[1] == 'txt'|| $temp[1] == 'rtf'|| $temp[1] == 'jpg'|| $temp[1] == 'png' || $temp[1] == 'rtf') {

                    $i++;
                    $ext = '.' . $temp[1];
                    $name = time().rand(000,999);

                    if( stristr ($_REQUEST['fileNames'], $file['name']) != false ){

                            $file_name[$i] = rand(111, 999)."_".$file['name'];
                            move_uploaded_file($file['tmp_name'] , $path1.$name.$ext );
                            copy( $path1.$name.$ext , $path2.$file_name[$i] );
                            $files .= $file_name[$i];
                            $files .= "|";

                    }

                }
                $message = new Messages;
                $message->to = $_REQUEST['recipient'];
                $message->message = $_REQUEST['text'];
                $message->from = \Yii::$app->user->identity->id;
                if (!empty($files)){
                    $message->file_app = $files;
                }else
                    $message->file_app = NULL;

            }

            $message->save();

//            if(!isset($this->currentUser->avatar))$data['sender_avatar'] = "no-img.png";
//            else $data['sender_avatar'] = $this->currentUser->avatar;
//
//            $data['sender_specialization'] = $this->currentUser->specialization;
//            $data['sender_username'] = $this->currentUser->first_name." ".$this->currentUser->last_name;
//            $data['url_avatar'] = Yii::getAlias('@avatar/'.$data['sender_avatar']);
//            $data['file_name'] = $file_name;
//            $data['path'] = $path2;
//            $data['message'] = $_REQUEST['text'];
//            $data['created_at'] = date("Y-m-d H:m:s");
//            $data = json_encode($data);
//
//            return $data;

        }

    }


    public function actionMessage(){

            $message = new Messages;
            $message->to = $_REQUEST['recipient'];
            $message->message = $_REQUEST['text'];
            $message->from = \Yii::$app->user->identity->id;
            $message->save();

//            if(!isset($this->currentUser->avatar))$data['sender_avatar'] = "no-img.png";
//            else $data['sender_avatar'] = $this->currentUser->avatar;
//            $data['sender_username'] = $this->currentUser->first_name." ".$this->currentUser->last_name;
//            $data['url_avatar'] = Yii::getAlias('@avatar/'.$data['sender_avatar']);
//            $data['message'] = $_REQUEST['text'];
//            $data['sender_specialization'] = $this->currentUser->specialization;
//            $data['created_at'] = date("Y-m-d H:m:s");
//            $data = json_encode($data);
//
//            return $data;

    }


    public function actionSearchrecipients(){

        $users = User::find()->select(['id', 'first_name', 'last_name'])->where(['or', ['like', 'username', $_REQUEST['search']], ['like', 'first_name', $_REQUEST['search']], ['like', 'last_name', $_REQUEST['search']]])->all();
        $i = -1; 

        foreach ($users as $user) {

            $i++;    
            $recipient[$i]['id'] = $user->id;
            $recipient[$i]['first_name'] = $user->first_name;
            $recipient[$i]['last_name'] = $user->last_name;
           
        }

        $recipient = json_encode($recipient);

        return($recipient);

    }


    
    private function getUserInformation($id){

        $userInformation = User::find()->select(['id', 'first_name', 'last_name', 'avatar', 'specialization'])->where(['id'=>$id])->asArray()->one();

        return $userInformation;
    }
}