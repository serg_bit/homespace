<?php

namespace frontend\account\controllers;

use yii;
use yii\web\Controller;
use frontend\account\models\User;
use frontend\models\UserHonours;
use frontend\models\UserVideos;
use frontend\models\UserLicenses;
use frontend\models\PortfolioCollections;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\account\models\Specialization;
use frontend\models\Language;


class EditController extends Controller
{

    private $user_id;
    private $moderation = 1;
    private $paid_status = 0;


    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->paid_status = Yii::$app->user->identity->paid_status;
    }

    // Views
    public function actionIndex()
    {
        $user_information = User::find()->where(['id' => $this->user_id])->one();
        $specialization = $this->getUserSpecialization($user_information);
        if (Yii::$app->user->identity->roles == 'agent') {
            return $this->render('index_agent', [
                'user_information' => $user_information,
                'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
            ]);
        } else {
            return $this->render('index', [
                'user_information' => $user_information,
                'specialization' => $specialization,
                'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
                'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
            ]);
        }


//            $lang = Language::getCurrent()->url;
//            return $this->render('index2b', [
//                'user_information' => $user_information,
//                'specialization' => $specialization,
//                'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
//                'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
//                'collections' => $this->getCollections($lang, $this->user_id),
//                'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
//            ]);

    }

    // Edit main information
    public function actionModalItems()
    {
        $lang = Language::getCurrent()->url;
        $modal_items = $this->getPortfolioModal($lang, $this->user_id);
        if ($modal_items) {
            foreach ($modal_items as $item) {
                echo '<div class="portfolio-item modal-item" >';
                echo '<img  src="' . $item['image'] . '" data-item="' . $item['id'] . '" data-title="' . $item['title'] . '">';
                echo '<p><input type="radio" id="bg-' . $item['id'] . '" name="items"><label for="bg-' . $item['id'] . '"></label></p>';
                echo '</div>';
            }
        }

    }

    // Edit main information
    public function actionMain()
    {

        $user_information = User::find()->where(['id' => $this->user_id])->one();
        $specialization = $this->getUserSpecialization($user_information);

        return $this->render('main', [
            'user_information' => $user_information,
            'specialization' => $specialization,
        ]);
    }

    // Edit honours
    public function actionHonours()
    {
        return $this->render('honours', [
            'honours' => UserHonours::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
        ]);
    }

    // Edit Licenses
    public function actionLicenses()
    {
        return $this->render('licenses', [
            'licenses' => UserLicenses::find()->where(['user_id' => $this->user_id, 'moderation' => $this->moderation])->all(),
        ]);
    }

    // Edit Videos
    public function actionVideos()
    {
        return $this->render('videos', [
            'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all(),
        ]);
    }

    // Edit Collections
    public function actionCollections()
    {
        return Yii::$app->getResponse()->redirect(array('/account/edit'));
        if ($this->paid_status >= 1) {
            $lang = Language::getCurrent()->url;
            return $this->render('collections', [
                'collections' => $this->getCollections($lang, $this->user_id),
            ]);
        } else Yii::$app->getResponse()->redirect(array('/account/edit'));
    }

    // End Views

    // Ajax Actions
    public function actionLoadingImage()
    {
        if (isset($_FILES['file'])) {

            $temp_path = isset($_POST['path']) ? $_POST['path'] : false;

            $file = $_FILES['file'];
            $temp = explode("/", $file['type']);

            if ($temp[1] == 'png' || $temp[1] == 'jpeg') {
                $path = Yii::getAlias('@media') . '/profile/licenses/';
                $root = '/media/profile/licenses/';
                if ($temp_path == 'avatar') {
                    $path = Yii::getAlias('@media') . '/profile/avatar/';
                    $root = '/media/profile/avatar/';
                }
                $ext = '.' . $temp[1];
                $name = time();
                move_uploaded_file($file['tmp_name'], $path . $name . $ext);
                echo $root . $name . $ext;
            }
        } else echo "/images/team1.png";
    }

    public function actionLoadBack()
    {
        if (isset($_FILES['file'])) {

            $file = $_FILES['file'];
            $temp = explode("/", $file['type']);

            if ($temp[1] == 'png' || $temp[1] == 'jpeg') {
                $path = Yii::getAlias('@media') . '/profile/background/';
                $ext = '.' . $temp[1];
                $name = time();
                move_uploaded_file($file['tmp_name'], $path . $name . $ext);
                echo $name . $ext;
            }
        } else echo "bg-img-1.png";
    }

    public function actionDeleteCollection()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : false;
        if ($id) {
            PortfolioCollections::findOne($id)->delete();
            echo "Data successfully removed!";
        }
    }

    public function actionSaveCollections()
    {
        $data = isset($_POST['data']) ? $_POST['data'] : false;
        if ($data) {
            foreach ($data as $value) {
                if ($value[0] == "") {
                    $collection = new PortfolioCollections();
                    $collection->isNewRecord = true;
                    $collection->user_id = $this->user_id;
                    $collection->items = $value[3];
                    $collection->main = $value[4];
                    $collection->name = $value[1];
                    $collection->about = $value[2];
                    $collection->save();
                } else {
                    $collection = PortfolioCollections::findOne(['id' => $value[0]]);
                    if ($collection) {
                        $collection->items = $value[3];
                        $collection->main = $value[4];
                        $collection->name = $value[1];
                        $collection->about = $value[2];
                        $collection->save();
                    }
                }
            }

            echo "Changes saved correctly!";
        }
    }

    public function actionSaveInformation()
    {
        $formData = isset($_POST['formData']) ? $_POST['formData'] : false;

        $profile_img = isset($_POST['profile_img']) ? $_POST['profile_img'] : "";
        $profile_img = explode('/', $profile_img);
        $profile_img = array_pop($profile_img);

        $specialization = isset($_POST['specialization']) ? $_POST['specialization'] : "";
        $background = isset($_POST['background']) ? $_POST['background'] : "";

        $logo = isset($_POST['logo']) ? $_POST['logo'] : "";
        $logo = explode('/', $logo);
        $logo = array_pop($logo);

        $item = User::findOne(['id' => $this->user_id]);
        $item->first_name = !empty($formData[0]['value']) ? $formData[0]['value'] : $item->first_name;
        $item->last_name = !empty($formData[1]['value']) ? $formData[1]['value'] : $item->last_name;
        $item->phone = !empty($formData[3]['value']) ? $formData[3]['value'] : $item->phone;
        $item->city = !empty($formData[2]['value']) ? $formData[2]['value'] : $item->city;
        //  company
        $item->company = $formData[4]['value'];
        $item->employees = $formData[5]['value'] . ',' . $formData[6]['value'] . ',' . $formData[7]['value'];

        $item->message = $formData[8]['value'];

        $item->link_user_site = $formData[9]['value'];
        $item->link_fb = $formData[10]['value'];
        $item->link_tw = $formData[11]['value'];
        $item->link_inst = $formData[12]['value'];
        $item->link_behance = $formData[13]['value'];
        $item->link_google = $formData[14]['value'];
        $item->link_pint = $formData[15]['value'];
        $item->link_tumblr = $formData[16]['value'];
        $item->link_linkedin = $formData[17]['value'];
        $item->link_blg = $formData[18]['value'];

        $item->avatar = !empty($profile_img) ? $profile_img : "noava.png";
        $item->specialization = !empty($specialization) ? $specialization : $item->specialization;
        $item->background = !empty($background) ? $background : "profile-bg.jpg";
        $item->logo = !empty($logo) ? $logo : "logo-comp.png";

        $item->update();
        echo "Changes saved correctly!";

    }

    public function actionSaveSecurity()
    {
        $formData = isset($_POST['formData']) ? $_POST['formData'] : false;

        $item = User::findOne(['id' => $this->user_id]);
        $item->email = !empty($formData[0]['value']) ? $formData[0]['value'] : $item->email;
        $item->username = !empty($formData[1]['value']) ? $formData[1]['value'] : $item->username;
        if ($formData[3]['value'] == $formData[2]['value'] && $formData[3]['value'] != "") {
            $item->password_hash = Yii::$app->security->generatePasswordHash($formData[3]['value']);
        }

        $item->update();
        echo "Changes saved correctly!";
    }

    // Honours
    public function actionSaveHonours()
    {
        $honours_data = isset($_POST['honours']) ? $_POST['honours'] : false;
        if ($honours_data) {
            foreach ($honours_data as $value) {

                if ($value[0] == "") {
                    $honour = new UserHonours();
                    $honour->isNewRecord = true;
                    $honour->user_id = $this->user_id;
                    $honour->name = $value[1];
                    $honour->image = $value[2];
                    $honour->save();

                } else {

                    $honour = UserHonours::findOne(['id' => $value[0]]);
                    if ($honour) {
                        $honour->name = $value[1];
                        $honour->image = $value[2];
                        $honour->save();
                    }

                }

            }

            echo "Changes saved correctly!";

        }
    }

    public function actionDeleteHonour()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : false;
        if ($id != "") {
            UserHonours::findOne($id)->delete();

            echo "Data successfully removed!";
        }
    }

    // Licenses
    public function actionSaveLicenses()
    {
        $licenses_data = isset($_POST['licenses']) ? $_POST['licenses'] : false;
        if ($licenses_data) {
            foreach ($licenses_data as $value) {

                if ($value[0] == "") {
                    $license = new UserLicenses();
                    $license->isNewRecord = true;
                    $license->user_id = $this->user_id;
                    $license->number = $value[1];
                    $license->issued_by = $value[2];
                    $license->images = $value[3];
                    $license->save();

                } else {

                    $license = UserLicenses::findOne(['id' => $value[0]]);
                    if ($license) {
                        $license->number = $value[1];
                        $license->issued_by = $value[2];
                        $license->images = $value[3];
                        $license->save();
                    }

                }

            }

            echo "Changes saved correctly!";

        }
    }

    public function actionDeleteLicense()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : false;
        if ($id != "") {
            UserLicenses::findOne($id)->delete();
            echo "Data successfully removed!";
        }
    }

    // Videos
    public function actionSaveVideos()
    {
        $videos_data = isset($_POST['videos']) ? $_POST['videos'] : false;
        if ($videos_data) {
            foreach ($videos_data as $value) {

                if ($value[0] == "") {
                    $video = new UserVideos();
                    $video->isNewRecord = true;
                    $video->user_id = $this->user_id;
                    $video->link = $value[1];
                    $video->name = $value[2];
                    $video->save();

                } else {

                    $video = UserVideos::findOne(['id' => $value[0]]);
                    if ($video) {
                        $video->link = $value[1];
                        $video->name = $value[2];
                        $video->save();
                    }

                }

            }

            echo "Changes saved correctly!";

        }
    }

    public function actionDeleteVideo()
    {
        $id = isset($_POST['id']) ? $_POST['id'] : false;
        if ($id != "") {
            UserVideos::findOne($id)->delete();
            echo "Data successfully removed!";
        }
    }

    public function actionSaveMainInformationAgent()
    {

        $profile_img = isset($_POST['profile_img']) ? $_POST['profile_img'] : "";
        $profile_img = explode('/', $profile_img);
        $profile_img = array_pop($profile_img);
        $background = isset($_POST['background']) ? $_POST['background'] : "";
        $logo = isset($_POST['logo']) ? $_POST['logo'] : "";
        $logo = explode('/', $logo);
        $logo = array_pop($logo);
        $item = User::findOne(['id' => $this->user_id]);
        $item->first_name = !empty($_POST['first_name']) ? $_POST['first_name'] : $item->first_name;
        $item->last_name = !empty($_POST['last_name']) ? $_POST['last_name'] : $item->last_name;
        $item->phone = !empty($_POST['phone']) ? $_POST['phone'] : $item->phone;
        $item->city = !empty($_POST['city']) ? $_POST['city'] : $item->city;
        $item->message = !empty($_POST['message']) ? $_POST['message'] : $item->message;
        $item->link_user_site = $_POST['link_user_site'];
        $item->link_fb = $_POST['link_fb'];
        $item->link_tw = $_POST['link_tw'];
        $item->link_inst = $_POST['link_inst'];
        $item->link_behance = $_POST['link_behance'];
        $item->link_google = $_POST['link_google'];
        $item->link_pint = $_POST['link_pint'];
        $item->link_tumblr = $_POST['link_tumblr'];
        $item->link_linkedin = $_POST['link_linkedin'];
        $item->link_blg = $_POST['link_blg'];
        $item->avatar = !empty($profile_img) ? $profile_img : "noava.png";
        $item->background = !empty($background) ? $background : "profile-bg.jpg";
        $item->email = !empty($_POST['email']) ? $_POST['email'] : $item->email;
        $item->username = !empty($_POST['username']) ? $_POST['username'] : $item->username;
        if ($_POST['password'] == $_POST['confirm'] && $_POST['password'] != "") {
            $item->password_hash = Yii::$app->security->generatePasswordHash($_POST['password']);
            $item->update();
            echo $item->update();
//            echo "Changes saved correctly!";
            die();
        } elseif ($_POST['password'] == "") {
            $item->update();
            echo $item->update();
//            echo "Changes saved correctly!";
            die();
        } else {
            echo "Passwords do not match!";
            die();
        }
    }

    private function getPortfolioModal($lang, $owner_id)
    {

        $portfolio = array();
        $temp = Portfolio::find()->where(['owner_id' => $owner_id, 'moderation' => $this->moderation])->all();
        foreach ($temp as $value) {

            $images = explode("|", $value['images']);
            $description = ItemDescription::find()->where(['item_id' => $value['item_id'], 'language' => $lang])->one();

            $portfolio[] = [
                'id' => $value['item_id'],
                'title' => $description['title'],
                'image' => '/media/portfolio/' . $images[0]

            ];

        }


        return $portfolio;

    }

    private function getCollections($lang, $user_id)
    {
        $data = array();
        $collections = PortfolioCollections::find()->where(['user_id' => $user_id])->all();
        foreach ($collections as $value) {
            $items = array();
            $items_id = explode("|", $value['items']);
            $i = 0;
            if ($items_id) {
                foreach ($items_id as $item) {
                    if ($item != "" && $item != " ") {
                        $portfolio = Portfolio::find()->where(['item_id' => $item])->one();
                        $temp_img = explode("|", $portfolio['images']);
                        $items[$i]['image'] = '/media/portfolio/' . $temp_img[0];
                        $description = ItemDescription::find()->where(['item_id' => $item, 'language' => $lang])->one();
                        $items[$i]['title'] = $description['title'];
                        $items[$i]['id'] = $item;
                        $i++;
                    }
                }
            }

            for ($i; $i < 3; $i++) {
                $items[$i]['image'] = '/images/no-img.png';
                $items[$i]['title'] = '';
                $items[$i]['id'] = "";
            }

            $data[] = [
                'id' => $value['id'],
                'name' => $value['name'],
                'main' => $value['main'],
                'about' => $value['about'],
                'items' => $items
            ];


        }


        return $data;
    }

    private function getUserSpecialization($user_information)
    {

        $lang = Language::getCurrent()->url;
        $specialization = Specialization::find()->where(['option' => 0])->all();
        $data = [];
        $temp = "";
        foreach ($specialization as $value) {
            if ($lang == 'ru') {
                $name = $value['name_ru'];
            } else $name = $value['name_en'];

            if ($user_information['specialization'] == $value['name_ru'] || $user_information['specialization'] == $value['name_en']) {
                $temp = $name;
            } else {
                $data[] = $name;

            }
        }

        array_unshift($data, $temp);

        return $data;
    }


}
