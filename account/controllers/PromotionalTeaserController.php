<?php

namespace frontend\account\controllers;

use frontend\account\models\User;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\models\Language;
use frontend\account\models\UserStatistics;
use frontend\account\models\ProductsTags;
use frontend\account\models\ProductsTagsUnite;
use frontend\account\models\ProductsList;
use frontend\account\models\Portfolio;
use frontend\account\models\AgentProjectDescription;

class PromotionalTeaserController extends Controller
{
    private $user_id;
    private $language = "en";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }

}