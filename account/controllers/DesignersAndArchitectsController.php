<?php

namespace frontend\account\controllers;

use yii;
use frontend\models\ProfileReviews;
use frontend\account\models\User;
use frontend\models\UserHonours;
use frontend\models\UserVideos;
use frontend\models\UserLicenses;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\models\PortfolioCollections;
use frontend\models\Language;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\account\models\AgentProjectDescription;

class DesignersAndArchitectsController extends Controller
{
    private $user_id;
    private $language = "ru";


    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex(){
        return $this->render('index',[
            'users_data' => $this->getMyDesignersAndArchitects()
        ]);
    }

    private function getMyDesignersAndArchitects(){

        $pagination = new Pagination([
            'defaultPageSize' => 6,
            'totalCount' => User::find()->where(['roles' => 'designer'])->count()
        ]);

        $users = User::myDesignersAndArchitects($pagination->offset, $pagination->limit);

        return ['users' => $users, 'pagination' => $pagination];
    }


}