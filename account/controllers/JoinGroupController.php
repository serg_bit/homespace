<?php

namespace frontend\account\controllers;

use yii;
use yii\web\Controller;
use frontend\account\models\Groups;
use frontend\account\models\Likes;
use frontend\account\models\CountGroupsForm;
use frontend\models\Language;
use yii\data\Pagination;
use yii\helpers\Json; 


class JoinGroupController extends \yii\web\Controller
{   
    public function init(){
       session_start();
       if (!isset($_SESSION['group'])) {
            $_SESSION['group'] =  ['sort'=>'views',
                                    'tags'=> "",
                                    'count' => 6,
                                ];
        }
    }
    
    public function actionIndex()
    {   
       
        if(isset($_SESSION['group'])) {
            $class = isset($_POST['parent_class']) ? $_POST['parent_class'] : "";
            $value = isset($_POST['value']) ? $_POST['value'] : "";

            if($class == 'count'){
                $_SESSION['group']['count'] =  $value;
            }

            else if($class == 'sort'){

                $_SESSION['group']['sort'] = $value;
            }else if ($class == 'group_tags'){

                $_SESSION['group']['tags'] = $value;
            }
            
 
        }
      
        // if ( $_SERVER['REQUEST_METHOD'] == 'POST') print_r( $_POST );
        $alias = Yii::$app->getUrlManager()->getBaseUrl();
        $lang = Language::getCurrent()->url;
        $select = new Groups;
        $get_url = Yii::$app->urlManager->createUrl(['account/join-group']);
        $sort  = $_SESSION['group']['sort'];
        $countGroups =  $_SESSION['group']['count'];
        $pagination = new Pagination([
                'defaultPageSize' => $countGroups,
                'totalCount' => Groups::find()->count(),
            ]);
   
            //if (Yii::$app->request->isAjax) $this->selectGroups = Yii::$app->request->post('count');
            
         if(empty($_SESSION['group']['tags'])){

             $groups = Groups::find()
                 ->where(['paid_status' => 0])
                 ->orderBy($sort.' DESC')
                 ->offset($pagination->offset)
                 ->limit($pagination->limit)
                 ->all();



         }else{

             $groups = Groups::find()
                 ->where(['and',['paid_status' => 0],['like', 'group_tags', $_SESSION['group']['tags']]])
                 ->orderBy($sort.' DESC')
                 ->offset($pagination->offset)
                 ->limit($pagination->limit)
                 ->all();

         }

            $paid = Groups::find()
                            ->where(['paid_status' => 1])
                            ->all();
                        
            return $this->render('index', [
                'alias' => $alias,
                'pagination' => $pagination,
                'groups' => $groups,
                'paid' => $paid,
                'get_url' => $get_url,
                'tags' => $_SESSION['group']['tags'],
            ]);
        
    }
    
    public function actionGroup()
    {
        $alias = Yii::$app->getUrlManager()->getBaseUrl();
        $get_url = Yii::$app->urlManager->createUrl(['account/join-group']);
        $group =  Groups::find()
                  ->where(['id'=>Yii::$app->request->getQueryString()])
                  ->one();
        
        $count_likes = count(Likes::find()->where(['object_id' => $group['id']])->all());
        $view = Groups::findOne($group['id']);
        $view->views = $group['views']+1;
        $view->save();
        
        return $this->render('group', [
            //'items' => $this->menuItems(),
            'alias' => $alias,
            'group' => $group,
            'likes' => $count_likes,
            'get_url' => $get_url,
        ]);
    }
    
    public function actionLikes()
  {   
        if (Likes::find()->where(['object_id' => $_POST['id'] , 'category' => 1 ,'user_id' => \Yii::$app->user->identity->id ])->one()) {
            
            Likes::deleteAll(['object_id' => $_POST['id'] , 'category' => 1 , 'user_id' => \Yii::$app->user->identity->id]);
            $like_group = Groups::findOne(['id' => $_POST['id']]);
            $like_group->likes = $like_group->likes - 1;
            $like_group->update();
            
        }else{
             $like = new Likes;
             $like->object_id = $_POST['id'];
             $like->user_id = \Yii::$app->user->identity->id;
             $like->category = 1;
             $like->save();
             $like_group = Groups::findOne(['id' => $_POST['id']]);
             $like_group->likes = $like_group->likes + 1;
             $like_group->update();
    }
      $count = Groups::findOne(['id' => $_POST['id']]);
       echo  $count->likes;
  
   //     $count = 0;
   //     foreach ($temp_groups as $value) {
   //         $my_like = Likes::find()->where(['user_id' => $user_id, 'object_id' => $value['id'], 'category' => 1])->one();
   //         if($my_like == null){
   //              $my_like = '';
   //         }else $my_like = 1;

   //         $data[$count] = [
   //             'id' => $value['id'],
   //             'image' => $alias.'/images/'.$value['images'],
   //             'title' => $value['title'],
   //             'views' => $value['views'],
   //             'followers' => $value['followers_count'],
   //             'likes' => Likes::find()->where(['object_id' => $value['id'], 'category' => 1])->count(),
   //             'user_like' => $my_like,
   //             'url' => '#',
   //         ];

   //         $count++;
   //     }

   //     return $data;
   }
    
    
    public static function textCutter($str, $length){   
               
        return substr($str, 0, $length)."...";
      }

    /*public function actionLike(){
        $groups = new Groups();
        $groups->likeGroup(Yii::$app->request->getQueryString());
        $get_url = Yii::$app->urlManager->createUrl(['account/join-group']);
        
        return $this->goBack($get_url);
    }*/
    
    public function actionSort($orderby){
       $pagination = new Pagination([
                'defaultPageSize' => $this->selectGroups,
                'totalCount' => Groups::find()->count(),
            ]);
       //if (Yii::$app->request->isGet){
       if ($orderby == 0)
            $groups = Groups::find()
            ->orderBy(['views' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->where(['paid_status' => 0])
            ->all();
       else
           $groups = Groups::find()
            ->orderBy('views')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->where(['paid_status' => 0])
            ->all();
       //}
        $groups = Json::encode($groups);
        echo $groups;
    }


    public function actionSubscribe(){
        
    }

    
 
    
}
