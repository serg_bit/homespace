<?php

namespace frontend\account\controllers;

use yii;
use frontend\models\ProfileReviews;
use frontend\account\models\User;
use frontend\models\UserHonours;
use frontend\models\UserVideos;
use frontend\models\UserLicenses;
use frontend\account\models\Portfolio;
use frontend\account\models\ItemDescription;
use frontend\models\PortfolioCollections;
use frontend\models\Language;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\account\models\AgentProjectDescription;

class ProfileAgentController extends Controller
{
    private $user_id;
    private $moderation = 1;
    private $language = "ru";
    private $paid_status = 2;


    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
        $this->paid_status = Yii::$app->user->identity->paid_status;
    }

    public function actionIndex(){
        $user_information = User::find()->where(['id' => $this->user_id])->one();
        $videos =  UserVideos::find()->where(['user_id' => $this->user_id])->all();

        list($controller) = Yii::$app->createController('account/profile');

        return $this->render('index',[
            'user_information' => $user_information,
            'videos' => $videos,
            'reviews' => $controller->getReviews($this->user_id),
            'approved' => $this->getApprovedConfirmationProjects($this->user_id, 6, 1),
        ]);
    }

    public function actionEdit(){
        return $this->render('edit',[
            'user_information' => User::find()->where(['id' => $this->user_id])->one(),
            'videos' => UserVideos::find()->where(['user_id' => $this->user_id])->all()
        ]);
    }

    //  Ajax Actions

    public function actionSaveInfo(){

        $main = Yii::$app->request->post('main', false);
        if($main){
            $social = Yii::$app->request->post('social');
            $message = Yii::$app->request->post('message');
            $background = Yii::$app->request->post('bg');
            $company = Yii::$app->request->post('company');
            $avatar = Yii::$app->request->post('avatar');
            $avatar = explode("/", $avatar);
            $avatar = array_pop($avatar);
            $logo = Yii::$app->request->post('logo');
            $logo = explode("/", $logo);
            $logo = array_pop($logo);

            $user = User::findOne(['id' => $this->user_id]);

            $user->specialization = !empty($main[0]['value']) ? $main[0]['value'] : $user->specialization;
            $user->first_name = !empty($main[1]['value']) ? $main[1]['value'] : $user->first_name;
            $user->last_name = !empty($main[2]['value']) ? $main[2]['value'] : $user->last_name;
            $user->country = !empty($main[3]['value']) ? $main[3]['value'] : $user->country;
            $user->phone = !empty($main[4]['value']) ? $main[4]['value'] : $user->phone;
            $user->city = !empty($main[5]['value']) ? $main[5]['value'] : $user->city;
            $user->address = !empty($main[6]['value']) ? $main[6]['value'] : $user->address;

            $user->message = $message;
            $user->background = !empty($background) ? $background : "profile-bg.jpg";
            $user->company = $company;
            $user->avatar = $avatar;
            $user->logo = $logo;

            //  social
            $user->link_user_site = $social[0]['value'];
            $user->link_fb = $social[1]['value'];
            $user->link_tw = $social[2]['value'];
            $user->link_inst = $social[3]['value'];
            $user->link_behance = $social[4]['value'];
            $user->link_google = $social[5]['value'];
            $user->link_pint = $social[6]['value'];
            $user->link_tumblr = $social[7]['value'];
            $user->link_linkedin = $social[8]['value'];
            $user->link_blg = $social[9]['value'];

            $user->update();
            echo "Changes saved correctly!";
        }

    }

    //  End Ajax Actions

    public function getApprovedConfirmationProjects($id, $view_by, $approved)
    {
        $pagination = new Pagination([
            'defaultPageSize' => $view_by,
            'totalCount' => AgentProjectDescription::find()->where(['id_agent' => $id, 'approved' => $approved])->count()
        ]);

        $projects = AgentProjectDescription::onConfirmationProjects($this->language, $id, $approved, $pagination->limit, $pagination->offset);

        return ['projects' => $projects, 'pagination' => $pagination];

    }
}