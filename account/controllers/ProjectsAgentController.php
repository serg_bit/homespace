<?php

namespace frontend\account\controllers;

use frontend\account\models\User;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use frontend\models\Language;
use frontend\account\models\UserStatistics;
use frontend\account\models\ProductsTags;
use frontend\account\models\ProductsTagsUnite;
use frontend\account\models\ProductsList;
use frontend\account\models\Portfolio;
use frontend\account\models\AgentProjectDescription;

class ProjectsAgentController extends Controller
{
    private $user_id;
    private $language = "en";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $viewed_all = isset($_SESSION['agent-projects-viewed-all']) ? $_SESSION['agent-projects-viewed-all'] : 3;
        $confirmation_view = isset($_SESSION['agent-projects-confirmation-view']) ? $_SESSION['agent-projects-confirmation-view'] : 3;
        $approved_view = isset($_SESSION['agent-projects-approved-view']) ? $_SESSION['agent-projects-approved-view'] : 3;
        $products = ProductsList::find()->where(['user_id' => $this->user_id])->all();
        $user_stat = new UserStatistics();
        $user_stat_res = $user_stat->getStatistics($this->user_id);
        $user_stat_to_string = implode(' ,', $user_stat_res['per_month']);
        $q = Yii::$app->request->get('q', false);
        return $this->render('index', [
            'products' => $products,
            'user_stat' => $user_stat_res,
            'user_stat_to_string' => $user_stat_to_string,
            'all_projects' => $this->getAllProjects($viewed_all, $q),
            'view_all' => $viewed_all,
            'confirmation_projects' => $this->getApprovedConfirmationProjects($confirmation_view, 0),
            'confirmation_view' => $confirmation_view,
            'approved_view' => $approved_view,
            'approved_projects' => $this->getApprovedConfirmationProjects($approved_view, 1),
        ]);
    }


    public function actionSelect()
    {
        $id = Yii::$app->request->get('id', 0);
        $products = ProductsList::find()->where(['user_id' => $this->user_id ])->all();
        $user_stat = new UserStatistics();
        $user_stat_res = $user_stat->getStatistics($this->user_id);
        $user_stat_to_string = implode(' ,', $user_stat_res['per_month']);

        list($controller) = Yii::$app->createController('about');
        $item = $controller->getDesignerItem($id, $this->language);
        if ($item) {
            $descriptions = AgentProjectDescription::find()->where(['id_project' => $item['id']])->all();
        }else
            $descriptions = false;

        return $this->render('select', [
            'products' => $products,
            'user_stat' => $user_stat_res,
            'user_stat_to_string' => $user_stat_to_string,
            'item' => $item,
            'descriptions' => $descriptions,
            'new_projects' => $this->getAllProjects(3)
        ]);
    }

    // End Views

    //------------------------------------------------
    // Add Agent own Description to Project
    //------------------------------------------------

    public function actionAddDescription(){
        $id = Yii::$app->request->post('id', 0);
        $agent = "<p><a href='" . Yii::$app->urlManager->createUrl(['manufacturers/profile', 'id' => $this->user_id]) . "'>" . Yii::$app->user->identity->first_name . " " . Yii::$app->user->identity->last_name . "</a><p>";
        $description = Yii::$app->request->post('description', '');
        $description = $agent . $description;
        $description = htmlentities($description);
        AgentProjectDescription::agentAddDescription($id, Yii::$app->user->identity->id, $description);

    }

    //------------------------------------------------
    // Get and save view count
    //------------------------------------------------

    public function actionViewed()
    {
        if (isset($_POST['all_view'])) {
            $viewed_all = Yii::$app->request->post('all_view', 3);
            $_SESSION['agent-projects-viewed-all'] = $viewed_all;
        } elseif (isset($_POST['confirmation_view'])) {
            $confirmation_view = Yii::$app->request->post('confirmation_view', 3);
            $_SESSION['agent-projects-confirmation-view'] = $confirmation_view;
        } elseif (isset($_POST['approved_view'])) {
            $approved_view = Yii::$app->request->post('approved_view', 3);
            $_SESSION['agent-projects-approved-view'] = $approved_view;
        }

    }

    public function actionUpload()
    {
        if (isset($_FILES['file'])) {
            $file = $_FILES['file'];
            $temp = explode("/", $file['type']);
            if ($temp[1] == 'png' || $temp[1] == 'jpeg' || $temp[1] == 'jpg' || $temp[1] == 'bmp') {
                $path = Yii::getAlias('@media') . '/upload/';
                $ext = '.' . $temp[1];
                $name = time();
                move_uploaded_file($file['tmp_name'], $path . $name . $ext);
                echo '/media/upload/' . $name . $ext;
            }
        }

    }
//------------------------------------------------
// search tags for autocomplete(add product)
//------------------------------------------------
    public function actionSearchTags()
    {
        $tags = ProductsTags::find()->select(['id', 'title'])->where(['like', 'title', $_REQUEST['search_tags']])->asArray()->all();
        $i = -1;
        $data = [];
        foreach ($tags as $item) {
            $i++;
            $data[$i]['id'] = $item['id'];
            $data[$i]['tag'] = $item['title'];
        }
        $data = json_encode($data);
        return $data;
    }

//------------------------------------------------
// search company name for autocomplete(add product)
//------------------------------------------------
    public function actionSearchCompanyName()
    {
        $request = Yii::$app->request;
        $companies = User::searchCompanyName($request->post('search_company'));
        return json_encode($companies);
    }
//------------------------------------------------
// create new product(modal)
//------------------------------------------------
    public function actionAddNewProduct()
    {
        $request = Yii::$app->request;
        if (ProductsList::findOne(['product_name' => $request->post('product_name'), 'user_id' => \Yii::$app->user->identity->id, 'company_name' => $request->post('company_name')])) {
            return Yii::t('account', 'product_name_exists');
        } elseif ($request->post('product_name') == null || $request->post('price') == null
            || $request->post('description') == null || $request->post('image1') == null || $request->post('image2') == null
            || $request->post('image3') == null || $request->post('tags_id') == null
        ) {
            return Yii::t('account', 'fill_in_all_the_fields');
        } else {
            $product = new ProductsList();
            $image1 = explode('/', $request->post('image1'));
            $image2 = explode('/', $request->post('image2'));
            $image3 = explode('/', $request->post('image3'));
            $product_id = $product->saveNewProduct($request->post('company_id'), $request->post('product_name'), $request->post('product_collection'),
                $request->post('price'), $request->post('description'), $image1[count($image1) - 1], $image2[count($image2) - 1], $image3[count($image3) - 1]);
            $tags = [];
            foreach ($request->post('tags_id') as $tag_id) {
                array_push($tags, [(int)$product_id, (int)$tag_id]);
            }
            if (Yii::$app->db->createCommand()->batchInsert('products_tags_unite', ['id_product', 'id_tag'], $tags)->execute()) {
                return 'done!';
            } else {
                return Yii::t('account', 'error_please_try_again_later');
            }
        }

    }

//------------------------------------------------
// Get Projects List and pagination
//------------------------------------------------

    public function getAllProjects($view_by, $q = '')
    {
        if ($q != '') {
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => Portfolio::agentAllProjectsCount($this->language, $q)
            ]);

            $projects = Portfolio::agentAllProjects($this->language, $pagination->limit, $pagination->offset, $q);
        } else {
            $pagination = new Pagination([
                'defaultPageSize' => $view_by,
                'totalCount' => Portfolio::find()->where(['active' => 1, 'moderation' => 1, 'catalog_id' => 0])->count()
            ]);

            $projects = Portfolio::agentAllProjects($this->language, $pagination->limit, $pagination->offset);
        }

        return ['projects' => $projects, 'pagination' => $pagination];

    }

    public function getApprovedConfirmationProjects($view_by, $approved)
    {
        $pagination = new Pagination([
            'defaultPageSize' => $view_by,
            'totalCount' => AgentProjectDescription::find()->where(['id_agent' => $this->user_id, 'approved' => $approved])->count()
        ]);

        $projects = AgentProjectDescription::onConfirmationProjects($this->language, $this->user_id, $approved, $pagination->limit, $pagination->offset);

        return ['projects' => $projects, 'pagination' => $pagination];

    }
}