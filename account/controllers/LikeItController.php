<?php
/**
 * Created by PhpStorm.
 * User: Alscon13
 * Date: 12.05.2016
 * Time: 11:28
 */
namespace frontend\account\controllers;

use yii;
use yii\web\Controller;
use frontend\account\models\LikeIt;
use frontend\models\Language;
use frontend\account\models\LikeItItems;

class LikeItController extends Controller
{

    private $user_id;
    private $lang;


    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->lang = Language::getCurrent()->url;
    }


    public function actionIndex()
    {

        $folder = LikeIt::find()->where(['>', 'root', 0])->all();

//        print_r($folder);

        return $this->render('index', [
            'folder' => $folder,
            'lang' => $this->lang
        ]);

    }

    public function actionRoot()
    {
        $root_id = Yii::$app->request->get('id', false);
        $folders = LikeIt::find()->where(['relative' => $root_id, 'user_id' => $this->user_id])->all();
        $items = LikeIt::rootDesignerItems($root_id, $this->user_id, $this->lang);

        return $this->render('root', [
            'folders' => $folders,
            'items' => $items,
            'lang' => $this->lang,
            'root_id' => $root_id
        ]);

    }

    public function actionFolderCreate()
    {

        $name = Yii::$app->request->post('name', false);
        $root = Yii::$app->request->post('root', false);
        if ($name && $root) {

            LikeIt::createFolder($name, $root, $this->user_id);
        }

    }

    public function actionFolderDelete()
    {

        $id_folder = Yii::$app->request->post('id_folder', false);

        if ($id_folder) {
            LikeIt::deleteFolder($id_folder);

        }


    }

    public function actionFolderRename()
    {

        $id_folder = Yii::$app->request->post('id_folder', false);
        $name = Yii::$app->request->post('name', false);

        if ($id_folder && $name) {
            LikeIt::renameFolder($id_folder, $name);
        }


    }

    public function actionAddProductsToCatalog()
    {
        $request = Yii::$app->request;
        $id_product = $request->post('id_product');
        $id_folder = $request->post('id_folder');
        $moved_item = LikeItItems::AddItemToFolder($id_product, $id_folder);

        echo $moved_item;
    }


}