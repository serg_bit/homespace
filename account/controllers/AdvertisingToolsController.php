<?php

namespace frontend\account\controllers;

use frontend\models\UserVideos;
use yii;
use yii\web\Controller;
use yii\data\Pagination;
use yii\helpers\Url;
use frontend\models\Language;
use frontend\account\models\ProductsCollections;
use frontend\account\models\User;
use frontend\account\models\UserCompanies;
use frontend\account\models\UserStatistics;
use frontend\models\CompaniesCollections;

class AdvertisingToolsController extends Controller
{
    private $user_id;
    private $language = "ru";

    public function init()
    {
        $this->user_id = Yii::$app->user->identity->id;
        $this->language = Language::getCurrent()->url;
    }

    public function actionIndex()
    {
        $user_stat = new UserStatistics();
        $companies_id = UserCompanies::getCompaniesByAgent($this->user_id);
        $user_stat_res = $user_stat->getStatistics($this->user_id);
        $pagination = new Pagination([
                    'defaultPageSize' => '4',
                    'totalCount' => count($companies_id),
                ]);
        $user_stat_to_string = implode(' ,', $user_stat_res['per_month']);

        $companies = User::getCompaniesById($companies_id, $pagination);
        $companies_collections_id = CompaniesCollections::getCollectionsIdByCompanyId($companies_id);
        $i = -1;
        foreach($companies as $item){
            $i++;
            if(isset($companies_collections_id[$item['id']])){
                $companies[$i]['collections'] = ProductsCollections::getCollectionsById($companies_collections_id[$item['id']], $this->language);
            }
        }
        return $this->render('index', [
            'user_stat_res' => $user_stat_res,
            'user_stat_to_string' => $user_stat_to_string,
            'companies_id' => $companies_id,
            'companies' => $companies,
            'companies_collections_id' => $companies_collections_id,
            'language' => $this->language,
            'pagination' => $pagination,
        ]);
    }

    public function actionCreate()
    {
        return $this->render('create');
    }

    public function actionManufacturer()
    {
        return $this->render('manufacturer');
    }

    public function actionEditManufacturer()
    {
        return $this->render('edit');
    }


    //------------------------------------------------
    // search categories for autocomplete(add company)
    //------------------------------------------------
    public function actionSearchCategories()
    {
        $request = Yii::$app->request;
        $collections = Collections::find()->select(['id', $this->language])->where(['like', $this->language, $request->post('search_categories')])->asArray()->all();
        $i = -1;
        foreach ($collections as $item) {
            $i++;
            $data[$i]['id'] = $item['id'];
            $data[$i]['category'] = $item[$this->language];
        }
        $data = json_encode($data);
        return $data;
    }
    //------------------
    // create new company
    //------------------
    public function actionCreateNewCompany()
    {
        $data = [];
        $request = Yii::$app->request;
        $company_avatar = $request->post('company_avatar');
        $company_avatar = explode('/', $company_avatar);
        $data['avatar'] = array_pop($company_avatar);
//        $data['background'] = $request->post('background');
        $company_logo = $request->post('company_logo');
        $company_logo = explode('/', $company_logo);
        $data['logo'] = array_pop($company_logo);
        $data['first_name'] = $request->post('company_first_name');
        $data['last_name'] = $request->post('company_last_name');
        $data['phone'] = $request->post('company_phone', '');
        $data['city'] = $request->post('company_city', '');
        $data['message'] = $request->post('company_message', '');
        $data['link_user_site'] = $request->post('company_site', '');
        $data['link_fb'] = $request->post('link_fb', '');
        $data['link_tw'] = $request->post('link_twitter', '');
        $data['link_inst'] = $request->post('link_instagram', '');
        $data['link_behance'] = $request->post('link_behance', '');
        $data['link_google'] = $request->post('link_google', '');
        $data['link_pint'] = $request->post('link_pinterest', '');
        $data['link_tumblr'] = $request->post('link_tumblr', '');
        $data['link_linkedin'] = $request->post('link_linkedin', '');
        $data['link_blg'] = $request->post('link_blogger', '');
        $data['avatar'] = !empty($data['avatar']) ? $data['avatar'] : "noava.png";
//        $data['background'] = !empty($data['background']) ? $data['background'] : "profile-bg.jpg";
        $data['email'] = $request->post('company_email');
        $data['username'] = $request->post('company_login');
        $data['company'] = $request->post('company_name');
        $data['brand'] = $request->post('brand_name');
//        $data['position'] = $request->post('position');
        $data['password_hash'] = Yii::$app->security->generatePasswordHash($request->post('company_password'));
        $company = new User;
        $user_company = new UserCompanies();
        $company_video = new UserVideos();
        $company_video->addVideo($this->user_id, $request->post('company_video_link'), $request->post('company_video_name'));
        $id_company = $company->saveNewCompany($data);
        $this->CompanyCollectionsUnite($id_company, $request->post('categories_id'));
        if ($user_company->uniteIds($id_company)) {
            echo "done";
            die();
        } else {
            echo "failed";
            die();
        }
    }

    protected function CompanyCollectionsUnite($id_company, $collections)
    {
        $tags = [];
        if (is_array($collections)) {
            foreach ($collections as $tag_id) {
                array_push($tags, [(int)$id_company, (int)$tag_id]);
            }
            Yii::$app->db->createCommand()->batchInsert('companies_collections', ['id_company', 'id_collection'], $tags)->execute();
            return true;
            die();
        } else {
            return false;
            die();
        }
    }

}
