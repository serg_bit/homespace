<?php
use yii\widgets\LinkPager;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="col-lg-7 central-content profile-content profile-agent  company create centralScroll ">
    <div class="row">
        <div class="content">
            <div class="agent-profil" style="background-image: url(images/bg-agents.png);">

                <div class="agents">
                    <p class="upload-portfolio download-portfolio edit">Edit
                        <a href="#"></a>
                    </p>
                    <div class="company-description">
                        <input accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileChange(this);" type="file">
                        <div class="dropzone" id="dropzone1">Drop files here to upload</div>
                        <form class="forma-input">

                            <div class="input-edit">
                                <div class="input-container">
                                    <input class="text-input floating-label" name="company-name" type="text">
                                    <label for="company-name">Company name</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <div class="input-container">
                                    <input class="text-input floating-label" name="brand-name" type="text">
                                    <label for="brand-name">brand agency</label>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="profile-edite">
                        <div class="profil-photo">
                            <input accept="image/*;capture=camera" style="display : none;" id="open_browse1" onchange="fileChange(this);" type="file">
                            <a href="#" id="profile-photo"><img src="" alt=""></a>
                        </div>
                    </div>
                    <div class="create-profile">
                        <form class="forma-input">

                            <div class="input-edit position">
                                <div class="input-container">
                                    <input class="text-input floating-label" name="position" type="text">
                                    <label for="position">Position</label>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="input-edit">
                                <i class="icon-user168"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="first-name" type="text">
                                    <label for="first-name">First</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-user168"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="last-name" type="text">
                                    <label for="last-name">Last</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-home"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="city" type="text">
                                    <label for="city">City</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-telephone46"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="phone" type="text">
                                    <label for="phone">Phone</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-location"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="address" type="text">
                                    <label for="address">Address</label>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>


                <div class="profil-edited">
                    <form class="forma-input" id="form_main_information">
                        <div class="input-edit-top sub-wrap">

                            <div class="input-edit">
                                <i class="icon-webpage2"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="site" value="mysite.com" type="text">
                                    <label for="site">Your Site</label>
                                </div>
                            </div>

                            <div class="input-edit">
                                <i class="icon-facebook55"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="facebook" value="fb.com" type="text">
                                    <label for="facebook">Link for Facebook.com</label>
                                </div>
                            </div>

                            <div class="input-edit">
                                <i class="icon-twitter1"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="twitter" value="twitter.com" type="text">
                                    <label for="twitter">Link for Twitter.com</label>
                                </div>
                            </div>

                            <div class="input-edit">
                                <i class="icon-instagram12"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="instagram" value="ins.com" type="text">
                                    <label for="instagram">Link for Instagram.com</label>
                                </div>
                            </div>

                            <div class="input-edit">
                                <i class="icon-behance2"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="behance" value="be.com" type="text">
                                    <label for="behance">Link for Behance.com</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-google116"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="google" value="google.com" type="text">
                                    <label for="google">Link for Google+</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-pinterest3"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="pinterest" value="pinterest.com" type="text">
                                    <label for="pinterest">Link for Pinterest.com</label>
                                </div>
                            </div>

                            <div class="input-edit">
                                <i class="icon-logotype1"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="tumblr" value="tubblr.com" type="text">
                                    <label for="tumblr">Link for Tumblr.com</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-linkedin11"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="linkedin" value="in.com" type="text">
                                    <label for="linkedin">Link for Linkedin.com</label>
                                </div>
                            </div>


                            <div class="input-edit">
                                <i class="icon-blogger8"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label dirty" name="blogger" value="bl.com" type="text">
                                    <label for="blogger">Link for Blogger.com</label>
                                </div>
                            </div>


                        </div>

                    </form>
                </div>
                <div class="mesages comp">

                    <div class="consultant">
                        <div class="profile-edite">
                            <div class="profil-photo">
                                <input accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="fileChange(this);" type="file">
                                <a href="#" id="consultant-photo"><img src="" alt=""></a>
                            </div>
                        </div>
                        <form class="forma-input ">

                            <div class="input-edit">
                                <i class="icon-user168"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="consultant-first-name" type="text">
                                    <label for="consultant-first-name">First</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-user168"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="consultant-last-name" type="text">
                                    <label for="consultant-last-name">Last</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <i class="icon-telephone46"></i>
                                <div class="input-container">
                                    <input class="text-input floating-label" name="consultant-phone" type="text">
                                    <label for="consultant-phone">Phone</label>
                                </div>
                            </div>
                            <div class="input-edit">
                                <div class="input-container">
                                    <input class="text-input floating-label" name="consultant-position" type="text">
                                    <label for="consultant-position">Position</label>
                                </div>
                            </div>
                            <div class="radio-button">
                                <p>Massages</p>
                                <input id="radio-1" name="featured" checked="" type="radio"><label for="radio-1">Anyone</label>
                            </div>
                            <div class="radio-button">
                                <input id="radio-2" name="featured" checked="" type="radio"><label for="radio-2">Everybody</label>
                            </div>
                            <button class="button" id="add-consultant">OK</button>
                        </form>
                        <p class="pass error-message" style="display:none;"><span class="text-danger"></span></p>
                        <hr class="sline">
                    </div>
                    <ul class="consultants-list">
<!--                        <li>-->
<!--                            <a href="#"><img src="images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            <a href="#"><img src="images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>-->
<!--                        </li>-->
                    </ul>
                </div>
                <div class="agent-text ">
                    <form class="forma-input">
                        <div class="input-container">
                            <input class="text-input floating-label " name="sample" type="text">
                            <label for="sample">Categories</label>
                        </div>
                        <div class="textarea"><textarea name="#" id="#" cols="30" rows="10" placeholder="Signature..."></textarea></div>
                    </form>
                </div>
                <div class="profile-video">
                    <div class="clearfix"></div>
                    <div class="video">
                        <iframe src="https://www.youtube.com/embed/NRplteE2EZk" allowfullscreen="" frameborder="0" height="181" width="330"></iframe>
                        <form class="forma-input">
                            <div class="input-container namb">
                                <input class="text-input floating-label dirty" name="sample" type="text">
                                <label for="sample">Video’s link</label>
                            </div>
                            <div class="input-container namb">
                                <input class="text-input floating-label dirty" name="sample" type="text">
                                <label for="sample">Video’s name</label>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <h4 class="promote promote-bottom">Security</h4>
            <form class="honours forma-input security security-info">
                <div>
                    <div class="input-edit">
                        <i class="icon-symbol20"></i>
                        <div class="input-container">
                            <input class="text-input floating-label dirty" name="email" value="halimov_renato@mail.ru" type="text">
                            <label for="email">E-mail</label>
                        </div>

                    </div>
                    <div class="input-edit">
                        <i class="icon-id16"></i>
                        <div class="input-container">
                            <input class="text-input floating-label dirty" name="username" value="user_agent" type="text">
                            <label for="username">Username</label>
                        </div>

                    </div>
                    <div class="input-edit">
                        <i class="icon-locked59"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" id="new_pass" name="password" value="" type="text">
                            <label for="password">New Password*</label>
                        </div>

                    </div>
                    <div class="input-edit">
                        <i class="icon-locked59"></i>
                        <div class="input-container">
                            <input class="text-input floating-label" id="confirm_pass" name="confirm" value="" type="text">
                            <label for="confirm">Confirm password*</label>
                        </div>

                    </div>
                    <p class="pass">*Password must be at least 6 characters</p>
                    <ul>
                        <li><a href="#" class="button">OK</a></li>
                        <li><a href="#" class="button">Cancel</a></li>
                    </ul>
                </div>


            </form>
        </div>
    </div>
</div>

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_company.js', ['depends' => 'frontend\assets\AppAsset']);
