<?php

/* @var $this \yii\web\View */
/* @var $content string */


use frontend\assets\AppAsset;
use frontend\widgets\Banner;
use frontend\widgets\SideBar;
use frontend\widgets\SideBarAgent;

AppAsset::register($this);
?>

<?php $this->beginContent('@frontend/views/layouts/main.php'); ?>
<section>
    <div class="container scrollSidebar">
        <div class="row">
            <!--START LEFT SIDEBAR-->
            <div class="col-lg-3 left-sidebar">
                <span class="sidebar-caption"><?= Yii::t('account', 'manage_account') ?></span>
                <?= SideBar::widget(); ?>

                <?= Banner::widget(['position' => 'left']); ?>
            </div>
            <!--END LEFT SIDEBAR-->

            <!--START CENTER-->

            <?= $content ?>

            <!--END CENTER-->

            <!--START RIGHT SIDEBAR-->
            <div class="col-lg-2 right-sidebar">
                <span class="sidebar-caption">Adverticing</span>
                <?= Banner::widget(['position' => 'right']); ?>
            </div>
            <!--END RIGHT SIDEBAR-->

        </div>
    </div>
</section>

<?php $this->endContent(); ?>














