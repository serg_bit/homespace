<?php

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'special_offers');
$this->params['breadcrumbs'][] = $this->title;

?>

<!--START CENTER-->
<div class="col-lg-7 central-content page-catalog page-in-catalog special-offers-catalog centralScroll">
    <div class="content page-offer_content">
        <div class="row">
            <div class="col-lg-12 catalog-column">
                <div class="sidebar-caption">Special offers<br></div>
                <div class="clearfix"></div>
                <div class="sel">
                    <div class="selects">
                        <p>Sort by</p>
                        <i class="icon-sort"></i>
                        <select class="sort">
                            <option class="hiden"></option>
                            <option>Most liked</option>
                            <option>Price high to low</option>
                            <option>Price low to high</option>
                        </select>
                    </div>
                    <div class="selects">
                        <p>View by</p>
                        <i class="icon-view"></i>
                        <select class="view">
                            <option>12</option>
                            <option>8</option>
                            <option>4</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <form class="forma-input">
                    <i class="icon-tag"></i>
                    <div class="input-container">
                        <input class="text-input floating-label" type="text" name="sample" value="" />
                        <label for="sample">Tags</label>
                    </div>
                </form>
                <!-- <p class="upload-portfolio download-portfolio join">Add folder<a href="#"></a></p> -->
                <div class="clearfix"></div>
                <div class="catalog-box">
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-31.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/catalog-img-7.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/catalog-img-2.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-32.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/catalog-img-4.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/catalog-img-5.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-32.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/catalog-img-1.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-31.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/catalog-img-6.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b> <br>save 30%</p>
                            </div>
                            <div class="like-calc"><i class="icon-heart2971"></i><span>2</span></div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span><br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <div class="social-cont">
                                <a class="main-like icon-heart297"></a>
                            </div>
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum<br>
                                Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="bread-crumbs">
                        <ul class="hvr-radial-out">
                            <li><a href="#"></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
    </div>
</div>
<!--END CENTER-->

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
?>
