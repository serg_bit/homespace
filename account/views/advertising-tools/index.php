<?php
use yii\widgets\LinkPager;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
//var_dump($companies);
//die();
?>
    <!--START CENTER-->
    <div class="col-lg-7 profile-content profile-agent central-content centralScroll adt">
        <div class="row">
            <div class="content">
                <div class="analytics">
                    <h4 class="promote promote-bottom">Analytics</h4>
                    <p class="last-days">Last 30 days</p>
                    <div class="clearfix"></div>
                    <div class="graphic">
                        <p>People,
                            <br>who have visited profile</p>
                        <aside class="chart vert">
                            <canvas id="graphic" width="150" height="150" data-values="<?= $user_stat_to_string ?>">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                    </div>
                    <ul>
                        <li class="Guests"><i></i>Guests<span><?= $user_stat_res['per_month']['guest'] ?></span></li>
                        <li class="Agent"><i></i>Agent<span><?= $user_stat_res['per_month']['agent'] ?></span></li>
                        <li class="Homeowner">
                            <i></i>Homeowner<span><?= $user_stat_res['per_month']['homeowner'] ?></span></li>
                        <li class="Manufacturer">
                            <i></i>Manufacturer<span><?= $user_stat_res['per_month']['manufacturer'] ?></span></li>
                        <li class="Designers">
                            <i></i>Designers<span><?= $user_stat_res['per_month']['designer'] ?></span></li>
                        <li class="Pros"><i></i>Pros<span><?= $user_stat_res['per_month']['professional'] ?></span></li>
                    </ul>
                    <a href="#" class="button">Load more information</a>
                </div>
                <div class="more-info">
                    <div class="graphics">
                        <p>Previous
                            <br>visited page</p>
                        <aside class="chart vert">
                            <canvas id="graphic-1" width="105" height="105" data-values="30, 30, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                            <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                        </ul>
                    </div>
                    <div class="graphics">
                        <p>Next
                            <br> visited page</p>
                        <aside class="chart vert">
                            <canvas id="graphic-2" width="105" height="105" data-values="30, 30, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                            <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                        </ul>
                    </div>
                    <div class="graphics-bottom">
                        <p>Avarage visit</p>
                        <div class="graphics">
                            <aside class="chart vert">
                                <canvas id="graphic-3" width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>
                        <div class="graphics">

                            <aside class="chart vert">
                                <canvas id="graphic-4" width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"> </i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>

                    </div>
                    <h5>WE DEVELOP STATISTIC DISPLAY AND CONNECT OT SOON</h5>
                </div>
                <hr class="sline">
                <div class="clearfix"></div>
                <div class="profile-agent-comps">
                    <div class="companys">
                        <h4 class="promote promote-bottom">Available manufacturer’s pages</h4>
                        <p class="upload-portfolio download-portfolio join">Add
                            <a href="<?= Yii::$app->urlManager->createUrl(['account/advertising-tools/create']); ?>"></a>
                        </p>
                        <div class="clearfix"></div>
                        <?php if ($companies) : ?>
                            <div class="companies-by-agent">
                                <?php while ($companies) :
                                    $company = array_shift($companies);
                                    $company_attr = explode("||", $company['company']); ?>
                                    <div class="company">
                                        <a href="<?= Yii::$app->urlManager->createUrl(['account/advertising-tools/manufacturer']) . '?id=' . $company['id'] ?>">
                                            <img class="company-img"
                                                 src="<?= '/media/upload/' . $company['logo'] ?>" alt="#">
                                        </a>
                                        <div class="name-company">
                                            <h5><?= $company_attr[0] ?> <span>(<?= $company_attr[1] ?>)</span></h5>
                                            <p>made in <span><?= $company['country'] ?></span></p>
                                        </div>
                                        <ul class="categories">
                                            <li><a href="#">Categories:</a></li>
                                            <?php if ($company['collections']) : ?>
                                                <?php while ($company['collections']) :
                                                    $collection = array_pop($company['collections']); ?>
                                                    <li>
                                                        <a href="<?= $collection['id'] ?>"><?= $collection[$language] ?></a>
                                                    </li>
                                                <?php endwhile; ?>
                                            <?php endif; ?>
                                        </ul>
                                        <ul class="description">
                                            <li>Products: <span><?= $company['products_count'] ?></span></li>
                                            <li>New: <span>26</span></li>
                                            <li>Special offers: <span>16</span></li>
                                            <li>Sale: <span>20%</span></li>
                                        </ul>
                                        <div class="author">
                                            <img src="<?= '/media/upload/' . $company['avatar'] ?>" alt="#">
                                            <p class="name"><?= $company['first_name'] . ' ' . $company['last_name'] ?></p>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $pagination,
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options' => ['class' => 'hvr-radial-out1, agent-comp-pagination'],
                            ]); ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <hr class="sline">
                    <div class="companys">
                        <h4 class="promote promote-bottom">Manufacturer’s special offers</h4>
                        <p class="upload-portfolio download-portfolio join">Add
                            <a href="#"></a>
                        </p>
                        <div class="clearfix"></div>
                        <div class="company">
                            <img class="company-img" src="/images/company.png" alt="#">
                            <div class="name-company">
                                <h5>TINY <span>(Brand studio)</span></h5>
                                <p>made in <span>New Zealand</span></p>
                            </div>
                            <ul class="categories">
                                <li><a href="#">Categories:</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                            </ul>
                            <ul class="description">
                                <li>Products: <span>56</span></li>
                                <li>New: <span>26</span></li>
                                <li>Special offers: <span>16</span></li>
                                <li>Sale: <span>20%</span></li>
                            </ul>
                            <div class="author">
                                <img src="/images/img-2.png" alt="#">
                                <p class="name">Sam Clarintence</p>
                            </div>
                        </div>


                        <div class="company" style="background-image: url(/images/bg-company-2.png);">
                            <img class="company-img" src="/images/company.png" alt="#">
                            <div class="name-company">
                                <h5>TINY <span>(Brand studio)</span></h5>
                                <p>made in <span>New Zealand</span></p>
                            </div>
                            <ul class="categories">
                                <li><a href="#">Categories:</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                            </ul>
                            <ul class="description">
                                <li>Products: <span>56</span></li>
                                <li>New: <span>26</span></li>
                                <li>Special offers: <span>16</span></li>
                                <li>Sale: <span>20%</span></li>
                            </ul>
                            <div class="author">
                                <img src="/images/img-2.png" alt="#">
                                <p class="name">Sam Clarintence</p>
                            </div>
                        </div>
                        <div class="bread-crumbs">
                            <ul class="hvr-radial-out">
                                <li>
                                    <a href="#"></a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li>
                                    <a href="#"></a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <hr class="sline">
                <?= frontend\widgets\PGroups::widget(); ?>
                <hr class="sline">
                <div class="mesages ">
                    <h4 class="promote promote-bottom">Architectors & designers</h4>
                    <p class="upload-portfolio download-portfolio join" data-toggle="modal"
                       data-target=".agent-modal-product">Add more
                        <a href="#"></a>
                    </p>

                    <div class="clearfix"></div>
                    <ul>
                        <li>
                            <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence
                                <span>(designer)</span></a><b><i
                                    class="icon-ascending24 ">2B</i><a href="#" class="button">Pay for tariff</a></b>
                        </li>
                        <li>
                            <a href="#"><img src="/images/img-2.png" alt="#">Sam Clarintence
                                <span>(designer)</span></a><b><i
                                    class="icon-ascending24 ">2B</i><a href="#" class="button">Pay for tariff</a></b>
                        </li>
                    </ul>
                </div>

                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">My special offers</h4>
                    <p class="upload-portfolio download-portfolio join" data-toggle="modal"
                       data-target=".agent-modal-product">Add
                        <a href="#"></a>
                    </p>

                    <div class="clearfix"></div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-31.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b>
                                    <br>save 30%</p>
                            </div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span>
                                <br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-32.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b>
                                    <br>save 30%</p>
                            </div>

                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span>
                                <br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">

                            <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-31.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b>
                                    <br>save 30%</p>
                            </div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span>
                                <br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-32.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b>
                                    <br>save 30%</p>
                            </div>

                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span>
                                <br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">

                            <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>

                </div>
                <div class="bread-crumbs">
                    <ul class="hvr-radial-out">
                        <li>
                            <a href="#"></a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li>
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="video">
                    <h4 class="promote promote-bottom">Video</h4>
								<span class="upload-portfolio download-portfolio edit" data-toggle="modal"
                                      data-target=".agent-modal-product">Edit
									<a href="#"></a>
								</span>
                    <div class="clearfix"></div>
                    <p><i class="icon-videoplayer5"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                    <iframe src="https://www.youtube.com/embed/LaP1H-JJroU" allowfullscreen="" frameborder="0"
                            height="360"
                            width="100%"></iframe>
                    <p><i class="icon-videoplayer5"></i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</p>
                    <iframe src="https://www.youtube.com/embed/LaP1H-JJroU" allowfullscreen="" frameborder="0"
                            height="360"
                            width="100%"></iframe>
                </div>
            </div>
        </div>
    </div>
    <!--END CENTER-->

<?php
$this->registerJsFile('agent_js/action_agent.js', ['depends' => 'frontend\assets\AppAsset']);
$script_honours = <<< JS

    $('#all-projects-view').change(function(){
        var all_view = $(this).val();
        var request = $.ajax({
			data : {all_view : all_view},
			url: '/account/projects-agent/viewed',
			type: 'post',
			dataType: 'html'
		});

		request.done(function(){
		    location.reload();
		});

    });

    $('body').delegate( ".agent-all-projects-pagination a", "click", function(e) {
          e.preventDefault();
          var link = $(this).attr('href');
          $('#agent-all-projects').load(link+' #agent-all-projects > *');
          $('.agent-all-projects-pagination').load(link+' .agent-all-projects-pagination > *');
    });

    var pieColors = [
		'rgb(51, 102, 102)',
		'rgb(40, 124, 122)',
		'rgb(51, 153, 153)',
		'rgb(84, 175, 172)',
		'rgb(153, 204, 204)',
		'rgb(203, 228, 228)'
	];
    function getTotal( arr ){
    var j,
        myTotal = 0;

    for( j = 0; j < arr.length; j++) {
        myTotal += ( typeof arr[j] === 'number' ) ? arr[j] : 0;
    }

    return myTotal;
}

    function drawPieChart( canvasId ) {
        var i,
            canvas = document.getElementById( canvasId ),
            pieData = canvas.dataset.values.split(',').map( function(x){ return parseInt( x, 10 )}),
            halfWidth = canvas.width * .5,
            halfHeight = canvas.height * .5,
            ctx = canvas.getContext( '2d' ),
            lastend = 0,
            myTotal = getTotal(pieData);

        ctx.clearRect( 0, 0, canvas.width, canvas.height );

        for( i = 0; i < pieData.length; i++) {
            ctx.fillStyle = pieColors[i];
            ctx.beginPath();
            ctx.moveTo( halfWidth, halfHeight );
            ctx.arc( halfWidth, halfHeight, halfHeight, lastend, lastend + ( Math.PI * 2 * ( pieData[i] / myTotal )), false );
            ctx.lineTo( halfWidth, halfHeight );
            ctx.fill();
            lastend += Math.PI * 2 * ( pieData[i] / myTotal );
        }
    }

    drawPieChart('graphic');
    drawPieChart('graphic-1');
    drawPieChart('graphic-2');
    drawPieChart('graphic-3');
    drawPieChart('graphic-4');

	$(document).ready(function(){
		$('.analytics .button').click(function(){
			$('.more-info').slideToggle();
		});
	});
JS;
$this->registerJs($script_honours, yii\web\View::POS_READY);
$this->registerJsFile('js/owl.carousel.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_company.js', ['depends' => 'frontend\assets\AppAsset']);





