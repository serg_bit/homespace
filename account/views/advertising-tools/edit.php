<?php
use yii\widgets\LinkPager;

//$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="col-lg-7 central-content profile-content profile-agent  company create centralScroll ">
        <div class="row">
            <div class="content">
                <div class="agent-profil" data-img-src="/images/bg-agents.png"
                     style="background-image: url(/media/upload/<?= $manufacturer['background'] ?>);">
                    <div class="agents">
                        <input accept="image/*;capture=camera" style="display : none;" id="open_browse2"
                               onchange="fileChange(this);" type="file">
                        <a href="#" class="bg-profile company-bg" data-toggle="modal" data-target=".model-img"><i
                                class="icon-camera"></i></a>
                        <div class="company-description">
                            <input accept="image/*;capture=camera" style="display : none;" id="open_browse"
                                   onchange="fileChange(this);" type="file">
                            <div class="dropzone" id="dropzone1"
                                 style="background: url('/media/upload/<?= $manufacturer['logo'] ?>')">Drop files here
                                to upload
                            </div>
                            <form class="forma-input">

                                <div class="input-edit">
                                    <?php $company = explode("||", $manufacturer['company']); ?>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="company-name" type="text"
                                               value="<?= $company[0] ?>">
                                        <label for="company-name">Company name</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="brand-name" type="text"
                                               value="<?= $company[1] ?>">
                                        <label for="brand-name">brand agency</label>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="profile-edite">
                            <div class="profil-photo">
                                <input accept="image/*;capture=camera" style="display : none;" id="open_browse1"
                                       onchange="fileChange(this);" type="file">
                                <a href="#" id="profile-photo"><img src="/media/upload/<?= $manufacturer['avatar'] ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="create-profile">
                            <form class="forma-input">

                                <div class="input-edit position">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="position" type="text"
                                               value="<?= $manufacturer['specialization'] ?>">
                                        <label for="position">Position</label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="input-edit">
                                    <i class="icon-user168"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="first-name" type="text"
                                               value="<?= $manufacturer['first_name'] ?>">
                                        <label for="first-name">First</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-user168"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="last-name" type="text"
                                               value="<?= $manufacturer['last_name'] ?>">
                                        <label for="last-name">Last</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-home"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="country" type="text"
                                               value="<?= $manufacturer['country'] ?>">
                                        <label for="country">Country</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-home"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="city" type="text"
                                               value="<?= $manufacturer['city'] ?>">
                                        <label for="city">City</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-telephone46"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="phone" type="text"
                                               value="<?= $manufacturer['phone'] ?>">
                                        <label for="phone">Phone</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-location"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="address" type="text"
                                               value="<?= $manufacturer['address'] ?>">
                                        <label for="address">Address</label>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>


                    <div class="profil-edited">
                        <form class="forma-input" id="form_main_information">
                            <div class="input-edit-top sub-wrap">

                                <div class="input-edit">
                                    <i class="icon-webpage2"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="site" type="text"
                                               value="<?= $manufacturer['link_user_site'] ?>">
                                        <label for="site">Your Site</label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-facebook55"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="facebook" type="text"
                                               value="<?= $manufacturer['link_fb'] ?>">
                                        <label for="facebook">Link for Facebook.com</label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-twitter1"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="twitter" type="text"
                                               value="<?= $manufacturer['link_tw'] ?>">
                                        <label for="twitter">Link for Twitter.com</label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-instagram12"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="instagram" type="text"
                                               value="<?= $manufacturer['link_inst'] ?>">
                                        <label for="instagram">Link for Instagram.com</label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-behance2"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="behance" type="text"
                                               value="<?= $manufacturer['link_behance'] ?>">
                                        <label for="behance">Link for Behance.com</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-google116"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="google" type="text"
                                               value="<?= $manufacturer['link_google'] ?>">
                                        <label for="google">Link for Google+</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-pinterest3"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="pinterest" type="text"
                                               value="<?= $manufacturer['link_pint'] ?>">
                                        <label for="pinterest">Link for Pinterest.com</label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-logotype1"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="tumblr" type="text"
                                               value="<?= $manufacturer['link_tumblr'] ?>">
                                        <label for="tumblr">Link for Tumblr.com</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-linkedin11"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="linkedin" type="text"
                                               value="<?= $manufacturer['link_linkedin'] ?>">
                                        <label for="linkedin">Link for Linkedin.com</label>
                                    </div>
                                </div>


                                <div class="input-edit">
                                    <i class="icon-blogger8"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label dirty" name="blogger" type="text"
                                               value="<?= $manufacturer['link_blg'] ?>">
                                        <label for="blogger">Link for Blogger.com</label>
                                    </div>
                                </div>


                            </div>

                        </form>
                    </div>
                    <div class="agent-text ">
                        <form class="forma-input">
                            <div class="input-container">
                                <?php if ($categories) :
                                    $category_list = '';
                                    while ($categories) :
                                        $category = array_shift($categories);
                                        $category_list .= $category[$language];
                                        $category_list .= ", ";
                                    endwhile;
                                endif; ?>
                                <input class="text-input floating-label " name="categories" type="text" value="<?= $category_list ?>">
                                <label for="categories">Categories</label>
                            </div>
                            <div class="textarea"><textarea name="message" id="#" cols="30" rows="10"
                                                            placeholder="Signature..."><?= $manufacturer['message'] ?></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="profile-video">
                        <div class="clearfix"></div>
                        <div class="video">
                            <img class="video-img" src="/images/no-video.png" alt="" height="181" width="330">
                            <form class="forma-input">
                                <div class="input-container namb">
                                    <input class="text-input floating-label dirty" name="video-link" type="text" value="<?= $video['link'] ?>">
                                    <label for="video-link">Video’s link</label>
                                </div>
                                <div class="input-container namb">
                                    <input class="text-input floating-label dirty" name="video-name" type="text" value="<?= $video['name'] ?>">
                                    <label for="video-name">Video’s name</label>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                <h4 class="promote promote-bottom">Security</h4>
                <form class="honours forma-input security security-info">
                    <div>
                        <div class="input-edit">
                            <i class="icon-symbol20"></i>
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="email" type="text" value="<?= $manufacturer['email'] ?>">
                                <label for="email">E-mail</label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-id16"></i>
                            <div class="input-container">
                                <input class="text-input floating-label dirty" name="login" type="text" value="<?= $manufacturer['username'] ?>">
                                <label for="login">Login</label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-locked59"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" id="new_pass" val="" name="password" type="text">
                                <label for="password">New Password*</label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-locked59"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" id="confirm_pass" name="confirm" type="text"  val="">
                                <label for="confirm">Confirm password*</label>
                            </div>

                        </div>
                        <p class="pass check-form-error-message" style="display: none;"><span
                                class="text-danger"></span></p>
                        <p class="pass result-success" style="display: none;"><span class="text-success"></span></p>
                        <ul>
                            <li><a href="#" class="button" id="edit-company" data-id="<?= $manufacturer['id'] ?>">OK</a></li>
                        </ul>
                    </div>


                </form>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_company.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']);
