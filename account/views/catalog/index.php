<?php
use yii\widgets\LinkPager;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'catalog');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="col-lg-7 central-content page-catalog script-catalog centralScroll">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 catalog-column">
                <!--                <div class="sidebar-caption">Catalog<br></div>-->
                <div class="sort-menu">
                    <ul class="sort-categories">
                        <li><a href="#" data-value = "1" class="active-category sort-teg">Furniture</a></li>
                        <li><a href="#" data-value = "2" class="sort-teg">Bed & Bath</a></li>
                        <li><a href="#" data-value = "3" class="sort-teg">Lighting</a></li>
                        <li><a href="#" data-value = "4" class="sort-teg">Floor covering</a></li>
                        <li><a href="#" data-value = "5" class="right-align-sort sort-teg">Wall decor</a></li>
                        <li><a href="#" data-value = "6" class="sort-teg">Art & Decor</a></li>
                        <li><a href="#" data-value = "7" class="left-align-sort sort-teg">Outdoor living</a></li>
                    </ul>
                </div>
                <?php Pjax::begin(); ?>
                <div class="clearfix"></div>

                <div class="catalog-box">
                    <?php $form = ActiveForm::begin(['id' => 'catalog-form', 'action' => ['index'], 'method' => 'get', 'options' => ['data-pjax' => TRUE]]); ?>
                    <?= $form->field($searchModel, 'category')->hiddenInput()->label(FALSE); ?>
                    <?php ActiveForm::end(); ?>
                    <?php foreach ($data->models as $key => $catalog): ?>
                        <div class="catalog-item">
                            <a href="<?= Url::to(['catalog/catalog', 'id' => $catalog->id_catalog, ])?>" data-pjax="0"><img src="/images/<?= $catalog->catalog_img; ?>" alt=""></a>
                            <div class="catalog-caption"><?= $catalog->title; ?></div>
                        </div>

                    <?php endforeach; ?>
                </div>
                <div class="bread-crumbs">
                    <?= LinkPager::widget(['pagination' => $data->pagination,
                        'disabledPageCssClass' => false,
                        'nextPageLabel' => '',
                        'prevPageLabel' => '',
                        'options'=>['class'=>'hvr-radial-out'],
                    ]); ?>
                </div>
                <?php Pjax::end(); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
    </div>
</div>

<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>


