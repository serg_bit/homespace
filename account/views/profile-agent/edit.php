<?php
use yii\widgets\LinkPager;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>


    <div class="col-lg-7 central-content profile-content profile-agent  company create centralScroll ">
        <div class="row">
            <div class="content">
                <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse" onchange="fileLogoChange(this);" />
                <input type="file" accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="userBackground(this)" />
                <div class="agent-profil" data-url="<?= $user_information['background'] ?>" style="background-image: url(/media/profile/background/<?= $user_information['background'] ?>);">

                    <div class="agents">
                        <a href="#" class="bg-profile company-bg"><i class="icon-camera"></i></a>
                        <br>
                        <br>
                        <div class="clearfix"></div>
                        <div class="profile-edite">
                            <div class="profil-photo">
                                <a href="#" id="profile-photo"><img src="<?= Yii::getAlias('@avatar/'.$user_information['avatar']) ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="create-profile">
                            <form class="forma-input" id="main-user-info">

                                <div class="input-edit position">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="position" value="<?= $user_information['specialization'] ?>" type="text">
                                        <label for="position">Position</label>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="input-edit">
                                    <i class="icon-user168"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="first-name" value="<?= $user_information['first_name'] ?>" type="text">
                                        <label for="first-name"><?= Yii::t('account', 'edit_first_name') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-user168"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="last-name" value="<?= $user_information['last_name'] ?>" type="text">
                                        <label for="last-name"><?= Yii::t('account', 'edit_last_name') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-home"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="country" value="<?= $user_information['country'] ?>" type="text">
                                        <label for="address">Country</label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-telephone46"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="phone" value="<?= $user_information['phone'] ?>" type="text">
                                        <label for="phone"><?= Yii::t('account', 'edit_phone') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-home"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="city" value="<?= $user_information['city'] ?>" type="text">
                                        <label for="city"><?= Yii::t('account', 'edit_city') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-location"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="address" value="<?= $user_information['address'] ?>" type="text">
                                        <label for="address">Address</label>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>


                    <div class="profil-edited">
                        <form class="forma-input" id="user-social">
                            <div class="input-edit-top sub-wrap">

                                <div class="input-edit">
                                    <i class="icon-webpage2"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="site" value="<?= $user_information['link_user_site'] ?>" />
                                        <label for="site"><?= Yii::t('account', 'edit_site') ?></label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-facebook55"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="facebook" value="<?= $user_information['link_fb'] ?>" />
                                        <label for="facebook"><?= Yii::t('account', 'edit_facebook') ?></label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-twitter1"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="twitter" value="<?= $user_information['link_tw'] ?>" />
                                        <label for="twitter"><?= Yii::t('account', 'edit_twitter') ?></label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-instagram12"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="instagram" value="<?= $user_information['link_inst'] ?>"/>
                                        <label for="instagram"><?= Yii::t('account', 'edit_instagram') ?></label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-behance2"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="behance" value="<?= $user_information['link_behance'] ?>"/>
                                        <label for="behance"><?= Yii::t('account', 'edit_behance') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-google116"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="google" value="<?= $user_information['link_google'] ?>"/>
                                        <label for="google"><?= Yii::t('account', 'edit_google') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-pinterest3"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="pinterest" value="<?= $user_information['link_pint'] ?>"/>
                                        <label for="pinterest"><?= Yii::t('account', 'edit_pinterest') ?></label>
                                    </div>
                                </div>

                                <div class="input-edit">
                                    <i class="icon-logotype1"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="tumblr" value="<?= $user_information['link_tumblr'] ?>" />
                                        <label for="tumblr"><?= Yii::t('account', 'edit_tumblr') ?></label>
                                    </div>
                                </div>
                                <div class="input-edit">
                                    <i class="icon-linkedin11"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="linkedin" value="<?= $user_information['link_linkedin'] ?>" />
                                        <label for="linkedin"><?= Yii::t('account', 'edit_linkedin') ?></label>
                                    </div>
                                </div>


                                <div class="input-edit">
                                    <i class="icon-blogger8"></i>
                                    <div class="input-container">
                                        <input class="text-input floating-label" type="text" name="blogger" value="<?= $user_information['link_blg'] ?>" />
                                        <label for="blogger"><?= Yii::t('account', 'edit_blogger') ?></label>
                                    </div>
                                </div>


                            </div>

                        </form>
                    </div>
<!--                                    <div class="mesages comp">-->
<!---->
<!--                                        <div class="consultant">-->
<!--                                            <div class="profile-edite">-->
<!--                                                <div class="profil-photo">-->
<!--                                                    <input accept="image/*;capture=camera" style="display : none;" id="open_browse2" onchange="fileChange(this);" type="file">-->
<!--                                                    <a href="#" id="consultant-photo"><img src="" alt=""></a>-->
<!--                                                </div>-->
<!--                                            </div>-->
<!--                                            <form class="forma-input ">-->
<!---->
<!--                                                <div class="input-edit">-->
<!--                                                    <i class="icon-user168"></i>-->
<!--                                                    <div class="input-container">-->
<!--                                                        <input class="text-input floating-label" name="consultant-first-name" type="text">-->
<!--                                                        <label for="consultant-first-name">First</label>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="input-edit">-->
<!--                                                    <i class="icon-user168"></i>-->
<!--                                                    <div class="input-container">-->
<!--                                                        <input class="text-input floating-label" name="consultant-last-name" type="text">-->
<!--                                                        <label for="consultant-last-name">Last</label>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="input-edit">-->
<!--                                                    <i class="icon-telephone46"></i>-->
<!--                                                    <div class="input-container">-->
<!--                                                        <input class="text-input floating-label" name="consultant-phone" type="text">-->
<!--                                                        <label for="consultant-phone">Phone</label>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="input-edit">-->
<!--                                                    <div class="input-container">-->
<!--                                                        <input class="text-input floating-label" name="consultant-position" type="text">-->
<!--                                                        <label for="consultant-position">Position</label>-->
<!--                                                    </div>-->
<!--                                                </div>-->
<!--                                                <div class="radio-button">-->
<!--                                                    <p>Massages</p>-->
<!--                                                    <input id="radio-1" name="featured" checked="" type="radio"><label for="radio-1">Anyone</label>-->
<!--                                                </div>-->
<!--                                                <div class="radio-button">-->
<!--                                                    <input id="radio-2" name="featured" checked="" type="radio"><label for="radio-2">Everybody</label>-->
<!--                                                </div>-->
<!--                                                <button class="button" id="add-consultant">OK</button>-->
<!--                                            </form>-->
<!--                                            <p class="pass error-message" style="display:none;"><span class="text-danger"></span></p>-->
<!--                                            <hr class="sline">-->
<!--                                        </div>-->
<!--                                        <ul class="consultants-list">-->
<!--                                            <li>-->
<!--                                                <a href="#"><img src="images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>-->
<!--                                            </li>-->
<!--                                            <li>-->
<!--                                                <a href="#"><img src="images/img-2.png" alt="#">Sam Clarintence <span>(consultant of bathrooms)</span></a><b>Give rights<i class="icon-black218 "></i></b>-->
<!--                                            </li>-->
<!--                                        </ul>-->
<!--                                    </div>-->
                    <div class="agent-text">
                        <form class="forma-input">

                            <div class="textarea"><textarea name="message" cols="30" rows="10" placeholder="Signature..."><?= $user_information['message'] ?></textarea></div>
                        </form>
                    </div>
                    <div class="agent-text agent-edit-company">
                        <a href="#" id="company-logo">
                            <img src="<?= Yii::getAlias('@avatar/'.$user_information['logo']) ?>" alt="">
                        </a>
                        <form class="forma-input">
                            <div class="input-edit">
                                <div class="input-container">
                                    <input class="text-input floating-label" name="company-name" type="text" value="<?= $user_information['company'] ?>">
                                    <label for="company-name">Company name</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                    <div class="block-button">
                        <p class="valid" id="main-save-valid"></p>
                        <p class="error" id="main-save-error">Changes not saved correctly!</p>
                        <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="main_save">
                    </div>
                </div>
                <!-- Security -->

                <h4 class="promote promote-bottom"><?= Yii::t('account', 'edit_security') ?></h4>
                <form class="honours forma-input security security-info">
                    <div>
                        <div class="input-edit">
                            <i class="icon-symbol20"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" type="text" name="email" value="<?= $user_information['email'] ?>"/>
                                <label for="email"><?= Yii::t('account', 'edit_email') ?></label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-id16"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" type="text" name="username" value="<?= $user_information['username'] ?>"/>
                                <label for="username"><?= Yii::t('account', 'edit_username') ?></label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-locked59"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" id="new_pass"	type="text" name="password" value=""/>
                                <label for="password"><?= Yii::t('account', 'edit_password') ?></label>
                            </div>

                        </div>
                        <div class="input-edit">
                            <i class="icon-locked59"></i>
                            <div class="input-container">
                                <input class="text-input floating-label" id="confirm_pass" type="text" name="confirm" value="" />
                                <label for="confirm"><?= Yii::t('account', 'edit_confirm') ?></label>
                            </div>

                        </div>
                    </div>
                    <div class="block-button">
                        <p class="valid" id="security-save-valid"></p>
                        <p class="error" id="security-save-error">Changes not saved correctly!</p>
                        <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="security_save">
                    </div>
                </form>

                <!-- End Security -->

                <hr class="sline">
                <!-- Video -->
                <input type="hidden" id="hidden-videos-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-link-text="<?= Yii::t('account', 'profile_video_link') ?>" data-name-text="<?= Yii::t('account', 'profile_video_name') ?>"/>
                <div class="profile-video">
                    <h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_video') ?></h4>
                    <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_video') ?><a href="#" id="add_video"></a></p>
                    <div class="clearfix"></div>
                    <div class="all-videos">
                        <?php if($videos){?>
                            <?php foreach ($videos as $video) {?>
                                <div class="clearfix"></div>
                                <div class="video videos_count" data-video-id="<?= $video['id'] ?>">
                                    <iframe width="330" height="181" src="<?= $video['link'] ?>" frameborder="0" allowfullscreen></iframe>
                                    <form class="forma-input">
                                        <div class="input-container namb">
                                            <input class="text-input floating-label video-input" type="text" name="link" value="<?= $video['link'] ?>">
                                            <label for="link"><?= Yii::t('account', 'profile_video_link') ?></label>
                                        </div>
                                        <div class="input-container namb">
                                            <input class="text-input floating-label video-input" type="text" name="name" value="<?= $video['name'] ?>" />
                                            <label for="name"><?= Yii::t('account', 'profile_video_name') ?></label>
                                        </div>
                                    </form>
                                    <a href="#" class="del del_video"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                </div>
                            <?php } ?>
                        <?php }else{ ?>
                            <div class="video videos_count" data-video-id="">
                                <img class="video-img" src="/images/no-video.png" alt="">
                                <form class="forma-input">
                                    <div class="input-container namb">
                                        <input class="text-input floating-label video-input" type="text" name="link" value=""/>
                                        <label for="link"><?= Yii::t('account', 'profile_video_link') ?></label>
                                    </div>
                                    <div class="input-container namb">
                                        <input class="text-input floating-label video-input" type="text" name="name" value=""/>
                                        <label for="name"><?= Yii::t('account', 'profile_video_name') ?></label>
                                    </div>
                                </form>
                                <a href="#" class="del del_video"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="block-button">
                        <p class="valid" id="videos-save-valid"></p>
                        <p class="error" id="videos-save-error">Changes not saved correctly!</p>
                        <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="videos_save">
                        <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="all-information-save">
                    </div>

                </div>
                <!--End Video -->

            </div>
        </div>
    </div>

<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/edit_profile.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/videos_profile.js', ['depends'=>'frontend\assets\AppAsset']);
