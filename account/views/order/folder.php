<?php
use yii\widgets\LinkPager;
use frontend\account\models\Application;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Language;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account').' - '.$folder['title'];
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CENTER-->
    <div class="col-lg-7 central-content page-catalog page-in-catalog in-order comp-ord script">
        <div class="content">
            <div class="row">
                <div class="col-lg-12 catalog-column">
                    <h4 class="promote promote-bottom complete"><?= $folder['title']; ?></h4>
                    <!--                    <a href="--><?//=\Yii::$app->request->referrer?><!--" class="folder-link-arrows"><i class="icon-arrows"></i></a>-->
                    <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="catalog-box">
                        <?php foreach ($data->models as $prod): ?>

                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="">
                                        <img src="/images/<?= $prod['images'] ?>" alt="">
                                    </a>
                                    <p id_order="<?= $prod['id_order'] ?>" class="add-order move-remove" data-toggle="modal" data-target=".move-order-model"><?= Yii::t('account', 'order_move') ?></p>
                                    <div class="like-calc"><i
                                            class="icon-heart2971"></i><span><?= $prod['likes'] ?></span></div>
                                    <div class="like-calc item-price"><i
                                            class="icon-label49"></i><span><?= $prod['price'] ?>$</span></div>
                                </div>
                                <div class="catalog-caption">
                                    <div class="social-cont">
                                        <a class="main-like icon-heart297"></a>

                                    </div>

                                    <p><?= $prod['product_name'] ?><br>
                                        <?= $prod['meta_tegs'] ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>

                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $data->pagination,
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options' => ['class' => 'hvr-radial-out'],

                            ]); ?>
                        </div>

                            <hr class="sline">
                            <div class="clearfix"></div>
                            <h4 class="promote promote-bottom"><?= Yii::t('account', 'order_folder') ?></h4>

                            <p id="create-child-folder" class="upload-portfolio download-portfolio"><?= Yii::t('account', 'order_add') ?><a href="#"></a></p>

                        <div id="child-folder" class="forma-input"  style="display: none" >
                            <div class="input-container">
                                <input id="folder-in-folder" folder="<?= $folder['id_folder']; ?>" class="text-input floating-label" type="text" name="sample" value=""/>
                                <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                            </div>
                            <input type="button" id="create_in_folder" value="<?= Yii::t('account', 'button_save') ?>">
                            <div class="folder-error" style="display: none" ><?= Yii::t('account', 'folder_error') ?></div>
                        </div>



                        <div class="clearfix"></div>

                    </div>
                    <div class="folder">
                        <?php if ($info_folder) {
                            foreach ($info_folder->models as $folder) : ?>
                                <div class="name-folder" id_folder="<?= $folder['id_folder']; ?>">
                                    <a href="<?= Url::to(['folder', 'id' => $folder['id_folder']]); ?>" class="folder_link">
                                        <div class="folder-img ">
                                            <?php $folder_image = \frontend\account\controllers\OrderController::onFolder(['id_folder' => $folder['id_folder']]);
                                            if($folder_image){?>
                                                <?php foreach($folder_image as $image) : ?>
                                                    <?php if(isset($image['folder_img'])){?>
                                                        <img class="folder-on-folder" src="/images/<?= $image['folder_img']; ?>" alt="">
                                                    <?php } else {?>
                                                        <img src="/images/<?= $image['miniature_img']; ?>" alt="">
                                                    <?php } ?>
                                                <?php endforeach; ?>
                                            <?php } else{ ?>
                                                <img class="folder-empty" src="/images/folder.png" alt="">
                                            <?php }?>
                                        </div>
                                    </a>
                                    <div class="folder-bottom change-folder forma-input">
                                        <a href="<?= Url::to(['folder', 'id' => $folder['id_folder']]); ?>" class="folder_link">
                                            <p><?= $folder['title']; ?></p>
                                        </a>
                                        <div class="input-container" style="display: none">
                                            <input class="text-input floating-label title" type="text" name="re-name" value="<?= $folder['title']; ?>"/>
                                            <label for="re-name" ></label>
                                        </div>
                                        <i class="icon-show8"></i>
                                        <ul>
                                            <li><a class="re_folder" url="<?= Url::to(['/account/order/rename', 'id' => $folder['id_folder']]); ?>" href="#"><i
                                                        class="icon-edit45"></i>Rename</a></li>
                                            <li><a class="del_folder" url="<?= Url::to(['/account/order/delete', 'id' => $folder['id_folder']]); ?>" href="#"><i
                                                        class="icon-rubbish"></i>Delete</a></li>
                                        </ul>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php } else {  } ?>
                    </div>

                    <div class="bread-crumbs">
                        <?= LinkPager::widget(['pagination' => $info_folder->pagination,
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options' => ['class' => 'hvr-radial-out'],

                        ]); ?>

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
        </div>
    </div>
    <!--END CENTER-->

<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile ('https://code.jquery.com/ui/1.11.4/jquery-ui.js', ['depends' => 'frontend\assets\AppAsset']);

