<?php
use yii\widgets\LinkPager;
use frontend\account\models\Application;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Language;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'order');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CENTER-->
    <div class="col-lg-7 central-content page-catalog page-in-catalog in-order comp-ord script centralScroll">
        <div class="content">
            <div class="row">
                <div class="col-lg-12 catalog-column">
                    <?php if (isset($model)): ?>
                        <h4 class="promote promote-bottom complete"><?= Yii::t('account', 'order_title') ?></h4>
                        <p class="upload-portfolio download-portfolio new-project"><?= Yii::t('account', 'new_project') ?><a href="#"></a></p>
                        <div class="clearfix"></div>
                        <div class="new_project">

                            <?php $form = ActiveForm::begin(['id' => 'application', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'forma-input textarea']]) ?>
                            <div id="application-name" style="display: none" data-name="<?php if(Language::getCurrent()->url==='en'){echo 'Name of application';} else echo 'Название проекта'; ?>"></div>
                            <div class="input-container">

                                <?= $form->field($model, 'title_app')->textInput(['class' => 'floating-label text-input', 'value' => ""])->label(false) ?>

                            </div>
                            <div class="shared-upload myshare">
                               <a href="#" class="file">
                                   <?= $form->field($model, 'file[]')->fileInput(['multiple' => true])->label(false); ?>
                                    <div id="application-upload" style="display: none" data-upload="<?php if(Language::getCurrent()->url==='en'){echo 'Upload files';} else echo 'Добавить файлы'; ?>">

                                    </div>
<!--                                   <div class="clearfix"></div>-->
                                    <div class="shared-with">
                                        <i class="icon-invite"></i>
                                        <p><?= Yii::t('account', 'order_share') ?></p>

                                    </div>
                                    <?php $data12 = array('24' => 'Group 1', '121' => 'Group 2', '125' => 'Group 3', '126' => 'Group 4', '131' => 'Group 5','150' => 'Group 6'); ?>
                                    <?= $form->field($model, 'recipients[]')->widget(Select2::classname(), [
                                        'data' => $data12,
                                        'options' => ['multiple' => true],
                                        'showToggleAll' => false,
                                        'pluginOptions' => [
                                            'maximumSelectionLength' => 5,
                                            'allowClear' => true,
                                        ],
                                    ])->label(false); ?>
                            </div>
                            <div class="sel">
                                <div class="selects prod">
                                    <p><?= Yii::t('account', 'order_category') ?></p>
                                    <?php $category = array( '2' => 'Furniture','3' => 'Sofa','4' => 'Bathroom','5' => 'Tables', '6' => 'Flowers');?>
                                    <?= $form->field($model, 'category_app')->dropDownList($category,['prompt' => ''])->label(false); ?>

                                </div>
                            </div>
                            <div class="sel">
                                <div class="selects prod">
                                    <p><?= Yii::t('account', 'order_choose_folder') ?></p>
                                    <?php $result = ArrayHelper::map($folder_list, 'id_folder', 'title'); ?>
                                    <?= $form->field($model, 'id_folder')->dropDownList($result,['prompt'=>''])->label(false); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <?= $form->field($model, 'description_app')->textarea(array('placeholder' =>  Yii::t('account', 'order_describe')), ['options' => ['id' => 'desc_app']])->label(false) ?>

                            <div class="block-button">
                                <div class="form-group">
                                    <?= Html::resetButton(Yii::t('account', 'cancel'), ['class' => 'button reset-app']) ?>
                                    <?= Html::submitButton(Yii::t('account', 'send'), ['class' => 'button']) ?>
                                </div>
                            </div>
                            <?php ActiveForm::end() ?>
                            <hr class="sline mt">
                        </div>
                    <?php else: ?>
                        <h4 class="promote promote-bottom complete"><?= $folder['title'] ?></h4>
                    <?php endif ?>

                    <div class="clearfix"></div>
                    <div class="catalog-box">
                        <?php foreach ($data->models as $prod): ?>

                            <div class="catalog-item">
                                <div class="catalog-img-box">
                                    <a href="">
                                        <img src="<?= Yii::getAlias('@products/'.$prod['main_image'] )?>" alt="">
                                    </a>
                                    <p id_order="<?= $prod['id_order'] ?>" class="add-order move-remove" data-toggle="modal" data-target=".move-order-model"><?= Yii::t('account', 'order_move') ?></p>
                                    <div class="like-calc"><i
                                            class="icon-heart2971"></i><span><?= $prod['likes'] ?></span></div>
                                    <div class="like-calc item-price"><i
                                            class="icon-label49"></i><span><?= $prod['price'] ?>$</span></div>
                                </div>
                                <div class="catalog-caption">
                                    <div class="social-cont">
                                        <a class="main-like icon-heart297"></a>
                                    </div>

                                    <p><?= $prod['product_name'] ?><br>
                                        <?= $prod['meta_tags'] ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>

                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $data->pagination,
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options' => ['class' => 'hvr-radial-out'],

                            ]); ?>
                        </div>
                        <hr class="sline">
                        <div class="clearfix"></div>
                        <h4 class="promote promote-bottom"><?= Yii::t('account', 'order_folder') ?></h4>
                        <p id="showCreate" class="upload-portfolio download-portfolio"><?= Yii::t('account', 'order_add') ?><a href="#"></a></p>
                        <div class="clearfix"></div>
                        <div id="addNewFolder" class="forma-input"  style="display: none" >
                            <div class="input-container">
                                <input id="folder"  class="text-input floating-label" type="text" name="sample" value=""/>
                                <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                            </div>
                            <input type="button" id="create_folder" value="<?= Yii::t('account', 'button_save') ?>">
                            <div class="folder-error" style="display: none"><?= Yii::t('account', 'folder_error') ?></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="folder">
                        <?php if ($info_folder) {
                            foreach ($info_folder->models as $folder) : ?>
                                <div class="name-folder" id_folder="<?= $folder['id_folder']; ?>">
                                    <a href="<?= Url::to(['folder', 'id' => $folder['id_folder']]); ?>" class="folder_link">
                                        <div class="folder-img ">
                                            <?php $folder_image = \frontend\account\controllers\OrderController::onFolder(['id_folder' => $folder['id_folder']]);
                                            if($folder_image){ ?>
                                                <?php foreach($folder_image as $image) : ?>
                                                    <?php if(isset($image['folder_img'])){?>
                                                        <img class="folder-on-folder" src="/images/<?= $image['folder_img']; ?>" alt="">
                                                    <?php } else {?>
                                                        <img src="/images/<?= $image['miniature_img']; ?>" alt="">
                                                    <?php } ?>
                                                <?php endforeach; ?>
                                            <?php } else{ ?>
                                                <img class="folder-empty" src="/images/folder.png" alt="">
                                            <?php }?>
                                        </div>
                                    </a>
                                    <div class="folder-bottom change-folder forma-input">
                                        <a href="<?= Url::to(['folder', 'id' => $folder['id_folder']]); ?>" class="folder_link">
                                            <p><?= $folder['title']; ?></p>
                                        </a>
                                        <div class="input-container" style="display: none">
                                            <input class="text-input floating-label title" type="text" name="re-name" value="<?= $folder['title']; ?>"/>
                                            <label for="re-name" ></label>
                                        </div>

                                        <i class="icon-show8"></i>
                                        <ul>
                                            <li>
                                                <a class="re_folder" url="<?= Url::to(['/account/order/rename', 'id' => $folder['id_folder']]); ?>" href="#">
                                                    <i class="icon-edit45"></i>Rename</a>
                                            </li>

                                            <li>
                                                <a class="del_folder"  url="<?= $url['delete']; ?>" href="#">
                                                    <i class="icon-rubbish"></i>Delete</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            <?php endforeach; ?>
                        <?php } else {  } ?>
                    </div>

                    <div class="bread-crumbs">
                        <?= LinkPager::widget(['pagination' => $info_folder->pagination,
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options' => ['class' => 'hvr-radial-out'],

                        ]); ?>

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
        </div>
    </div>
    <!--END CENTER-->

<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile ('https://code.jquery.com/ui/1.11.4/jquery-ui.js', ['depends' => 'frontend\assets\AppAsset']);

