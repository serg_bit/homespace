<?php
use yii\widgets\LinkPager;
use mihaildev\ckeditor\CKEditor;
use frontend\models\Language;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CENTER-->
    <div class="col-lg-7 central-content profile-content profile-agent centralScroll">
        <div class="row">
            <div class="content">
                <div class="analytics">
                    <h4 class="promote promote-bottom">Analytics</h4>
                    <p class="last-days">Last 30 days</p>
                    <div class="clearfix"></div>
                    <div class="graphic">
                        <p>People, <br>who have visited profile</p>
                        <aside class="chart vert">
                            <canvas id="graphic" width="150" height="150" data-values="<?= $user_stat_to_string ?>">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                    </div>
                    <ul>
                        <li class="Guests"><i></i>Guests<span><?= $user_stat['per_month']['guest'] ?></span></li>
                        <li class="Agent"><i></i>Agent<span><?= $user_stat['per_month']['agent'] ?></span></li>
                        <li class="Homeowner"><i></i>Homeowner<span><?= $user_stat['per_month']['homeowner'] ?></span></li>
                        <li class="Manufacturer"><i></i>Manufacturer<span><?= $user_stat['per_month']['manufacturer'] ?></span></li>
                        <li class="Designers"><i></i>Designers<span><?= $user_stat['per_month']['designer'] ?></span></li>
                        <li class="Pros"><i></i>Pros<span><?= $user_stat['per_month']['professional'] ?></span></li>
                    </ul>
                    <a href="#" class="button">Load more information</a>
                </div>
                <!--         Comment       -->
                <div class="more-info">
                    <div class="graphics">
                        <p>Previous <br>visited page</p>
                        <aside class="chart vert">
                            <canvas id="graphic-1"  width="105" height="105" data-values="30, 100, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                            <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                        </ul>
                    </div>
                    <div class="graphics">
                        <p>Next <br> visited page</p>
                        <aside class="chart vert">
                            <canvas id="graphic-2"  width="105" height="105" data-values="30, 30, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                            <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                        </ul>
                    </div>
                    <div class="graphics-bottom">
                        <p>Avarage visit</p>
                        <div class="graphics">
                            <aside class="chart vert">
                                <canvas id="graphic-3"  width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>
                        <div class="graphics">

                            <aside class="chart vert">
                                <canvas id="graphic-4"  width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"> </i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>

                    </div>
                    <h5>WE DEVELOP STATISTIC DISPLAY AND CONNECT OT SOON</h5>
                </div>
                <hr class="sline">
                <div class="clearfix"></div>

<!--     Display item information           -->

                <?php if ($item): ?>
                    <div class="row">
                        <div class="sidebar-caption"><?= $item['title'] ?><br></div>
                        <p class="auth auth-agent">
                            <a href="<?= $item['user_link'] ?>"><?= $item['user'] ?></a>
                            <span>, <?= Yii::t('main', 'role_designer') ?></span>
                        </p>
                    </div>

                    <div class="row row-portfolio">
                        <div class="about_portfolio-images agent-select-images">
                            <?php foreach ($item['images'] as $image) { ?>
                                <a href="<?= $image ?>" data-lightbox="roadtrip" style=background-image:url(<?= $image ?>)></a>
                            <?php } ?>

                        </div>
                        <div class="clearfix"></div>
                        <div class="about_portfolio-caption">
                            <p class="about-date"><?= $item['date'] ?></p>
                            <div class="hashtag agent-hashtag-project">
                                <?php foreach ($item['tags'] as $tag) { ?>
                                    <?= $tag ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="portfolio-article">
                            <?= $item['description'] ?>
                        </div>
                        <div class="portfolio-article">
                            <?php foreach ($descriptions as $description) { ?>
                                <?= html_entity_decode($description['description']) ?>
                            <?php } ?>
                        </div>
                    </div>
                    <hr class="sline">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">Add your project description</h4>
                    <p class="upload-portfolio download-portfolio edit">Add<a href="#"></a></p>
                    <div id="item-editor" style="display: none">
                        <?= CKEditor::widget([
                            'name' => 'description',
                            'editorOptions' => [
                                'preset' => 'full',
                                'inline' => false,
                                'language' => Language::getCurrent()->url,
                            ]
                        ]);?>
                    <button class="button" data-id="<?= $item['id'] ?>">Save</button>
                    </div>

                    <hr class="sline">
                    <div class="clearfix"></div>
                <?php endif; ?>

<!--    End Display item information     -->
                <h4 class="promote promote-bottom">New project</h4>

                <div class="clearfix"></div>

                <?php if ($new_projects['projects']): ?>
                    <div id="agent-new-projects">
                    <?php foreach ($new_projects['projects'] as $project) { ?>
                        <?php $images = explode("|", $project['images']); ?>
                        <div class="profil-img">
                            <a href="<?= Yii::$app->urlManager->createUrl(['account/projects-agent/select', 'id' => $project['item_id']]) ?>"><img src="<?= Yii::getAlias('@portfolio/' . $images[0]) ?>" alt=""></a>
                        </div>
                    <?php } ?>
                    </div>
                <?php endif; ?>
                <div class="clearfix"></div>
                <div class="bread-crumbs">
                    <?= LinkPager::widget(['pagination' => $new_projects['pagination'],
                        'disabledPageCssClass' => false,
                        'nextPageLabel' => '',
                        'prevPageLabel' => '',
                        'options' => ['class' => 'hvr-radial-out agent-new-projects-pagination'],
                    ]); ?>
                </div>
                <hr class="sline">
                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">Product</h4>
                    <p class="upload-portfolio download-portfolio join" data-toggle="modal"
                       data-target=".agent-modal-product">Add<a href="#"></a></p>
                    <form class="forma-input">
                        <div class="input-edit">
                            <div class="input-container">
                                <input class="text-input floating-label" name="sample1" type="text">
                                <label for="sample1">Search</label>
                                <button type="submit" class="submit"></button>
                            </div>
                        </div>
                    </form>
                    <script type="text/javascript"
                            src="https%3A%2F%2Fcse.google.com%2Fcse/tools/onthefly?form=searchbox_demo&lang="></script>
                    <div class="clearfix"></div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-31.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b>
                                    <br>save 30%</p>
                            </div>
                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span>
                                <br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">
                            <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>
                    <div class="catalog-item">
                        <div class="catalog-img-box">
                            <a href="">
                                <img src="/images/mainPage-column-32.jpg" alt="">
                            </a>
                            <div class="limited-offer">
                                <p><b>Limited offer</b>
                                    <br>save 30%</p>
                            </div>

                            <div class="like-calc item-price">
                                <i class="icon-label49"></i>
                                <span>1800$</span>
                                <br>
                                <p class="last-price">3200$</p>
                            </div>
                        </div>
                        <div class="catalog-caption">

                            <p>Lorem ipsum Lorem ipsum Lorem ipsum
                                <br> Lorem ipsum Lorem ipsum Lorem ipsum</p>
                        </div>
                    </div>


                </div>
                <div class="bread-crumbs">
                    <ul class="hvr-radial-out">
                        <li>
                            <a href="#"></a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li>
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>
                <hr class="sline">
                <div class="clearfix"></div>
                <h4 class="promote promote-bottom">My project</h4>

                <div class="clearfix"></div>
                <div class="profil-img">
                    <img src="/images/img-portfolio.png" alt="">
                </div>
                <div class="profil-img">
                    <img src="/images/img-portfolio.png" alt="">
                </div>
                <div class="profil-img">
                    <img src="/images/img-portfolio-3.png" alt="">
                </div>

                <div class="clearfix"></div>
                <div class="bread-crumbs">
                    <ul class="hvr-radial-out">
                        <li>
                            <a href="#"></a>
                        </li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">6</a></li>
                        <li><a href="#">7</a></li>
                        <li>
                            <a href="#"></a>
                        </li>
                    </ul>
                </div>

                <div class="bottom-baner">
                    <img src="/images/mainPage-banner-bottom.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    <!--END CENTER-->
<?php
$this->registerJsFile('js/jquery.magnific-popup.min.js', ['depends' => 'frontend\assets\AppAsset']);
$this->registerJsFile('agent_js/action_agent.js', ['depends' => 'frontend\assets\AppAsset']);
$script_honours = <<< JS

    $('#item-editor button').click(function(){
       var description = $('[name=description]').val();
       var id = $(this).data('id');
       if(description.length > 20){
            var request = $.ajax({
	        	data : {id : id, description : description},
	        	url: '/account/projects-agent/add-description',
	        	type: 'post',
	        	dataType: 'html'
	        });

	        request.done(function(response){
	            $('[name=description]').val('');
                $('#item-editor').slideToggle();
	        });
       }
    });

    $('body').delegate( ".agent-new-projects-pagination a", "click", function(e) {
          e.preventDefault();
          var link = $(this).attr('href');
          $('#agent-new-projects').load(link+' #agent-new-projects > *');
          $('.agent-new-projects-pagination').load(link+' .agent-new-projects-pagination > *');
    });

    $('.about_portfolio-images').magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true
		},
	});

	$('.upload-portfolio.edit a').click(function(e){
	    e.preventDefault();
	    $('#item-editor').slideToggle();
	});

   var pieColors = [
   					'rgb(51, 102, 102)',
   					'rgb(40, 124, 122)',
   					'rgb(51, 153, 153)',
   					'rgb(84, 175, 172)',
   					'rgb(153, 204, 204)',
   					'rgb(203, 228, 228)'
   				];
   function getTotal( arr ){
       var j,
           myTotal = 0;

       for( j = 0; j < arr.length; j++) {
           myTotal += ( typeof arr[j] === 'number' ) ? arr[j] : 0;
       }

       return myTotal;
   }

   function drawPieChart( canvasId ) {
       var i,
           canvas = document.getElementById( canvasId ),
           pieData = canvas.dataset.values.split(',').map( function(x){ return parseInt( x, 10 )}),
           halfWidth = canvas.width * .5,
           halfHeight = canvas.height * .5,
           ctx = canvas.getContext( '2d' ),
           lastend = 0,
           myTotal = getTotal(pieData);

       ctx.clearRect( 0, 0, canvas.width, canvas.height );

       for( i = 0; i < pieData.length; i++) {
           ctx.fillStyle = pieColors[i];
           ctx.beginPath();
           ctx.moveTo( halfWidth, halfHeight );
           ctx.arc( halfWidth, halfHeight, halfHeight, lastend, lastend + ( Math.PI * 2 * ( pieData[i] / myTotal )), false );
           ctx.lineTo( halfWidth, halfHeight );
           ctx.fill();
           lastend += Math.PI * 2 * ( pieData[i] / myTotal );
       }
   }

   drawPieChart('graphic');
   drawPieChart('graphic-1');
   drawPieChart('graphic-2');
   drawPieChart('graphic-3');
   drawPieChart('graphic-4');

   $(document).ready(function(){
   	$('.analytics .button').click(function(){
   		$('.more-info').slideToggle();
   	});
   });
JS;
$this->registerJs($script_honours, yii\web\View::POS_READY);





