<?php
use yii\widgets\LinkPager;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'account_profile');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CENTER-->
    <div class="col-lg-7 central-content profile-content profile-agent profile-project centralScroll">
        <div class="row">
            <div class="content">
                <div class="analytics">
                    <h4 class="promote promote-bottom">Analytics</h4>
                    <p class="last-days">Last 30 days</p>
                    <div class="clearfix"></div>
                    <div class="graphic">
                        <p>People, <br>who have visited profile</p>
                        <aside class="chart vert">
                            <canvas id="graphic" width="150" height="150" data-values="<?= $user_stat_to_string ?>">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                    </div>
                    <ul>
                        <li class="Guests"><i></i>Guests<span><?= $user_stat['per_month']['guest'] ?></span></li>
                        <li class="Agent"><i></i>Agent<span><?= $user_stat['per_month']['agent'] ?></span></li>
                        <li class="Homeowner"><i></i>Homeowner<span><?= $user_stat['per_month']['homeowner'] ?></span>
                        </li>
                        <li class="Manufacturer">
                            <i></i>Manufacturer<span><?= $user_stat['per_month']['manufacturer'] ?></span></li>
                        <li class="Designers"><i></i>Designers<span><?= $user_stat['per_month']['designer'] ?></span>
                        </li>
                        <li class="Pros"><i></i>Pros<span><?= $user_stat['per_month']['professional'] ?></span></li>
                    </ul>
                    <a href="#" class="button">Load more information</a>
                </div>
                <!--         Comment       -->
                <div class="more-info">
                    <div class="graphics">
                        <p>Previous <br>visited page</p>
                        <aside class="chart vert">
                            <canvas id="graphic-1" width="105" height="105" data-values="30, 100, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                            <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                        </ul>
                    </div>
                    <div class="graphics">
                        <p>Next <br> visited page</p>
                        <aside class="chart vert">
                            <canvas id="graphic-2" width="105" height="105" data-values="30, 30, 20, 60">
                                This browser does not support HTML5 Canvas.
                            </canvas>
                        </aside>
                        <ul>
                            <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                            <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                            <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                            <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                        </ul>
                    </div>
                    <div class="graphics-bottom">
                        <p>Avarage visit</p>
                        <div class="graphics">
                            <aside class="chart vert">
                                <canvas id="graphic-3" width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"></i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>
                        <div class="graphics">

                            <aside class="chart vert">
                                <canvas id="graphic-4" width="105" height="105" data-values="30, 30, 20, 60">
                                    This browser does not support HTML5 Canvas.
                                </canvas>
                            </aside>
                            <ul>
                                <li class="project"><i style="background-color: #cbe4e4;"></i>Project</li>
                                <li class="manufacturer"><i style="background-color: #cbe4e4;"></i>Manufacturers</li>
                                <li class="pros"><i style="background-color: #cbe4e4;"> </i>Pros</li>
                                <li class="other"><i style="background-color: #cbe4e4;"></i>Other</li>
                            </ul>
                        </div>

                    </div>
                    <h5>WE DEVELOP STATISTIC DISPLAY AND CONNECT OT SOON</h5>
                </div>
                <div class="clearfix"></div>
                <hr class="sline">
                <!--    All projects            -->
                <div class="project fd">
                    <h4 class="promote promote-bottom">The foreground project</h4>
                    <div class="src-select">

                        <form class="forma-input">
                            <div class="search">
                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" onchange="changeInput(this)" value="<?= $_GET['q'] ?>" name="q"
                                               type="text">
                                        <label for="q">Search</label>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="selects">
                            <p>View by</p>
                            <i class="icon-view"></i>
                            <?php $view_arr = [3, 6, 9, 12]; ?>
                            <select class="view" id="all-projects-view">
                                <?php foreach ($view_arr as $view): ?>
                                    <?php if ($view == $view_all): ?>
                                        <option selected><?= $view ?></option>
                                    <?php else: ?>
                                        <option><?= $view ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="hashtag">
                        <a href="#">#lorem</a>
                        <a href="#"> #ipsum</a>
                        <a href="#"> #dolor</a>
                        <a href="#"> #sit</a>
                    </div>
                    <?php if ($all_projects['projects']): ?>
                        <div id="agent-all-projects">
                            <?php foreach ($all_projects['projects'] as $project): ?>
                                <?php $images = explode("|", $project['images']); ?>
                                <div class="portfolio-item white">
                                    <a href="<?= Yii::$app->urlManager->createUrl(['account/projects-agent/select', 'id' => $project['item_id']]) ?>"><img
                                            src="<?= Yii::getAlias('@portfolio/' . $images[0]) ?>" alt=""></a>
                                    <p><?= $project['title'] ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="bread-crumbs">
                            <?= LinkPager::widget(['pagination' => $all_projects['pagination'],
                                'disabledPageCssClass' => false,
                                'nextPageLabel' => '',
                                'prevPageLabel' => '',
                                'options' => ['class' => 'hvr-radial-out agent-all-projects-pagination'],
                            ]); ?>
                        </div>

                    <?php endif; ?>
                </div>
                <!--   End All projects            -->


                <!--    On Confirmation Projects  -->
                <?php if ($confirmation_projects['projects']): ?>
                    <hr class="sline">
                    <div class="project">
                        <h4 class="promote promote-bottom">The projects on confirmation</h4>
                        <div class="selects">
                            <p>View by</p>
                            <i class="icon-view"></i>
                            <select class="view" id="confirmation-projects-view">
                                <?php foreach ($view_arr as $view): ?>
                                    <?php if ($view == $confirmation_view): ?>
                                        <option selected><?= $view ?></option>
                                    <?php else: ?>
                                        <option><?= $view ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>

                        <div class="hashtag">
                            <a href="#">#lorem</a>
                            <a href="#"> #ipsum</a>
                            <a href="#"> #dolor</a>
                            <a href="#"> #sit</a>
                        </div>

                        <div id="agent-conf-projects">
                            <?php foreach ($confirmation_projects['projects'] as $project): ?>
                                <?php $images = explode("|", $project['images']); ?>
                                <div class="portfolio-item white">
                                    <img src="<?= Yii::getAlias('@portfolio/' . $images[0]) ?>" alt="">
                                    <a href="#" class="conferm">Confirm</a>
                                    <p><?= $project['title'] ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <div class="bread-crumbs">
                        <?= LinkPager::widget(['pagination' => $confirmation_projects['pagination'],
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options' => ['class' => 'hvr-radial-out agent-conf-projects-pagination'],
                        ]); ?>
                    </div>

                <?php endif; ?>

<!--    Approved Projects  -->

                <?php if ($approved_projects['projects']): ?>

                    <hr class="sline">
                    <div class="project">
                        <h4 class="promote promote-bottom">The approved projects</h4>
                        <div class="selects">
                            <p>View by</p>
                            <i class="icon-view"></i>
                            <select class="view" id="approved-projects-view">
                                <?php foreach ($view_arr as $view): ?>
                                    <?php if ($view == $approved_view): ?>
                                        <option selected><?= $view ?></option>
                                    <?php else: ?>
                                        <option><?= $view ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="hashtag">
                            <a href="#">#lorem</a>
                            <a href="#"> #ipsum</a>
                            <a href="#"> #dolor</a>
                            <a href="#"> #sit</a>
                        </div>

                        <div id="agent-approved-projects">
                            <?php foreach ($approved_projects['projects'] as $project): ?>
                                <?php $images = explode("|", $project['images']); ?>
                                <div class="portfolio-item white">
                                    <a href="<?= Yii::$app->urlManager->createUrl(['account/projects-agent/select', 'id' => $project['item_id']]) ?>"><img src="<?= Yii::getAlias('@portfolio/' . $images[0]) ?>" alt=""></a>
                                    <p><?= $project['title'] ?></p>
                                </div>
                            <?php endforeach; ?>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                    <div class="bread-crumbs">
                        <?= LinkPager::widget(['pagination' => $approved_projects['pagination'],
                            'disabledPageCssClass' => false,
                            'nextPageLabel' => '',
                            'prevPageLabel' => '',
                            'options' => ['class' => 'hvr-radial-out agent-approved-projects-pagination'],
                        ]); ?>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
    <!--END CENTER-->
<?php
$this->registerJsFile('agent_js/action_agent.js', ['depends' => 'frontend\assets\AppAsset']);
$script_honours = <<< JS

    $('#all-projects-view').change(function(){
        var all_view = $(this).val();
        var request = $.ajax({
			data : {all_view : all_view},
			url: '/account/projects-agent/viewed',
			type: 'post',
			dataType: 'html'
		});

		request.done(function(){
		    location.reload();
		});

    });

    $('#confirmation-projects-view').change(function(){
        var all_view = $(this).val();
        var request = $.ajax({
			data : {confirmation_view: all_view},
			url: '/account/projects-agent/viewed',
			type: 'post',
			dataType: 'html'
		});

		request.done(function(){
		    location.reload();
		});

    });

    $('#approved-projects-view').change(function(){
        var all_view = $(this).val();
        var request = $.ajax({
			data : {approved_view: all_view},
			url: '/account/projects-agent/viewed',
			type: 'post',
			dataType: 'html'
		});

		request.done(function(){
		    location.reload();
		});

    });

    $('body').delegate( ".agent-all-projects-pagination a", "click", function(e) {
          e.preventDefault();
          var link = $(this).attr('href');
          $('#agent-all-projects').load(link+' #agent-all-projects > *');
          $('.agent-all-projects-pagination').load(link+' .agent-all-projects-pagination > *');
    });

    $('body').delegate( ".agent-conf-projects-pagination a", "click", function(e) {
          e.preventDefault();
          var link = $(this).attr('href');
          $('#agent-conf-projects').load(link+' #agent-conf-projects > *');
          $('.agent-conf-projects-pagination').load(link+' .agent-conf-projects-pagination > *');
    });

    $('body').delegate( ".agent-approved-projects-pagination a", "click", function(e) {
          e.preventDefault();
          var link = $(this).attr('href');
          $('#agent-approved-projects').load(link+' #agent-approved-projects > *');
          $('.agent-approved-projects-pagination').load(link+' .agent-approved-projects-pagination > *');
    });



    var pieColors = [
		'rgb(51, 102, 102)',
		'rgb(40, 124, 122)',
		'rgb(51, 153, 153)',
		'rgb(84, 175, 172)',
		'rgb(153, 204, 204)',
		'rgb(203, 228, 228)'
	];
    function getTotal( arr ){
    var j,
        myTotal = 0;

    for( j = 0; j < arr.length; j++) {
        myTotal += ( typeof arr[j] === 'number' ) ? arr[j] : 0;
    }

    return myTotal;
}

    function drawPieChart( canvasId ) {
        var i,
            canvas = document.getElementById( canvasId ),
            pieData = canvas.dataset.values.split(',').map( function(x){ return parseInt( x, 10 )}),
            halfWidth = canvas.width * .5,
            halfHeight = canvas.height * .5,
            ctx = canvas.getContext( '2d' ),
            lastend = 0,
            myTotal = getTotal(pieData);

        ctx.clearRect( 0, 0, canvas.width, canvas.height );

        for( i = 0; i < pieData.length; i++) {
            ctx.fillStyle = pieColors[i];
            ctx.beginPath();
            ctx.moveTo( halfWidth, halfHeight );
            ctx.arc( halfWidth, halfHeight, halfHeight, lastend, lastend + ( Math.PI * 2 * ( pieData[i] / myTotal )), false );
            ctx.lineTo( halfWidth, halfHeight );
            ctx.fill();
            lastend += Math.PI * 2 * ( pieData[i] / myTotal );
        }
    }

    drawPieChart('graphic');
    drawPieChart('graphic-1');
    drawPieChart('graphic-2');
    drawPieChart('graphic-3');
    drawPieChart('graphic-4');

	$(document).ready(function(){
		$('.analytics .button').click(function(){
			$('.more-info').slideToggle();
		});
	});
JS;
$this->registerJs($script_honours, yii\web\View::POS_READY);





