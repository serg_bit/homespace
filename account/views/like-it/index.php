<?php
use yii\widgets\LinkPager;
use frontend\account\models\Application;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Language;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'order');
$this->params['breadcrumbs'][] = $this->title;
?>
<!--START CENTER-->

<div class="col-lg-7 central-content page-catalog page-in-catalog in-order comp-ord script centralScroll">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 catalog-column">
                <div class="folder">
                    <?php if ($folder) {
                        foreach ($folder as $value) : ?>
                            <div class="name-folder" id_folder="<?= $value['root']; ?>">
                                <a href="<?= Url::to(['/account/like-it/root/', 'id' => $value['root']]); ?>"
                                   class="folder_link">
                                    <div class="folder-img ">
                                        <img class="folder-empty" src="/images/folder.png" alt="">
                                    </div>
                                </a>
                                <div class="folder-bottom change-folder forma-input">
                                    <a href="<?= Url::to(['/account/like-it/root/', 'id' => $value['root']]); ?>"
                                       class="folder_link">
                                        <p><?= ($lang == 'ru') ? $value['name_ru'] : $value['name_en']; ?></p>
                                    </a>
                                    <div class="input-container" style="display: none">
                                        <input class="text-input floating-label title" type="text" name="re-name"
                                               value="<?= ($lang == 'ru') ? $value['name_ru'] : $value['name_en']; ?>"/>
                                        <label for="re-name"></label>
                                    </div>
                                </div>
                            </div>

                        <?php endforeach; ?>
                    <?php } else {
                    } ?>
                </div>
            </div>
        </div>
        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
        </div>
    </div>
</div>
<!--END CENTER-->

<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']); ?>


