<?php
/**
 * Created by PhpStorm.
 * User: Alscon13
 * Date: 12.05.2016
 * Time: 15:03
 */

use yii\widgets\LinkPager;
use frontend\account\models\Application;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Language;
use yii\helpers\Url;

$this->title = Yii::t('titles', 'account') . Yii::t('titles', 'order');
$this->params['breadcrumbs'][] = $this->title;
?>


<!--START CENTER-->
<div class="col-lg-7 central-content page-catalog page-in-catalog in-order comp-ord script centralScroll">
    <div class="content">
        <div class="row">
            <div class="col-lg-12 catalog-column">

                <div class="clearfix"></div>
                <?php if($_GET['id'] < 3): ?>
                <div id="addNewFolder" class="forma-input"  style="display: none" >
                    <div class="input-container">
                        <input id="folder"  class="text-input floating-label" type="text" name="sample" value=""/>
                        <label for="sample"><?= Yii::t('account', 'name_folder') ?></label>
                        <input type="hidden" name="root" value="<?=$root_id?>">
                    </div>
                    <input type="submit" id="create_folder" value="<?= Yii::t('account', 'button_save') ?>">
                </div>
                <p id="showCreate" class="upload-portfolio download-portfolio"><?= Yii::t('account', 'order_add') ?><a href="#"></a></p>
                <div class="clearfix"></div>
                <?php endif; ?>
                <div class="folder">
                    <?php if ($folders) : ?>
                        <?php foreach ($folders as $folder) : ?>
                            <div class="name-folder" data-id="<?= $folder['root']; ?>">

                                <a href="<?= Url::to(['/account/like-it/root/', 'id' => $folder['id']]); ?>"
                                   class="folder_link">
                                    <div class="folder-img ">
                                        <img class="folder-empty" src="/images/folder.png" alt="">
                                    </div>
                                </a>
                                <div class="folder-bottom change-folder forma-input">
                                    <p><?= ($lang == 'ru') ? $folder['name_ru'] : $folder['name_en']; ?></p>
                                    <div class="input-container" style="display: none">
                                        <input class="text-input floating-label title" type="text" name="re-name"
                                               value="<?= ($lang == 'ru') ? $folder['name_ru'] : $folder['name_en']; ?>"/>
                                        <label for="re-name"></label>
                                    </div>
                                    <i class="icon-show8"></i>
                                    <ul>
                                        <li>
                                            <a class="re_folder" url="" href="#" data-attr-id="<?= $folder['id']?>">
                                                <i class="icon-edit45"></i>Rename</a>
                                        </li>

                                        <li>
                                            <a class="del_folder" url="" href="#" data-attr-id="<?= $folder['id']?>">
                                                <i class="icon-rubbish"></i>Delete</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if($items): ?>
                 <div class="col-lg-12 catalog-column">
                     <div class="catalog-box">
                     <?php foreach ($items as $item): ?>
                         <div class="catalog-item">
                             <div class="catalog-img-box">
                                 <?php
                                 $m_img = explode('|', $item['images']);
                                 ?>
                                 <a href="<?= Url::to(['/about/item', 'id' => $item['item_id']]); ?>">
                                     <img src="<?= Yii::getAlias('@portfolio/' . $m_img[0]) ?>" alt=""
                                          id_prod="<?= $item['item_id'] ?>">
                                 </a>
                                 <div class="like-calc product-likes"><i class="icon-heart2971"></i><span
                                         likes_id="<?= $item['item_id'] ?>"
                                         class="prod-likes-count"><?= $item['likes'] ?></span>
                                 </div>
                             </div>
                             <div class="catalog-caption">
                                 <div class="social-cont">
                                     <a data-id="<?= $item['item_id'] ?>" class="main-like icon-heart297"></a>
                                 </div>
                                 <p><?= $item['title'] ?></p>
                             </div>
                         </div>
                     <?php endforeach; ?>
                     </div>
                 </div>
            <?php endif; ?>
        </div>
<!--        <div class="row">-->
<!--            --><?//= frontend\widgets\Banner::widget(['position' => 'bottom']); ?>
<!--        </div>-->
    </div>
</div>

<!--END CENTER-->

<?php $this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php //$this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php //$this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('https://code.jquery.com/ui/1.11.4/jquery-ui.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('scripts/like_it.js', ['depends' => 'frontend\assets\AppAsset']); ?>
