<?php
use yii\widgets\LinkPager;

$this->title = 'My Designers & Architects';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-7  profile-content profile-agent-comps agents centralScroll archdes">
    <div class="row">
        <div class="content">
            <div class="portfolio-top profile-conten-me ">
                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">My Designers & Architects</h4>
                    <p class="upload-portfolio download-portfolio join">Add
                        <a href="#"></a>
                    </p>
                    <div class="clearfix"></div>
                    <?php if ($users_data['users']): ?>
                        <form class="forma-input">
                            <div class="search">
                                <div class="input-edit">
                                    <div class="input-container">
                                        <input class="text-input floating-label" name="sample1" type="text">
                                        <label for="sample1">Search</label>
                                    </div>
                                </div>
                            </div>
                            <div class="selects">
                                <select name="#">
                                    <option value="#">Search by</option>
                                    <option value="#">Search by</option>
                                    <option value="#">Search by</option>
                                    <option value="#">Search by</option>
                                </select>
                            </div>
                        </form>

                        <div class="companys">

                            <div id="my-designers-architects">
                                <?php foreach ($users_data['users'] as $user): ?>
                                <div class="company agents" style="background-image: url(<?= Yii::getAlias('@background/' . $user['background']) ?>);">
                                    <div class="author"><img src="<?= Yii::getAlias('@avatar/' . $user['logo'] ) ?>" alt="#">
                                        <p class="name"><?= $user['company'] ?></p>
                                    </div>
                                    <div class="author-description">
                                        <img class="company-img" src="<?= Yii::getAlias('@avatar/' . $user['avatar'] ) ?>" alt="#">
                                        <div class="name-company">
                                            <h5><?= $user['first_name'] ?> <span><?= $user['last_name'] ?></span></h5>
                                            <p><?= $user['country'] ?> <span><?= $user['city'] ?></span></p>
                                            <a href="#" class="messages"><i class="icon-black218"></i>Message</a>
                                        </div>
                                    </div>
                                    <p class="descriptions">
                                        <?= substr($user['message'], 0, 80) ?>...
                                    </p>

                                    <ul class="description">
                                        <li>Projects: <span><?= $user['portfolio'] ?></span></li>
<!--                                        <li class="new">New: <span>6</span></li>-->
                                        <li>Confirmed supplements: <span><?= $user['confirmed'] ?></span></li>
                                        <li><span class="dolor">$</span>: 8.01.15 - 10.03.15</li>
                                    </ul>
                                    <ul class="buttons">
                                        <li><a href="<?= Yii::$app->urlManager->createUrl(['about/user', 'id' => $user['id']]) ?>" class="button">View</a></li>
                                        <li><a href="#" class="button">Follow</a></li>
                                        <li><a href="#" data-id="<?= $user['id'] ?>" class="button">Be a sponsor</a></li>
                                    </ul>
                                </div>
                                <?php endforeach; ?>
                            </div>

                            <div class="clearfix"></div>
                            <div class="bread-crumbs">
                                <?= LinkPager::widget(['pagination' => $users_data['pagination'],
                                    'disabledPageCssClass' => false,
                                    'nextPageLabel' => '',
                                    'prevPageLabel' => '',
                                    'options' => ['class' => 'hvr-radial-out my-designers-architects-pagination'],
                                ]); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <!--END CENTER-->
</div>