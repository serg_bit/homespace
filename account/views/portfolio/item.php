<?php

use frontend\widgets\Banner;



$this->title = Yii::t('titles', 'account').' - '.$item['title'];
$this->params['breadcrumbs'][] = $this->title;
?>

			
<!--START CENTER-->			
            <div class="col-lg-7 central-content about-content centralScroll">
                <div class="content">
                <?php if($item){ ?>
                    <div class="row">
                        <div class="sidebar-caption"><?= $item['title'] ?><br></div>
                        <p class="upload-portfolio download-portfolio edit"><?= Yii::t('account', 'edit') ?><a href="<?= $edit_url ?>"></a></p>
                    </div>
                    <div class="row row-portfolio">
                        <div class="about_portfolio-images">
                            <?php foreach ($item['images'] as $image) { ?>
                                <a href="<?= $image ?>"><img src="<?= $image ?>" alt=""></a>
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="about_portfolio-caption">
                            <p class="about-date"><?= $item['date'] ?></p>
                            <div class="hashtag">
                                <?php foreach ($item['tags'] as $tag) { ?>
                                    <?= $tag ?>
                                <?php } ?> 
                            </div>
                        </div>
                        <div class="portfolio-article">
                            <?= $item['description'] ?>
                        </div>
                    </div>
                    <div class="row">
                        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
                    </div>
                <?php }else{ ?>
                    <div class="row">
                        <h2><?= Yii::t('account', 'not_found') ?></h2>
                        <br>
                        <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
                    </div>
                <?php } ?>
                </div>
            </div>
            <!--END CENTER-->			
<?php
$this->registerJsFile('js/jquery.magnific-popup.min.js', ['depends'=>'frontend\assets\AppAsset']);
$script = <<< JS
    $('.about_portfolio-images').magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true
		},
	});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>



