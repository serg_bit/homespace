<?php
use frontend\widgets\Banner;
use kartik\social\FacebookPlugin;



$this->title = Yii::t('titles', 'account').' - '. $group['title'];
$this->params['breadcrumbs'][] = $this->title;
?>

<!--START CENTRAL AREA-->
            <div class="col-lg-7 central-content this-group centralScroll">

                <div class="content">

                        <?php if ($group) :?>

                            <div class="row">

                                <h4 class="promote"><?=$group['title']?></h4>

                                <img src="<?='/images/'.$group['images'] ?> "class="group-logo" alt="">

                                <p><?=$group['text']?></p>

                                <div class ="like-ico">

                                    <i class="icon-user168" title="<?=$group['followers']?>"><?=$group['followers_count']?></i>

                                    <i class="icon-eye110"><?=$group['views']?></i>

                                    <a href="#"><i class="icon-heart2971 like-button"  id="<?=$group['id']?>"><?=$likes?></i></a>

                                    <i class="facebook-share"><div class="fb-like" data-href="http://hspace.alscon-clients.com/account/join-group/group?<?=$group['id']?>" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div></i>

                                    <i class="twitter-share"><a href="https://twitter.com/share" data-url="http://hspace.alscon-clients.com/account/join-group/group?<?=$group['id']?>" class="twitter-share-button"{count}>Tweet</a></i>

                                </div>

                                <p><a class="button" href="?<?=$group['id']?>"><?= Yii::t('account', 'subscribe') ?> &raquo;</a></p>


                            </div>

                        <?php else : ?>

                                <div class="row">

                                        <h3>Ничего не найдено</h3>
                                                
                                </div>

                        <?php endif; ?>

                        <div class="row">


                                <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>


                        </div>

                </div>

            </div>
<!--END CENTRAL AREA-->
 

<?php
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/owl.carousel.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/action.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/fb_likes.js', ['depends'=>'frontend\assets\AppAsset']);
?>
