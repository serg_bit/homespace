<?php
use frontend\account\models\Application;
use yii\helpers\Url;
use frontend\account\models\AppRecipients;

$this->title = Yii::t('titles', 'account').Yii::t('titles','app_send');
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--START CENTER-->
    <div class="col-lg-7  central-content centralScroll">
        <div class="content page-catalog page-in-catalog in-order applications">
            <div class="row">
                <h5><?= Yii::t('account', 'application_send') ?></h5>
                <?php if ($models) {
                    foreach ($models as $app) { ?>
                        <h4 class="promote promote-bottom"><?= $app['title_app']; ?> </h4>
                        <p class="upload-portfolio download-portfolio del" url=""><?= Yii::t('account', 'button_delete') ?><a
                                href="<?= Url::to(['application/delete', 'id' => $app['id_app']]); ?>"></a></p>
                        <div class="clearfix"></div>
                        <div class="send-to">
                            <span><?= Yii::t('account', 'application_to') ?>:</span>
                            <ul class="send">
                                <?php $recipients = Application::findRecipient(['app_id' => $app['id_app']]);
                                foreach ($recipients as $recipient) {
                                    ?>
                                    <li value="<?= $recipient; ?>"><a href="#"><img src="/images/team1.png" alt=""></a>
                                    </li>
                                <?php } ?>
                            </ul>
                            <p><?= $app['description_app']; ?></p>
                        </div>
                        <div class="send-to-img">
                            <div class="user-ava">
                                <img src="<?= Yii::getAlias('@avatar/' . $user->avatar); ?>" alt="">
                            </div>
                            <p class="name"><?= $user->first_name . ' ' . $user->last_name ?>
                                <span><?= $user->specialization; ?></span>
                            </p>
                        </div>
                        <a href="#" class="title atachFile"><i class="icon-silhouette107"></i><?= Yii::t('account', 'application_atach') ?></a>
                        <div class="atached" style="display: none">
                            <?php if ($app['id_folder']) {
                                $folder = Application::findFolder(['id_folder' => $app['id_folder'] ]); ?>
                                <div class="folder">
                                    <div class="clearfix"></div>
                                    <div class="name-folder ">
                                        <div class="folder-img change_f">
                                            <?php foreach($folder['0']['image'] as $image) : ?>
                                                <img src="/images/<?= $image['miniature_img']; ?>" alt="">
                                            <?php endforeach; ?>
                                            <div class="folder-bottom">
                                                <p><?= $folder[0]['folder']['title'] ?></p>
                                                <ul>
                                                    <li><a class="re_folder" url="<?= Url::to(['order/rename']); ?>"
                                                           href="#"><i class="icon-edit45"></i>Rename</a></li>
                                                    <li><a class="del_folder" url="<?= Url::to(['order/delete']); ?>"
                                                           href="#"><i class="icon-rubbish"></i>Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="clearfix"></div>
                            <?php if ($app['url_app']) {
                                $filess = explode('|', $app['url_app']); ?>
                                <ul class="load-files">
                                    <?php foreach ($filess as $file) { ?>
                                        <li><a href="<?= Yii::getAlias('@application/new_app/' . $file); ?>"
                                               download><?= substr($file, 4); ?></a><? ?></li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </div>
                        <div class="reviews aplication">

                                <h4 class="promote"><?= Yii::t('account', 'application_review') ?></h4>
                            <?php $coment = Application::coment(['app_id' => $app['id_app']]); ?>
                                <?php foreach($coment as $one_coment): ?>
                                    <?php if(isset($one_coment['message']) || isset($one_coment['file'])){ ?>
                                    <div class="reviews-comment">
                                        <div class="reviews-photo">
                                            <img src="<?= Yii::getAlias('@avatar/' . $one_coment['avatar']); ?>" alt="">
                                            <p class="name"><?= $one_coment['first_name'] . ' ' . $one_coment['last_name']; ?><span><?= $one_coment['specialization']?></span></p>
                                            <div class="send-chat-message">
                                                <a href="#"><i class="icon-black218"></i></a>
                                            </div>
                                        </div>
                                        <div class="my-content-messages">
                                            <p><?= $one_coment['message'] ?></p>
                                            <?php if ($one_coment['files']) { ?>
                                                <?php $files_send = explode('|', $one_coment['files']); ?>
                                                <ul class='uploading-files'>
                                                    <?php foreach ($files_send as $send) : ?>
                                                        <li>
                                                            <a href="<?= Yii::getAlias('@application/send_app/' . $send); ?>" download><?= substr($send, 4); ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        <p class="data"><?= $one_coment['date']; ?></p>
                                    </div>
                                        <?php }?>
                                <?php endforeach; ?>

                        </div>
                        <hr class="sline">
                    <?php }
                } else { ?>
                    <div class="empty-app">
                        <br>
                        <a href="/account/order/index"><?= Yii::t('account', 'application_create') ?></a>
                    </div>
                <?php } ?>
            </div>

        </div>

        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
        </div>
    </div>
    <!--END CENTER-->
<?php //$this->registerJsFile('js/common.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>
<?php $this->registerJsFile('js/new-js.js', ['depends' => 'frontend\assets\AppAsset']); ?>