<?php

use frontend\account\models\Application;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


$this->title = Yii::t('titles', 'account').Yii::t('titles','app_inbox');
$this->params['breadcrumbs'][] = $this->title;

?>
    <!--START CENTER-->
    <div class="col-lg-7 central-content centralScroll">
        <div class="content page-catalog page-in-catalog in-order applications inbox">
            <div class="row">
                <h5><?= Yii::t('account', 'inbox') ?></h5>
                <?php if ($app) { ?>
                    <?php foreach ($app as $model) : ?>
                        <div class="<?php if ($model['status'] == 0){ echo'new-messages'; } ?>">

                            <p class="upload-portfolio download-portfolio del" url=""><?= Yii::t('account', 'button_delete') ?>
                                <a href="<?= Url::to(['application/remove', 'id' => $model['id']]); ?>"></a>
                            </p>
                            <div class="inbox_app">
                                <h4 class="promote promote-bottom"><?= $model['title_app']; ?></h4>
                                <div style="display: none" class="status_app"><?= $model['status']; ?></div>
                                <div class="clearfix"></div>
                                <div class="send-to" id_app="<?= $model['id_app'] ?>">
                                    <p><?= $model['description_app']; ?></p>
                                </div>
                                <div class="send-to-img">
                                    <div class="user-ava">
                                        <img src="<?= Yii::getAlias('@avatar/' . $model['avatar']); ?>" alt="">
                                    </div>
                                    <p class="name"><?= $model['first_name'] . ' ' . $model['last_name']; ?>
                                    <span><?= $model['specialization']; ?></span></p>
                                </div>

                            </div>
                            <a href="#" class="title atachFile"><i class="icon-silhouette107"></i><?= Yii::t('account', 'application_atach') ?></a>
                            <div class="atached" style="display: none">
                                <?php if ($model['id_folder']) { ?>
                                    <?php $folder = Application::findFolder(['id_folder' => $app['id_folder'] ]); ?>
                                    <div class="folder">
                                        <div class="clearfix"></div>
                                        <div class="name-folder ">
                                            <div class="folder-img">
                                                <?php foreach($folder['0']['image'] as $image) : ?>
                                                    <img src="/images/<?= $image['miniature_img']; ?>" alt="">
                                                <?php endforeach; ?>
                                                <div class="folder-bottom">
                                                    <p><?= $folder[0]['title'] ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="clearfix"></div>
                                <?php if ($model['url_app']) { ?>
                                    <?php $filess = explode('|', $model['url_app']); ?>
                                    <ul class="load-files">
                                        <?php foreach ($filess as $file) : ?>
                                            <li>
                                                <a href="<?= Yii::getAlias('@application/new_app/' . $file); ?>" download><?= substr($file, 4); ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php } ?>
                            </div>
                            <?php if ($model['message'] || $model['files']) { ?>

                            <div class="reviews aplication">
                                <h4 class="promote"><?= Yii::t('account', 'application_review') ?></h4>

                                    <div class="reviews-comment">
                                        <div class="reviews-photo">
                                            <img src="<?= Yii::getAlias('@avatar/' . $curent_user['avatar']); ?>" alt="">
                                            <p class="name"><?= $curent_user['first_name'] . ' ' . $curent_user['last_name']; ?>
                                            <span><?= $curent_user['specialization']; ?></span></p>
                                        </div>
                                        <div class="my-content-messages">
                                            <p><?= $model['message']; ?></p>
                                            <?php if ($model['files']) { ?>
                                                <?php $files_send = explode('|', $model['files']); ?>
                                                <ul class='uploading-files'>
                                                    <?php foreach ($files_send as $send) : ?>
                                                        <li>
                                                            <a href="<?= Yii::getAlias('@application/send_app/' . $send); ?>" download><?= substr($send, 4); ?></a>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        <p class="data"><?= $model['date']; ?></p>
                                    </div>

                            </div>
                            <?php } else { ?>

                            <div class="app_coment" send_id_app="<?= $model['id_app'] ?>" style="display: none">
                                <form action="" enctype="multipart/form-data" class="answer-form">
                                    <textarea name="#" class="app_review" required placeholder="<?= Yii::t('account', 'profile_reviews_place') ?>"></textarea>
                                    <div class="app_answer">
                                        <input type="file" class="answer_file" name="file"  accept=".txt, .pdf, .rtf, .doc, .jpg, .png" multiple="">
                                        <i class="icon-symbols"></i>
                                    </div>
                                    <div class="block-button">
                                        <input class="button submit-app" id_app="<?= $model['id_app'] ?>" value="<?= Yii::t('account', 'send') ?>" type="submit">
                                    </div>
                                </form>
                            </div>
                                <?php } ?>
                            <div class="input-app-file"></div>
                        </div>
                        <hr class="sline">
                        <div class="clearfix"></div>

                    <?php endforeach; ?>
                <?php } else {?>
                    <br>
                    <?= Yii::t('account', 'application_list'); ?>
                <?php } ?>

            </div>

        </div>

        <div class="row">
            <?= frontend\widgets\Banner::widget(['position' => 'bottom']);?>
        </div>

    </div>


    <!--END CENTER-->
<?php $this->registerJsFile('js/setOptions.js', ['depends' => 'frontend\assets\AppAsset']); ?>