<?php

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'edit_videos');
$this->params['breadcrumbs'][] = $this->title;

?>
<!--START CENTER-->	
				
<div class="col-lg-7 profile-content profile-edite central-content icon-edit centralScroll">
	<div class="content">

		<!-- Video -->
		<input type="hidden" id="hidden-videos-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-link-text="<?= Yii::t('account', 'profile_video_link') ?>" data-name-text="<?= Yii::t('account', 'profile_video_name') ?>"/>
		<div class="profile-video">
			<h4 class="promote promote-bottom"><?= Yii::t('account', 'profile_video') ?></h4>
			<p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_video') ?><a href="#" id="add_video"></a></p>
			<div class="clearfix"></div>
			<div class="all-videos">
				<?php if($videos){?>
					<?php foreach ($videos as $video) {?>
						<div class="clearfix"></div>
						<div class="video videos_count" data-video-id="<?= $video['id'] ?>">
							<iframe width="330" height="181" src="<?= $video['link'] ?>" frameborder="0" allowfullscreen></iframe>
							<form class="forma-input">
								<div class="input-container namb">
									<input class="text-input floating-label video-input" type="text" name="link" value="<?= $video['link'] ?>">
									<label for="link"><?= Yii::t('account', 'profile_video_link') ?></label>
								</div>
								<div class="input-container namb">
									<input class="text-input floating-label video-input" type="text" name="name" value="<?= $video['name'] ?>" />
									<label for="name"><?= Yii::t('account', 'profile_video_name') ?></label>
								</div>
							</form>
							<a href="#" class="del del_video"><?= Yii::t('account', 'button_delete') ?><i></i></a>
						</div>
					<?php } ?>
				<?php }else{ ?>
					<div class="video videos_count" data-video-id="">
						<img class="video-img" src="/images/no-video.png" alt="">
						<form class="forma-input">
							<div class="input-container namb">
								<input class="text-input floating-label video-input" type="text" name="link" value=""/>
								<label for="link"><?= Yii::t('account', 'profile_video_link') ?></label>
							</div>
							<div class="input-container namb">
								<input class="text-input floating-label video-input" type="text" name="name" value=""/>
								<label for="name"><?= Yii::t('account', 'profile_video_name') ?></label>
							</div>
						</form>
						<a href="#" class="del del_video"><?= Yii::t('account', 'button_delete') ?><i></i></a>
					</div>
				<?php } ?>
			</div>
			<div class="clearfix"></div>
			<div class="block-button">
				<p class="valid" id="videos-save-valid"></p>
				<p class="error" id="videos-save-error">Changes not saved correctly!</p>
				<input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="videos_save">
			</div>

		</div>
		<!--End Video -->

	</div>


</div>



<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/videos_profile.js', ['depends'=>'frontend\assets\AppAsset']);
?>

