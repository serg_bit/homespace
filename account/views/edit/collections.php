<?php

$this->title = Yii::t('titles', 'account').Yii::t('titles', 'edit_collections');
$this->params['breadcrumbs'][] = $this->title;

?>
<!--START CENTER-->

<div class="col-lg-7 profile-content profile-edite central-content icon-edit centralScroll">
    <div class="content">

        <!-- Collections -->
        <input type="hidden" id="hidden-collections-text" data-del-text="<?= Yii::t('account', 'button_delete') ?>" data-name-text="<?= Yii::t('account', 'profile_collection_name') ?>" data-main-photo="<?= Yii::t('account', 'profile_collection_main') ?>"/>
        <?php if($collections){?>
                <div class="portfolio-edit">
                    <h4 class="promote promote-bottom"><?= Yii::t('account', 'my_items') ?></h4>
                    <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_collection') ?><a href="#" id="add_collection"></a></p>
                    <?php $count = 1; ?>
                    <?php $count_name = 1; ?>
                    <div class="all-collections">
                        <?php foreach ($collections as $collection) {?>
                        <div class="one-collection" id="<?= $collection['id'] ?>">
                            <form class="forma-input">
                                <div class="input-container">
                                    <a href="#" class="del delete-collection"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    <input class="text-input floating-label" type="text" name="collection-name" value="<?= $collection['name'] ?>" />
                                    <label for="collection-name"><?= Yii::t('account', 'profile_collection_name') ?></label>
                                </div>
                            </form>
                            <div class="portfolio_items">

                                <?php foreach ($collection['items'] as $item) {?>
                                    <div class="portfolio-items" id="<?= $item['id'] ?>">
                                        <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="<?= $item['image'] ?>" alt=""></a>
                                        <h5 class="name"><?= $item['title'] ?></h5>
                                        <div class="checkbox">
                                            <p>
                                                <?php if($collection['main'] == $item['id']){?>
                                                    <input id="test<?= $count ?>" name="main<?= $count_name ?>"  type="radio" data-item="<?= $item['id'] ?>" checked/>
                                                <?php }else{ ?>
                                                    <input id="test<?= $count ?>" name="main<?= $count_name ?>"  type="radio" data-item="<?= $item['id'] ?>" />
                                                <?php } ?>
                                                <label for="test<?= $count ?>"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                            </p>
                                            <div class="licenses-del">
                                                <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $count++;?>
                                <?php } ?>


                            </div>
                            <form class="portfolio-textarea">
                                <textarea class="text-ar"><?= $collection['about'] ?></textarea>
                            </form>
                        </div>
                            <div class="clearfix"></div>
                            <?php $count_name++; ?>


                        <?php } ?>
                    </div>

                    <div class="clearfix"></div>
                    <div class="block-button">
                        <p class="valid" id="collections-save-valid"></p>
                        <p class="error" id="collections-save-error">Changes not saved correctly!</p>
                        <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="collections_save">
                    </div>

                </div>

        <?php }else{ ?>
            <div class="portfolio-edit">
                <h4 class="promote promote-bottom"><?= Yii::t('account', 'my_items') ?></h4>
                <p class="upload-portfolio download-portfolio join"><?= Yii::t('account', 'profile_add_collection') ?><a href="#" id="add_collection"></a></p>

                <div class="all-collections">
                    <div class="one-collection" id="">
                        <form class="forma-input">
                            <div class="input-container">
                                <a href="#" class="del delete-collection"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                <input class="text-input floating-label" type="text" name="collection-name" />
                                <label for="collection-name"><?= Yii::t('account', 'profile_collection_name') ?></label>
                            </div>
                        </form>
                        <div class="portfolio_items">

                            <div class="portfolio-items" id="">
                                <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a>
                                <h5 class="name"></h5>
                                <div class="checkbox">
                                    <p>
                                        <input id="test1" name="main"  type="radio" data-item="" checked/>
                                        <label for="test1"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                    </p>
                                    <div class="licenses-del">
                                        <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-items" id="">
                                <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a>
                                <h5 class="name"></h5>
                                <div class="checkbox">
                                    <p>
                                        <input id="test2" name="main" type="radio" data-item=""/>
                                        <label for="test2"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                    </p>
                                    <div class="licenses-del">
                                        <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-items" id="">
                                <a href="#" class="select-item" data-toggle="modal" data-target="#best_3_works"><img src="/images/no-img.png" alt=""></a>
                                <h5 class="name"></h5>
                                <div class="checkbox">
                                    <p>
                                        <input id="test3" name="main" type="radio" data-item=""/>
                                        <label for="test3"><?= Yii::t('account', 'profile_collection_main') ?></label>
                                    </p>
                                    <div class="licenses-del">
                                        <a href="#" class="del delete-item"><?= Yii::t('account', 'button_delete') ?><i></i></a>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <form class="portfolio-textarea">
                            <textarea class="text-ar"></textarea>
                        </form>
                    </div>
                </div>

                <div class="clearfix"></div>
                <div class="block-button">
                    <p class="valid" id="collections-save-valid"></p>
                    <p class="error" id="collections-save-error">Changes not saved correctly!</p>
                    <input type="submit" class="button" value="<?= Yii::t('account', 'button_save') ?>" id="collections_save">
                </div>

            </div>
        <?php } ?>

        <!-- End Collections -->

    </div>


</div>
<!--END CENTER-->



<?php
$this->registerJsFile('js/bootstrap.min.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('js/common.js', ['depends'=>'frontend\assets\AppAsset']);
$this->registerJsFile('scripts/collections_profile.js', ['depends'=>'frontend\assets\AppAsset']);

?>

