<div class="col-lg-7 profile-content profile-agent-comps agents centralScroll archdes promteaser">
    <div class="row">
        <div class="content">
            <div class="portfolio-top profile-conten-me ">
                <div class="catalog-box special-offers-catalog page-in-catalog">
                    <div class="clearfix"></div>
                    <h4 class="promote promote-bottom">Promotional teaser</h4>
                    <p class="upload-portfolio download-portfolio join">Add
                        <a href="#"></a>
                    </p>
                    <div class="clearfix"></div>
                    <form class="forma-input">
                        <div class="search">
                            <div class="input-edit">
                                <div class="input-container">
                                    <input class="text-input floating-label" name="sample1" type="text">
                                    <label for="sample1">Search</label>
                                </div>
                            </div>
                        </div>
                        <div class="selects">
                            <select name="#">
                                <option value="#">Search by</option>
                                <option value="#">Search by</option>
                                <option value="#">Search by</option>
                                <option value="#">Search by</option>
                            </select>
                        </div>
                    </form>
                    <div class="companys">
                        <div class="company agents" style="background-image: url('/images/pic_50.png');">
                            <div class="author"><img src="/images/tiny-logo.png" alt="#">
                                <p class="name">Zaha Hadid Studio</p>
                            </div>
                            <div class="author-description">
                                <img class="company-img" src="/images/agent.png" alt="#">
                                <div class="name-company">
                                    <h5>Zaha <span>Hadid</span></h5>
                                </div>
                            </div>
                            <div class="city">
                                <ul>
                                    <li>London</li>
                                    <li>Mexico</li>
                                    <li>Tokyo</li>
                                    <li>Moskow</li>
                                    <li>New York</li>
                                </ul>
                                <div class="showRooms">
												<span class="showRoom">
													show <br> room
												</span>
												<span class="showRoom">
													show <br> room
												</span>
                                </div>
                                <p>Madrid</p>
                            </div>
                            <p class="descriptions">
                                Lorem ipsum dolor sit amet, consectetur adipisc- ing elit. Curabitur tempor nunc
                                accumsan urna...
                            </p>
                            <ul class="categories">
                                <li><a href="#">Categories:</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                            </ul>
                            <ul class="description">
                                <li>Added products: <span>56</span></li>
                                <li>New: <span>6</span></li>
                                <li>Identified objects: <span>16</span></li>
                                <li>Special offers: <span>16</span></li>
                                <li>Sale: <span>16%</span></li>

                            </ul>
                            <ul class="categories best">
                                <li><a href="#">Best seller for:</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                            </ul>
                            <ul class="buttons">
                                <li><a href="#" class="button">View</a></li>
                            </ul>
                            <a href="#" class="messages"><i class="icon-black218"></i>Message</a>
                        </div>
                        <div class="company agents" style="background-image: url('/images/pic_50.png');">
                            <div class="author"><img src="/images/tiny-logo.png" alt="#">
                                <p class="name">Zaha Hadid Studio</p>
                            </div>
                            <div class="author-description">
                                <img class="company-img" src="/images/agent.png" alt="#">
                                <div class="name-company">
                                    <h5>Zaha <span>Hadid</span></h5>
                                </div>
                            </div>
                            <div class="city">
                                <ul>
                                    <li>London</li>
                                    <li>Mexico</li>
                                    <li>Tokyo</li>
                                    <li>Moskow</li>
                                    <li>New York</li>
                                </ul>
                                <div class="showRooms">
												<span class="showRoom">
													show <br> room
												</span>
												<span class="showRoom">
													show <br> room
												</span>
                                </div>
                                <p>Madrid</p>
                            </div>
                            <p class="descriptions">
                                Lorem ipsum dolor sit amet, consectetur adipisc- ing elit. Curabitur tempor nunc
                                accumsan urna...
                            </p>
                            <ul class="categories">
                                <li><a href="#">Categories:</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                            </ul>
                            <ul class="description">
                                <li>Added products: <span>56</span></li>
                                <li>New: <span>6</span></li>
                                <li>Identified objects: <span>16</span></li>
                                <li>Special offers: <span>16</span></li>
                                <li>Sale: <span>16%</span></li>

                            </ul>
                            <ul class="categories best">
                                <li><a href="#">Best seller for:</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                            </ul>
                            <ul class="buttons">
                                <li><a href="#" class="button">View</a></li>
                            </ul>
                            <a href="#" class="messages"><i class="icon-black218"></i>Message</a>
                        </div>

                        <div class="company agents" style="background-image: url('/images/pic_50.png');">
                            <div class="author"><img src="/images/tiny-logo.png" alt="#">
                                <p class="name">Zaha Hadid Studio</p>
                            </div>
                            <div class="author-description">
                                <img class="company-img" src="/images/agent.png" alt="#">
                                <div class="name-company">
                                    <h5>Zaha <span>Hadid</span></h5>
                                </div>
                            </div>
                            <div class="city">
                                <ul>
                                    <li>London</li>
                                    <li>Mexico</li>
                                    <li>Tokyo</li>
                                    <li>Moskow</li>
                                    <li>New York</li>
                                </ul>
                                <div class="showRooms">
												<span class="showRoom">
													show <br> room
												</span>
												<span class="showRoom">
													show <br> room
												</span>
                                </div>
                                <p>Madrid</p>
                            </div>
                            <p class="descriptions">
                                Lorem ipsum dolor sit amet, consectetur adipisc- ing elit. Curabitur tempor nunc
                                accumsan urna...
                            </p>
                            <ul class="categories">
                                <li><a href="#">Categories:</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                            </ul>
                            <ul class="description">
                                <li>Added products: <span>56</span></li>
                                <li>New: <span>6</span></li>
                                <li>Identified objects: <span>16</span></li>
                                <li>Special offers: <span>16</span></li>
                                <li>Sale: <span>16%</span></li>

                            </ul>
                            <ul class="categories best">
                                <li><a href="#">Best seller for:</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                            </ul>
                            <ul class="buttons">
                                <li><a href="#" class="button">View</a></li>
                            </ul>
                            <a href="#" class="messages"><i class="icon-black218"></i>Message</a>
                        </div>

                        <div class="company agents" style="background-image: url('/images/pic_50.png');">
                            <div class="author"><img src="/images/tiny-logo.png" alt="#">
                                <p class="name">Zaha Hadid Studio</p>
                            </div>
                            <div class="author-description">
                                <img class="company-img" src="/images/agent.png" alt="#">
                                <div class="name-company">
                                    <h5>Zaha <span>Hadid</span></h5>
                                </div>
                            </div>
                            <div class="city">
                                <ul>
                                    <li>London</li>
                                    <li>Mexico</li>
                                    <li>Tokyo</li>
                                    <li>Moskow</li>
                                    <li>New York</li>
                                </ul>
                                <div class="showRooms">
                                    <span class="showRoom">
                                        show <br> room
                                    </span>
                                    <span class="showRoom">
                                        show <br> room
                                    </span>
                                </div>
                                <p>Madrid</p>
                            </div>
                            <p class="descriptions">
                                Lorem ipsum dolor sit amet, consectetur adipisc- ing elit. Curabitur tempor nunc
                                accumsan urna...
                            </p>
                            <ul class="categories">
                                <li><a href="#">Categories:</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                                <li><a href="#">Furniture</a></li>
                            </ul>
                            <ul class="description">
                                <li>Added products: <span>56</span></li>
                                <li>New: <span>6</span></li>
                                <li>Identified objects: <span>16</span></li>
                                <li>Special offers: <span>16</span></li>
                                <li>Sale: <span>16%</span></li>

                            </ul>
                            <ul class="categories best">
                                <li><a href="#">Best seller for:</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                                <li><a href="#">-Furniture</a></li>
                            </ul>
                            <ul class="buttons">
                                <li><a href="#" class="button">View</a></li>
                            </ul>
                            <a href="#" class="messages"><i class="icon-black218"></i>Message</a>
                        </div>

                        <div class="clearfix"></div>
                        <div class="bread-crumbs">
                            <ul class="hvr-radial-out">
                                <li>
                                    <a href="#"></a>
                                </li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">6</a></li>
                                <li><a href="#">7</a></li>
                                <li>
                                    <a href="#"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--END CENTER-->
</div>